using SDRSharp.Radio;
using System;
using System.Threading;

namespace SDRSharp.AudioRecorder
{
	public class SimpleRecorder : IDisposable
	{
		private const int BufferCount = 8;

		private const int DefaultAudioGain = 30;

		private readonly float _audioGain = (float)Math.Pow(3.0, 10.0);

		private readonly SharpEvent _bufferEvent = new SharpEvent(initialState: false);

		private FloatFifoStream _audioBuffer;

		private UnsafeBuffer _buffer;

		private unsafe float* _bufferPtr;

		private bool _diskWriterRunning;

		private bool _diskWriterPause;

		private string _fileName;

		private double _sampleRate;

		private string _infoName;

		private WavSampleFormat _wavSampleFormat;

		private SimpleWavWriter _wavWriter;

		private Thread _diskWriter;

		private readonly RecordingAudioProcessor _audioProcessor;

		private int _samplerateOut;

		private bool _unityGain;

		private int _audioBufferLength;

		private int _lostBuffers;

		private int _bufferUsage;

		public int SamplerateOut
		{
			set
			{
				_samplerateOut = value;
			}
		}

		public bool IsRecording => _diskWriterRunning;

		public bool DiskWriterPause
		{
			get
			{
				return _diskWriterPause;
			}
			set
			{
				_diskWriterPause = value;
			}
		}

		public bool IsStreamFull
		{
			get
			{
				if (_wavWriter != null)
				{
					return _wavWriter.IsStreamFull;
				}
				return false;
			}
		}

		public long BytesWritten
		{
			get
			{
				if (_wavWriter != null)
				{
					return _wavWriter.Length;
				}
				return 0L;
			}
		}

		public WavSampleFormat Format
		{
			get
			{
				return _wavSampleFormat;
			}
			set
			{
				if (_diskWriterRunning)
				{
					throw new ArgumentException("Format cannot be set while recording");
				}
				_wavSampleFormat = value;
			}
		}

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				if (_diskWriterRunning)
				{
					throw new ArgumentException("SampleRate cannot be set while recording");
				}
				_sampleRate = value;
			}
		}

		public string FileName
		{
			get
			{
				return _fileName;
			}
			set
			{
				if (_diskWriterRunning)
				{
					throw new ArgumentException("FileName cannot be set while recording");
				}
				_fileName = value;
			}
		}

		public string infoName
		{
			get
			{
				return _infoName;
			}
			set
			{
				if (_diskWriterRunning)
				{
					throw new ArgumentException("Media info tags cannot be set while recording");
				}
				_infoName = value;
			}
		}

		public bool UnityGain
		{
			get
			{
				return _unityGain;
			}
			set
			{
				_unityGain = value;
			}
		}

		public int BufferUsage => _bufferUsage;

		public int LostBuffers => _lostBuffers;

		public unsafe SimpleRecorder(RecordingAudioProcessor audioProcessor)
		{
			_audioProcessor = audioProcessor;
			_audioProcessor.AudioReady += AudioSamplesIn;
			_audioProcessor.Enabled = false;
			_audioBuffer = new FloatFifoStream(BlockMode.None);
		}

		~SimpleRecorder()
		{
			Dispose();
		}

		public void Dispose()
		{
			FreeBuffers();
		}

		public unsafe void AudioSamplesIn(float* audio, int length)
		{
			_audioBufferLength = length;
			if (!_diskWriterPause)
			{
				if ((double)_audioBuffer.Length < _sampleRate)
				{
					_audioBuffer.Write(audio, length);
					_bufferEvent.Set();
				}
				else
				{
					_lostBuffers++;
				}
			}
			_bufferUsage = (int)((double)_audioBuffer.Length / _sampleRate * 100.0);
		}

		public unsafe void ScaleAudio(float* audio, int length)
		{
			for (int i = 0; i < length; i++)
			{
				audio[i] *= _audioGain;
			}
		}

		private unsafe void DiskWriterThread()
		{
			while (_diskWriterRunning && !_wavWriter.IsStreamFull)
			{
				if (_audioBuffer == null || _audioBufferLength == 0)
				{
					Thread.Sleep(10);
					continue;
				}
				if (_audioBuffer.Length < _audioBufferLength)
				{
					_bufferEvent.WaitOne();
					continue;
				}
				if (_buffer == null || _buffer.Length != _audioBufferLength)
				{
					if (_buffer != null)
					{
						_buffer.Dispose();
						_buffer = null;
						_bufferPtr = null;
					}
					_buffer = UnsafeBuffer.Create(_audioBufferLength, 4);
					_bufferPtr = (float*)(void*)_buffer;
				}
				_audioBuffer.Read(_bufferPtr, _audioBufferLength);
				if (!_unityGain)
				{
					ScaleAudio(_bufferPtr, _audioBufferLength);
				}
				_wavWriter.Write(_bufferPtr, _audioBufferLength);
			}
			while (_audioBuffer.Length >= _audioBufferLength && _audioBufferLength > 0 && !_wavWriter.IsStreamFull)
			{
				_audioBuffer.Read(_bufferPtr, _audioBufferLength);
				if (!_unityGain)
				{
					ScaleAudio(_bufferPtr, _audioBufferLength);
				}
				_wavWriter.Write(_bufferPtr, _audioBufferLength);
			}
			_diskWriterRunning = false;
		}

		private void Flush()
		{
			if (_wavWriter != null)
			{
				_wavWriter.Close();
			}
			_audioBuffer.Flush();
			_audioBufferLength = 0;
		}

		private unsafe void FreeBuffers()
		{
			if (_audioBuffer != null)
			{
				_audioBuffer.Close();
				_audioBuffer.Dispose();
				_audioBuffer = null;
			}
			if (_buffer != null)
			{
				_buffer.Dispose();
				_buffer = null;
				_bufferPtr = null;
			}
		}

		public void StartRecording()
		{
			if (_diskWriter == null)
			{
				_bufferEvent.Reset();
				_wavWriter = new SimpleWavWriter(_fileName, _wavSampleFormat, (uint)_sampleRate, _samplerateOut);
				MediaInfo mediaInfo = new MediaInfo(_infoName);
				_wavWriter._MediaInfo = mediaInfo;
				_wavWriter.Open();
				_diskWriter = new Thread(DiskWriterThread);
				_diskWriterRunning = true;
				_diskWriter.Start();
				_audioProcessor.Enabled = true;
				_lostBuffers = 0;
			}
		}

		public void StopRecording()
		{
			_audioProcessor.Enabled = false;
			_diskWriterRunning = false;
			if (_diskWriter != null)
			{
				_bufferEvent.Set();
				_diskWriter.Join();
			}
			Flush();
			_diskWriter = null;
			_wavWriter = null;
			_bufferUsage = 0;
		}
	}
}

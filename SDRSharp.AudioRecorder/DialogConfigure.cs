using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SDRSharp.AudioRecorder
{
	public class DialogConfigure : Form
	{
		private AudioRecorderPanel _recorder;

		private int _samplerateOut;

		private IContainer components;

		private Button btnOk;

		private TabControl tabControl1;

		private TabPage fileOptions;

		private Label label4;

		private CheckBox createNewFileCheckBox;

		private TextBox folderTextBox;

		private NumericUpDown maxWriteLengthMbNumericUpDown;

		private Label label1;

		private ComboBox sampleRateComboBox;

		private ComboBox sampleFormatComboBox;

		private Label sampleFormatLbl;

		private TabPage recorderTabPage;

		private NumericUpDown continueRecordTimeNumericUpDown;

		private CheckBox continueRecordEnableCheckBox;

		private CheckBox dontWritePauseCheckBox;

		private NumericUpDown minWriteLengthNumericUpDown;

		private CheckBox deleteSmallFilesCheckBox;

		private CheckBox newFileTimeEnableCheckBox;

		private CheckBox writeOneFileCheckBox;

		private Label resultLabel;

		private Timer displayTimer;

		private CheckBox createFileIfFrequencyCheckBox;

		private Label label2;

		private NumericUpDown NewFileTimeNumericUpDown;

		private CheckBox autoStartCheckBox;

		private Label label5;

		private Label label3;

		private CheckBox useMuteCheckBox;

		private CheckBox useSquelchCheckBox;

		public DialogConfigure(AudioRecorderPanel recorder)
		{
			_recorder = recorder;
			InitializeComponent();
			autoStartCheckBox.Checked = _recorder.AutoStartRecording;
			AddSamplerate(_recorder.OutputSamplerateArray);
			_samplerateOut = _recorder.SamplerateOut;
			sampleRateComboBox.SelectedIndex = SamplerateSelect(_samplerateOut);
			sampleRateComboBox_SelectedIndexChanged(null, null);
			sampleFormatComboBox.SelectedIndex = _recorder.SampleFormatSelectedIndex;
			writeOneFileCheckBox.Checked = _recorder.WriteOneFile;
			dontWritePauseCheckBox.Checked = _recorder.DontWritePause;
			useSquelchCheckBox.Checked = _recorder.UseSquelch;
			useMuteCheckBox.Checked = _recorder.UseMute;
			continueRecordEnableCheckBox.Checked = _recorder.ContinueRecordTimeEnable;
			continueRecordTimeNumericUpDown.Value = _recorder.ContinueRecordTime / 1000;
			newFileTimeEnableCheckBox.Checked = _recorder.NewFileTimeEnable;
			NewFileTimeNumericUpDown.Value = _recorder.NewFileTime / 1000;
			createFileIfFrequencyCheckBox.Checked = _recorder.NewFileFrequencyEnable;
			createNewFileCheckBox.Checked = _recorder.MaxFileSizeEnable;
			maxWriteLengthMbNumericUpDown.Value = _recorder.MaxWriteLength;
			deleteSmallFilesCheckBox.Checked = _recorder.DeleteSmallFiles;
			minWriteLengthNumericUpDown.Value = (decimal)_recorder.MinWriteLength;
			folderTextBox.Text = _recorder.FileNameRules;
			folderTextBox_TextChanged(null, null);
		}

		private void AddSamplerate(string outputSamplerate)
		{
			sampleRateComboBox.Items.Add("without resample");
			sampleRateComboBox.Items.AddRange(outputSamplerate.Split(','));
		}

		private void sampleRateComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			int samplerateOut = 0;
			Match match = Regex.Match(sampleRateComboBox.Text, "([0-9\\.]+) ([k]?)Hz", RegexOptions.IgnoreCase);
			if (match.Success)
			{
				samplerateOut = (int)(double.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture) * 1000.0);
			}
			_samplerateOut = samplerateOut;
		}

		private int SamplerateSelect(int samplerate)
		{
			int num = 0;
			foreach (object item in sampleRateComboBox.Items)
			{
				Match match = Regex.Match(item.ToString(), "([0-9\\.]+) ([k]?)Hz", RegexOptions.IgnoreCase);
				if (match.Success && (int)(double.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture) * 1000.0) == samplerate)
				{
					return num;
				}
				num++;
			}
			return 0;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			_recorder.DontWritePause = dontWritePauseCheckBox.Checked;
			_recorder.WriteOneFile = writeOneFileCheckBox.Checked;
			_recorder.UseMute = useMuteCheckBox.Checked;
			_recorder.UseSquelch = useSquelchCheckBox.Checked;
			_recorder.ContinueRecordTimeEnable = continueRecordEnableCheckBox.Checked;
			_recorder.ContinueRecordTime = (int)continueRecordTimeNumericUpDown.Value * 1000;
			_recorder.NewFileTimeEnable = newFileTimeEnableCheckBox.Checked;
			_recorder.NewFileTime = (int)NewFileTimeNumericUpDown.Value * 1000;
			_recorder.NewFileFrequencyEnable = createFileIfFrequencyCheckBox.Checked;
			_recorder.SampleFormatSelectedIndex = sampleFormatComboBox.SelectedIndex;
			_recorder.SamplerateOut = _samplerateOut;
			_recorder.MaxFileSizeEnable = createNewFileCheckBox.Checked;
			_recorder.MaxWriteLength = (long)maxWriteLengthMbNumericUpDown.Value;
			_recorder.DeleteSmallFiles = deleteSmallFilesCheckBox.Checked;
			_recorder.MinWriteLength = (float)minWriteLengthNumericUpDown.Value;
			_recorder.FileNameRules = folderTextBox.Text;
			_recorder.AutoStartRecording = autoStartCheckBox.Checked;
			base.DialogResult = DialogResult.OK;
		}

		private void folderTextBox_TextChanged(object sender, EventArgs e)
		{
			resultLabel.Text = _recorder.ParseStringToPath(folderTextBox.Text);
		}

		private void displayTimer_Tick(object sender, EventArgs e)
		{
			continueRecordEnableCheckBox.Enabled = dontWritePauseCheckBox.Checked;
			continueRecordTimeNumericUpDown.Enabled = dontWritePauseCheckBox.Checked;
			useMuteCheckBox.Enabled = dontWritePauseCheckBox.Checked;
			useSquelchCheckBox.Enabled = dontWritePauseCheckBox.Checked;
			newFileTimeEnableCheckBox.Enabled = !writeOneFileCheckBox.Checked;
			NewFileTimeNumericUpDown.Enabled = !writeOneFileCheckBox.Checked;
			createFileIfFrequencyCheckBox.Enabled = !writeOneFileCheckBox.Checked;
			sampleRateComboBox.Enabled = (sampleFormatComboBox.SelectedIndex > 2);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			btnOk = new System.Windows.Forms.Button();
			tabControl1 = new System.Windows.Forms.TabControl();
			fileOptions = new System.Windows.Forms.TabPage();
			label2 = new System.Windows.Forms.Label();
			resultLabel = new System.Windows.Forms.Label();
			minWriteLengthNumericUpDown = new System.Windows.Forms.NumericUpDown();
			deleteSmallFilesCheckBox = new System.Windows.Forms.CheckBox();
			label4 = new System.Windows.Forms.Label();
			createNewFileCheckBox = new System.Windows.Forms.CheckBox();
			folderTextBox = new System.Windows.Forms.TextBox();
			maxWriteLengthMbNumericUpDown = new System.Windows.Forms.NumericUpDown();
			label1 = new System.Windows.Forms.Label();
			sampleRateComboBox = new System.Windows.Forms.ComboBox();
			sampleFormatComboBox = new System.Windows.Forms.ComboBox();
			sampleFormatLbl = new System.Windows.Forms.Label();
			recorderTabPage = new System.Windows.Forms.TabPage();
			autoStartCheckBox = new System.Windows.Forms.CheckBox();
			NewFileTimeNumericUpDown = new System.Windows.Forms.NumericUpDown();
			createFileIfFrequencyCheckBox = new System.Windows.Forms.CheckBox();
			writeOneFileCheckBox = new System.Windows.Forms.CheckBox();
			newFileTimeEnableCheckBox = new System.Windows.Forms.CheckBox();
			continueRecordTimeNumericUpDown = new System.Windows.Forms.NumericUpDown();
			continueRecordEnableCheckBox = new System.Windows.Forms.CheckBox();
			dontWritePauseCheckBox = new System.Windows.Forms.CheckBox();
			displayTimer = new System.Windows.Forms.Timer(components);
			useSquelchCheckBox = new System.Windows.Forms.CheckBox();
			useMuteCheckBox = new System.Windows.Forms.CheckBox();
			label3 = new System.Windows.Forms.Label();
			label5 = new System.Windows.Forms.Label();
			tabControl1.SuspendLayout();
			fileOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)minWriteLengthNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)maxWriteLengthMbNumericUpDown).BeginInit();
			recorderTabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)NewFileTimeNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)continueRecordTimeNumericUpDown).BeginInit();
			SuspendLayout();
			btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			btnOk.Location = new System.Drawing.Point(434, 255);
			btnOk.Margin = new System.Windows.Forms.Padding(2);
			btnOk.Name = "btnOk";
			btnOk.Size = new System.Drawing.Size(56, 23);
			btnOk.TabIndex = 7;
			btnOk.Text = "O&K";
			btnOk.UseVisualStyleBackColor = true;
			btnOk.Click += new System.EventHandler(btnOk_Click);
			tabControl1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			tabControl1.Controls.Add(fileOptions);
			tabControl1.Controls.Add(recorderTabPage);
			tabControl1.Location = new System.Drawing.Point(12, 3);
			tabControl1.Name = "tabControl1";
			tabControl1.SelectedIndex = 0;
			tabControl1.Size = new System.Drawing.Size(478, 247);
			tabControl1.TabIndex = 26;
			fileOptions.BackColor = System.Drawing.SystemColors.Menu;
			fileOptions.Controls.Add(label2);
			fileOptions.Controls.Add(resultLabel);
			fileOptions.Controls.Add(minWriteLengthNumericUpDown);
			fileOptions.Controls.Add(deleteSmallFilesCheckBox);
			fileOptions.Controls.Add(label4);
			fileOptions.Controls.Add(createNewFileCheckBox);
			fileOptions.Controls.Add(folderTextBox);
			fileOptions.Controls.Add(maxWriteLengthMbNumericUpDown);
			fileOptions.Controls.Add(label1);
			fileOptions.Controls.Add(sampleRateComboBox);
			fileOptions.Controls.Add(sampleFormatComboBox);
			fileOptions.Controls.Add(sampleFormatLbl);
			fileOptions.Location = new System.Drawing.Point(4, 22);
			fileOptions.Name = "fileOptions";
			fileOptions.Padding = new System.Windows.Forms.Padding(3);
			fileOptions.Size = new System.Drawing.Size(470, 221);
			fileOptions.TabIndex = 0;
			fileOptions.Text = "File options";
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(6, 149);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(434, 13);
			label2.TabIndex = 46;
			label2.Text = "You can use: date, start_time, end_time, length, name, group, frequency, \"any text\", +, \\, /";
			resultLabel.AutoSize = true;
			resultLabel.Location = new System.Drawing.Point(6, 198);
			resultLabel.Name = "resultLabel";
			resultLabel.Size = new System.Drawing.Size(13, 13);
			resultLabel.TabIndex = 45;
			resultLabel.Text = "_";
			minWriteLengthNumericUpDown.DecimalPlaces = 1;
			minWriteLengthNumericUpDown.Location = new System.Drawing.Point(221, 86);
			minWriteLengthNumericUpDown.Maximum = new decimal(new int[4]
			{
				3600,
				0,
				0,
				0
			});
			minWriteLengthNumericUpDown.Name = "minWriteLengthNumericUpDown";
			minWriteLengthNumericUpDown.Size = new System.Drawing.Size(57, 20);
			minWriteLengthNumericUpDown.TabIndex = 40;
			deleteSmallFilesCheckBox.AutoSize = true;
			deleteSmallFilesCheckBox.Location = new System.Drawing.Point(9, 87);
			deleteSmallFilesCheckBox.Name = "deleteSmallFilesCheckBox";
			deleteSmallFilesCheckBox.Size = new System.Drawing.Size(183, 17);
			deleteSmallFilesCheckBox.TabIndex = 39;
			deleteSmallFilesCheckBox.Text = "Delete file if the file size < second";
			deleteSmallFilesCheckBox.UseVisualStyleBackColor = true;
			label4.AutoSize = true;
			label4.Location = new System.Drawing.Point(6, 130);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(143, 13);
			label4.TabIndex = 37;
			label4.Text = "Rules for creating file names.";
			createNewFileCheckBox.AutoSize = true;
			createNewFileCheckBox.Location = new System.Drawing.Point(9, 61);
			createNewFileCheckBox.Name = "createNewFileCheckBox";
			createNewFileCheckBox.Size = new System.Drawing.Size(196, 17);
			createNewFileCheckBox.TabIndex = 33;
			createNewFileCheckBox.Text = "Create a new file if the file size > MB";
			createNewFileCheckBox.UseVisualStyleBackColor = true;
			folderTextBox.Location = new System.Drawing.Point(9, 168);
			folderTextBox.Name = "folderTextBox";
			folderTextBox.Size = new System.Drawing.Size(458, 20);
			folderTextBox.TabIndex = 31;
			folderTextBox.Text = "/ date / name + \"_\" + frequency / time + \"_\" + name + \"_\" + frequency";
			folderTextBox.TextChanged += new System.EventHandler(folderTextBox_TextChanged);
			maxWriteLengthMbNumericUpDown.Location = new System.Drawing.Point(221, 60);
			maxWriteLengthMbNumericUpDown.Maximum = new decimal(new int[4]
			{
				2048,
				0,
				0,
				0
			});
			maxWriteLengthMbNumericUpDown.Name = "maxWriteLengthMbNumericUpDown";
			maxWriteLengthMbNumericUpDown.Size = new System.Drawing.Size(57, 20);
			maxWriteLengthMbNumericUpDown.TabIndex = 30;
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(4, 35);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(60, 13);
			label1.TabIndex = 29;
			label1.Text = "Samplerate";
			sampleRateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			sampleRateComboBox.FormattingEnabled = true;
			sampleRateComboBox.Location = new System.Drawing.Point(103, 33);
			sampleRateComboBox.Name = "sampleRateComboBox";
			sampleRateComboBox.Size = new System.Drawing.Size(175, 21);
			sampleRateComboBox.TabIndex = 28;
			sampleRateComboBox.SelectedIndexChanged += new System.EventHandler(sampleRateComboBox_SelectedIndexChanged);
			sampleFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			sampleFormatComboBox.DropDownWidth = 120;
			sampleFormatComboBox.FormattingEnabled = true;
			sampleFormatComboBox.Items.AddRange(new object[5]
			{
				"8 Bit PCM Stereo",
				"16 Bit PCM Stereo",
				"32 Bit IEEE Float",
				"8 Bit PCM Mono",
				"16 Bit PCM Mono"
			});
			sampleFormatComboBox.Location = new System.Drawing.Point(103, 6);
			sampleFormatComboBox.Name = "sampleFormatComboBox";
			sampleFormatComboBox.Size = new System.Drawing.Size(175, 21);
			sampleFormatComboBox.TabIndex = 26;
			sampleFormatLbl.AutoSize = true;
			sampleFormatLbl.Location = new System.Drawing.Point(4, 9);
			sampleFormatLbl.Name = "sampleFormatLbl";
			sampleFormatLbl.Size = new System.Drawing.Size(77, 13);
			sampleFormatLbl.TabIndex = 27;
			sampleFormatLbl.Text = "Sample Format";
			recorderTabPage.BackColor = System.Drawing.SystemColors.Menu;
			recorderTabPage.Controls.Add(label5);
			recorderTabPage.Controls.Add(label3);
			recorderTabPage.Controls.Add(useMuteCheckBox);
			recorderTabPage.Controls.Add(useSquelchCheckBox);
			recorderTabPage.Controls.Add(autoStartCheckBox);
			recorderTabPage.Controls.Add(NewFileTimeNumericUpDown);
			recorderTabPage.Controls.Add(createFileIfFrequencyCheckBox);
			recorderTabPage.Controls.Add(writeOneFileCheckBox);
			recorderTabPage.Controls.Add(newFileTimeEnableCheckBox);
			recorderTabPage.Controls.Add(continueRecordTimeNumericUpDown);
			recorderTabPage.Controls.Add(continueRecordEnableCheckBox);
			recorderTabPage.Controls.Add(dontWritePauseCheckBox);
			recorderTabPage.Location = new System.Drawing.Point(4, 22);
			recorderTabPage.Name = "recorderTabPage";
			recorderTabPage.Padding = new System.Windows.Forms.Padding(3);
			recorderTabPage.Size = new System.Drawing.Size(470, 221);
			recorderTabPage.TabIndex = 1;
			recorderTabPage.Text = "Recorder options";
			autoStartCheckBox.AutoSize = true;
			autoStartCheckBox.Location = new System.Drawing.Point(6, 6);
			autoStartCheckBox.Name = "autoStartCheckBox";
			autoStartCheckBox.Size = new System.Drawing.Size(115, 17);
			autoStartCheckBox.TabIndex = 48;
			autoStartCheckBox.Text = "Autostart recording";
			autoStartCheckBox.UseVisualStyleBackColor = true;
			NewFileTimeNumericUpDown.Location = new System.Drawing.Point(227, 123);
			NewFileTimeNumericUpDown.Name = "NewFileTimeNumericUpDown";
			NewFileTimeNumericUpDown.Size = new System.Drawing.Size(63, 20);
			NewFileTimeNumericUpDown.TabIndex = 27;
			createFileIfFrequencyCheckBox.AutoSize = true;
			createFileIfFrequencyCheckBox.Location = new System.Drawing.Point(6, 146);
			createFileIfFrequencyCheckBox.Name = "createFileIfFrequencyCheckBox";
			createFileIfFrequencyCheckBox.Size = new System.Drawing.Size(236, 17);
			createFileIfFrequencyCheckBox.TabIndex = 26;
			createFileIfFrequencyCheckBox.Text = "Create a new file if the frequency is changed";
			createFileIfFrequencyCheckBox.UseVisualStyleBackColor = true;
			writeOneFileCheckBox.AutoSize = true;
			writeOneFileCheckBox.Location = new System.Drawing.Point(6, 29);
			writeOneFileCheckBox.Name = "writeOneFileCheckBox";
			writeOneFileCheckBox.Size = new System.Drawing.Size(112, 17);
			writeOneFileCheckBox.TabIndex = 25;
			writeOneFileCheckBox.Text = "Write all in one file";
			writeOneFileCheckBox.UseVisualStyleBackColor = true;
			newFileTimeEnableCheckBox.AutoSize = true;
			newFileTimeEnableCheckBox.Location = new System.Drawing.Point(6, 123);
			newFileTimeEnableCheckBox.Name = "newFileTimeEnableCheckBox";
			newFileTimeEnableCheckBox.Size = new System.Drawing.Size(218, 17);
			newFileTimeEnableCheckBox.TabIndex = 23;
			newFileTimeEnableCheckBox.Text = "Waiting time to create a new file, second";
			newFileTimeEnableCheckBox.UseVisualStyleBackColor = true;
			continueRecordTimeNumericUpDown.Location = new System.Drawing.Point(328, 98);
			continueRecordTimeNumericUpDown.Name = "continueRecordTimeNumericUpDown";
			continueRecordTimeNumericUpDown.Size = new System.Drawing.Size(57, 20);
			continueRecordTimeNumericUpDown.TabIndex = 22;
			continueRecordTimeNumericUpDown.Value = new decimal(new int[4]
			{
				5,
				0,
				0,
				0
			});
			continueRecordEnableCheckBox.AutoSize = true;
			continueRecordEnableCheckBox.Location = new System.Drawing.Point(6, 98);
			continueRecordEnableCheckBox.Name = "continueRecordEnableCheckBox";
			continueRecordEnableCheckBox.Size = new System.Drawing.Size(319, 17);
			continueRecordEnableCheckBox.TabIndex = 21;
			continueRecordEnableCheckBox.Text = "Continue recording after the squelch has been closed, second";
			continueRecordEnableCheckBox.UseVisualStyleBackColor = true;
			dontWritePauseCheckBox.AutoSize = true;
			dontWritePauseCheckBox.Location = new System.Drawing.Point(6, 52);
			dontWritePauseCheckBox.Name = "dontWritePauseCheckBox";
			dontWritePauseCheckBox.Size = new System.Drawing.Size(108, 17);
			dontWritePauseCheckBox.TabIndex = 20;
			dontWritePauseCheckBox.Text = "Don't write pause";
			dontWritePauseCheckBox.UseVisualStyleBackColor = true;
			displayTimer.Enabled = true;
			displayTimer.Tick += new System.EventHandler(displayTimer_Tick);
			useSquelchCheckBox.AutoSize = true;
			useSquelchCheckBox.Location = new System.Drawing.Point(38, 75);
			useSquelchCheckBox.Name = "useSquelchCheckBox";
			useSquelchCheckBox.Size = new System.Drawing.Size(63, 17);
			useSquelchCheckBox.TabIndex = 49;
			useSquelchCheckBox.Text = "squelch";
			useSquelchCheckBox.UseVisualStyleBackColor = true;
			useMuteCheckBox.AutoSize = true;
			useMuteCheckBox.Location = new System.Drawing.Point(141, 75);
			useMuteCheckBox.Name = "useMuteCheckBox";
			useMuteCheckBox.Size = new System.Drawing.Size(49, 17);
			useMuteCheckBox.TabIndex = 50;
			useMuteCheckBox.Text = "mute";
			useMuteCheckBox.UseVisualStyleBackColor = true;
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(3, 76);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(26, 13);
			label3.TabIndex = 51;
			label3.Text = "Use";
			label5.AutoSize = true;
			label5.Location = new System.Drawing.Point(107, 76);
			label5.Name = "label5";
			label5.Size = new System.Drawing.Size(25, 13);
			label5.TabIndex = 52;
			label5.Text = "and";
			base.AcceptButton = btnOk;
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(501, 285);
			base.Controls.Add(tabControl1);
			base.Controls.Add(btnOk);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			base.Margin = new System.Windows.Forms.Padding(2);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "DialogConfigure";
			base.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			base.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			Text = "Configure audio recorder";
			tabControl1.ResumeLayout(performLayout: false);
			fileOptions.ResumeLayout(performLayout: false);
			fileOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)minWriteLengthNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)maxWriteLengthMbNumericUpDown).EndInit();
			recorderTabPage.ResumeLayout(performLayout: false);
			recorderTabPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)NewFileTimeNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)continueRecordTimeNumericUpDown).EndInit();
			ResumeLayout(performLayout: false);
		}
	}
}

using SDRSharp.Common;
using SDRSharp.PluginsCom;
using SDRSharp.Radio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SDRSharp.AudioRecorder
{
	public class AudioRecorderPanel : UserControl
	{
		private const string recorderStartCom = "Audio_recorder_Start";

		private const string recorderStopCom = "Audio_recorder_Stop";

		private PluginsComProxy _pluginsCom;

		private const float bytesToMb = 9.536743E-07f;

		private const string tempFileName = "\\temporaryAudioRecord.wav";

		private readonly ISharpControl _control;

		private readonly RecordingAudioProcessor _audioProcessor = new RecordingAudioProcessor();

		private readonly SimpleRecorder _simpleRecorder;

		private List<MemoryEntry> _entriesInManager;

		private readonly SettingsPersister _settingsPersister;

		private DateTime _startTime;

		private DateTime _endTime;

		private long _oldFrequency;

		private float _writeAllMbyte;

		private int _filesCounter;

		private int _waitTime;

		private bool _recordIsStarted;

		private int _newFileWaitTime;

		private int _recordSamplerate;

		private float _writeLength;

		private float _writeAllSecond;

		private int _fileIndexer;

		private IContainer components;

		private Button recBtn;

		private Timer recDisplayTimer;

		private Label allLabel;

		private Label label2;

		private Panel panel1;

		private Timer recordTimer;

		private Label recordLabel;

		private FolderBrowserDialog writeFolderBrowserDialog;

		private Button selectFolderDialog;

		private Button openFolderButton;

		private Button configureButton;

		private Label currentLabel;

		private ProgressBar progressBar;

		private Label label1;

		public bool AutoStartRecording
		{
			get;
			set;
		}

		public bool WriteOneFile
		{
			get;
			set;
		}

		public bool DeleteSmallFiles
		{
			get;
			set;
		}

		public float MinWriteLength
		{
			get;
			set;
		}

		public WavSampleFormat SampleFormat
		{
			get;
			set;
		}

		public int SampleFormatSelectedIndex
		{
			get;
			set;
		}

		public int SamplerateOut
		{
			get;
			set;
		}

		public bool DontWritePause
		{
			get;
			set;
		}

		public bool ContinueRecordTimeEnable
		{
			get;
			set;
		}

		public int ContinueRecordTime
		{
			get;
			set;
		}

		public bool NewFileTimeEnable
		{
			get;
			set;
		}

		public int NewFileTime
		{
			get;
			set;
		}

		public bool NewFileFrequencyEnable
		{
			get;
			set;
		}

		public bool MaxFileSizeEnable
		{
			get;
			set;
		}

		public long MaxWriteLength
		{
			get;
			set;
		}

		public string OutputSamplerateArray
		{
			get;
			set;
		}

		public string FileNameRules
		{
			get;
			set;
		}

		public bool UseSquelch
		{
			get;
			set;
		}

		public bool UseMute
		{
			get;
			set;
		}

		public AudioRecorderPanel(ISharpControl control)
		{
			InitializeComponent();
			_control = control;
			_control.PropertyChanged += PropertyChangedHandler;
			_audioProcessor.Enabled = false;
			_control.RegisterStreamHook(_audioProcessor, ProcessorType.FilteredAudioOutput);
			_settingsPersister = new SettingsPersister();
			_entriesInManager = _settingsPersister.ReadStoredFrequencies();
			_simpleRecorder = new SimpleRecorder(_audioProcessor);
			AutoStartRecording = Utils.GetBooleanSetting("AutoStartRecording");
			OutputSamplerateArray = Utils.GetStringSetting("WriterOutputSampleRate", "48 kHz,32 kHz,16 kHz,8 kHz");
			FileNameRules = Utils.GetStringSetting("AudioRecorderFileName", "/ date / group / frequency + \" \" + name / time + \" \" + name + \" \" + frequency");
			SampleFormatSelectedIndex = Utils.GetIntSetting("RecordSampleFormat", 0);
			SampleFormat = (WavSampleFormat)SampleFormatSelectedIndex;
			DontWritePause = Utils.GetBooleanSetting("DontWritePause");
			UseMute = Utils.GetBooleanSetting("AudioRecorderUseMute");
			UseSquelch = Utils.GetBooleanSetting("AudioRecorderUseSquelch");
			writeFolderBrowserDialog.SelectedPath = Utils.GetStringSetting("WriteFolder", Path.GetDirectoryName(Application.ExecutablePath));
			SamplerateOut = Utils.GetIntSetting("WriteSamplerate", 0);
			MinWriteLength = (float)Utils.GetDoubleSetting("MinimumWriteLengthInSecond", 0.0);
			DeleteSmallFiles = Utils.GetBooleanSetting("DeleteSmallFile");
			ContinueRecordTimeEnable = Utils.GetBooleanSetting("ContinueRecordTimeEnabled");
			ContinueRecordTime = Utils.GetIntSetting("ContinueRecordTime", 1000);
			NewFileTimeEnable = Utils.GetBooleanSetting("NewFileWaitTimeEnable");
			NewFileTime = Utils.GetIntSetting("NewFileWaitTime", 10000);
			WriteOneFile = Utils.GetBooleanSetting("WriteOneFile");
			NewFileFrequencyEnable = Utils.GetBooleanSetting("NewFileIfFrequencyChange");
			MaxFileSizeEnable = Utils.GetBooleanSetting("NewFileIfMaxWriteLength");
			MaxWriteLength = Utils.GetLongSetting("MaxWriteLength", 2048L);
			ConfigureGUI();
			_pluginsCom = new PluginsComProxy(NewCommandAvailable);
			_pluginsCom.AddAvailableCommands("Audio_recorder_Start");
			_pluginsCom.AddAvailableCommands("Audio_recorder_Stop");
		}

		private void NewCommandAvailable(string command)
		{
			int num = command.IndexOf('<');
			int num2 = command.IndexOf('>');
			string a = command;
			if (num > 0)
			{
				a = command.Substring(0, num + 1).Trim();
			}
			string empty = string.Empty;
			if (num2 > 0)
			{
				command.Substring(num + 1, num2 - num - 1).Trim();
			}
			try
			{
				if (!(a == "Audio_recorder_Start"))
				{
					if (a == "Audio_recorder_Stop" && _recordIsStarted)
					{
						recBtn_Click(null, null);
					}
				}
				else if (!_recordIsStarted)
				{
					recBtn_Click(null, null);
				}
			}
			catch
			{
				MessageBox.Show("Error command translate - " + command);
			}
		}

		public void SaveSettings()
		{
			Utils.SaveSetting("AudioRecorderFileName", FileNameRules);
			Utils.SaveSetting("DeleteSmallFile", DeleteSmallFiles);
			Utils.SaveSetting("MinimumWriteLengthInSecond", MinWriteLength);
			Utils.SaveSetting("DontWritePause", DontWritePause);
			Utils.SaveSetting("AudioRecorderUseMute", UseMute);
			Utils.SaveSetting("AudioRecorderUseSquelch", UseSquelch);
			Utils.SaveSetting("WriteFolder", writeFolderBrowserDialog.SelectedPath);
			Utils.SaveSetting("WriteSamplerate", SamplerateOut);
			Utils.SaveSetting("RecordSampleFormat", SampleFormatSelectedIndex);
			Utils.SaveSetting("AutoStartRecording", AutoStartRecording);
			Utils.SaveSetting("ContinueRecordTimeEnabled", ContinueRecordTimeEnable);
			Utils.SaveSetting("ContinueRecordTime", ContinueRecordTime);
			Utils.SaveSetting("WriteOneFile", WriteOneFile);
			Utils.SaveSetting("NewFileWaitTimeEnable", NewFileTimeEnable);
			Utils.SaveSetting("NewFileWaitTime", NewFileTime);
			Utils.SaveSetting("NewFileIfFrequencyChange", NewFileFrequencyEnable);
			Utils.SaveSetting("NewFileIfMaxWriteLength", MaxFileSizeEnable);
			Utils.SaveSetting("MaxWriteLength", MaxWriteLength);
		}

		private void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			string propertyName = e.PropertyName;
			if (!(propertyName == "StartRadio"))
			{
				if (!(propertyName == "StopRadio"))
				{
					if (propertyName == "UnityGain" && _simpleRecorder != null)
					{
						_simpleRecorder.UnityGain = _control.UnityGain;
					}
				}
				else if (_recordIsStarted)
				{
					recBtn_Click(null, null);
				}
			}
			else
			{
				ConfigureGUI();
				if (AutoStartRecording)
				{
					recBtn_Click(null, null);
				}
			}
		}

		private void RecordStart()
		{
			_startTime = DateTime.Now;
			_endTime = _startTime;
			_oldFrequency = _control.Frequency;
			_simpleRecorder.Format = (WavSampleFormat)SampleFormatSelectedIndex;
			_simpleRecorder.FileName = writeFolderBrowserDialog.SelectedPath + "\\temporaryAudioRecord.wav";
			_simpleRecorder.SampleRate = _audioProcessor.SampleRate;
			if (SamplerateOut == 0 || SampleFormatSelectedIndex < 3)
			{
				_simpleRecorder.SamplerateOut = (int)_audioProcessor.SampleRate;
				_recordSamplerate = (int)_audioProcessor.SampleRate;
			}
			else
			{
				_simpleRecorder.SamplerateOut = SamplerateOut;
				_recordSamplerate = SamplerateOut;
			}
			if (SampleFormatSelectedIndex < 3)
			{
				_recordSamplerate *= 2;
			}
			try
			{
				_simpleRecorder.StartRecording();
			}
			catch
			{
				MessageBox.Show("Unable to start recording", "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}

		private void RecordStop()
		{
			long bytesWritten = _simpleRecorder.BytesWritten;
			_endTime = DateTime.Now;
			_writeLength = (float)bytesWritten / (float)_recordSamplerate;
			_simpleRecorder.StopRecording();
			if (_writeLength > MinWriteLength || !DeleteSmallFiles)
			{
				_writeAllMbyte += (float)bytesWritten * 9.536743E-07f;
				_writeAllSecond += _writeLength;
				_filesCounter++;
				string text = MakeFileName();
				string text2 = text;
				int length = text.LastIndexOf('.');
				while (File.Exists(text2))
				{
					text2 = text.Substring(0, length) + "(" + _fileIndexer.ToString("D3") + ").wav";
					_fileIndexer++;
				}
				try
				{
					File.Move(writeFolderBrowserDialog.SelectedPath + "\\temporaryAudioRecord.wav", text2);
				}
				catch (Exception ex)
				{
					MessageBox.Show("Unable to rename file. " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
			}
			else
			{
				File.Delete(writeFolderBrowserDialog.SelectedPath + "\\temporaryAudioRecord.wav");
			}
		}

		private void recBtn_Click(object sender, EventArgs e)
		{
			if (!_recordIsStarted)
			{
				_entriesInManager = _settingsPersister.ReadStoredFrequencies();
				recordTimer.Enabled = true;
				recDisplayTimer.Enabled = true;
				_recordIsStarted = true;
				_simpleRecorder.DiskWriterPause = true;
				_writeAllMbyte = 0f;
				_filesCounter = 0;
				_writeAllSecond = 0f;
				RecordLabel("Create new file", Color.Black);
			}
			else
			{
				_recordIsStarted = false;
				recordTimer.Enabled = false;
				recDisplayTimer.Enabled = false;
				if (_simpleRecorder.IsRecording)
				{
					RecordStop();
				}
				recordLabel.Visible = false;
			}
			ConfigureGUI();
		}

		private void recDisplayTimer_Tick(object sender, EventArgs e)
		{
			float num = (float)_simpleRecorder.BytesWritten * 9.536743E-07f;
			int second = 0;
			if (_recordSamplerate != 0)
			{
				second = (int)(_simpleRecorder.BytesWritten / _recordSamplerate);
			}
			allLabel.Text = $"all {_filesCounter} file(s) {SecondToDate((int)_writeAllSecond):HH:mm:ss} - {_writeAllMbyte:f3} MB";
			currentLabel.Text = $"current {SecondToDate(second):HH:mm:ss} - {num:f3} MB ";
			progressBar.Value = Math.Min(_simpleRecorder.BufferUsage, progressBar.Maximum);
			label1.Text = "Dropped buffers: " + _simpleRecorder.LostBuffers.ToString();
		}

		private void SelectFolderDialog_Click(object sender, EventArgs e)
		{
			writeFolderBrowserDialog.ShowDialog();
		}

		private void openFolderButton_Click(object sender, EventArgs e)
		{
			Process.Start("explorer.exe", writeFolderBrowserDialog.SelectedPath);
		}

		private void configureButton_Click(object sender, EventArgs e)
		{
			new DialogConfigure(this).ShowDialog();
		}

		private void ConfigureGUI()
		{
			if (_control.IsPlaying)
			{
				recBtn.Enabled = true;
				recBtn.Text = (_recordIsStarted ? "Stop" : "Record");
			}
			else
			{
				recBtn.Enabled = false;
				recBtn.Text = "Record";
			}
			recordLabel.Visible = _recordIsStarted;
			selectFolderDialog.Enabled = !_recordIsStarted;
			configureButton.Enabled = !_recordIsStarted;
		}

		private string MakeFileName()
		{
			string selectedPath = writeFolderBrowserDialog.SelectedPath;
			selectedPath += ParseStringToPath(FileNameRules);
			string text = selectedPath.Substring(0, selectedPath.LastIndexOf("\\"));
			try
			{
				Directory.CreateDirectory(text);
				return selectedPath;
			}
			catch
			{
				MessageBox.Show("Unable to create directory", text, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				return selectedPath;
			}
		}

		public string ParseStringToPath(string nameString)
		{
			string frequency = GetFrequency(_oldFrequency);
			MemoryEntry entryFromManager = GetEntryFromManager(_oldFrequency);
			string str = ReplaceInvalidChars(entryFromManager.Name);
			string str2 = ReplaceInvalidChars(entryFromManager.GroupName);
			string str3 = _startTime.ToString("yyyy_MM_dd");
			string str4 = _startTime.ToString("HH-mm-ss");
			string str5 = _endTime.ToString("HH-mm-ss");
			string str6 = SecondToDate((int)_writeLength).ToString("HH-mm-ss");
			string text = "";
			int num = 0;
			while (num < nameString.Length)
			{
				if (CompareString(nameString, "date", num))
				{
					num += "date".Length;
					text += str3;
					continue;
				}
				if (CompareString(nameString, "time", num))
				{
					num += "time".Length;
					text += str4;
					continue;
				}
				if (CompareString(nameString, "start_time", num))
				{
					num += "start_time".Length;
					text += str4;
					continue;
				}
				if (CompareString(nameString, "end_time", num))
				{
					num += "end_time".Length;
					text += str5;
					continue;
				}
				if (CompareString(nameString, "length", num))
				{
					num += "length".Length;
					text += str6;
					continue;
				}
				if (CompareString(nameString, "frequency", num))
				{
					num += "frequency".Length;
					text += frequency;
					continue;
				}
				if (CompareString(nameString, "name", num))
				{
					num += "name".Length;
					text += str;
					continue;
				}
				if (CompareString(nameString, "group", num))
				{
					num += "group".Length;
					text += str2;
					continue;
				}
				if (CompareString(nameString, "\\", num) || CompareString(nameString, "/", num))
				{
					num++;
					text += "\\";
					continue;
				}
				if (CompareString(nameString, "+", num) || CompareString(nameString, " ", num))
				{
					num++;
					continue;
				}
				if (CompareString(nameString, "\"", num))
				{
					num++;
					int num2 = nameString.IndexOf('"', num);
					if (num2 <= 0)
					{
						return text + "-error!";
					}
					text += ReplaceInvalidChars(nameString.Substring(num, num2 - num));
					num = num2 + 1;
					continue;
				}
				return text + "-error!";
			}
			if (text.Length > 5)
			{
				int length = ".wav".Length;
				if (text.Substring(text.Length - length, length) != ".wav")
				{
					text += ".wav";
				}
				if (text.Substring(0, 1) != "\\")
				{
					text = "\\" + text;
				}
			}
			return text;
		}

		private string ReplaceInvalidChars(string sourceString)
		{
			char[] invalidPathChars = Path.GetInvalidPathChars();
			char[] invalidFileNameChars = Path.GetInvalidFileNameChars();
			while (true)
			{
				int num = sourceString.IndexOfAny(invalidPathChars);
				if (num == -1)
				{
					break;
				}
				sourceString = sourceString.Replace(sourceString[num], '_');
			}
			while (true)
			{
				int num2 = sourceString.IndexOfAny(invalidFileNameChars);
				if (num2 == -1)
				{
					break;
				}
				sourceString = sourceString.Replace(sourceString[num2], '_');
			}
			return sourceString;
		}

		private bool CompareString(string source, string compare, int index)
		{
			if (index + compare.Length > source.Length)
			{
				return false;
			}
			return source.Substring(index, compare.Length) == compare;
		}

		private MemoryEntry GetEntryFromManager(long frequency)
		{
			MemoryEntry memoryEntry = new MemoryEntry();
			memoryEntry.Name = "Unknown";
			memoryEntry.GroupName = "No group";
			if (_entriesInManager == null)
			{
				return memoryEntry;
			}
			foreach (MemoryEntry item in _entriesInManager)
			{
				if (item.Frequency == frequency)
				{
					memoryEntry.Name = item.Name;
					memoryEntry.GroupName = item.GroupName;
					return memoryEntry;
				}
			}
			return memoryEntry;
		}

		private static string GetFrequency(long frequency)
		{
			long num = Math.Abs(frequency);
			if (num == 0L)
			{
				return "DC";
			}
			if (num > 1500000000)
			{
				return $"{(double)frequency / 1000000000.0:#,0.000 000} GHz";
			}
			if (num > 30000000)
			{
				return $"{(double)frequency / 1000000.0:0,0.000###} MHz";
			}
			if (num > 1000)
			{
				return $"{(double)frequency / 1000.0:#,#.###} kHz";
			}
			return frequency.ToString();
		}

		public void AbortRecording()
		{
			if (_recordIsStarted)
			{
				recBtn_Click(null, null);
			}
		}

		private void recordTimer_Tick(object sender, EventArgs e)
		{
			if (!_recordIsStarted)
			{
				return;
			}
			if (MaxFileSizeEnable && (float)_simpleRecorder.BytesWritten * 9.536743E-07f >= (float)MaxWriteLength)
			{
				RecordStop();
				RecordStart();
			}
			if (_simpleRecorder.IsStreamFull)
			{
				RecordStop();
				RecordStart();
			}
			if (!WriteOneFile && _simpleRecorder.IsRecording)
			{
				if (_oldFrequency != _control.Frequency && (NewFileFrequencyEnable || SignalIsActive()))
				{
					RecordStop();
					RecordLabel("Create new file", Color.Black);
				}
				if (NewFileTimeEnable)
				{
					if (SignalIsActive())
					{
						_newFileWaitTime = NewFileTime;
					}
					else if (_newFileWaitTime > 0)
					{
						_newFileWaitTime -= recordTimer.Interval;
					}
					else
					{
						RecordStop();
						RecordLabel("Create new file", Color.Black);
					}
				}
			}
			if (!DontWritePause && !_simpleRecorder.IsRecording)
			{
				RecordStart();
				_simpleRecorder.DiskWriterPause = false;
				RecordLabel("Record", Color.Red);
			}
			if (!DontWritePause)
			{
				return;
			}
			if (SignalIsActive())
			{
				if (!_simpleRecorder.IsRecording)
				{
					RecordStart();
				}
				_simpleRecorder.DiskWriterPause = false;
				RecordLabel("Record", Color.Red);
				_waitTime = ContinueRecordTime;
			}
			if (!SignalIsActive() && _simpleRecorder.IsRecording)
			{
				if (ContinueRecordTimeEnable && _waitTime > 0)
				{
					_waitTime -= recordTimer.Interval;
					RecordLabel($"Record {(float)_waitTime / 1000f:f1}", Color.Green);
				}
				else
				{
					_simpleRecorder.DiskWriterPause = true;
					RecordLabel("Pause", Color.Black);
				}
			}
		}

		private bool SignalIsActive()
		{
			bool flag = true;
			if (UseSquelch)
			{
				flag = (_control.IsSquelchOpen || !_control.SquelchEnabled);
			}
			if (UseMute)
			{
				flag &= !_control.AudioIsMuted;
			}
			return flag;
		}

		private void RecordLabel(string text, Color color)
		{
			recordLabel.Text = text;
			recordLabel.ForeColor = color;
			recordLabel.Visible = true;
		}

		private DateTime SecondToDate(int second)
		{
			return new DateTime(1, 1, 1, 0, 0, 0).AddSeconds(second);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			recBtn = new System.Windows.Forms.Button();
			recDisplayTimer = new System.Windows.Forms.Timer(components);
			allLabel = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			panel1 = new System.Windows.Forms.Panel();
			label1 = new System.Windows.Forms.Label();
			progressBar = new System.Windows.Forms.ProgressBar();
			currentLabel = new System.Windows.Forms.Label();
			configureButton = new System.Windows.Forms.Button();
			openFolderButton = new System.Windows.Forms.Button();
			selectFolderDialog = new System.Windows.Forms.Button();
			recordLabel = new System.Windows.Forms.Label();
			recordTimer = new System.Windows.Forms.Timer(components);
			writeFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			panel1.SuspendLayout();
			SuspendLayout();
			recBtn.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			recBtn.Enabled = false;
			recBtn.Location = new System.Drawing.Point(111, 125);
			recBtn.Name = "recBtn";
			recBtn.Size = new System.Drawing.Size(93, 23);
			recBtn.TabIndex = 0;
			recBtn.Text = "Record";
			recBtn.UseVisualStyleBackColor = true;
			recBtn.Click += new System.EventHandler(recBtn_Click);
			recDisplayTimer.Interval = 1000;
			recDisplayTimer.Tick += new System.EventHandler(recDisplayTimer_Tick);
			allLabel.AutoSize = true;
			allLabel.Location = new System.Drawing.Point(50, 29);
			allLabel.Name = "allLabel";
			allLabel.Size = new System.Drawing.Size(153, 13);
			allLabel.TabIndex = 3;
			allLabel.Text = "all 0 file(s) 00:00:00 - 0.000 MB";
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(9, 9);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(35, 13);
			label2.TabIndex = 4;
			label2.Text = "Write:";
			panel1.Controls.Add(label1);
			panel1.Controls.Add(progressBar);
			panel1.Controls.Add(currentLabel);
			panel1.Controls.Add(configureButton);
			panel1.Controls.Add(openFolderButton);
			panel1.Controls.Add(selectFolderDialog);
			panel1.Controls.Add(label2);
			panel1.Controls.Add(allLabel);
			panel1.Controls.Add(recordLabel);
			panel1.Controls.Add(recBtn);
			panel1.Location = new System.Drawing.Point(0, 0);
			panel1.Name = "panel1";
			panel1.Size = new System.Drawing.Size(217, 190);
			panel1.TabIndex = 7;
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(50, 48);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(95, 13);
			label1.TabIndex = 23;
			label1.Text = "Dropped buffers: 0";
			progressBar.Location = new System.Drawing.Point(111, 67);
			progressBar.Name = "progressBar";
			progressBar.Size = new System.Drawing.Size(93, 23);
			progressBar.TabIndex = 8;
			currentLabel.AutoSize = true;
			currentLabel.Location = new System.Drawing.Point(50, 9);
			currentLabel.Name = "currentLabel";
			currentLabel.Size = new System.Drawing.Size(140, 13);
			currentLabel.TabIndex = 22;
			currentLabel.Text = "current 00:00:00 - 0.000 MB";
			configureButton.Location = new System.Drawing.Point(12, 125);
			configureButton.Name = "configureButton";
			configureButton.Size = new System.Drawing.Size(93, 23);
			configureButton.TabIndex = 21;
			configureButton.Text = "Configure";
			configureButton.UseVisualStyleBackColor = true;
			configureButton.Click += new System.EventHandler(configureButton_Click);
			openFolderButton.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			openFolderButton.Location = new System.Drawing.Point(12, 96);
			openFolderButton.Name = "openFolderButton";
			openFolderButton.Size = new System.Drawing.Size(93, 23);
			openFolderButton.TabIndex = 20;
			openFolderButton.Text = "Open folder";
			openFolderButton.UseVisualStyleBackColor = true;
			openFolderButton.Click += new System.EventHandler(openFolderButton_Click);
			selectFolderDialog.Location = new System.Drawing.Point(12, 67);
			selectFolderDialog.Name = "selectFolderDialog";
			selectFolderDialog.Size = new System.Drawing.Size(93, 23);
			selectFolderDialog.TabIndex = 12;
			selectFolderDialog.Text = "Folder select";
			selectFolderDialog.UseVisualStyleBackColor = true;
			selectFolderDialog.Click += new System.EventHandler(SelectFolderDialog_Click);
			recordLabel.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			recordLabel.AutoSize = true;
			recordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			recordLabel.ForeColor = System.Drawing.Color.Black;
			recordLabel.Location = new System.Drawing.Point(111, 101);
			recordLabel.Name = "recordLabel";
			recordLabel.Size = new System.Drawing.Size(92, 13);
			recordLabel.TabIndex = 9;
			recordLabel.Text = "Create new file";
			recordLabel.Visible = false;
			recordTimer.Tick += new System.EventHandler(recordTimer_Tick);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(panel1);
			base.Name = "AudioRecorderPanel";
			base.Size = new System.Drawing.Size(217, 193);
			panel1.ResumeLayout(performLayout: false);
			panel1.PerformLayout();
			ResumeLayout(performLayout: false);
		}
	}
}

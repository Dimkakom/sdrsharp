using System;

namespace SDRSharp.AudioRecorder
{
	public struct MediaInfo
	{
		public string IART;

		public string IGNR;

		public string INAM;

		public string ICMT;

		public string IPRD;

		public string IKEY;

		public string ICRD;

		public string ISRC;

		public string ITCH;

		public string ISBJ;

		public string ICOP;

		public MediaInfo(string pINAM)
		{
			IART = "";
			IGNR = "";
			INAM = pINAM;
			ICMT = "";
			IPRD = "";
			IKEY = "";
			ICRD = DateTime.Now.ToString("yyyyMMddHHmmss");
			ISRC = "";
			ITCH = "";
			ISBJ = "";
			ICOP = "****************";
		}
	}
}

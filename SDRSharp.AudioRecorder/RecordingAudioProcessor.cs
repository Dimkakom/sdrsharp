using SDRSharp.Radio;

namespace SDRSharp.AudioRecorder
{
	public class RecordingAudioProcessor : IRealProcessor, IStreamProcessor, IBaseProcessor
	{
		public unsafe delegate void AudioReadyDelegate(float* audio, int length);

		private double _sampleRate;

		private bool _enabled;

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				_sampleRate = value;
			}
		}

		public event AudioReadyDelegate AudioReady;

		public unsafe void Process(float* audio, int length)
		{
			this.AudioReady?.Invoke(audio, length);
		}
	}
}

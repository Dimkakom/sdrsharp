namespace SDRSharp.AudioRecorder
{
	public enum WavSampleFormat
	{
		PCM8Stereo,
		PCM16Stereo,
		Float32,
		PCM8Mono,
		PCM16Mono
	}
}

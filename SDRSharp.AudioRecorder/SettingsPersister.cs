using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SDRSharp.AudioRecorder
{
	public class SettingsPersister
	{
		private const string FreqMgrFilename = "frequencies.xml";

		private readonly string _settingsFolder;

		public SettingsPersister()
		{
			_settingsFolder = Path.GetDirectoryName(Application.ExecutablePath);
		}

		public List<MemoryEntry> ReadStoredFrequencies()
		{
			List<MemoryEntry> list = ReadObject<List<MemoryEntry>>("frequencies.xml");
			if (list != null)
			{
				list.Sort((MemoryEntry e1, MemoryEntry e2) => e1.Frequency.CompareTo(e2.Frequency));
				return list;
			}
			return new List<MemoryEntry>();
		}

		public void PersistStoredFrequencies(List<MemoryEntry> entries)
		{
			WriteObject(entries, "frequencies.xml");
		}

		private T ReadObject<T>(string fileName)
		{
			string path = Path.Combine(_settingsFolder, fileName);
			if (File.Exists(path))
			{
				using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
				{
					return (T)new XmlSerializer(typeof(T)).Deserialize(stream);
				}
			}
			return default(T);
		}

		private void WriteObject<T>(T obj, string fileName)
		{
			using (FileStream stream = new FileStream(Path.Combine(_settingsFolder, fileName), FileMode.Create))
			{
				new XmlSerializer(obj.GetType()).Serialize(stream, obj);
			}
		}
	}
}

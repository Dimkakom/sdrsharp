using SDRSharp.Radio;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace SDRSharp.AudioRecorder
{
	public class SimpleWavWriter
	{
		private const long MaxStreamLength = 2147483648L;

		private readonly string _filename;

		private readonly WavFormatHeader _format;

		private readonly WavSampleFormat _wavSampleFormat;

		private BinaryWriter _outputStream;

		private long _fileSizeOffs;

		private long _dataSizeOffs;

		private long _length;

		private byte[] _outputBuffer;

		private UnsafeBuffer _resampleBuffer;

		private unsafe float* _resampleBufferPtr;

		private bool _isStreamFull;

		private int _samplerateOut;

		private Resampler _resampler;

		private float _resampleCoeff;

		public MediaInfo _MediaInfo;

		public WavSampleFormat SampleFormat => _wavSampleFormat;

		public WavFormatHeader WaveFormat => _format;

		public string FileName => _filename;

		public long Length => _length;

		public bool IsStreamFull => _isStreamFull;

		public SimpleWavWriter(string filename, WavSampleFormat recordingFormat, uint sampleRateIn, int samplerateOut)
		{
			_filename = filename;
			_wavSampleFormat = recordingFormat;
			_samplerateOut = samplerateOut;
			_format = new WavFormatHeader(recordingFormat, (uint)_samplerateOut);
			if (sampleRateIn != samplerateOut)
			{
				_resampler = new Resampler(sampleRateIn, samplerateOut);
				_resampleCoeff = (float)samplerateOut / (float)(double)sampleRateIn;
			}
			else
			{
				_resampler = null;
			}
		}

		public void Open()
		{
			if (_outputStream == null)
			{
				_outputStream = new BinaryWriter(File.Create(_filename));
				WriteHeader();
				return;
			}
			throw new InvalidOperationException("Stream already open");
		}

		public void Close()
		{
			if (_outputStream != null)
			{
				UpdateLength();
				_outputStream.Flush();
				_outputStream.Close();
				_outputStream = null;
				_outputBuffer = null;
				return;
			}
			throw new InvalidOperationException("Stream not open");
		}

		public unsafe void Write(float* data, int length)
		{
			if (_outputStream != null)
			{
				int lengthOut = length;
				float* bufferOut = data;
				switch (_wavSampleFormat)
				{
				case WavSampleFormat.PCM8Stereo:
					WritePCM8(data, length);
					break;
				case WavSampleFormat.PCM8Mono:
					length /= 2;
					StereoToMono(data, length);
					Resample(data, length, out bufferOut, out lengthOut);
					WritePCM8(bufferOut, lengthOut);
					break;
				case WavSampleFormat.PCM16Stereo:
					WritePCM16(data, length);
					break;
				case WavSampleFormat.PCM16Mono:
					length /= 2;
					StereoToMono(data, length);
					Resample(data, length, out bufferOut, out lengthOut);
					WritePCM16(bufferOut, lengthOut);
					break;
				case WavSampleFormat.Float32:
					WriteFloat(data, length);
					break;
				}
				return;
			}
			throw new InvalidOperationException("Stream not open");
		}

		private unsafe void StereoToMono(float* data, int length)
		{
			int num = 0;
			for (int i = 0; i < length; i++)
			{
				data[i] = data[num];
				num += 2;
			}
		}

		private unsafe void Resample(float* data, int length, out float* bufferOut, out int lengthOut)
		{
			if (_resampler != null)
			{
				int num = (int)((float)length * _resampleCoeff) + 100;
				if (_resampleBuffer == null || _resampleBuffer.Length != num)
				{
					_resampleBuffer = null;
					_resampleBuffer = UnsafeBuffer.Create(num, 4);
					_resampleBufferPtr = (float*)(void*)_resampleBuffer;
				}
				lengthOut = _resampler.Process(data, _resampleBufferPtr, length);
				bufferOut = _resampleBufferPtr;
			}
			else
			{
				lengthOut = length;
				bufferOut = data;
			}
		}

		private unsafe void WritePCM8(float* data, int length)
		{
			if (_outputBuffer == null || _outputBuffer.Length != length)
			{
				_outputBuffer = null;
				_outputBuffer = new byte[length];
			}
			for (int i = 0; i < length; i++)
			{
				_outputBuffer[i] = (byte)(data[i] * 127f + 128f);
			}
			WriteStream(_outputBuffer);
		}

		private unsafe void WritePCM16(float* data, int length)
		{
			if (_outputBuffer == null || _outputBuffer.Length != length * 2)
			{
				_outputBuffer = null;
				_outputBuffer = new byte[length * 2];
			}
			int num = 0;
			for (int i = 0; i < length; i++)
			{
				short num2 = (short)(data[i] * 32767f);
				_outputBuffer[num] = (byte)(num2 & 0xFF);
				num++;
				_outputBuffer[num] = (byte)(num2 >> 8);
				num++;
			}
			WriteStream(_outputBuffer);
		}

		private unsafe void WriteFloat(float* data, int length)
		{
			if (_outputBuffer == null || _outputBuffer.Length != length * 4 * 2)
			{
				_outputBuffer = null;
				_outputBuffer = new byte[length * 4 * 2];
			}
			Marshal.Copy((IntPtr)(void*)data, _outputBuffer, 0, _outputBuffer.Length);
			WriteStream(_outputBuffer);
		}

		private void WriteStream(byte[] data)
		{
			if (_outputStream != null)
			{
				int num = (int)Math.Min(2147483648u - _outputStream.BaseStream.Length, data.Length);
				_outputStream.Write(data, 0, num);
				_length += num;
				UpdateLength();
				_isStreamFull = (_outputStream.BaseStream.Length >= 2147483648u);
			}
		}

		private void WriteHeader()
		{
			if (_outputStream != null)
			{
				WriteTag("RIFF");
				_fileSizeOffs = _outputStream.BaseStream.Position;
				_outputStream.Write(0u);
				WriteTag("WAVE");
				WriteTag("fmt ");
				_outputStream.Write(16u);
				_outputStream.Write(_format.FormatTag);
				_outputStream.Write(_format.Channels);
				_outputStream.Write(_format.SamplesPerSec);
				_outputStream.Write(_format.AvgBytesPerSec);
				_outputStream.Write(_format.BlockAlign);
				_outputStream.Write(_format.BitsPerSample);
				WriteTag("data");
				_dataSizeOffs = _outputStream.BaseStream.Position;
				_outputStream.Write(0u);
			}
		}

		private void WriteMediaInfo()
		{
			WriteTag("LIST");
			_outputStream.Write(560u);
			WriteTag("INFO");
			WriteTag("IART");
			_outputStream.Write(64u);
			WriteTagL(_MediaInfo.IART, 64);
			WriteTag("IGNR");
			_outputStream.Write(64u);
			WriteTagL(_MediaInfo.IGNR, 64);
			WriteTag("INAM");
			_outputStream.Write(64u);
			WriteTagL(_MediaInfo.INAM, 64);
			WriteTag("ICMT");
			_outputStream.Write(64u);
			WriteTagL(_MediaInfo.ICMT, 64);
			WriteTag("IPRD");
			_outputStream.Write(64u);
			WriteTagL(_MediaInfo.IPRD, 64);
			WriteTag("IKEY");
			_outputStream.Write(20u);
			WriteTagL(_MediaInfo.IKEY, 20);
			WriteTag("ICRD");
			_outputStream.Write(16u);
			WriteTagL(_MediaInfo.ICRD, 16);
			WriteTag("ISRC");
			_outputStream.Write(16u);
			WriteTagL(_MediaInfo.IART, 16);
			WriteTag("ITCH");
			_outputStream.Write(64u);
			WriteTagL(_MediaInfo.ITCH, 64);
			WriteTag("ISBJ");
			_outputStream.Write(64u);
			WriteTagL(_MediaInfo.ISBJ, 64);
			WriteTag("ICOP");
			_outputStream.Write(16u);
			WriteTagL(_MediaInfo.ICOP, 16);
		}

		private void UpdateLength()
		{
			if (_outputStream != null)
			{
				_outputStream.Seek((int)_fileSizeOffs, SeekOrigin.Begin);
				_outputStream.Write((uint)(_outputStream.BaseStream.Length - 8));
				_outputStream.Seek((int)_dataSizeOffs, SeekOrigin.Begin);
				_outputStream.Write((uint)_length);
				_outputStream.BaseStream.Seek(0L, SeekOrigin.End);
			}
		}

		private void WriteTag(string tag)
		{
			byte[] bytes = Encoding.ASCII.GetBytes(tag);
			_outputStream.Write(bytes, 0, bytes.Length);
		}

		private void WriteTagL(string tag, int alength)
		{
			byte[] array = Encoding.ASCII.GetBytes(tag);
			Array.Resize(ref array, alength);
			_outputStream.Write(array, 0, array.Length);
		}
	}
}

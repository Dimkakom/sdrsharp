using SDRSharp.Common;
using System.Windows.Forms;

namespace SDRSharp.AudioRecorder
{
	public class AudioRecorderPlugin : ISharpPlugin
	{
		private const string DefaultDisplayName = "Audio Recorder";

		private ISharpControl _control;

		private AudioRecorderPanel _guiControl;

		public UserControl Gui => _guiControl;

		public string DisplayName => "Audio Recorder";

		public void Initialize(ISharpControl control)
		{
			_control = control;
			_guiControl = new AudioRecorderPanel(_control);
		}

		public void Close()
		{
			if (_guiControl != null)
			{
				_guiControl.AbortRecording();
				_guiControl.SaveSettings();
			}
		}
	}
}

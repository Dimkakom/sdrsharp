using SDRSharp.Common;
using SDRSharp.Radio;
using System.Windows.Forms;

namespace SDRSharp.DigitalIfProcessor
{
	public class DigitalIfProcessorPlugin : ISharpPlugin
	{
		private const string _displayName = "IF processor";

		private ISharpControl _control;

		private IFProcessor _ifProcessor;

		private DigitalIfProcessorPanel _guiControl;

		public string DisplayName => "IF processor";

		public UserControl Gui => _guiControl;

		public void Initialize(ISharpControl control)
		{
			_control = control;
			_ifProcessor = new IFProcessor();
			_control.RegisterStreamHook(_ifProcessor, ProcessorType.DecimatedAndFilteredIQ);
			_guiControl = new DigitalIfProcessorPanel(_ifProcessor, _control);
		}

		public void Close()
		{
			_guiControl.StoreSettings();
		}
	}
}

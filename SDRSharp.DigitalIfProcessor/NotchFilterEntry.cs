namespace SDRSharp.DigitalIfProcessor
{
	public class NotchFilterEntry
	{
		public long Frequency
		{
			get;
			set;
		}

		public int FilterWidth
		{
			get;
			set;
		}

		public int Attenuation
		{
			get;
			set;
		}
	}
}

using SDRSharp.Radio;

namespace SDRSharp.DigitalIfProcessor
{
	public class IFProcessor : IRealProcessor, IIQProcessor, IStreamProcessor, IBaseProcessor
	{
		public const int FFTSize = 8192;

		private DigitalProcessor _digitalProcessor;

		private double _sampleRate;

		private bool _enabled;

		private bool _needNewFilter;

		private unsafe float* _gainsBufferPtr;

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				_sampleRate = value;
			}
		}

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public bool FilterEnable
		{
			get;
			set;
		}

		public bool ShowSpectrumAfterProcess
		{
			get;
			set;
		}

		public event DigitalProcessor.FFTReadyDelegate FFTReady;

		public unsafe void Process(Complex* buffer, int length)
		{
			if (_sampleRate != 0.0)
			{
				if (_digitalProcessor == null)
				{
					_digitalProcessor = new DigitalProcessor();
					_digitalProcessor.FFTReady += this.FFTReady;
				}
				if (_needNewFilter)
				{
					_needNewFilter = false;
					_digitalProcessor.BuildFilter(_gainsBufferPtr);
				}
				_digitalProcessor.ShowSpectrumAfterProcess = ShowSpectrumAfterProcess;
				_digitalProcessor.FilterEnable = FilterEnable;
				_digitalProcessor.Process(buffer, length);
			}
		}

        public unsafe void Process(float* buffer, int length)
        {
            throw new System.NotImplementedException();
        }

        public unsafe void UpdateFilter(float* gainsBuffer)
		{
			_gainsBufferPtr = gainsBuffer;
			_needNewFilter = true;
		}
	}
}

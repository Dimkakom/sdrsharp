using SDRSharp.Common;
using SDRSharp.PanView;
using SDRSharp.Radio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace SDRSharp.DigitalIfProcessor
{
	public class DigitalIfProcessorPanel : UserControl
	{
		private const int fftBins = 8192;

		private readonly SortableBindingList<NotchFilterEntry> _displayedEntries = new SortableBindingList<NotchFilterEntry>();

		private readonly SettingsPersister _settingsPersister;

		private List<NotchFilterEntry> _entries;

		private IFProcessor _ifProcessor;

		private ISharpControl _control;

		private const int FFTTimerInterval = 50;

		private int _fftBins;

		private bool _fftSpectrumAvailable;

		private UnsafeBuffer _fftBuffer;

		private unsafe Complex* _fftPtr;

		private UnsafeBuffer _fftSpectrum;

		private unsafe float* _fftSpectrumPtr;

		private UnsafeBuffer _gainsBuffer;

		private unsafe float* _gainsBufferPtr;

		private double _sampleRate;

		private Thread _fftThread;

		private bool _fftThreadRunning;

		private System.Windows.Forms.Timer _fftDisplayTimer;

		private SpectrumAnalyzer _spectrumAnalyzer;

		private readonly float _fftOffset = (float)Utils.GetDoubleSetting("fftOffset", -40.0);

		private readonly SharpEvent _fftEvent = new SharpEvent(initialState: false);

		private readonly ComplexFifoStream _iqStream = new ComplexFifoStream(BlockMode.BlockingRead);

		private float _correction;

		private List<NotchFilterEntry> _notchActiveFrequencies;

		private int _endFilterShift;

		private int _startFilterShift;

		private bool _needReinit;

		private bool _moveLowFilterFrequency;

		private bool _moveHighFilterFrequency;

		private bool _moveFrequency;

		private bool _mouseKeyDown;

		private bool _showCursor;

		private Point _cursorPosition;

		private List<RectangleF> _notchActiveFrequenciesPoint;

		private bool _moveNotchFrequency;

		private Rectangle _notchEnableRectangle;

		private Rectangle _filterEnableRectangle;

		private Rectangle _nrEnableRectangle;

		private bool _filterClick;

		private bool _nrClick;

		private bool _notchClick;

		private Rectangle _beforeEnableRectangle;

		private Rectangle _afterEnableRectangle;

		private bool _beforeClick;

		private bool _afterClick;

		private int _lastCursorPosition;

		private bool _moveNotchWidth;

		private float _currentRectangleCenter;

		private List<NotchFilterEntry> _notchVisibleFrequencies;

		private string _cursorText;

		private bool _deleteCurrentNotches;

		private RectangleF _asimetricFilterLowRectangle;

		private RectangleF _asimetricFilterHighRectangle;

		private IContainer components;

		private NumericUpDown widthNumericUpDown;

		private NumericUpDown attenuatNumericUpDown;

		private Label label2;

		private Label label3;

		private GroupBox groupBox2;

		private Button delButton;

		private Button addButton;

		private CheckBox trackingCheckBox;

		private DataGridView notchesDataGridView;

		private BindingSource notchFilterEntryBindingSource;

		private DataGridViewTextBoxColumn frequencyDataGridViewTextBoxColumn;

		private DataGridViewTextBoxColumn attenuationDataGridViewTextBoxColumn;

		private DataGridViewTextBoxColumn filterWidthDataGridViewTextBoxColumn;

		private System.Windows.Forms.Timer displayTimer;

		private CheckBox filterEnableCheckBox;

		private GroupBox groupBox5;

		private RadioButton afterRadioButton;

		private RadioButton beforeRadioButton;

		private CheckBox ifSpectrumCheckBox;

		private ComboBox spectrumPositionComboBox;

		private Label label7;

		private CheckBox showNotchOnMainSpectrumCheckBox;

		public unsafe DigitalIfProcessorPanel(IFProcessor ifProcessor, ISharpControl control)
		{
			InitializeComponent();
			_ifProcessor = ifProcessor;
			_ifProcessor.FFTReady += _ifProcessor_FFTReady;
			_control = control;
			_control.PropertyChanged += PropertyChangedHandler;
			_settingsPersister = new SettingsPersister();
			_entries = _settingsPersister.ReadStoredFrequencies();
			_notchActiveFrequencies = new List<NotchFilterEntry>();
			_notchActiveFrequenciesPoint = new List<RectangleF>();
			_notchVisibleFrequencies = new List<NotchFilterEntry>();
			notchFilterEntryBindingSource.DataSource = _displayedEntries;
			DisplayEntriesUpdate();
			IfProcessorEnabled();
			_fftDisplayTimer = new System.Windows.Forms.Timer();
			_fftDisplayTimer.Tick += fftDisplayTimer_Tick;
			_fftDisplayTimer.Interval = 50;
			_fftBins = 8192;
			InitFFTBuffers();
			_spectrumAnalyzer = new SpectrumAnalyzer();
			_spectrumAnalyzer.Dock = DockStyle.Fill;
			_spectrumAnalyzer.Margin = new Padding(0, 0, 0, 0);
			_spectrumAnalyzer.DisplayRange = 130;
			_spectrumAnalyzer.EnableFilter = false;
			_spectrumAnalyzer.EnableHotTracking = false;
			_spectrumAnalyzer.EnableSideFilterResize = false;
			_spectrumAnalyzer.EnableFilterMove = false;
			_spectrumAnalyzer.EnableFrequencyMarker = false;
			_spectrumAnalyzer.BandType = BandType.Center;
			_spectrumAnalyzer.StepSize = 1000;
			_spectrumAnalyzer.UseSmoothing = true;
			_spectrumAnalyzer.Attack = 0.9f;
			_spectrumAnalyzer.Decay = 0.6f;
			_spectrumAnalyzer.StatusText = "";
			_spectrumAnalyzer.FrequencyChanged += spectrumAnalyzer_FrequencyChanged;
			_spectrumAnalyzer.CenterFrequencyChanged += spectrumAnalyzer_CenterFrequencyChanged;
			_spectrumAnalyzer.VisibleChanged += spectrumAnalyzer_VisibleChanged;
			_spectrumAnalyzer.CustomPaint += spectrumCustomPaint;
			_spectrumAnalyzer.MouseDown += _spectrumAnalyzer_MouseDown;
			_spectrumAnalyzer.MouseUp += _spectrumAnalyzer_MouseUp;
			_spectrumAnalyzer.MouseMove += _spectrumAnalyzer_MouseMove;
			_spectrumAnalyzer.MouseLeave += _spectrumAnalyzer_MouseLeave;
			_spectrumAnalyzer.MouseWheel += _spectrumAnalyzer_MouseWheel;
			_spectrumAnalyzer.Visible = false;
			spectrumAnalyzer_VisibleChanged(null, null);
			float num = (float)(10.0 * Math.Log10((double)_fftBins / 2.0));
			_correction = 24f - num + _fftOffset;
			_control.SpectrumAnalyzerCustomPaint += mainSpectrumCustomPaint;
			widthNumericUpDown.Value = Utils.GetIntSetting("NotchWidth", 300);
			attenuatNumericUpDown.Value = Utils.GetIntSetting("NotchAttenuation", -160);
			trackingCheckBox.Checked = Utils.GetBooleanSetting("trackingFilterEnable");
			filterEnableCheckBox.Checked = Utils.GetBooleanSetting("AsimetricFilterEnable");
			ifSpectrumCheckBox.Checked = Utils.GetBooleanSetting("IfSpectrumShow");
			spectrumPositionComboBox.SelectedIndex = Utils.GetIntSetting("ifSpectrumPosition", 1);
			beforeRadioButton.Checked = Utils.GetBooleanSetting("ShowIFSpectrumBefore");
			beforeRadioButton_CheckedChanged(null, null);
			showNotchOnMainSpectrumCheckBox.Checked = Utils.GetBooleanSetting("ShowNotchesOnMainSpectrum");
			_control.RegisterFrontControl(_spectrumAnalyzer, (PluginPosition)spectrumPositionComboBox.SelectedIndex);
		}

		public void StoreSettings()
		{
			if (_fftThreadRunning)
			{
				Stop();
			}
			Utils.SaveSetting("NotchWidth", widthNumericUpDown.Value);
			Utils.SaveSetting("NotchAttenuation", attenuatNumericUpDown.Value);
			Utils.SaveSetting("trackingFilterEnable", trackingCheckBox.Checked);
			Utils.SaveSetting("AsimetricFilterEnable", filterEnableCheckBox.Checked);
			Utils.SaveSetting("IfSpectrumShow", ifSpectrumCheckBox.Checked);
			Utils.SaveSetting("ifSpectrumPosition", spectrumPositionComboBox.SelectedIndex);
			Utils.SaveSetting("ShowIFSpectrumBefore", beforeRadioButton.Checked);
			Utils.SaveSetting("ShowNotchesOnMainSpectrum", showNotchOnMainSpectrumCheckBox.Checked);
			_settingsPersister.PersistStoredFrequencies(_entries);
		}

		private string GetFrequencyDisplay(long frequency)
		{
			long num = Math.Abs(frequency);
			if (num == 0L)
			{
				return "DC";
			}
			if (num > 1500000000)
			{
				return $"{(double)frequency / 1000000000.0:#,0.000 000} GHz";
			}
			if (num > 30000000)
			{
				return $"{(double)frequency / 1000000.0:0,0.000###} MHz";
			}
			if (num > 1000)
			{
				return $"{(double)frequency / 1000.0:#,#.###} kHz";
			}
			return frequency.ToString() + " Hz";
		}

		private void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
			case "StartRadio":
				IfProcessorEnabled();
				ConfigureSpectrumAnalyzer();
				UpdateNotchesFrequencies();
				UpdateVisibleFrequencies();
				break;
			case "StopRadio":
				IfProcessorEnabled();
				break;
			case "Frequency":
				ConfigureSpectrumAnalyzer();
				UpdateNotchesFrequencies();
				break;
			case "CenterFrequency":
				UpdateVisibleFrequencies();
				break;
			case "DetectorType":
			case "FilterBandwidth":
				ConfigureSpectrumAnalyzer();
				UpdateAsymmetricFilter();
				UpdateNotchesFrequencies();
				break;
			case "StepSize":
			case "FFTRange":
			case "FFTOffset":
			case "SAttack":
			case "SDecay":
				ConfigureSpectrumAnalyzer();
				break;
			}
		}

		private void _spectrumAnalyzer_MouseWheel(object sender, MouseEventArgs e)
		{
			if (_moveNotchFrequency)
			{
				SelectCurrentEntry(_spectrumAnalyzer.PointToFrequency(e.X));
				decimal value = attenuatNumericUpDown.Value;
				value += (decimal)(int)((float)e.Delta * 0.1f);
				if (value > attenuatNumericUpDown.Maximum)
				{
					value = attenuatNumericUpDown.Maximum;
				}
				if (value < attenuatNumericUpDown.Minimum)
				{
					value = attenuatNumericUpDown.Minimum;
				}
				attenuatNumericUpDown.Value = value;
				_cursorText = "Attenuation " + value.ToString() + "dB";
			}
		}

		private void _spectrumAnalyzer_MouseLeave(object sender, EventArgs e)
		{
			_showCursor = false;
			_cursorText = "";
		}

		private void _spectrumAnalyzer_MouseMove(object sender, MouseEventArgs e)
		{
			_cursorPosition.X = e.X;
			_cursorPosition.Y = e.Y;
			if (_lastCursorPosition == e.X)
			{
				SetCursors();
			}
			else if (!_mouseKeyDown)
			{
				_moveFrequency = (_moveHighFilterFrequency = (_moveLowFilterFrequency = (_moveNotchFrequency = (_moveNotchWidth = false))));
				_filterClick = (_nrClick = (_notchClick = (_beforeClick = (_afterClick = false))));
				_cursorText = "";
				_showCursor = false;
				if (_cursorPosition.Y > 30 && _cursorPosition.Y < _spectrumAnalyzer.Height - 30 && _cursorPosition.X > 30 && _cursorPosition.X < _spectrumAnalyzer.Width - 30)
				{
					if (filterEnableCheckBox.Checked && _asimetricFilterLowRectangle.Contains(_cursorPosition))
					{
						_moveLowFilterFrequency = true;
					}
					else if (filterEnableCheckBox.Checked && _asimetricFilterHighRectangle.Contains(_cursorPosition))
					{
						_moveHighFilterFrequency = true;
					}
					else if (_notchActiveFrequenciesPoint.Count > 0)
					{
						for (int i = 0; i < _notchActiveFrequenciesPoint.Count; i++)
						{
							RectangleF rectangleF = new RectangleF(_notchActiveFrequenciesPoint[i].Location, _notchActiveFrequenciesPoint[i].Size);
							if (rectangleF.Contains(_cursorPosition))
							{
								_moveNotchFrequency = true;
								_currentRectangleCenter = rectangleF.Location.X + rectangleF.Size.Width / 2f;
								break;
							}
							rectangleF.Inflate(4f, 0f);
							if (rectangleF.Contains(_cursorPosition))
							{
								_moveNotchWidth = true;
								_currentRectangleCenter = rectangleF.Location.X + rectangleF.Size.Width / 2f;
								break;
							}
						}
					}
					_moveFrequency = (!_moveHighFilterFrequency && !_moveLowFilterFrequency && !_moveNotchFrequency && !_moveNotchWidth);
					_showCursor = (_moveFrequency && trackingCheckBox.Checked);
				}
				else if (_cursorPosition.Y < 30 && _cursorPosition.Y > 0)
				{
					_showCursor = false;
					if (_filterEnableRectangle.Contains(_cursorPosition))
					{
						_filterClick = true;
					}
					else if (_nrEnableRectangle.Contains(_cursorPosition))
					{
						_nrClick = true;
					}
					else if (_notchEnableRectangle.Contains(_cursorPosition))
					{
						_notchClick = true;
					}
					else if (_beforeEnableRectangle.Contains(_cursorPosition))
					{
						_beforeClick = true;
					}
					else if (_afterEnableRectangle.Contains(_cursorPosition))
					{
						_afterClick = true;
					}
				}
				else if (_cursorPosition.Y < _spectrumAnalyzer.Height && _cursorPosition.Y > _spectrumAnalyzer.Height - 30)
				{
					_showCursor = false;
				}
				SetCursors();
			}
			else
			{
				if (!_mouseKeyDown)
				{
					return;
				}
				_showCursor = false;
				if (_moveLowFilterFrequency)
				{
					_startFilterShift = (int)(_spectrumAnalyzer.PointToFrequency(e.X) - _control.Frequency);
					CreateFilter();
					_cursorText = GetFrequencyDisplay(_startFilterShift);
				}
				else if (_moveHighFilterFrequency)
				{
					_endFilterShift = (int)(_spectrumAnalyzer.PointToFrequency(e.X) - _control.Frequency);
					CreateFilter();
					_cursorText = GetFrequencyDisplay(_endFilterShift);
				}
				else if (_moveNotchFrequency)
				{
					long frequency = _spectrumAnalyzer.PointToFrequency(_cursorPosition.X);
					EditCurrentEntryFrequency(frequency);
					_cursorText = GetFrequencyDisplay(frequency);
				}
				else if (_moveNotchWidth)
				{
					long num = Math.Abs(_spectrumAnalyzer.PointToFrequency(_currentRectangleCenter) - _spectrumAnalyzer.PointToFrequency(_cursorPosition.X)) * 2;
					if ((decimal)num > widthNumericUpDown.Maximum)
					{
						num = (long)widthNumericUpDown.Maximum;
					}
					widthNumericUpDown.Value = num;
					_cursorText = GetFrequencyDisplay(num);
				}
			}
		}

		private void SetCursors()
		{
			if (_moveNotchFrequency)
			{
				if (_spectrumAnalyzer.Cursor != Cursors.VSplit)
				{
					_spectrumAnalyzer.Cursor = Cursors.VSplit;
				}
			}
			else if (_filterClick || _nrClick || _notchClick || _beforeClick || _afterClick)
			{
				if (_spectrumAnalyzer.Cursor != Cursors.Hand)
				{
					_spectrumAnalyzer.Cursor = Cursors.Hand;
				}
			}
			else if (_moveHighFilterFrequency || _moveLowFilterFrequency || _moveNotchWidth)
			{
				if (_spectrumAnalyzer.Cursor != Cursors.SizeWE)
				{
					_spectrumAnalyzer.Cursor = Cursors.SizeWE;
				}
			}
			else if (_spectrumAnalyzer.Cursor != Cursors.Default)
			{
				_spectrumAnalyzer.Cursor = Cursors.Default;
			}
		}

		private void _spectrumAnalyzer_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.X == _lastCursorPosition)
			{
				if (_moveNotchFrequency)
				{
					DeleteCurrentEntry();
					_moveNotchFrequency = false;
					_showCursor = true;
				}
				else if (_showCursor)
				{
					AddNewNotch(_spectrumAnalyzer.PointToFrequency(e.X));
					_showCursor = false;
					_moveNotchFrequency = true;
				}
			}
			_mouseKeyDown = false;
		}

		private void _spectrumAnalyzer_MouseDown(object sender, MouseEventArgs e)
		{
			_lastCursorPosition = e.X;
			if (_filterClick)
			{
				filterEnableCheckBox.Checked = !filterEnableCheckBox.Checked;
			}
			else if (_notchClick)
			{
				trackingCheckBox.Checked = !trackingCheckBox.Checked;
			}
			else if (_beforeClick || _afterClick)
			{
				beforeRadioButton.Checked = !beforeRadioButton.Checked;
			}
			else if (_moveNotchFrequency)
			{
				SelectCurrentEntry(_spectrumAnalyzer.PointToFrequency(_currentRectangleCenter));
			}
			else if (_moveNotchWidth)
			{
				SelectCurrentEntry(_spectrumAnalyzer.PointToFrequency(_currentRectangleCenter));
			}
			_mouseKeyDown = true;
		}

		private void spectrumAnalyzer_VisibleChanged(object sender, EventArgs e)
		{
			if (_spectrumAnalyzer.Visible)
			{
				Start();
			}
			else
			{
				Stop();
			}
			_spectrumAnalyzer.ResetSpectrum();
		}

		private void spectrumAnalyzer_FrequencyChanged(object sender, FrequencyEventArgs e)
		{
			e.Cancel = true;
		}

		private void spectrumAnalyzer_CenterFrequencyChanged(object sender, FrequencyEventArgs e)
		{
			e.Cancel = true;
			if (_moveFrequency)
			{
				_control.Frequency = e.Frequency + GetShift();
			}
		}

		private void ConfigureSpectrumAnalyzer()
		{
			_sampleRate = _ifProcessor.SampleRate;
			if (_control.DetectorType != DetectorType.LSB && _control.DetectorType != DetectorType.USB)
			{
				_spectrumAnalyzer.SpectrumWidth = (int)Math.Min(_sampleRate, (double)_control.FilterBandwidth * 1.5);
			}
			else
			{
				_spectrumAnalyzer.SpectrumWidth = (int)Math.Min(_sampleRate / 2.0, (double)_control.FilterBandwidth * 1.5);
			}
			long num = _control.Frequency - GetShift();
			_spectrumAnalyzer.Frequency = num;
			_spectrumAnalyzer.CenterFrequency = num;
			_spectrumAnalyzer.DisplayOffset = (int)_control.FFTOffset;
			if (_sampleRate > 0.0)
			{
				_spectrumAnalyzer.DisplayRange = (int)Math.Ceiling(((double)_control.FFTRange + 6.02 * Math.Log((double)_control.RFBandwidth / _sampleRate) / Math.Log(4.0)) * 0.1) * 10;
			}
			_spectrumAnalyzer.StepSize = _control.StepSize;
			_spectrumAnalyzer.Attack = _control.SAttack;
			_spectrumAnalyzer.Decay = _control.SDecay;
		}

		private unsafe void fftDisplayTimer_Tick(object sender, EventArgs e)
		{
			if (_control.IsPlaying && _fftSpectrumAvailable)
			{
				double num = (double)_spectrumAnalyzer.SpectrumWidth / _sampleRate;
				int num2 = Math.Min(_fftBins, (int)((double)_fftBins * num));
				float* powerSpectrum = _fftSpectrumPtr + (_fftBins - num2) / 2;
				if (_control.DetectorType == DetectorType.USB)
				{
					powerSpectrum = _fftSpectrumPtr + _fftBins / 2;
				}
				if (_control.DetectorType == DetectorType.LSB)
				{
					powerSpectrum = _fftSpectrumPtr + _fftBins / 2 - num2;
				}
				_spectrumAnalyzer.Render(powerSpectrum, num2);
				_fftSpectrumAvailable = false;
			}
		}

		private unsafe void InitFFTBuffers()
		{
			_fftBuffer = UnsafeBuffer.Create(_fftBins, sizeof(Complex));
			_fftSpectrum = UnsafeBuffer.Create(_fftBins, 4);
			_gainsBuffer = UnsafeBuffer.Create(_fftBins, 4);
			_fftPtr = (Complex*)(void*)_fftBuffer;
			_fftSpectrumPtr = (float*)(void*)_fftSpectrum;
			_gainsBufferPtr = (float*)(void*)_gainsBuffer;
		}

		private void Start()
		{
			_fftThreadRunning = true;
			if (_fftThread == null)
			{
				_fftThread = new Thread(ProcessFFT);
				_fftThread.Name = "IF Processor Thread";
				_fftThread.Start();
			}
			_fftDisplayTimer.Enabled = true;
		}

		public void Stop()
		{
			_fftDisplayTimer.Enabled = false;
			_fftThreadRunning = false;
			if (_fftThread != null)
			{
				_fftEvent.Set();
				_fftThread.Join();
				_fftThread = null;
			}
		}

		private unsafe void ProcessFFT(object parameter)
		{
			while (_fftThreadRunning)
			{
				_fftEvent.WaitOne();
				Fourier.SpectrumPower(_fftPtr, _fftSpectrumPtr, _fftBins, _correction);
				InverseSpectrum(_fftSpectrumPtr, _fftBins);
				_fftSpectrumAvailable = true;
			}
		}

		private unsafe void InverseSpectrum(float* buffer, int length)
		{
			int num = length / 4;
			int num2 = length / 2;
			for (int i = 0; i < num; i++)
			{
				float num3 = buffer[i];
				buffer[i] = buffer[num2 + i + 1];
				buffer[num2 + i + 1] = num3;
				num3 = buffer[num2 - i];
				buffer[num2 - i] = buffer[length - i - 1];
				buffer[length - i - 1] = num3;
			}
		}

		private unsafe void _ifProcessor_FFTReady(Complex* buffer, int length)
		{
			if (_sampleRate != _ifProcessor.SampleRate)
			{
				_needReinit = true;
			}
			if (!_fftSpectrumAvailable)
			{
				Utils.Memcpy(_fftPtr, buffer, length * sizeof(Complex));
				_fftEvent.Set();
			}
		}

		private void DisplayEntriesUpdate()
		{
			notchFilterEntryBindingSource.Clear();
			_displayedEntries.Clear();
			foreach (NotchFilterEntry entry in _entries)
			{
				_displayedEntries.Add(entry);
			}
			notchesDataGridView.ClearSelection();
		}

		private void notchesDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			if (notchesDataGridView.Columns[e.ColumnIndex].DataPropertyName == "Frequency" && e.Value != null)
			{
				long frequency = (long)e.Value;
				e.Value = GetFrequencyDisplay(frequency);
				e.FormattingApplied = true;
			}
		}

		private void notchesDataGridView_SelectionChanged(object sender, EventArgs e)
		{
			if (!_deleteCurrentNotches)
			{
				bool flag = notchesDataGridView.SelectedRows.Count > 0;
				delButton.Enabled = flag;
				attenuatNumericUpDown.Enabled = flag;
				widthNumericUpDown.Enabled = flag;
				if (flag)
				{
					UpdateEditedValue();
				}
			}
		}

		private void UpdateEditedValue()
		{
			NotchFilterEntry notchFilterEntry = (NotchFilterEntry)notchFilterEntryBindingSource.Current;
			widthNumericUpDown.Value = notchFilterEntry.FilterWidth;
			attenuatNumericUpDown.Value = notchFilterEntry.Attenuation;
		}

		private void UpdateAsymmetricFilter()
		{
			_startFilterShift = -_control.FilterBandwidth / 2;
			_endFilterShift = _control.FilterBandwidth / 2;
			if (_control.DetectorType == DetectorType.LSB)
			{
				_startFilterShift = -_control.FilterBandwidth;
				_endFilterShift = 0;
			}
			else if (_control.DetectorType == DetectorType.USB)
			{
				_startFilterShift = 0;
				_endFilterShift = _control.FilterBandwidth;
			}
		}

		private void IfProcessorEnabled()
		{
			_ifProcessor.Enabled = (_control.IsPlaying && (trackingCheckBox.Checked || filterEnableCheckBox.Checked || ifSpectrumCheckBox.Checked));
			_ifProcessor.FilterEnable = (trackingCheckBox.Checked || filterEnableCheckBox.Checked);
			addButton.Enabled = (trackingCheckBox.Checked && _control.IsPlaying);
		}

		private void SelectCurrentEntry(long frequency)
		{
			foreach (NotchFilterEntry entry in _entries)
			{
				if (Math.Abs(entry.Frequency - frequency) <= entry.FilterWidth)
				{
					int num = notchFilterEntryBindingSource.IndexOf(entry);
					notchFilterEntryBindingSource.Position = num;
					if (!notchesDataGridView.Rows[num].Displayed)
					{
						notchesDataGridView.FirstDisplayedScrollingRowIndex = num;
					}
					return;
				}
			}
			notchesDataGridView.ClearSelection();
		}

		private void widthNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			NotchFilterEntry notchFilterEntry = (NotchFilterEntry)notchFilterEntryBindingSource.Current;
			if (notchFilterEntry != null)
			{
				notchFilterEntry.FilterWidth = (int)widthNumericUpDown.Value;
				_displayedEntries.ResetItem(notchFilterEntryBindingSource.IndexOf(notchFilterEntry));
				UpdateNotchesFrequencies();
				UpdateVisibleFrequencies();
			}
		}

		private void attenuatNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			NotchFilterEntry notchFilterEntry = (NotchFilterEntry)notchFilterEntryBindingSource.Current;
			if (notchFilterEntry != null)
			{
				notchFilterEntry.Attenuation = (int)attenuatNumericUpDown.Value;
				_displayedEntries.ResetItem(notchFilterEntryBindingSource.IndexOf(notchFilterEntry));
				UpdateNotchesFrequencies();
			}
		}

		private unsafe void CreateFilter()
		{
			bool @checked = filterEnableCheckBox.Checked;
			int num = FrequencyToBins(_control.Frequency + _startFilterShift);
			int num2 = FrequencyToBins(_control.Frequency + _endFilterShift);
			for (int i = 0; i < _gainsBuffer.Length; i++)
			{
				if ((i < num || i > num2) & @checked)
				{
					_gainsBufferPtr[i] = 0f;
				}
				else
				{
					_gainsBufferPtr[i] = 1f;
				}
			}
			if (trackingCheckBox.Checked && _notchActiveFrequencies.Count > 0)
			{
				foreach (NotchFilterEntry notchActiveFrequency in _notchActiveFrequencies)
				{
					int num3 = FrequencyToBins(notchActiveFrequency.Frequency - notchActiveFrequency.FilterWidth / 2);
					int num4 = FrequencyToBins(notchActiveFrequency.Frequency + notchActiveFrequency.FilterWidth / 2);
					float num5 = (float)Math.Pow(10.0, (double)notchActiveFrequency.Attenuation * 0.05);
					for (int j = num3; j <= num4; j++)
					{
						_gainsBufferPtr[j] *= num5;
					}
				}
			}
			InverseSpectrum(_gainsBufferPtr, _gainsBuffer.Length);
			_ifProcessor.UpdateFilter(_gainsBufferPtr);
		}

		public int FrequencyToBins(long frequency)
		{
			frequency -= _control.Frequency;
			int num = 0;
			num = ((frequency > 0) ? ((int)((double)(_fftBins / 2) + (double)_fftBins / _sampleRate * (double)frequency)) : ((int)((double)(_fftBins / 2) - (double)_fftBins / _sampleRate * (double)(-frequency))));
			if (num < 0)
			{
				num = 0;
			}
			if (num > _fftBins)
			{
				num = _fftBins;
			}
			return num;
		}

		private void UpdateNotchesFrequencies()
		{
			_notchActiveFrequencies.Clear();
			if (trackingCheckBox.Checked)
			{
				foreach (NotchFilterEntry entry in _entries)
				{
					if (IsActive(entry.Frequency))
					{
						NotchFilterEntry notchFilterEntry = new NotchFilterEntry();
						notchFilterEntry.Frequency = entry.Frequency;
						notchFilterEntry.Attenuation = entry.Attenuation;
						notchFilterEntry.FilterWidth = entry.FilterWidth;
						_notchActiveFrequencies.Add(notchFilterEntry);
					}
				}
			}
			CreateFilter();
		}

		private int GetShift()
		{
			if (_control.DetectorType == DetectorType.LSB)
			{
				return _spectrumAnalyzer.SpectrumWidth / 2;
			}
			if (_control.DetectorType == DetectorType.USB)
			{
				return -_spectrumAnalyzer.SpectrumWidth / 2;
			}
			return 0;
		}

		private bool IsActive(long frequency)
		{
			long num = _control.Frequency + _control.FilterBandwidth / 2;
			long num2 = _control.Frequency - _control.FilterBandwidth / 2;
			if (_control.DetectorType == DetectorType.LSB)
			{
				num = _control.Frequency;
				num2 = _control.Frequency - _control.FilterBandwidth;
			}
			else if (_control.DetectorType == DetectorType.USB)
			{
				num2 = _control.Frequency;
				num = _control.Frequency + _control.FilterBandwidth;
			}
			if (frequency <= num)
			{
				return frequency >= num2;
			}
			return false;
		}

		private void addButton_Click(object sender, EventArgs e)
		{
			AddNewNotch(_control.Frequency);
		}

		private void AddNewNotch(long frequency)
		{
			NotchFilterEntry notchFilterEntry = new NotchFilterEntry();
			notchFilterEntry.Attenuation = (int)attenuatNumericUpDown.Value;
			notchFilterEntry.FilterWidth = (int)widthNumericUpDown.Value;
			notchFilterEntry.Frequency = frequency;
			_entries.Add(notchFilterEntry);
			_displayedEntries.Add(notchFilterEntry);
			UpdateNotchesFrequencies();
			SelectCurrentEntry(frequency);
			UpdateVisibleFrequencies();
		}

		private void EditCurrentEntryFrequency(long frequency)
		{
			NotchFilterEntry notchFilterEntry = (NotchFilterEntry)notchFilterEntryBindingSource.Current;
			if (notchFilterEntry != null)
			{
				notchFilterEntry.Frequency = frequency;
				_displayedEntries.ResetItem(notchFilterEntryBindingSource.IndexOf(notchFilterEntry));
				UpdateNotchesFrequencies();
				UpdateVisibleFrequencies();
			}
		}

		private void delButton_Click(object sender, EventArgs e)
		{
			DeleteCurrentEntry();
		}

		private void DeleteCurrentEntry()
		{
			NotchFilterEntry notchFilterEntry = (NotchFilterEntry)notchFilterEntryBindingSource.Current;
			if (notchFilterEntry != null)
			{
				_entries.Remove(notchFilterEntry);
				_deleteCurrentNotches = true;
				_displayedEntries.Remove(notchFilterEntry);
				_deleteCurrentNotches = false;
				notchesDataGridView.ClearSelection();
			}
			UpdateNotchesFrequencies();
			UpdateVisibleFrequencies();
		}

		private void trackingCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			IfProcessorEnabled();
			UpdateNotchesFrequencies();
			UpdateVisibleFrequencies();
			notchesDataGridView.ClearSelection();
		}

		private void displayTimer_Tick(object sender, EventArgs e)
		{
			if (_needReinit)
			{
				_needReinit = false;
				ConfigureSpectrumAnalyzer();
				UpdateNotchesFrequencies();
				UpdateVisibleFrequencies();
			}
		}

		private void filterEnableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			IfProcessorEnabled();
			UpdateAsymmetricFilter();
			CreateFilter();
		}

		private void beforeRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			_ifProcessor.ShowSpectrumAfterProcess = !beforeRadioButton.Checked;
			afterRadioButton.Checked = !beforeRadioButton.Checked;
		}

		private void ifSpectrumCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_spectrumAnalyzer.Visible = ifSpectrumCheckBox.Checked;
			IfProcessorEnabled();
		}

		private void showNotchOnMainSpectrumCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_notchVisibleFrequencies.Clear();
			UpdateVisibleFrequencies();
		}

		private void spectrumCustomPaint(object sender, CustomPaintEventArgs e)
		{
			SpectrumAnalyzer spectrumAnalyzer = (SpectrumAnalyzer)sender;
			using (SolidBrush brush2 = new SolidBrush(Color.FromArgb(100, Color.Silver)))
			{
				using (SolidBrush brush6 = new SolidBrush(Color.FromArgb(150, Color.Silver)))
				{
					using (SolidBrush brush5 = new SolidBrush(Color.FromArgb(200, Color.White)))
					{
						using (SolidBrush brush7 = new SolidBrush(Color.Black))
						{
							using (SolidBrush brush4 = new SolidBrush(Color.FromArgb(200, Color.White)))
							{
								using (SolidBrush brush3 = new SolidBrush(Color.FromArgb(100, Color.Silver)))
								{
									using (SolidBrush brush = new SolidBrush(Color.FromArgb(150, Color.Red)))
									{
										using (Font font = new Font("Lucida Console", 9f))
										{
											using (StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic))
											{
												stringFormat.Alignment = StringAlignment.Near;
												_notchActiveFrequenciesPoint.Clear();
												using (Pen pen = new Pen(brush, 3f))
												{
													float num = spectrumAnalyzer.FrequencyToPoint(_control.Frequency);
													float y = 30f;
													float y2 = (float)spectrumAnalyzer.Height - 30f;
													e.Graphics.DrawLine(pen, num, y, num, y2);
												}
												RectangleF rectangleF = default(RectangleF);
												if (_notchActiveFrequencies != null && _notchActiveFrequencies.Count > 0)
												{
													foreach (NotchFilterEntry notchActiveFrequency in _notchActiveFrequencies)
													{
														float val = spectrumAnalyzer.FrequencyToPoint(notchActiveFrequency.Frequency - notchActiveFrequency.FilterWidth / 2);
														float val2 = spectrumAnalyzer.FrequencyToPoint(notchActiveFrequency.Frequency + notchActiveFrequency.FilterWidth / 2);
														val = Math.Max(val, 30f);
														val2 = Math.Min(val2, spectrumAnalyzer.Width - 30);
														rectangleF.X = val;
														rectangleF.Y = 30f;
														rectangleF.Width = Math.Max(3f, val2 - val);
														rectangleF.Height = spectrumAnalyzer.Height - 60;
														_notchActiveFrequenciesPoint.Add(rectangleF);
														e.Graphics.FillRectangle(brush2, rectangleF);
													}
												}
												if (filterEnableCheckBox.Checked)
												{
													float val3 = spectrumAnalyzer.FrequencyToPoint(_control.Frequency + _startFilterShift);
													val3 = Math.Max(val3, 30f);
													rectangleF.X = 30f;
													rectangleF.Y = 30f;
													rectangleF.Width = val3 - 30f;
													rectangleF.Height = spectrumAnalyzer.Height - 60;
													_asimetricFilterLowRectangle = new RectangleF(rectangleF.X + rectangleF.Width - 2f, rectangleF.Y, 4f, rectangleF.Height);
													e.Graphics.FillRectangle(brush3, rectangleF);
												}
												if (filterEnableCheckBox.Checked)
												{
													float val4 = spectrumAnalyzer.FrequencyToPoint(_control.Frequency + _endFilterShift);
													val4 = (rectangleF.X = Math.Min(val4, spectrumAnalyzer.Width - 30));
													rectangleF.Y = 30f;
													rectangleF.Width = (float)(spectrumAnalyzer.Width - 30) - val4;
													rectangleF.Height = spectrumAnalyzer.Height - 60;
													_asimetricFilterHighRectangle = new RectangleF(rectangleF.X - 2f, rectangleF.Y, 4f, rectangleF.Height);
													e.Graphics.FillRectangle(brush3, rectangleF);
												}
												if (_showCursor)
												{
													using (Pen pen2 = new Pen(brush, 1f))
													{
														float num3 = _cursorPosition.X;
														float y3 = 30f;
														float x = num3;
														float y4 = (float)spectrumAnalyzer.Height - 30f;
														e.Graphics.DrawLine(pen2, num3, y3, x, y4);
													}
												}
												if (_cursorText != "")
												{
													Point p = default(Point);
													string cursorText = _cursorText;
													e.Graphics.MeasureString(cursorText, font, spectrumAnalyzer.Width, stringFormat);
													p.X = _cursorPosition.X;
													p.Y = _cursorPosition.Y + 10;
													e.Graphics.DrawString(cursorText, font, brush4, p, stringFormat);
												}
												string text = "IF Spectrum:";
												Point p2 = default(Point);
												SizeF sizeF = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												p2.X = 30;
												p2.Y = 10;
												e.Graphics.DrawString(text, font, brush5, p2, stringFormat);
												text = "before";
												int num6 = p2.X = (p2.X = p2.X + (int)sizeF.Width + 5);
												sizeF = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_beforeEnableRectangle = new Rectangle(p2.X, p2.Y, (int)sizeF.Width, (int)sizeF.Height);
												if (beforeRadioButton.Checked)
												{
													e.Graphics.FillRectangle(brush5, p2.X - 1, p2.Y - 2, sizeF.Width + 2f, sizeF.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF.Width + 2f, sizeF.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush7, p2, stringFormat);
												text = "after";
												num6 = (p2.X = (p2.X = p2.X + (int)sizeF.Width + 5));
												sizeF = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_afterEnableRectangle = new Rectangle(p2.X, p2.Y, (int)sizeF.Width, (int)sizeF.Height);
												if (afterRadioButton.Checked)
												{
													e.Graphics.FillRectangle(brush5, p2.X - 1, p2.Y - 2, sizeF.Width + 2f, sizeF.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF.Width + 2f, sizeF.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush7, p2, stringFormat);
												text = "Notch";
												num6 = (p2.X = (p2.X = p2.X + (int)sizeF.Width + 10));
												sizeF = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_notchEnableRectangle = new Rectangle(p2.X, p2.Y, (int)sizeF.Width, (int)sizeF.Height);
												if (trackingCheckBox.Checked)
												{
													e.Graphics.FillRectangle(brush5, p2.X - 1, p2.Y - 2, sizeF.Width + 2f, sizeF.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF.Width + 2f, sizeF.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush7, p2, stringFormat);
												text = "Filter";
												p2.X = p2.X + (int)sizeF.Width + 10;
												sizeF = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_filterEnableRectangle = new Rectangle(p2.X, p2.Y, (int)sizeF.Width, (int)sizeF.Height);
												if (filterEnableCheckBox.Checked)
												{
													e.Graphics.FillRectangle(brush5, p2.X - 1, p2.Y - 2, sizeF.Width + 2f, sizeF.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF.Width + 2f, sizeF.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush7, p2, stringFormat);
												if (trackingCheckBox.Checked)
												{
													if (_showCursor)
													{
														text = "Click to add notch";
														num6 = (p2.X = (p2.X = p2.X + (int)sizeF.Width + 20));
														sizeF = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
														e.Graphics.DrawString(text, font, brush6, p2, stringFormat);
													}
													else if (_moveNotchFrequency)
													{
														text = "Click to remove notch, mouse wheel to attenuation";
														num6 = (p2.X = (p2.X = p2.X + (int)sizeF.Width + 20));
														sizeF = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
														e.Graphics.DrawString(text, font, brush6, p2, stringFormat);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		private void UpdateVisibleFrequencies()
		{
			if (_entries == null || _entries.Count == 0 || !trackingCheckBox.Checked || !showNotchOnMainSpectrumCheckBox.Checked)
			{
				if (_notchVisibleFrequencies.Count > 0)
				{
					_notchVisibleFrequencies.Clear();
				}
			}
			else
			{
				_notchVisibleFrequencies.Clear();
				long num = _control.CenterFrequency + _control.RFDisplayBandwidth / 2;
				long num2 = _control.CenterFrequency - _control.RFDisplayBandwidth / 2;
				foreach (NotchFilterEntry entry in _entries)
				{
					long num3 = entry.Frequency + entry.FilterWidth / 2;
					long num4 = entry.Frequency - entry.FilterWidth / 2;
					if ((num3 > num2 && num3 < num) || (num4 < num && num4 > num2))
					{
						_notchVisibleFrequencies.Add(entry);
					}
				}
			}
		}

		private void mainSpectrumCustomPaint(object sender, CustomPaintEventArgs e)
		{
			SpectrumAnalyzer spectrumAnalyzer = (SpectrumAnalyzer)sender;
			if (_notchVisibleFrequencies != null && _notchVisibleFrequencies.Count != 0)
			{
				using (SolidBrush brush = new SolidBrush(Color.FromArgb(150, Color.Red)))
				{
					RectangleF rect = default(RectangleF);
					foreach (NotchFilterEntry notchVisibleFrequency in _notchVisibleFrequencies)
					{
						float val = spectrumAnalyzer.FrequencyToPoint(notchVisibleFrequency.Frequency - notchVisibleFrequency.FilterWidth / 2);
						float val2 = spectrumAnalyzer.FrequencyToPoint(notchVisibleFrequency.Frequency + notchVisibleFrequency.FilterWidth / 2);
						val = Math.Max(val, 30f);
						val2 = Math.Min(val2, spectrumAnalyzer.Width - 30);
						rect.X = val;
						rect.Y = 30f;
						rect.Width = Math.Max(1f, val2 - val);
						rect.Height = spectrumAnalyzer.Height - 60;
						e.Graphics.FillRectangle(brush, rect);
					}
				}
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			widthNumericUpDown = new System.Windows.Forms.NumericUpDown();
			attenuatNumericUpDown = new System.Windows.Forms.NumericUpDown();
			label2 = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			groupBox2 = new System.Windows.Forms.GroupBox();
			showNotchOnMainSpectrumCheckBox = new System.Windows.Forms.CheckBox();
			notchesDataGridView = new System.Windows.Forms.DataGridView();
			frequencyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			attenuationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			filterWidthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			notchFilterEntryBindingSource = new System.Windows.Forms.BindingSource(components);
			delButton = new System.Windows.Forms.Button();
			addButton = new System.Windows.Forms.Button();
			trackingCheckBox = new System.Windows.Forms.CheckBox();
			displayTimer = new System.Windows.Forms.Timer(components);
			filterEnableCheckBox = new System.Windows.Forms.CheckBox();
			groupBox5 = new System.Windows.Forms.GroupBox();
			spectrumPositionComboBox = new System.Windows.Forms.ComboBox();
			label7 = new System.Windows.Forms.Label();
			afterRadioButton = new System.Windows.Forms.RadioButton();
			beforeRadioButton = new System.Windows.Forms.RadioButton();
			ifSpectrumCheckBox = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)widthNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)attenuatNumericUpDown).BeginInit();
			groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)notchesDataGridView).BeginInit();
			((System.ComponentModel.ISupportInitialize)notchFilterEntryBindingSource).BeginInit();
			groupBox5.SuspendLayout();
			SuspendLayout();
			widthNumericUpDown.Increment = new decimal(new int[4]
			{
				10,
				0,
				0,
				0
			});
			widthNumericUpDown.Location = new System.Drawing.Point(98, 159);
			widthNumericUpDown.Maximum = new decimal(new int[4]
			{
				100000,
				0,
				0,
				0
			});
			widthNumericUpDown.Name = "widthNumericUpDown";
			widthNumericUpDown.Size = new System.Drawing.Size(69, 20);
			widthNumericUpDown.TabIndex = 2;
			widthNumericUpDown.Value = new decimal(new int[4]
			{
				100,
				0,
				0,
				0
			});
			widthNumericUpDown.ValueChanged += new System.EventHandler(widthNumericUpDown_ValueChanged);
			attenuatNumericUpDown.Location = new System.Drawing.Point(98, 185);
			attenuatNumericUpDown.Minimum = new decimal(new int[4]
			{
				160,
				0,
				0,
				-2147483648
			});
			attenuatNumericUpDown.Name = "attenuatNumericUpDown";
			attenuatNumericUpDown.Size = new System.Drawing.Size(69, 20);
			attenuatNumericUpDown.TabIndex = 3;
			attenuatNumericUpDown.Value = new decimal(new int[4]
			{
				80,
				0,
				0,
				-2147483648
			});
			attenuatNumericUpDown.ValueChanged += new System.EventHandler(attenuatNumericUpDown_ValueChanged);
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(7, 161);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(51, 13);
			label2.TabIndex = 5;
			label2.Text = "Width Hz";
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(7, 187);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(77, 13);
			label3.TabIndex = 6;
			label3.Text = "Attenuation dB";
			groupBox2.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			groupBox2.Controls.Add(showNotchOnMainSpectrumCheckBox);
			groupBox2.Controls.Add(notchesDataGridView);
			groupBox2.Controls.Add(label3);
			groupBox2.Controls.Add(delButton);
			groupBox2.Controls.Add(addButton);
			groupBox2.Controls.Add(trackingCheckBox);
			groupBox2.Controls.Add(label2);
			groupBox2.Controls.Add(widthNumericUpDown);
			groupBox2.Controls.Add(attenuatNumericUpDown);
			groupBox2.Location = new System.Drawing.Point(3, 26);
			groupBox2.Name = "groupBox2";
			groupBox2.Size = new System.Drawing.Size(206, 239);
			groupBox2.TabIndex = 7;
			groupBox2.TabStop = false;
			groupBox2.Text = "groupBox2";
			showNotchOnMainSpectrumCheckBox.AutoSize = true;
			showNotchOnMainSpectrumCheckBox.Location = new System.Drawing.Point(6, 211);
			showNotchOnMainSpectrumCheckBox.Name = "showNotchOnMainSpectrumCheckBox";
			showNotchOnMainSpectrumCheckBox.Size = new System.Drawing.Size(139, 17);
			showNotchOnMainSpectrumCheckBox.TabIndex = 7;
			showNotchOnMainSpectrumCheckBox.Text = "Show on main spectrum";
			showNotchOnMainSpectrumCheckBox.UseVisualStyleBackColor = true;
			showNotchOnMainSpectrumCheckBox.CheckedChanged += new System.EventHandler(showNotchOnMainSpectrumCheckBox_CheckedChanged);
			notchesDataGridView.AllowUserToAddRows = false;
			notchesDataGridView.AllowUserToDeleteRows = false;
			notchesDataGridView.AllowUserToResizeColumns = false;
			notchesDataGridView.AllowUserToResizeRows = false;
			notchesDataGridView.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			notchesDataGridView.AutoGenerateColumns = false;
			notchesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			notchesDataGridView.Columns.AddRange(frequencyDataGridViewTextBoxColumn, attenuationDataGridViewTextBoxColumn, filterWidthDataGridViewTextBoxColumn);
			notchesDataGridView.DataSource = notchFilterEntryBindingSource;
			notchesDataGridView.Location = new System.Drawing.Point(6, 46);
			notchesDataGridView.MultiSelect = false;
			notchesDataGridView.Name = "notchesDataGridView";
			notchesDataGridView.ReadOnly = true;
			notchesDataGridView.RowHeadersVisible = false;
			notchesDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			notchesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			notchesDataGridView.ShowCellErrors = false;
			notchesDataGridView.ShowCellToolTips = false;
			notchesDataGridView.ShowEditingIcon = false;
			notchesDataGridView.ShowRowErrors = false;
			notchesDataGridView.Size = new System.Drawing.Size(194, 107);
			notchesDataGridView.TabIndex = 3;
			notchesDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(notchesDataGridView_CellFormatting);
			notchesDataGridView.SelectionChanged += new System.EventHandler(notchesDataGridView_SelectionChanged);
			frequencyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			frequencyDataGridViewTextBoxColumn.DataPropertyName = "Frequency";
			frequencyDataGridViewTextBoxColumn.HeaderText = "Frequency";
			frequencyDataGridViewTextBoxColumn.Name = "frequencyDataGridViewTextBoxColumn";
			frequencyDataGridViewTextBoxColumn.ReadOnly = true;
			attenuationDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			attenuationDataGridViewTextBoxColumn.DataPropertyName = "Attenuation";
			attenuationDataGridViewTextBoxColumn.FillWeight = 50f;
			attenuationDataGridViewTextBoxColumn.HeaderText = "Atten";
			attenuationDataGridViewTextBoxColumn.Name = "attenuationDataGridViewTextBoxColumn";
			attenuationDataGridViewTextBoxColumn.ReadOnly = true;
			attenuationDataGridViewTextBoxColumn.Width = 50;
			filterWidthDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			filterWidthDataGridViewTextBoxColumn.DataPropertyName = "FilterWidth";
			filterWidthDataGridViewTextBoxColumn.FillWeight = 50f;
			filterWidthDataGridViewTextBoxColumn.HeaderText = "Width";
			filterWidthDataGridViewTextBoxColumn.Name = "filterWidthDataGridViewTextBoxColumn";
			filterWidthDataGridViewTextBoxColumn.ReadOnly = true;
			filterWidthDataGridViewTextBoxColumn.Width = 50;
			notchFilterEntryBindingSource.DataSource = typeof(SDRSharp.DigitalIfProcessor.NotchFilterEntry);
			delButton.Enabled = false;
			delButton.Location = new System.Drawing.Point(89, 19);
			delButton.Name = "delButton";
			delButton.Size = new System.Drawing.Size(77, 21);
			delButton.TabIndex = 2;
			delButton.Text = "Delete";
			delButton.UseVisualStyleBackColor = true;
			delButton.Click += new System.EventHandler(delButton_Click);
			addButton.Location = new System.Drawing.Point(6, 19);
			addButton.Name = "addButton";
			addButton.Size = new System.Drawing.Size(77, 21);
			addButton.TabIndex = 1;
			addButton.Text = "Add new";
			addButton.UseVisualStyleBackColor = true;
			addButton.Click += new System.EventHandler(addButton_Click);
			trackingCheckBox.AutoSize = true;
			trackingCheckBox.Location = new System.Drawing.Point(6, 0);
			trackingCheckBox.Name = "trackingCheckBox";
			trackingCheckBox.Size = new System.Drawing.Size(96, 17);
			trackingCheckBox.TabIndex = 0;
			trackingCheckBox.Text = "Notch tracking";
			trackingCheckBox.UseVisualStyleBackColor = true;
			trackingCheckBox.CheckedChanged += new System.EventHandler(trackingCheckBox_CheckedChanged);
			displayTimer.Enabled = true;
			displayTimer.Tick += new System.EventHandler(displayTimer_Tick);
			filterEnableCheckBox.AutoSize = true;
			filterEnableCheckBox.Location = new System.Drawing.Point(10, 3);
			filterEnableCheckBox.Name = "filterEnableCheckBox";
			filterEnableCheckBox.Size = new System.Drawing.Size(101, 17);
			filterEnableCheckBox.TabIndex = 1;
			filterEnableCheckBox.Text = "Asymmetric filter";
			filterEnableCheckBox.UseVisualStyleBackColor = true;
			filterEnableCheckBox.CheckedChanged += new System.EventHandler(filterEnableCheckBox_CheckedChanged);
			groupBox5.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			groupBox5.Controls.Add(spectrumPositionComboBox);
			groupBox5.Controls.Add(label7);
			groupBox5.Controls.Add(afterRadioButton);
			groupBox5.Controls.Add(beforeRadioButton);
			groupBox5.Controls.Add(ifSpectrumCheckBox);
			groupBox5.Location = new System.Drawing.Point(4, 271);
			groupBox5.Name = "groupBox5";
			groupBox5.Size = new System.Drawing.Size(205, 72);
			groupBox5.TabIndex = 8;
			groupBox5.TabStop = false;
			groupBox5.Text = "groupBox5";
			spectrumPositionComboBox.FormattingEnabled = true;
			spectrumPositionComboBox.Items.AddRange(new object[4]
			{
				"Up",
				"Bottom",
				"Left",
				"Right"
			});
			spectrumPositionComboBox.Location = new System.Drawing.Point(67, 42);
			spectrumPositionComboBox.Name = "spectrumPositionComboBox";
			spectrumPositionComboBox.Size = new System.Drawing.Size(97, 21);
			spectrumPositionComboBox.TabIndex = 5;
			label7.AutoSize = true;
			label7.Location = new System.Drawing.Point(6, 45);
			label7.Name = "label7";
			label7.Size = new System.Drawing.Size(44, 13);
			label7.TabIndex = 4;
			label7.Text = "Position";
			afterRadioButton.AutoSize = true;
			afterRadioButton.Location = new System.Drawing.Point(67, 19);
			afterRadioButton.Name = "afterRadioButton";
			afterRadioButton.Size = new System.Drawing.Size(100, 17);
			afterRadioButton.TabIndex = 2;
			afterRadioButton.TabStop = true;
			afterRadioButton.Text = "after processing";
			afterRadioButton.UseVisualStyleBackColor = true;
			beforeRadioButton.AutoSize = true;
			beforeRadioButton.Location = new System.Drawing.Point(6, 19);
			beforeRadioButton.Name = "beforeRadioButton";
			beforeRadioButton.Size = new System.Drawing.Size(55, 17);
			beforeRadioButton.TabIndex = 1;
			beforeRadioButton.TabStop = true;
			beforeRadioButton.Text = "before";
			beforeRadioButton.UseVisualStyleBackColor = true;
			beforeRadioButton.CheckedChanged += new System.EventHandler(beforeRadioButton_CheckedChanged);
			ifSpectrumCheckBox.AutoSize = true;
			ifSpectrumCheckBox.Location = new System.Drawing.Point(6, 0);
			ifSpectrumCheckBox.Name = "ifSpectrumCheckBox";
			ifSpectrumCheckBox.Size = new System.Drawing.Size(83, 17);
			ifSpectrumCheckBox.TabIndex = 0;
			ifSpectrumCheckBox.Text = "IF Spectrum";
			ifSpectrumCheckBox.UseVisualStyleBackColor = true;
			ifSpectrumCheckBox.CheckedChanged += new System.EventHandler(ifSpectrumCheckBox_CheckedChanged);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(filterEnableCheckBox);
			base.Controls.Add(groupBox5);
			base.Controls.Add(groupBox2);
			base.Name = "DigitalIfProcessorPanel";
			base.Size = new System.Drawing.Size(210, 368);
			((System.ComponentModel.ISupportInitialize)widthNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)attenuatNumericUpDown).EndInit();
			groupBox2.ResumeLayout(performLayout: false);
			groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)notchesDataGridView).EndInit();
			((System.ComponentModel.ISupportInitialize)notchFilterEntryBindingSource).EndInit();
			groupBox5.ResumeLayout(performLayout: false);
			groupBox5.PerformLayout();
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

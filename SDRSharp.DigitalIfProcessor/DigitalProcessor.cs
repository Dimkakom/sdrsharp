using SDRSharp.Radio;

namespace SDRSharp.DigitalIfProcessor
{
	public class DigitalProcessor : OverlapAddProcessor
	{
		public unsafe delegate void FFTReadyDelegate(Complex* buffer, int length);

		private const int FftSize = 8192;

		private readonly UnsafeBuffer _gainBuffer;

		private unsafe readonly float* _gainPtr;

		public bool ShowSpectrumAfterProcess
		{
			get;
			set;
		}

		public bool FilterEnable
		{
			get;
			set;
		}

		public event FFTReadyDelegate FFTReady;

		public unsafe DigitalProcessor(int fftSize = 8192)
			: base(fftSize)
		{
			_gainBuffer = UnsafeBuffer.Create(fftSize, 4);
			_gainPtr = (float*)(void*)_gainBuffer;
		}

		public unsafe void BuildFilter(float* gainBuffer)
		{
			Utils.Memcpy(_gainPtr, gainBuffer, 32768);
		}

		protected unsafe override void ProcessFft(Complex* buffer, int length)
		{
			if (this.FFTReady != null && !ShowSpectrumAfterProcess)
			{
				this.FFTReady(buffer, length);
			}
			if (FilterEnable)
			{
				for (int i = 0; i < length; i++)
				{
					Complex* reference = buffer + i;
					*reference *= _gainPtr[i];
				}
			}
			if (this.FFTReady != null && ShowSpectrumAfterProcess)
			{
				this.FFTReady(buffer, length);
			}
		}
	}
}

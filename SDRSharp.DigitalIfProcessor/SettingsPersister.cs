using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SDRSharp.DigitalIfProcessor
{
	public class SettingsPersister
	{
		private const string Filename = "notches.xml";

		private readonly string _settingsFolder;

		public SettingsPersister()
		{
			_settingsFolder = Path.GetDirectoryName(Application.ExecutablePath);
		}

		public List<NotchFilterEntry> ReadStoredFrequencies()
		{
			List<NotchFilterEntry> list = ReadObject<List<NotchFilterEntry>>("notches.xml");
			if (list != null)
			{
				list.Sort((NotchFilterEntry e1, NotchFilterEntry e2) => e1.Frequency.CompareTo(e2.Frequency));
				return list;
			}
			return new List<NotchFilterEntry>();
		}

		public void PersistStoredFrequencies(List<NotchFilterEntry> entries)
		{
			WriteObject(entries, "notches.xml");
		}

		private T ReadObject<T>(string fileName)
		{
			string path = Path.Combine(_settingsFolder, fileName);
			if (File.Exists(path))
			{
				using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
				{
					return (T)new XmlSerializer(typeof(T)).Deserialize(stream);
				}
			}
			return default(T);
		}

		private void WriteObject<T>(T obj, string fileName)
		{
			using (FileStream stream = new FileStream(Path.Combine(_settingsFolder, fileName), FileMode.Create))
			{
				new XmlSerializer(obj.GetType()).Serialize(stream, obj);
			}
		}
	}
}

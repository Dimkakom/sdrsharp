using SDRSharp.Radio;
using System;

namespace SDRSharp.TimeShift
{
	public class TimeMachine
	{
		private UnsafeBuffer[] _timeBackBuffer;

		private unsafe short*[] _timeBackPtr;

		private int _timeBackBufferSize;

		private int _timeBack;

		private int _timeBackInBufferIndex;

		private int _timeBackOutBufferIndex;

		private int _timeBackBufferLength;

		private bool _timeBackBufferNeedClear;

		private bool _bufferIsFull;

		public int TimeBack
		{
			get
			{
				return _timeBack;
			}
			set
			{
				if (_timeBack != value)
				{
					_timeBack = value;
				}
			}
		}

		public int TimeBackBufferSize
		{
			get
			{
				return _timeBackBufferSize;
			}
			set
			{
				if (_timeBackBufferSize != value)
				{
					_timeBackBufferSize = value;
				}
			}
		}

		public bool BufferIsFull => _bufferIsFull;

		public int BufferFill
		{
			get
			{
				if (_bufferIsFull)
				{
					return _timeBackBufferSize;
				}
				return _timeBackInBufferIndex;
			}
		}

		public bool BufferClean
		{
			set
			{
				_timeBackBufferNeedClear = value;
			}
		}

		public unsafe void Process(Complex* iqBuffer, int length)
		{
			if (_timeBackBufferSize == 0)
			{
				return;
			}
			if ((double)_timeBackBufferSize * 2.0 * 2.0 * (double)length >= 1610612736.0)
			{
				_bufferIsFull = false;
				_timeBackInBufferIndex = 0;
				return;
			}
			if (_timeBackBuffer == null || _timeBackBufferLength != length || _timeBackBufferNeedClear)
			{
				_timeBackBufferNeedClear = false;
				_bufferIsFull = false;
				FreeTimeBackBuffers();
				CreateTimeBackBuffers(length);
			}
			CopyToBuffers(_timeBackPtr[_timeBackInBufferIndex], iqBuffer, length);
			if (_timeBack >= _timeBackBuffer.Length)
			{
				_timeBack = _timeBackBuffer.Length - 1;
			}
			_timeBackOutBufferIndex = _timeBackInBufferIndex - _timeBack;
			if (_timeBackOutBufferIndex < 0)
			{
				_timeBackOutBufferIndex = _timeBackBuffer.Length + _timeBackOutBufferIndex;
			}
			CopyFromBuffers(iqBuffer, _timeBackPtr[_timeBackOutBufferIndex], length);
			_timeBackInBufferIndex++;
			if (_timeBackInBufferIndex == _timeBackBuffer.Length)
			{
				_bufferIsFull = true;
				_timeBackInBufferIndex = 0;
			}
		}

		private unsafe void CopyToBuffers(short* dest, Complex* source, int length)
		{
			int num = 0;
			for (int i = 0; i < length; i++)
			{
				dest[num] = (short)(source[i].Real * 65535f);
				num++;
				dest[num] = (short)(source[i].Imag * 65535f);
				num++;
			}
		}

		private unsafe void CopyFromBuffers(Complex* dest, short* source, int length)
		{
			int num = 0;
			for (int i = 0; i < length; i++)
			{
				dest[i].Real = (float)source[num] / 65535f;
				num++;
				dest[i].Imag = (float)source[num] / 65535f;
				num++;
			}
		}

		private unsafe void CreateTimeBackBuffers(int length)
		{
			GC.Collect();
			_timeBackBuffer = new UnsafeBuffer[_timeBackBufferSize];
			_timeBackPtr = new short*[_timeBackBufferSize];
			int length2 = length * 2;
			for (int i = 0; i < _timeBackBufferSize; i++)
			{
				_timeBackBuffer[i] = UnsafeBuffer.Create(length2, 2);
				_timeBackPtr[i] = (short*)(void*)_timeBackBuffer[i];
			}
			_timeBackInBufferIndex = 0;
			_timeBackBufferLength = length;
		}

		public void FreeTimeBackBuffers()
		{
			if (_timeBackBuffer == null)
			{
				return;
			}
			for (int i = 0; i < _timeBackBuffer.GetLength(0); i++)
			{
				if (_timeBackBuffer[i] != null)
				{
					_timeBackBuffer[i].Dispose();
					_timeBackBuffer[i] = null;
					unsafe
					{
						_timeBackPtr[i] = null;
					}
				}
			}
			_timeBackBufferLength = 0;
			_timeBackInBufferIndex = 0;
			_timeBackBuffer = null;
		}
	}
}

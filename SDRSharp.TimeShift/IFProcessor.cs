using SDRSharp.Radio;

namespace SDRSharp.TimeShift
{
	public class IFProcessor : IIQProcessor, IStreamProcessor, IBaseProcessor
	{
		public unsafe delegate void IQReadyDelegate(Complex* buffer, int length);

		private double _sampleRate;

		private bool _enabled;

		private int _length;

		private int _bufferSize;

		private bool _needClean;

		private TimeMachine _timeMachine;

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				if (value != _sampleRate)
				{
					_sampleRate = value;
					_needClean = true;
				}
			}
		}

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public int TimeBack
		{
			get;
			set;
		}

		public int BufferSizeInSecond
		{
			set
			{
				_bufferSize = value;
				_needClean = true;
			}
		}

		public bool BufferClean
		{
			set
			{
				_needClean = value;
			}
		}

		public bool BufferIsFull
		{
			get
			{
				if (_timeMachine != null)
				{
					return _timeMachine.BufferIsFull;
				}
				return false;
			}
		}

		public int BufferFill
		{
			get
			{
				if (_timeMachine != null)
				{
					return _timeMachine.BufferFill;
				}
				return 0;
			}
		}

		public event IQReadyDelegate IQReady;

		public unsafe void Process(Complex* buffer, int length)
		{
			_length = length;
			if (this.IQReady != null)
			{
				this.IQReady(buffer, length);
			}
			if (_timeMachine == null)
			{
				_timeMachine = new TimeMachine();
			}
			_timeMachine.TimeBack = TimeBack;
			if (_needClean)
			{
				_timeMachine.TimeBackBufferSize = SecondToBufferSize(_bufferSize);
				_timeMachine.BufferClean = true;
				_needClean = false;
			}
			_timeMachine.Process(buffer, length);
		}

		public int SecondToBufferSize(int second)
		{
			return (int)(_sampleRate / (double)_length) * second;
		}

		public int BufferSizeInWord()
		{
			return (int)_sampleRate * _bufferSize;
		}
	}
}

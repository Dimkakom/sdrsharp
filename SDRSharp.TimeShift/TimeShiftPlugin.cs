using SDRSharp.Common;
using System.Windows.Forms;

namespace SDRSharp.TimeShift
{
	public class TimeShiftPlugin : ISharpPlugin
	{
		private const string _displayName = "TimeShift";

		private ISharpControl _control;

		private TimeShiftPanel _guiControl;

		public string DisplayName => "TimeShift";

		public bool HasGui => true;

		public UserControl GuiControl => _guiControl;

		public UserControl Gui => _guiControl;


		public void Initialize(ISharpControl control)
		{
			_control = control;
			_guiControl = new TimeShiftPanel(_control);
		}

		public void Close()
		{
			_guiControl.Stop();
			_guiControl.SaveSettings();
		}
	}
}

using SDRSharp.Common;
using SDRSharp.PanView;
using SDRSharp.Radio;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace SDRSharp.TimeShift
{
	public class TimeShiftPanel : UserControl
	{
		private const int FFTBins = 8192;

		private int _fftBins;

		private UnsafeBuffer _iqBuffer;

		private unsafe Complex* _iqPtr;

		private UnsafeBuffer _fftBuffer;

		private unsafe Complex* _fftPtr;

		private UnsafeBuffer _fftWindow;

		private unsafe float* _fftWindowPtr;

		private UnsafeBuffer _fftSpectrum;

		private unsafe float* _fftSpectrumPtr;

		private UnsafeBuffer _scaledFFTSpectrum;

		private unsafe byte* _scaledFFTSpectrumPtr;

		private float _fftOffset = 40f;

		private Thread _fftThread;

		private bool _fftThreadRunning;

		private Waterfall _waterfall;

		private readonly SharpEvent _bufferEvent = new SharpEvent(initialState: false);

		private readonly ISharpControl _control;

		private IFProcessor _ifProcessor;

		private bool _fftBufferAvailable;

		private PluginPosition _pluginPosition;

		private int _bufferCount;

		private int _mouseX;

		private int _mouseY;

		private bool _mouseIn;

		private float _waterfallSpeed;

		private int _writePos;

		private bool _needStart;

		private IContainer components;

		private CheckBox timeShiftEnableCheckBox;

		private GroupBox groupBox1;

		private System.Windows.Forms.Timer displayUpdateTimer;

		private Label averageLevelLabel;

		private Label label1;

		private NumericUpDown BufferSizeNumericUpDown;

		private GroupBox groupBox2;

		private Label label2;

		private TrackBar contrastTrackBar;

		private Label bufferSizeLabel;

		private Label label3;

		private ComboBox positionComboBox;

		private Label label4;

		private TrackBar waterfallSpeedTrackBar;

		private CheckBox useZoomCheckBox;

		private Label label5;

		private ComboBox resolutionComboBox;

		public TimeShiftPanel(ISharpControl control)
		{
			InitializeComponent();
			_control = control;
			_control.PropertyChanged += NotifyPropertyChangedHandler;
			_ifProcessor = new IFProcessor();
			_control.RegisterStreamHook(_ifProcessor, ProcessorType.RawIQ);
			_ifProcessor.Enabled = false;
			_waterfall = new Waterfall();
			_waterfall.Visible = false;
			_waterfall.Dock = DockStyle.Fill;
			_waterfall.Margin = new Padding(0);
			_waterfall.DisplayRange = (int)_control.FFTRange;
			_waterfall.DisplayOffset = (int)_control.FFTOffset;
			_waterfall.EnableFilter = true;
			_waterfall.BandType = BandType.Center;
			_waterfall.StepSize = _control.StepSize;
			_waterfall.UseSnap = _control.SnapToGrid;
			_waterfall.EnableHotTracking = true;
			_waterfall.EnableFrequencyMarker = _control.SnapToGrid;
			_waterfall.Zoom = 0;
			_waterfall.UseSmoothing = true;
			_waterfall.Attack = 0.9f;
			_waterfall.Decay = 0.6f;
			_waterfall.AutoSize = false;
			_waterfall.CenterFrequency = 0L;
			_waterfall.FilterBandwidth = 10000;
			_waterfall.FilterOffset = 0;
			_waterfall.Frequency = 0L;
			_waterfall.CustomPaint += WaterfallCustomPaint;
			_waterfall.MouseMove += MouseMoveOnWaterfall;
			_waterfall.MouseEnter += MouseEnterOnWaterfall;
			_waterfall.MouseLeave += MouseLeaveWaterfall;
			_waterfall.MouseDown += MouseClickOnWaterfall;
			_waterfall.FrequencyChanged += FrequencyChanged;
			_waterfall.CenterFrequencyChanged += CenterFrequencyChanged;
			_waterfall.BandwidthChanged += BandwidthChanged;
			_pluginPosition = (PluginPosition)Utils.GetIntSetting("TimeShiftPluginPosition", 1);
			positionComboBox.SelectedIndex = Convert.ToInt32(_pluginPosition);
			_control.RegisterFrontControl(_waterfall, _pluginPosition);
			timeShiftEnableCheckBox.Checked = Utils.GetBooleanSetting("TimeShiftEnable");
			_waterfall.Visible = timeShiftEnableCheckBox.Checked;
			BufferSizeNumericUpDown.Value = Utils.GetIntSetting("TimeShiftBuffer", 30);
			bufferSizeNumericUpDown_ValueChanged(null, null);
			waterfallSpeedTrackBar.Value = Utils.GetIntSetting("TimeShiftWaterfallSpeed", 1);
			waterfallSpeedTrackBar_Scroll(null, null);
			useZoomCheckBox.Checked = Utils.GetBooleanSetting("TimeShiftUseZoom");
			contrastTrackBar.Value = Utils.GetIntSetting("TimeShiftWaterfallContrast", 0);
			contrastTrackBar_Scroll(null, null);
			resolutionComboBox.SelectedIndex = Utils.GetIntSetting("TimeShiftWaterfallResolution", 1);
			resolutionComboBox_SelectedIndexChanged(null, null);
		}

		private void BandwidthChanged(object sender, BandwidthEventArgs e)
		{
			if ((_control.DetectorType == DetectorType.WFM && e.Bandwidth <= 250000) || (_control.DetectorType != DetectorType.WFM && e.Bandwidth <= 32000))
			{
				_control.FilterBandwidth = e.Bandwidth;
			}
			else
			{
				e.Cancel = true;
			}
		}

		private void CenterFrequencyChanged(object sender, FrequencyEventArgs e)
		{
			if (_control.SourceIsTunable)
			{
				_control.CenterFrequency = e.Frequency;
			}
			else
			{
				e.Cancel = true;
			}
		}

		private void FrequencyChanged(object sender, FrequencyEventArgs e)
		{
			_control.Frequency = e.Frequency;
		}

		private void MouseMoveOnWaterfall(object sender, MouseEventArgs e)
		{
			_mouseX = e.X;
			_mouseY = e.Y;
		}

		private void MouseClickOnWaterfall(object sender, MouseEventArgs e)
		{
			int num = e.Y;
			if (e.Button == MouseButtons.Right)
			{
				num = 0;
			}
			if ((float)num > (float)_ifProcessor.BufferFill / _waterfallSpeed)
			{
				num = (int)((float)_ifProcessor.BufferFill / _waterfallSpeed);
			}
			_ifProcessor.TimeBack = (int)((float)num * _waterfallSpeed);
		}

		private void MouseEnterOnWaterfall(object sender, EventArgs e)
		{
			_mouseIn = true;
		}

		private void MouseLeaveWaterfall(object sender, EventArgs e)
		{
			_mouseIn = false;
		}

		public void SaveSettings()
		{
			Utils.SaveSetting("TimeShiftEnable", timeShiftEnableCheckBox.Checked);
			Utils.SaveSetting("TimeShiftBuffer", BufferSizeNumericUpDown.Value);
			Utils.SaveSetting("TimeShiftWaterfallSpeed", waterfallSpeedTrackBar.Value);
			Utils.SaveSetting("TimeShiftWaterfallContrast", contrastTrackBar.Value);
			Utils.SaveSetting("TimeShiftUseZoom", useZoomCheckBox.Checked);
			Utils.SaveSetting("TimeShiftPluginPosition", Convert.ToInt32(_pluginPosition));
			Utils.SaveSetting("TimeShiftWaterfallResolution", resolutionComboBox.SelectedIndex);
		}

		private void NotifyPropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
			case "StartRadio":
				ConfigureWaterfall();
				ResetBuffer();
				_needStart = true;
				break;
			case "StopRadio":
				Stop();
				break;
			case "Frequency":
				ConfigureWaterfall();
				break;
			case "CenterFrequency":
				ConfigureWaterfall();
				_ifProcessor.TimeBack = 0;
				break;
			case "FFTRange":
				_waterfall.DisplayRange = (int)_control.FFTRange;
				break;
			case "FFTOffset":
				_waterfall.DisplayOffset = (int)_control.FFTOffset;
				break;
			case "StepSize":
				_waterfall.StepSize = _control.StepSize;
				break;
			case "SnapToGrid":
				_waterfall.UseSnap = _control.SnapToGrid;
				_waterfall.EnableFrequencyMarker = _control.SnapToGrid;
				break;
			case "Zoom":
				if (useZoomCheckBox.Checked)
				{
					_waterfall.Zoom = _control.Zoom * 2;
				}
				else
				{
					_waterfall.Zoom = 0;
				}
				break;
			case "FilterBandwidth":
				_waterfall.FilterBandwidth = _control.FilterBandwidth;
				break;
			}
		}

		private void Start()
		{
			if (_control.IsPlaying)
			{
				InitFFTBuffers();
				BuildFFTWindow();
				_fftThreadRunning = true;
				_bufferEvent.Reset();
				_fftBufferAvailable = false;
				if (_fftThread == null)
				{
					_fftThread = new Thread(ProcessFFT);
					_fftThread.Name = "TimeShift FFT";
					_fftThread.Start();
				}
				ResetBuffer();
				_ifProcessor.Enabled = true;
				_waterfall.Visible = timeShiftEnableCheckBox.Checked;
				BufferSizeNumericUpDown.Enabled = false;
				resolutionComboBox.Enabled = false;
			}
		}

		public void Stop()
		{
			if (_fftThreadRunning)
			{
				_fftThreadRunning = false;
				if (_fftThread != null)
				{
					_bufferEvent.Set();
					_fftThread.Join();
					_fftThread = null;
				}
				_fftBufferAvailable = false;
				ResetBuffer();
				_ifProcessor.Enabled = false;
				_waterfall.Visible = timeShiftEnableCheckBox.Checked;
				BufferSizeNumericUpDown.Enabled = true;
				resolutionComboBox.Enabled = true;
			}
		}

		private void ResetBuffer()
		{
			_ifProcessor.BufferClean = true;
			_ifProcessor.TimeBack = 0;
		}

		private unsafe void ProcessFFT(object parameter)
		{
			_ifProcessor.IQReady += IQSamplesIn;
			while (_fftThreadRunning)
			{
				_bufferEvent.WaitOne();
				float num = (float)(10.0 * Math.Log10((double)_fftBins / 2.0));
				float num2 = 24f - num + _fftOffset - 70f;
				Utils.Memcpy(_fftPtr, _iqPtr, _fftBins * sizeof(Complex));
				Fourier.ApplyFFTWindow(_fftPtr, _fftWindowPtr, _fftBins);
				Fourier.ForwardTransform(_fftPtr, _fftBins);
				Fourier.SpectrumPower(_fftPtr, _fftSpectrumPtr, _fftBins, num2);
				_fftBufferAvailable = true;
			}
			_ifProcessor.IQReady -= IQSamplesIn;
		}

		private unsafe void IQSamplesIn(Complex* buffer, int length)
		{
			_bufferCount++;
			if ((float)_bufferCount >= _waterfallSpeed)
			{
				int num = Math.Min(length, _fftBins - _writePos);
				Utils.Memcpy(_iqPtr + _writePos, buffer, num * sizeof(Complex));
				_writePos += num;
				if (_writePos >= _fftBins)
				{
					_writePos = 0;
					_bufferCount -= (int)_waterfallSpeed;
					_bufferEvent.Set();
				}
			}
		}

		private unsafe void BuildFFTWindow()
		{
			float[] array = FilterBuilder.MakeWindow(WindowType.BlackmanHarris7, _fftBins);
			float[] array2 = array;
			fixed (float* src = array2)
			{
				Utils.Memcpy(_fftWindow, src, _fftBins * 4);
			}
		}

		private unsafe void InitFFTBuffers()
		{
			_iqBuffer = UnsafeBuffer.Create(_fftBins, sizeof(Complex));
			_fftBuffer = UnsafeBuffer.Create(_fftBins, sizeof(Complex));
			_fftWindow = UnsafeBuffer.Create(_fftBins, 4);
			_fftSpectrum = UnsafeBuffer.Create(_fftBins, 4);
			_scaledFFTSpectrum = UnsafeBuffer.Create(_fftBins, 1);
			_iqPtr = (Complex*)(void*)_iqBuffer;
			_fftPtr = (Complex*)(void*)_fftBuffer;
			_fftWindowPtr = (float*)(void*)_fftWindow;
			_fftSpectrumPtr = (float*)(void*)_fftSpectrum;
			_scaledFFTSpectrumPtr = (byte*)(void*)_scaledFFTSpectrum;
		}

		private void ConfigureWaterfall()
		{
			_waterfall.SpectrumWidth = _control.RFBandwidth;
			_waterfall.Frequency = _control.Frequency;
			if (!_control.FrequencyShiftEnabled)
			{
				_waterfall.CenterFrequency = _control.CenterFrequency;
			}
			else
			{
				_waterfall.CenterFrequency = _control.CenterFrequency + _control.FrequencyShift;
			}
		}

		private void timeShiftEnableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (timeShiftEnableCheckBox.Checked)
			{
				Start();
			}
			else
			{
				Stop();
			}
		}

		private unsafe void displayUpdateTimer_Tick(object sender, EventArgs e)
		{
			if (_needStart)
			{
				_needStart = false;
				timeShiftEnableCheckBox_CheckedChanged(null, null);
			}
			if (_fftBufferAvailable)
			{
				int num = _ifProcessor.BufferSizeInWord() / 1024 / 1024 * 2 * 2;
				bufferSizeLabel.Text = $"Memory used {num:F1} MB max. 1536 MB";
				if (num >= 1536)
				{
					bufferSizeLabel.ForeColor = Color.Red;
				}
				else
				{
					bufferSizeLabel.ForeColor = Color.Black;
				}
				if (_waterfall.SpectrumWidth == 0)
				{
					ConfigureWaterfall();
				}
				double num2 = (double)_waterfall.SpectrumWidth / _ifProcessor.SampleRate;
				int num3 = Math.Min(_fftBins, (int)((double)_fftBins * num2));
				float* powerSpectrum = _fftSpectrumPtr + (_fftBins - num3) / 2;
				_waterfall.Render(powerSpectrum, num3);
				_fftBufferAvailable = false;
			}
		}

		private void bufferSizeNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_ifProcessor.BufferSizeInSecond = (int)BufferSizeNumericUpDown.Value;
			ResetBuffer();
		}

		private void contrastTrackBar_Scroll(object sender, EventArgs e)
		{
			_waterfall.Contrast = contrastTrackBar.Value;
		}

		private void WaterfallCustomPaint(object sender, CustomPaintEventArgs e)
		{
			using (SolidBrush brush = new SolidBrush(Color.FromArgb(70, Color.Silver)))
			{
				using (Pen pen2 = new Pen(Color.FromArgb(250, Color.Red)))
				{
					using (Pen pen = new Pen(Color.FromArgb(250, Color.LightGreen)))
					{
						if (timeShiftEnableCheckBox.Checked)
						{
							Rectangle rect = default(Rectangle);
							rect.X = 30;
							rect.Y = Math.Min(_waterfall.Height, (int)((float)_ifProcessor.BufferFill / _waterfallSpeed));
							rect.Width = _waterfall.Width - 60;
							rect.Height = _waterfall.Height - rect.Y;
							e.Graphics.FillRectangle(brush, rect);
							if (_waterfallSpeed > 0f)
							{
								float num = (float)_ifProcessor.TimeBack / _waterfallSpeed;
								e.Graphics.DrawLine(pen, 30f, num, _waterfall.Width - 30, num);
								e.Graphics.DrawLine(pen, 30f, num + 5f, _waterfall.Width - 30, num + 5f);
							}
							if (_mouseIn)
							{
								e.Graphics.DrawLine(pen2, 30, _mouseY, _waterfall.Width - 30, _mouseY);
							}
						}
						float num2 = Math.Max(0f, _waterfall.FrequencyToPoint(_waterfall.Frequency - _control.FilterBandwidth / 2));
						float num3 = Math.Min(_waterfall.Width, _waterfall.FrequencyToPoint(_waterfall.Frequency + _control.FilterBandwidth / 2));
						e.Graphics.DrawLine(pen, num2, 0f, num2, _waterfall.Height);
						e.Graphics.DrawLine(pen, num3, 0f, num3, _waterfall.Height);
					}
				}
			}
		}

		private void positionComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			_pluginPosition = (PluginPosition)positionComboBox.SelectedIndex;
		}

		private void waterfallSpeedTrackBar_Scroll(object sender, EventArgs e)
		{
			ResetBuffer();
			_waterfallSpeed = waterfallSpeedTrackBar.Value * 10;
		}

		private void useZoomCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (!useZoomCheckBox.Checked)
			{
				_waterfall.Zoom = 0;
			}
		}

		private void resolutionComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			_fftBins = Convert.ToInt32(resolutionComboBox.SelectedItem);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			timeShiftEnableCheckBox = new System.Windows.Forms.CheckBox();
			groupBox1 = new System.Windows.Forms.GroupBox();
			bufferSizeLabel = new System.Windows.Forms.Label();
			label1 = new System.Windows.Forms.Label();
			BufferSizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
			averageLevelLabel = new System.Windows.Forms.Label();
			displayUpdateTimer = new System.Windows.Forms.Timer(components);
			groupBox2 = new System.Windows.Forms.GroupBox();
			label5 = new System.Windows.Forms.Label();
			resolutionComboBox = new System.Windows.Forms.ComboBox();
			useZoomCheckBox = new System.Windows.Forms.CheckBox();
			label4 = new System.Windows.Forms.Label();
			waterfallSpeedTrackBar = new System.Windows.Forms.TrackBar();
			label3 = new System.Windows.Forms.Label();
			positionComboBox = new System.Windows.Forms.ComboBox();
			label2 = new System.Windows.Forms.Label();
			contrastTrackBar = new System.Windows.Forms.TrackBar();
			groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)BufferSizeNumericUpDown).BeginInit();
			groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)waterfallSpeedTrackBar).BeginInit();
			((System.ComponentModel.ISupportInitialize)contrastTrackBar).BeginInit();
			SuspendLayout();
			timeShiftEnableCheckBox.AutoSize = true;
			timeShiftEnableCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			timeShiftEnableCheckBox.Location = new System.Drawing.Point(10, 19);
			timeShiftEnableCheckBox.Name = "timeShiftEnableCheckBox";
			timeShiftEnableCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			timeShiftEnableCheckBox.Size = new System.Drawing.Size(59, 17);
			timeShiftEnableCheckBox.TabIndex = 4;
			timeShiftEnableCheckBox.Text = "Enable";
			timeShiftEnableCheckBox.UseVisualStyleBackColor = true;
			timeShiftEnableCheckBox.CheckedChanged += new System.EventHandler(timeShiftEnableCheckBox_CheckedChanged);
			groupBox1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			groupBox1.Controls.Add(bufferSizeLabel);
			groupBox1.Controls.Add(label1);
			groupBox1.Controls.Add(BufferSizeNumericUpDown);
			groupBox1.Controls.Add(averageLevelLabel);
			groupBox1.Controls.Add(timeShiftEnableCheckBox);
			groupBox1.Location = new System.Drawing.Point(0, 3);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new System.Drawing.Size(201, 72);
			groupBox1.TabIndex = 7;
			groupBox1.TabStop = false;
			groupBox1.Text = "TimeShift";
			bufferSizeLabel.AutoSize = true;
			bufferSizeLabel.Location = new System.Drawing.Point(7, 53);
			bufferSizeLabel.Name = "bufferSizeLabel";
			bufferSizeLabel.Size = new System.Drawing.Size(98, 13);
			bufferSizeLabel.TabIndex = 9;
			bufferSizeLabel.Text = "Memory used 0 MB";
			label1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(76, 20);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(55, 13);
			label1.TabIndex = 10;
			label1.Text = "Buffer sec";
			BufferSizeNumericUpDown.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			BufferSizeNumericUpDown.Location = new System.Drawing.Point(137, 18);
			BufferSizeNumericUpDown.Maximum = new decimal(new int[4]
			{
				300,
				0,
				0,
				0
			});
			BufferSizeNumericUpDown.Minimum = new decimal(new int[4]
			{
				1,
				0,
				0,
				0
			});
			BufferSizeNumericUpDown.Name = "BufferSizeNumericUpDown";
			BufferSizeNumericUpDown.Size = new System.Drawing.Size(49, 20);
			BufferSizeNumericUpDown.TabIndex = 9;
			BufferSizeNumericUpDown.Value = new decimal(new int[4]
			{
				30,
				0,
				0,
				0
			});
			BufferSizeNumericUpDown.ValueChanged += new System.EventHandler(bufferSizeNumericUpDown_ValueChanged);
			averageLevelLabel.AutoSize = true;
			averageLevelLabel.Location = new System.Drawing.Point(85, 66);
			averageLevelLabel.Name = "averageLevelLabel";
			averageLevelLabel.Size = new System.Drawing.Size(0, 13);
			averageLevelLabel.TabIndex = 7;
			displayUpdateTimer.Enabled = true;
			displayUpdateTimer.Interval = 10;
			displayUpdateTimer.Tick += new System.EventHandler(displayUpdateTimer_Tick);
			groupBox2.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			groupBox2.Controls.Add(label5);
			groupBox2.Controls.Add(resolutionComboBox);
			groupBox2.Controls.Add(useZoomCheckBox);
			groupBox2.Controls.Add(label4);
			groupBox2.Controls.Add(waterfallSpeedTrackBar);
			groupBox2.Controls.Add(label3);
			groupBox2.Controls.Add(positionComboBox);
			groupBox2.Controls.Add(label2);
			groupBox2.Controls.Add(contrastTrackBar);
			groupBox2.Location = new System.Drawing.Point(1, 81);
			groupBox2.Name = "groupBox2";
			groupBox2.Size = new System.Drawing.Size(200, 198);
			groupBox2.TabIndex = 8;
			groupBox2.TabStop = false;
			groupBox2.Text = "Waterfall";
			label5.AutoSize = true;
			label5.Location = new System.Drawing.Point(6, 146);
			label5.Name = "label5";
			label5.Size = new System.Drawing.Size(57, 13);
			label5.TabIndex = 8;
			label5.Text = "Resolution";
			resolutionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			resolutionComboBox.FormattingEnabled = true;
			resolutionComboBox.Items.AddRange(new object[5]
			{
				"4096",
				"8192",
				"16384",
				"32768",
				"65536"
			});
			resolutionComboBox.Location = new System.Drawing.Point(115, 143);
			resolutionComboBox.Name = "resolutionComboBox";
			resolutionComboBox.Size = new System.Drawing.Size(79, 21);
			resolutionComboBox.TabIndex = 7;
			resolutionComboBox.SelectedIndexChanged += new System.EventHandler(resolutionComboBox_SelectedIndexChanged);
			useZoomCheckBox.AutoSize = true;
			useZoomCheckBox.Location = new System.Drawing.Point(9, 115);
			useZoomCheckBox.Name = "useZoomCheckBox";
			useZoomCheckBox.Size = new System.Drawing.Size(163, 17);
			useZoomCheckBox.TabIndex = 6;
			useZoomCheckBox.Text = "Use Spectrum analyzer zoom";
			useZoomCheckBox.UseVisualStyleBackColor = true;
			useZoomCheckBox.CheckedChanged += new System.EventHandler(useZoomCheckBox_CheckedChanged);
			label4.AutoSize = true;
			label4.Location = new System.Drawing.Point(6, 61);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(38, 13);
			label4.TabIndex = 5;
			label4.Text = "Speed";
			waterfallSpeedTrackBar.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			waterfallSpeedTrackBar.AutoSize = false;
			waterfallSpeedTrackBar.LargeChange = 1;
			waterfallSpeedTrackBar.Location = new System.Drawing.Point(6, 77);
			waterfallSpeedTrackBar.Minimum = 1;
			waterfallSpeedTrackBar.Name = "waterfallSpeedTrackBar";
			waterfallSpeedTrackBar.Size = new System.Drawing.Size(188, 32);
			waterfallSpeedTrackBar.TabIndex = 4;
			waterfallSpeedTrackBar.Value = 1;
			waterfallSpeedTrackBar.Scroll += new System.EventHandler(waterfallSpeedTrackBar_Scroll);
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(6, 173);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(121, 13);
			label3.TabIndex = 3;
			label3.Text = "Position (restart needed)";
			positionComboBox.AutoCompleteCustomSource.AddRange(new string[4]
			{
				"Up",
				"Down",
				"Left",
				"Right"
			});
			positionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			positionComboBox.FormattingEnabled = true;
			positionComboBox.Items.AddRange(new object[4]
			{
				"Up",
				"Down",
				"Left",
				"Right"
			});
			positionComboBox.Location = new System.Drawing.Point(142, 170);
			positionComboBox.Name = "positionComboBox";
			positionComboBox.Size = new System.Drawing.Size(52, 21);
			positionComboBox.TabIndex = 2;
			positionComboBox.SelectedIndexChanged += new System.EventHandler(positionComboBox_SelectedIndexChanged);
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(6, 16);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(46, 13);
			label2.TabIndex = 1;
			label2.Text = "Contrast";
			contrastTrackBar.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			contrastTrackBar.AutoSize = false;
			contrastTrackBar.Location = new System.Drawing.Point(5, 31);
			contrastTrackBar.Maximum = 50;
			contrastTrackBar.Minimum = -50;
			contrastTrackBar.Name = "contrastTrackBar";
			contrastTrackBar.Size = new System.Drawing.Size(189, 36);
			contrastTrackBar.TabIndex = 0;
			contrastTrackBar.TickFrequency = 10;
			contrastTrackBar.Scroll += new System.EventHandler(contrastTrackBar_Scroll);
			base.Controls.Add(groupBox2);
			base.Controls.Add(groupBox1);
			base.Name = "TimeShiftPanel";
			base.Size = new System.Drawing.Size(204, 309);
			groupBox1.ResumeLayout(performLayout: false);
			groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)BufferSizeNumericUpDown).EndInit();
			groupBox2.ResumeLayout(performLayout: false);
			groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)waterfallSpeedTrackBar).EndInit();
			((System.ComponentModel.ISupportInitialize)contrastTrackBar).EndInit();
			ResumeLayout(performLayout: false);
		}
	}
}

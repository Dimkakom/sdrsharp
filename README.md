# SDRSharp Build 1632 Core Full Source (.NET Framework 4.6)
Source Code for solution sceleton copied from https://github.com/SDRSharpR/SDRSharp
Thanks author named SDRSharpR  for this repository

Current solution indcluded:
Signals Source (FrontEnds):
- AIRSPY
- AIRSPY HF+
- Spy Server
- Hack RF
- RTL-SDR (USB)
- RTL-SDR (TCP)
- SoftRock (Si570)
- RFSPACE SDR-IQ (USB)
- RFSPACE Networked Radios
- Afedri SDR-Net
- FUNcube Dongle Pro
- FUNcube Dongle Pro+
- RFSPACE Networked Radios
- AFEDRI Networked Radios
- BladeRF
- LimeSDR

Plugins:
- IQ Balancer
- AF DNR
- IF DN
- Baseband Noise Blanker
- Demodulator Noise Blanker
- Wave Recorder
- Zoom FFT
- Band Plan
- Frequency Manager
- TimeShift
- Signal Diagnostics
- BasebandRecorder
- DigitalAudioProcessor
- AudioRecorder
- Digital Noise Reduction
- DSD
- Frequency Scanner
- Frequency Manager
- DigitalIfProcessor
- DCSDecoder
- CTCSSDecoder
- AuxVFO

For build project just build main prokect named SDRSharp.Core and looking for binary at bin folder.

I DO NOT IN ANY WAY claim authorship of any of the modules added to this collection.
I am very grateful to the developers of all plugins (and SDR SHARP itself) for sharing the fruits of their work and giving the opportunity to publish this collection.
Thank you guys :)
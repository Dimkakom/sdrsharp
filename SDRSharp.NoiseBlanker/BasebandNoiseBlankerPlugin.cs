using SDRSharp.Common;
using SDRSharp.Radio;
using System.Windows.Forms;

namespace SDRSharp.NoiseBlanker
{
	public class BasebandNoiseBlankerPlugin : ISharpPlugin
	{
		private const string _displayName = "Baseband Noise Blanker";

		private ISharpControl _control;

		private NoiseBlankerProcessor _processor;

		private ProcessorPanel _guiControl;

		public string DisplayName => "Baseband Noise Blanker";

		public UserControl Gui => _guiControl;

		public void Initialize(ISharpControl control)
		{
			_control = control;
			_processor = new NoiseBlankerProcessor();
			_processor.Enabled = Utils.GetBooleanSetting("nb.bb.enabled");
			_processor.NoiseThreshold = Utils.GetDoubleSetting("nb.bb.threshold", 5.0);
			_processor.PulseWidth = Utils.GetDoubleSetting("nb.bb.pulseWidth", 50.0);
			_processor.LookupWindow = Utils.GetDoubleSetting("nb.bb.lookupWindow", 20.0);
			_guiControl = new ProcessorPanel(_processor);
			_control.RegisterStreamHook(_processor, ProcessorType.RawIQ);
		}

		public void Close()
		{
			Utils.SaveSetting("nb.bb.enabled", _processor.Enabled);
			Utils.SaveSetting("nb.bb.threshold", _processor.NoiseThreshold);
			Utils.SaveSetting("nb.bb.pulseWidth", _processor.PulseWidth);
			Utils.SaveSetting("nb.bb.lookupWindow", _processor.LookupWindow);
		}
	}
}

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.NoiseBlanker
{
	public class ProcessorPanel : UserControl
	{
		private NoiseBlankerProcessor _processor;

		private IContainer components;

		private CheckBox enableCheckBox;

		private Label thresholdLabel;

		private TrackBar thresholdTrackBar;

		private Label pulseWidthLabel;

		private TrackBar pulseWidthTrackBar;

		private Label label2;

		private TableLayoutPanel tableLayoutPanel1;

		private TrackBar lookupWindowTrackBar;

		private Label label1;

		private Label lookupWindpwLabel;

		public ProcessorPanel(NoiseBlankerProcessor processor)
		{
			_processor = processor;
			InitializeComponent();
			enableCheckBox.Checked = _processor.Enabled;
			thresholdTrackBar.Value = (int)(_processor.NoiseThreshold * 2.0);
			pulseWidthTrackBar.Value = (int)(_processor.PulseWidth * 10.0);
			thresholdTrackBar_Scroll(null, null);
			pulseWidthTrackBar_Scroll(null, null);
			lookupWindowTrackBar_Scroll(null, null);
		}

		private void enableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_processor.Enabled = enableCheckBox.Checked;
		}

		private void thresholdTrackBar_Scroll(object sender, EventArgs e)
		{
			_processor.NoiseThreshold = (double)thresholdTrackBar.Value * 0.5;
			thresholdLabel.Text = _processor.NoiseThreshold.ToString("0.0") + " dB";
		}

		private void pulseWidthTrackBar_Scroll(object sender, EventArgs e)
		{
			_processor.PulseWidth = (float)pulseWidthTrackBar.Value * 0.1f;
			pulseWidthLabel.Text = _processor.PulseWidth.ToString("0.00") + " µs";
		}

		private void lookupWindowTrackBar_Scroll(object sender, EventArgs e)
		{
			_processor.LookupWindow = (float)lookupWindowTrackBar.Value * 0.1f;
			lookupWindpwLabel.Text = _processor.LookupWindow.ToString("0.00") + " ms";
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			enableCheckBox = new System.Windows.Forms.CheckBox();
			thresholdLabel = new System.Windows.Forms.Label();
			thresholdTrackBar = new System.Windows.Forms.TrackBar();
			pulseWidthLabel = new System.Windows.Forms.Label();
			pulseWidthTrackBar = new System.Windows.Forms.TrackBar();
			label2 = new System.Windows.Forms.Label();
			tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			lookupWindowTrackBar = new System.Windows.Forms.TrackBar();
			label1 = new System.Windows.Forms.Label();
			lookupWindpwLabel = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)thresholdTrackBar).BeginInit();
			((System.ComponentModel.ISupportInitialize)pulseWidthTrackBar).BeginInit();
			tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)lookupWindowTrackBar).BeginInit();
			SuspendLayout();
			enableCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
			enableCheckBox.AutoSize = true;
			enableCheckBox.Location = new System.Drawing.Point(3, 3);
			enableCheckBox.Name = "enableCheckBox";
			enableCheckBox.Size = new System.Drawing.Size(65, 17);
			enableCheckBox.TabIndex = 0;
			enableCheckBox.Text = "Enabled";
			enableCheckBox.UseVisualStyleBackColor = true;
			enableCheckBox.CheckedChanged += new System.EventHandler(enableCheckBox_CheckedChanged);
			thresholdLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			thresholdLabel.AutoSize = true;
			thresholdLabel.Location = new System.Drawing.Point(172, 5);
			thresholdLabel.Name = "thresholdLabel";
			thresholdLabel.Size = new System.Drawing.Size(29, 13);
			thresholdLabel.TabIndex = 6;
			thresholdLabel.Text = "3 dB";
			thresholdTrackBar.Anchor = (System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			tableLayoutPanel1.SetColumnSpan(thresholdTrackBar, 2);
			thresholdTrackBar.Location = new System.Drawing.Point(3, 32);
			thresholdTrackBar.Maximum = 30;
			thresholdTrackBar.Name = "thresholdTrackBar";
			thresholdTrackBar.Size = new System.Drawing.Size(198, 45);
			thresholdTrackBar.TabIndex = 1;
			thresholdTrackBar.TickFrequency = 5;
			thresholdTrackBar.Value = 10;
			thresholdTrackBar.Scroll += new System.EventHandler(thresholdTrackBar_Scroll);
			pulseWidthLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			pulseWidthLabel.AutoSize = true;
			pulseWidthLabel.Location = new System.Drawing.Point(168, 86);
			pulseWidthLabel.Name = "pulseWidthLabel";
			pulseWidthLabel.Size = new System.Drawing.Size(33, 13);
			pulseWidthLabel.TabIndex = 8;
			pulseWidthLabel.Text = "25 µs";
			pulseWidthTrackBar.Anchor = (System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			tableLayoutPanel1.SetColumnSpan(pulseWidthTrackBar, 2);
			pulseWidthTrackBar.Location = new System.Drawing.Point(3, 108);
			pulseWidthTrackBar.Maximum = 10000;
			pulseWidthTrackBar.Minimum = 1;
			pulseWidthTrackBar.Name = "pulseWidthTrackBar";
			pulseWidthTrackBar.Size = new System.Drawing.Size(198, 45);
			pulseWidthTrackBar.TabIndex = 2;
			pulseWidthTrackBar.TickFrequency = 500;
			pulseWidthTrackBar.Value = 100;
			pulseWidthTrackBar.Scroll += new System.EventHandler(pulseWidthTrackBar_Scroll);
			label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(3, 86);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(64, 13);
			label2.TabIndex = 9;
			label2.Text = "Pulse Width";
			tableLayoutPanel1.AutoSize = true;
			tableLayoutPanel1.ColumnCount = 2;
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			tableLayoutPanel1.Controls.Add(lookupWindowTrackBar, 0, 5);
			tableLayoutPanel1.Controls.Add(enableCheckBox, 0, 0);
			tableLayoutPanel1.Controls.Add(pulseWidthTrackBar, 0, 3);
			tableLayoutPanel1.Controls.Add(label2, 0, 2);
			tableLayoutPanel1.Controls.Add(thresholdTrackBar, 0, 1);
			tableLayoutPanel1.Controls.Add(thresholdLabel, 1, 0);
			tableLayoutPanel1.Controls.Add(pulseWidthLabel, 1, 2);
			tableLayoutPanel1.Controls.Add(label1, 0, 4);
			tableLayoutPanel1.Controls.Add(lookupWindpwLabel, 1, 4);
			tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			tableLayoutPanel1.Name = "tableLayoutPanel1";
			tableLayoutPanel1.RowCount = 6;
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333f));
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333f));
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333f));
			tableLayoutPanel1.Size = new System.Drawing.Size(204, 239);
			tableLayoutPanel1.TabIndex = 10;
			lookupWindowTrackBar.Anchor = (System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			tableLayoutPanel1.SetColumnSpan(lookupWindowTrackBar, 2);
			lookupWindowTrackBar.Location = new System.Drawing.Point(3, 184);
			lookupWindowTrackBar.Maximum = 1000;
			lookupWindowTrackBar.Minimum = 1;
			lookupWindowTrackBar.Name = "lookupWindowTrackBar";
			lookupWindowTrackBar.Size = new System.Drawing.Size(198, 45);
			lookupWindowTrackBar.TabIndex = 11;
			lookupWindowTrackBar.TickFrequency = 50;
			lookupWindowTrackBar.Value = 100;
			lookupWindowTrackBar.Scroll += new System.EventHandler(lookupWindowTrackBar_Scroll);
			label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(3, 162);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(85, 13);
			label1.TabIndex = 10;
			label1.Text = "Lookup Window";
			lookupWindpwLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			lookupWindpwLabel.AutoSize = true;
			lookupWindpwLabel.Location = new System.Drawing.Point(172, 162);
			lookupWindpwLabel.Name = "lookupWindpwLabel";
			lookupWindpwLabel.Size = new System.Drawing.Size(29, 13);
			lookupWindpwLabel.TabIndex = 8;
			lookupWindpwLabel.Text = "2 ms";
			base.AutoScaleDimensions = new System.Drawing.SizeF(96f, 96f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			base.Controls.Add(tableLayoutPanel1);
			base.Name = "ProcessorPanel";
			base.Size = new System.Drawing.Size(204, 239);
			((System.ComponentModel.ISupportInitialize)thresholdTrackBar).EndInit();
			((System.ComponentModel.ISupportInitialize)pulseWidthTrackBar).EndInit();
			tableLayoutPanel1.ResumeLayout(performLayout: false);
			tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)lookupWindowTrackBar).EndInit();
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

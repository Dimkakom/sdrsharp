using SDRSharp.Radio;
using System;

namespace SDRSharp.NoiseBlanker
{
	public class NoiseBlankerProcessor : IIQProcessor, IStreamProcessor, IBaseProcessor, IRealProcessor
	{
		private const int CircularBufferSize = 4;

		private double _sampleRate;

		private double _pulseWidth;

		private double _lookupWindow;

		private double _threshold;

		private bool _enabled;

		private bool _needConfigure = true;

		private int _blankingWindowLength;

		private int _index;

		private float _ratio;

		private float _avg;

		private float _alpha;

		private UnsafeBuffer _delay;

		private unsafe Complex* _delayPtr;

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				_sampleRate = value;
				_avg = 1f;
				_needConfigure = true;
			}
		}

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public double NoiseThreshold
		{
			get
			{
				return _threshold;
			}
			set
			{
				_threshold = value;
				_ratio = Fourier.DecibelToRatio((float)_threshold);
			}
		}

		public double PulseWidth
		{
			get
			{
				return _pulseWidth;
			}
			set
			{
				_pulseWidth = value;
				_needConfigure = true;
			}
		}

		public double LookupWindow
		{
			get
			{
				return _lookupWindow;
			}
			set
			{
				_lookupWindow = value;
				_needConfigure = true;
			}
		}

		private unsafe void Configure()
		{
			_alpha = (float)(1.0 - Math.Exp(-1.0 / (_sampleRate * _lookupWindow * 0.001)));
			_blankingWindowLength = ((int)Math.Max(_pulseWidth * 1E-06 * _sampleRate, 3.0) | 1);
			int num = _blankingWindowLength * 4;
			if (_delay == null || _delay.Length < num)
			{
				_delay = UnsafeBuffer.Create(num, sizeof(Complex));
				_delayPtr = (Complex*)(void*)_delay;
			}
		}

		public unsafe void Process(float* buffer, int length)
		{
			if (_needConfigure)
			{
				Configure();
				_needConfigure = false;
			}
			for (int i = 0; i < length; i++)
			{
				Complex* ptr = _delayPtr + _index;
				*ptr = buffer[i];
				if (Math.Abs(ptr[_blankingWindowLength / 2].Real) > _ratio * _avg)
				{
					_delay.Clear();
				}
				_avg += _alpha * (Math.Abs(buffer[i]) - _avg);
				buffer[i] = ptr[_blankingWindowLength - 1].Real;
				if (--_index < 0)
				{
					_index = _blankingWindowLength * 3;
					Utils.Memcpy(_delayPtr + _index + 1, _delayPtr, (_blankingWindowLength - 1) * sizeof(Complex));
				}
			}
		}

		public unsafe void Process(Complex* buffer, int length)
		{
			if (_needConfigure)
			{
				Configure();
				_needConfigure = false;
			}
			for (int i = 0; i < length; i++)
			{
				Complex* ptr = _delayPtr + _index;
				*ptr = buffer[i];
				if (ptr[_blankingWindowLength / 2].FastMagnitude() > _ratio * _avg)
				{
					_delay.Clear();
				}
				_avg += _alpha * (buffer[i].FastMagnitude() - _avg);
				buffer[i] = ptr[_blankingWindowLength - 1];
				if (--_index < 0)
				{
					_index = _blankingWindowLength * 3;
					Utils.Memcpy(_delayPtr + _index + 1, _delayPtr, (_blankingWindowLength - 1) * sizeof(Complex));
				}
			}
		}
	}
}

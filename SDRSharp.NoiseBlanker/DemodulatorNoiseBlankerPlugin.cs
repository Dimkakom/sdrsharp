using SDRSharp.Common;
using SDRSharp.Radio;
using System.Windows.Forms;

namespace SDRSharp.NoiseBlanker
{
	public class DemodulatorNoiseBlankerPlugin : ISharpPlugin
	{
		private const string _displayName = "Demodulator Noise Blanker";

		private ISharpControl _control;

		private NoiseBlankerProcessor _processor;

		private ProcessorPanel _guiControl;

		public string DisplayName => "Demodulator Noise Blanker";

		public UserControl Gui => _guiControl;

		public void Initialize(ISharpControl control)
		{
			_control = control;
			_processor = new NoiseBlankerProcessor();
			_processor.Enabled = Utils.GetBooleanSetting("nb.demod.enabled");
			_processor.NoiseThreshold = Utils.GetDoubleSetting("nb.demod.threshold", 5.0);
			_processor.PulseWidth = Utils.GetDoubleSetting("nb.demod.pulseWidth", 50.0);
			_processor.LookupWindow = Utils.GetDoubleSetting("nb.demod.lookupWindow", 20.0);
			_guiControl = new ProcessorPanel(_processor);
			_control.RegisterStreamHook(_processor, ProcessorType.DecimatedAndFilteredIQ);
		}

		public void Close()
		{
			Utils.SaveSetting("nb.demod.enabled", _processor.Enabled);
			Utils.SaveSetting("nb.demod.threshold", _processor.NoiseThreshold);
			Utils.SaveSetting("nb.demod.pulseWidth", _processor.PulseWidth);
			Utils.SaveSetting("nb.demod.lookupWindow", _processor.LookupWindow);
		}
	}
}

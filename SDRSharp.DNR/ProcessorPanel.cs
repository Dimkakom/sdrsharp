using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.DNR
{
	public class ProcessorPanel : UserControl
	{
		private INoiseProcessor _aControl;

		private INoiseProcessor _iControl;

		private IContainer components;

		private CheckBox ifEnableCheckBox;

		private GroupBox groupBox1;

		private Label ifThresholdLabel;

		private TrackBar ifThresholdTrackBar;

		private GroupBox groupBox2;

		private Label audioThresholdLabel;

		private TrackBar audioThresholdTrackBar;

		private CheckBox audioEnableCheckBox;

		private TableLayoutPanel tableLayoutPanel3;

		private TableLayoutPanel tableLayoutPanel2;

		private TableLayoutPanel tableLayoutPanel1;

		public ProcessorPanel(INoiseProcessor iControl, INoiseProcessor aControl)
		{
			_iControl = iControl;
			_aControl = aControl;
			InitializeComponent();
			ifThresholdTrackBar.Value = _iControl.NoiseThreshold;
			ifEnableCheckBox.Checked = _iControl.Enabled;
			audioThresholdTrackBar.Value = _aControl.NoiseThreshold;
			audioEnableCheckBox.Checked = _aControl.Enabled;
			ifThresholdTrackBar_Scroll(null, null);
			audioThresholdTrackBar_Scroll(null, null);
		}

		private void ifEnableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_iControl.Enabled = ifEnableCheckBox.Checked;
			ifThresholdTrackBar_Scroll(null, null);
		}

		private void audioEnableCheckbox_CheckedChanged(object sender, EventArgs e)
		{
			_aControl.Enabled = audioEnableCheckBox.Checked;
			audioThresholdTrackBar_Scroll(null, null);
		}

		private void ifThresholdTrackBar_Scroll(object sender, EventArgs e)
		{
			ifThresholdLabel.Text = ifThresholdTrackBar.Value + " dB";
			_iControl.NoiseThreshold = ifThresholdTrackBar.Value;
		}

		private void audioThresholdTrackBar_Scroll(object sender, EventArgs e)
		{
			audioThresholdLabel.Text = audioThresholdTrackBar.Value + " dB";
			_aControl.NoiseThreshold = audioThresholdTrackBar.Value;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ifEnableCheckBox = new System.Windows.Forms.CheckBox();
			groupBox1 = new System.Windows.Forms.GroupBox();
			tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			ifThresholdTrackBar = new System.Windows.Forms.TrackBar();
			ifThresholdLabel = new System.Windows.Forms.Label();
			groupBox2 = new System.Windows.Forms.GroupBox();
			tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			audioThresholdTrackBar = new System.Windows.Forms.TrackBar();
			audioEnableCheckBox = new System.Windows.Forms.CheckBox();
			audioThresholdLabel = new System.Windows.Forms.Label();
			tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			groupBox1.SuspendLayout();
			tableLayoutPanel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)ifThresholdTrackBar).BeginInit();
			groupBox2.SuspendLayout();
			tableLayoutPanel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)audioThresholdTrackBar).BeginInit();
			tableLayoutPanel1.SuspendLayout();
			SuspendLayout();
			ifEnableCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
			ifEnableCheckBox.AutoSize = true;
			ifEnableCheckBox.Location = new System.Drawing.Point(3, 3);
			ifEnableCheckBox.Name = "ifEnableCheckBox";
			ifEnableCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			ifEnableCheckBox.Size = new System.Drawing.Size(65, 17);
			ifEnableCheckBox.TabIndex = 4;
			ifEnableCheckBox.Text = "Enabled";
			ifEnableCheckBox.UseVisualStyleBackColor = true;
			ifEnableCheckBox.CheckedChanged += new System.EventHandler(ifEnableCheckBox_CheckedChanged);
			groupBox1.AutoSize = true;
			groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			groupBox1.Controls.Add(tableLayoutPanel3);
			groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			groupBox1.Location = new System.Drawing.Point(3, 3);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new System.Drawing.Size(198, 91);
			groupBox1.TabIndex = 7;
			groupBox1.TabStop = false;
			groupBox1.Text = "IF";
			tableLayoutPanel3.AutoSize = true;
			tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			tableLayoutPanel3.ColumnCount = 2;
			tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			tableLayoutPanel3.Controls.Add(ifEnableCheckBox, 0, 0);
			tableLayoutPanel3.Controls.Add(ifThresholdTrackBar, 0, 1);
			tableLayoutPanel3.Controls.Add(ifThresholdLabel, 1, 0);
			tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
			tableLayoutPanel3.Name = "tableLayoutPanel3";
			tableLayoutPanel3.RowCount = 2;
			tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100f));
			tableLayoutPanel3.Size = new System.Drawing.Size(192, 72);
			tableLayoutPanel3.TabIndex = 7;
			ifThresholdTrackBar.Anchor = (System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			tableLayoutPanel3.SetColumnSpan(ifThresholdTrackBar, 2);
			ifThresholdTrackBar.Location = new System.Drawing.Point(3, 26);
			ifThresholdTrackBar.Maximum = 20;
			ifThresholdTrackBar.Minimum = -80;
			ifThresholdTrackBar.Name = "ifThresholdTrackBar";
			ifThresholdTrackBar.Size = new System.Drawing.Size(186, 43);
			ifThresholdTrackBar.TabIndex = 5;
			ifThresholdTrackBar.TickFrequency = 5;
			ifThresholdTrackBar.Value = -30;
			ifThresholdTrackBar.Scroll += new System.EventHandler(ifThresholdTrackBar_Scroll);
			ifThresholdLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			ifThresholdLabel.AutoSize = true;
			ifThresholdLabel.Location = new System.Drawing.Point(157, 5);
			ifThresholdLabel.Name = "ifThresholdLabel";
			ifThresholdLabel.Size = new System.Drawing.Size(32, 13);
			ifThresholdLabel.TabIndex = 6;
			ifThresholdLabel.Text = "-5 dB";
			groupBox2.AutoSize = true;
			groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			groupBox2.Controls.Add(tableLayoutPanel2);
			groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			groupBox2.Location = new System.Drawing.Point(3, 100);
			groupBox2.Name = "groupBox2";
			groupBox2.Size = new System.Drawing.Size(198, 92);
			groupBox2.TabIndex = 8;
			groupBox2.TabStop = false;
			groupBox2.Text = "Audio";
			tableLayoutPanel2.AutoSize = true;
			tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			tableLayoutPanel2.ColumnCount = 2;
			tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			tableLayoutPanel2.Controls.Add(audioEnableCheckBox, 0, 0);
			tableLayoutPanel2.Controls.Add(audioThresholdTrackBar, 0, 1);
			tableLayoutPanel2.Controls.Add(audioThresholdLabel, 1, 0);
			tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
			tableLayoutPanel2.Name = "tableLayoutPanel2";
			tableLayoutPanel2.RowCount = 2;
			tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			tableLayoutPanel2.Size = new System.Drawing.Size(192, 73);
			tableLayoutPanel2.TabIndex = 7;
			audioThresholdTrackBar.Anchor = (System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			tableLayoutPanel2.SetColumnSpan(audioThresholdTrackBar, 2);
			audioThresholdTrackBar.Location = new System.Drawing.Point(3, 26);
			audioThresholdTrackBar.Maximum = -20;
			audioThresholdTrackBar.Minimum = -120;
			audioThresholdTrackBar.Name = "audioThresholdTrackBar";
			audioThresholdTrackBar.Size = new System.Drawing.Size(186, 45);
			audioThresholdTrackBar.TabIndex = 5;
			audioThresholdTrackBar.TickFrequency = 5;
			audioThresholdTrackBar.Value = -70;
			audioThresholdTrackBar.Scroll += new System.EventHandler(audioThresholdTrackBar_Scroll);
			audioEnableCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			audioEnableCheckBox.AutoSize = true;
			audioEnableCheckBox.Location = new System.Drawing.Point(3, 3);
			audioEnableCheckBox.Name = "audioEnableCheckBox";
			audioEnableCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			audioEnableCheckBox.Size = new System.Drawing.Size(65, 17);
			audioEnableCheckBox.TabIndex = 4;
			audioEnableCheckBox.Text = "Enabled";
			audioEnableCheckBox.UseVisualStyleBackColor = true;
			audioEnableCheckBox.CheckedChanged += new System.EventHandler(audioEnableCheckbox_CheckedChanged);
			audioThresholdLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			audioThresholdLabel.AutoSize = true;
			audioThresholdLabel.Location = new System.Drawing.Point(157, 5);
			audioThresholdLabel.Name = "audioThresholdLabel";
			audioThresholdLabel.Size = new System.Drawing.Size(32, 13);
			audioThresholdLabel.TabIndex = 6;
			audioThresholdLabel.Text = "-5 dB";
			tableLayoutPanel1.ColumnCount = 1;
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100f));
			tableLayoutPanel1.Controls.Add(groupBox2, 0, 1);
			tableLayoutPanel1.Controls.Add(groupBox1, 0, 0);
			tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			tableLayoutPanel1.Name = "tableLayoutPanel1";
			tableLayoutPanel1.RowCount = 2;
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50f));
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50f));
			tableLayoutPanel1.Size = new System.Drawing.Size(204, 195);
			tableLayoutPanel1.TabIndex = 9;
			base.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			base.Controls.Add(tableLayoutPanel1);
			base.Name = "ProcessorPanel";
			base.Size = new System.Drawing.Size(204, 220);
			groupBox1.ResumeLayout(performLayout: false);
			groupBox1.PerformLayout();
			tableLayoutPanel3.ResumeLayout(performLayout: false);
			tableLayoutPanel3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)ifThresholdTrackBar).EndInit();
			groupBox2.ResumeLayout(performLayout: false);
			groupBox2.PerformLayout();
			tableLayoutPanel2.ResumeLayout(performLayout: false);
			tableLayoutPanel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)audioThresholdTrackBar).EndInit();
			tableLayoutPanel1.ResumeLayout(performLayout: false);
			tableLayoutPanel1.PerformLayout();
			ResumeLayout(performLayout: false);
		}
	}
}

using SDRSharp.Radio;
using System;

namespace SDRSharp.DNR
{
	public class NoiseFilter : OverlapAddProcessor
	{
		private const int WindowSize = 16;

		private const int FftSize = 4096;

		private const float Attack = 0.9f;

		private const float Decay = 0.9f;

		private float _noiseThreshold;

		private readonly UnsafeBuffer _gainBuffer1;

		private unsafe readonly float* _gainPtr1;

		private readonly UnsafeBuffer _gainBuffer2;

		private unsafe readonly float* _gainPtr2;

		public float NoiseThreshold
		{
			get
			{
				return _noiseThreshold;
			}
			set
			{
				_noiseThreshold = value;
			}
		}

		public unsafe NoiseFilter(int fftSize = 4096)
			: base(fftSize)
		{
			_gainBuffer1 = UnsafeBuffer.Create(fftSize, 4);
			_gainPtr1 = (float*)(void*)_gainBuffer1;
			_gainBuffer2 = UnsafeBuffer.Create(fftSize, 4);
			_gainPtr2 = (float*)(void*)_gainBuffer2;
		}

		protected unsafe override void ProcessFft(Complex* buffer, int length)
		{
			float num = 1f / (float)Math.Pow(10.0, (double)_noiseThreshold * 0.1);
			for (int i = 0; i < length; i++)
			{
				float num2 = buffer[i].ModulusSquared() * num;
				float num3 = (num2 >= 1f) ? 1f : (num2 * num2);
				float num4 = num3 - _gainPtr1[i];
				float num5 = (num4 > 0f) ? 0.9f : 0.9f;
				_gainPtr1[i] += num4 * num5;
			}
			for (int j = 0; j < length; j++)
			{
				float num6 = 0f;
				for (int k = -8; k <= 8; k++)
				{
					int num7 = j + k;
					if (num7 >= length)
					{
						num7 -= length;
					}
					else if (num7 < 0)
					{
						num7 += length;
					}
					num6 += _gainPtr1[num7];
				}
				float num8 = num6 * 0.0625f;
				_gainPtr2[j] = num8;
				buffer[j] *= num8;
			}
			Utils.Memcpy(_gainPtr1, _gainPtr2, 16384);
		}
	}
}

namespace SDRSharp.FrequencyScanner
{
	public class MemoryEntryNewFrequency
	{
		private long _frequency;

		private float _activity;

		private long _centerFrequency;

		public long Frequency
		{
			get
			{
				return _frequency;
			}
			set
			{
				_frequency = value;
			}
		}

		public long CenterFrequency
		{
			get
			{
				return _centerFrequency;
			}
			set
			{
				_centerFrequency = value;
			}
		}

		public float Activity
		{
			get
			{
				return _activity;
			}
			set
			{
				_activity = value;
			}
		}

		public MemoryEntryNewFrequency()
		{
		}

		public MemoryEntryNewFrequency(MemoryEntryNewFrequency memoryEntryNewFrequency)
		{
			_frequency = memoryEntryNewFrequency._frequency;
			_activity = memoryEntryNewFrequency._activity;
			_centerFrequency = memoryEntryNewFrequency._centerFrequency;
		}
	}
}

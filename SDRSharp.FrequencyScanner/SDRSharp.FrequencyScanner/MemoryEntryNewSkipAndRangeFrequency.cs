using System.Collections.Generic;
using System.Drawing;

namespace SDRSharp.FrequencyScanner
{
	public class MemoryEntryNewSkipAndRangeFrequency
	{
		private List<long> _skipFrequencyArray;

		private List<MemoryEntryFrequencyRange> _scanFrequencyRange;

		private List<MemoryEntryNewFrequency> _newFrequency;

		private Point _lastLocationScreen;

		private Size _lastSizeScreen;

		private Point _lastLocationMultiSelect;

		private Size _lastSizeMultiSelect;

		public List<long> SkipFrequencyArray
		{
			get
			{
				return _skipFrequencyArray;
			}
			set
			{
				_skipFrequencyArray = value;
			}
		}

		public List<MemoryEntryFrequencyRange> FrequencyRange
		{
			get
			{
				return _scanFrequencyRange;
			}
			set
			{
				_scanFrequencyRange = value;
			}
		}

		public List<MemoryEntryNewFrequency> NewFrequency
		{
			get
			{
				return _newFrequency;
			}
			set
			{
				_newFrequency = value;
			}
		}

		public Size LastSizeScreen
		{
			get
			{
				return _lastSizeScreen;
			}
			set
			{
				_lastSizeScreen = value;
			}
		}

		public Point LastLocationScreen
		{
			get
			{
				return _lastLocationScreen;
			}
			set
			{
				_lastLocationScreen = value;
			}
		}

		public Size LastSizeMultiSelect
		{
			get
			{
				return _lastSizeMultiSelect;
			}
			set
			{
				_lastSizeMultiSelect = value;
			}
		}

		public Point LastLocationMultiSelect
		{
			get
			{
				return _lastLocationMultiSelect;
			}
			set
			{
				_lastLocationMultiSelect = value;
			}
		}

		public MemoryEntryNewSkipAndRangeFrequency()
		{
			_scanFrequencyRange = new List<MemoryEntryFrequencyRange>();
			_newFrequency = new List<MemoryEntryNewFrequency>();
			_skipFrequencyArray = new List<long>();
			_lastLocationScreen = default(Point);
			_lastSizeScreen = default(Size);
			_lastLocationMultiSelect = default(Point);
			_lastSizeMultiSelect = default(Size);
		}

		public MemoryEntryNewSkipAndRangeFrequency(MemoryEntryNewSkipAndRangeFrequency memoryEntry)
		{
			_skipFrequencyArray = memoryEntry._skipFrequencyArray;
			_scanFrequencyRange = memoryEntry._scanFrequencyRange;
			_newFrequency = memoryEntry._newFrequency;
			_lastLocationScreen = memoryEntry._lastLocationScreen;
			_lastSizeScreen = memoryEntry._lastSizeScreen;
			_lastLocationMultiSelect = memoryEntry._lastLocationMultiSelect;
			_lastSizeMultiSelect = memoryEntry._lastSizeMultiSelect;
		}
	}
}

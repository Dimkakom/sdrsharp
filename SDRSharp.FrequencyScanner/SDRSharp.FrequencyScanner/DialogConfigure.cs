using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.FrequencyScanner
{
	public class DialogConfigure : Form
	{
		private FrequencyScannerPanel _frequencyScannerPanel;

		private IContainer components;

		private CheckBox autoSkipCheckBox;

		private CheckBox autoLockCheckBox;

		private CheckBox autoClearCheckBox;

		private NumericUpDown LockIntervalNumericUpDown;

		private NumericUpDown activityNumericUpDown;

		private NumericUpDown autoClearIntervalNumericUpDown;

		private NumericUpDown skipIntervalNumericUpDown;

		private Label label1;

		private Label label2;

		private Button okButton;

		private Label label3;

		private Label label4;

		private Label label5;

		private CheckBox channelDetectMetodCheckBox;

		private CheckBox useMuteCheckBox;

		private NumericUpDown unMuteNumericUpDown;

		private Label label7;

		private CheckBox maximumChannelCheckBox;

		private GroupBox groupBox2;

		private CheckBox debugCheckBox;

		private Label label8;

		private NumericUpDown maxAlphaNumericUpDown;

		private NumericUpDown minAlphaNumericUpDown;

		private Label label6;

		private Label label9;

		private Label label10;

		private ComboBox pluginPositionComboBox;

		public DialogConfigure(FrequencyScannerPanel frequencyScannerPanel)
		{
			InitializeComponent();
			_frequencyScannerPanel = frequencyScannerPanel;
			autoSkipCheckBox.Checked = _frequencyScannerPanel.AutoSkipEnabled;
			skipIntervalNumericUpDown.Value = _frequencyScannerPanel.AutoSkipInterval;
			autoLockCheckBox.Checked = _frequencyScannerPanel.AutoLockEnabled;
			LockIntervalNumericUpDown.Value = _frequencyScannerPanel.AutoLockInterval;
			autoClearCheckBox.Checked = _frequencyScannerPanel.AutoClearEnabled;
			activityNumericUpDown.Value = (decimal)_frequencyScannerPanel.AutoClearActivityLevel;
			autoClearIntervalNumericUpDown.Value = _frequencyScannerPanel.AutoClearInterval;
			channelDetectMetodCheckBox.Checked = _frequencyScannerPanel.ChannelDetectMetod;
			useMuteCheckBox.Checked = _frequencyScannerPanel.UseMute;
			unMuteNumericUpDown.Value = _frequencyScannerPanel.UnMuteDelay;
			maximumChannelCheckBox.Checked = _frequencyScannerPanel.MaxLevelSelect;
			debugCheckBox.Checked = _frequencyScannerPanel.ShowDebugInfo;
			maxAlphaNumericUpDown.Value = _frequencyScannerPanel.MaxButtonsAlpha;
			minAlphaNumericUpDown.Value = _frequencyScannerPanel.MinButtonsAlpha;
			pluginPositionComboBox.SelectedIndex = _frequencyScannerPanel.ScannerPluginPosition;
		}

		private void okButton_Click(object sender, EventArgs e)
		{
			_frequencyScannerPanel.AutoSkipEnabled = autoSkipCheckBox.Checked;
			_frequencyScannerPanel.AutoSkipInterval = (int)skipIntervalNumericUpDown.Value;
			_frequencyScannerPanel.AutoLockEnabled = autoLockCheckBox.Checked;
			_frequencyScannerPanel.AutoLockInterval = (int)LockIntervalNumericUpDown.Value;
			_frequencyScannerPanel.AutoClearEnabled = autoClearCheckBox.Checked;
			_frequencyScannerPanel.AutoClearActivityLevel = (float)activityNumericUpDown.Value;
			_frequencyScannerPanel.AutoClearInterval = (int)autoClearIntervalNumericUpDown.Value;
			_frequencyScannerPanel.ChannelDetectMetod = channelDetectMetodCheckBox.Checked;
			_frequencyScannerPanel.UseMute = useMuteCheckBox.Checked;
			_frequencyScannerPanel.UnMuteDelay = (int)unMuteNumericUpDown.Value;
			_frequencyScannerPanel.MaxLevelSelect = maximumChannelCheckBox.Checked;
			_frequencyScannerPanel.MaxButtonsAlpha = (int)maxAlphaNumericUpDown.Value;
			_frequencyScannerPanel.MinButtonsAlpha = (int)minAlphaNumericUpDown.Value;
			_frequencyScannerPanel.ScannerPluginPosition = pluginPositionComboBox.SelectedIndex;
		}

		private void useMuteCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_frequencyScannerPanel.UseMute = useMuteCheckBox.Checked;
			unMuteNumericUpDown.Enabled = useMuteCheckBox.Checked;
		}

		private void unMuteNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_frequencyScannerPanel.UnMuteDelay = (int)unMuteNumericUpDown.Value;
		}

		private void debugCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_frequencyScannerPanel.ShowDebugInfo = debugCheckBox.Checked;
		}

		private void minAlphaNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_frequencyScannerPanel.MinButtonsAlpha = (int)minAlphaNumericUpDown.Value;
		}

		private void maxAlphaNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_frequencyScannerPanel.MaxButtonsAlpha = (int)maxAlphaNumericUpDown.Value;
		}

		private void label10_Click(object sender, EventArgs e)
		{
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			autoSkipCheckBox = new System.Windows.Forms.CheckBox();
			autoLockCheckBox = new System.Windows.Forms.CheckBox();
			autoClearCheckBox = new System.Windows.Forms.CheckBox();
			LockIntervalNumericUpDown = new System.Windows.Forms.NumericUpDown();
			activityNumericUpDown = new System.Windows.Forms.NumericUpDown();
			autoClearIntervalNumericUpDown = new System.Windows.Forms.NumericUpDown();
			skipIntervalNumericUpDown = new System.Windows.Forms.NumericUpDown();
			label1 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			okButton = new System.Windows.Forms.Button();
			label3 = new System.Windows.Forms.Label();
			label4 = new System.Windows.Forms.Label();
			label5 = new System.Windows.Forms.Label();
			channelDetectMetodCheckBox = new System.Windows.Forms.CheckBox();
			useMuteCheckBox = new System.Windows.Forms.CheckBox();
			unMuteNumericUpDown = new System.Windows.Forms.NumericUpDown();
			label7 = new System.Windows.Forms.Label();
			maximumChannelCheckBox = new System.Windows.Forms.CheckBox();
			groupBox2 = new System.Windows.Forms.GroupBox();
			label9 = new System.Windows.Forms.Label();
			label8 = new System.Windows.Forms.Label();
			maxAlphaNumericUpDown = new System.Windows.Forms.NumericUpDown();
			minAlphaNumericUpDown = new System.Windows.Forms.NumericUpDown();
			label6 = new System.Windows.Forms.Label();
			debugCheckBox = new System.Windows.Forms.CheckBox();
			label10 = new System.Windows.Forms.Label();
			pluginPositionComboBox = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)LockIntervalNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)activityNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)autoClearIntervalNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)skipIntervalNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)unMuteNumericUpDown).BeginInit();
			groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)maxAlphaNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)minAlphaNumericUpDown).BeginInit();
			SuspendLayout();
			autoSkipCheckBox.AutoSize = true;
			autoSkipCheckBox.Location = new System.Drawing.Point(6, 19);
			autoSkipCheckBox.Name = "autoSkipCheckBox";
			autoSkipCheckBox.Size = new System.Drawing.Size(70, 17);
			autoSkipCheckBox.TabIndex = 0;
			autoSkipCheckBox.Text = "Auto skip";
			autoSkipCheckBox.UseVisualStyleBackColor = true;
			autoLockCheckBox.AutoSize = true;
			autoLockCheckBox.Location = new System.Drawing.Point(6, 42);
			autoLockCheckBox.Name = "autoLockCheckBox";
			autoLockCheckBox.Size = new System.Drawing.Size(71, 17);
			autoLockCheckBox.TabIndex = 1;
			autoLockCheckBox.Text = "Auto lock";
			autoLockCheckBox.UseVisualStyleBackColor = true;
			autoClearCheckBox.AutoSize = true;
			autoClearCheckBox.Location = new System.Drawing.Point(6, 65);
			autoClearCheckBox.Name = "autoClearCheckBox";
			autoClearCheckBox.Size = new System.Drawing.Size(15, 14);
			autoClearCheckBox.TabIndex = 2;
			autoClearCheckBox.UseVisualStyleBackColor = true;
			LockIntervalNumericUpDown.Location = new System.Drawing.Point(82, 41);
			LockIntervalNumericUpDown.Maximum = new decimal(new int[4]
			{
				3600,
				0,
				0,
				0
			});
			LockIntervalNumericUpDown.Minimum = new decimal(new int[4]
			{
				1,
				0,
				0,
				0
			});
			LockIntervalNumericUpDown.Name = "LockIntervalNumericUpDown";
			LockIntervalNumericUpDown.Size = new System.Drawing.Size(55, 20);
			LockIntervalNumericUpDown.TabIndex = 3;
			LockIntervalNumericUpDown.Value = new decimal(new int[4]
			{
				30,
				0,
				0,
				0
			});
			activityNumericUpDown.DecimalPlaces = 1;
			activityNumericUpDown.Increment = new decimal(new int[4]
			{
				1,
				0,
				0,
				65536
			});
			activityNumericUpDown.Location = new System.Drawing.Point(166, 63);
			activityNumericUpDown.Maximum = new decimal(new int[4]
			{
				1000,
				0,
				0,
				0
			});
			activityNumericUpDown.Minimum = new decimal(new int[4]
			{
				1,
				0,
				0,
				65536
			});
			activityNumericUpDown.Name = "activityNumericUpDown";
			activityNumericUpDown.Size = new System.Drawing.Size(47, 20);
			activityNumericUpDown.TabIndex = 4;
			activityNumericUpDown.Value = new decimal(new int[4]
			{
				1,
				0,
				0,
				0
			});
			autoClearIntervalNumericUpDown.Location = new System.Drawing.Point(258, 63);
			autoClearIntervalNumericUpDown.Maximum = new decimal(new int[4]
			{
				1000,
				0,
				0,
				0
			});
			autoClearIntervalNumericUpDown.Minimum = new decimal(new int[4]
			{
				1,
				0,
				0,
				0
			});
			autoClearIntervalNumericUpDown.Name = "autoClearIntervalNumericUpDown";
			autoClearIntervalNumericUpDown.Size = new System.Drawing.Size(43, 20);
			autoClearIntervalNumericUpDown.TabIndex = 5;
			autoClearIntervalNumericUpDown.Value = new decimal(new int[4]
			{
				1,
				0,
				0,
				0
			});
			skipIntervalNumericUpDown.Location = new System.Drawing.Point(82, 18);
			skipIntervalNumericUpDown.Maximum = new decimal(new int[4]
			{
				3600,
				0,
				0,
				0
			});
			skipIntervalNumericUpDown.Minimum = new decimal(new int[4]
			{
				1,
				0,
				0,
				0
			});
			skipIntervalNumericUpDown.Name = "skipIntervalNumericUpDown";
			skipIntervalNumericUpDown.Size = new System.Drawing.Size(55, 20);
			skipIntervalNumericUpDown.TabIndex = 6;
			skipIntervalNumericUpDown.Value = new decimal(new int[4]
			{
				10,
				0,
				0,
				0
			});
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(27, 65);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(133, 13);
			label1.TabIndex = 7;
			label1.Text = "Delete rows with activity  <";
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(219, 65);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(33, 13);
			label2.TabIndex = 8;
			label2.Text = "every";
			okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			okButton.Location = new System.Drawing.Point(304, 253);
			okButton.Name = "okButton";
			okButton.Size = new System.Drawing.Size(75, 23);
			okButton.TabIndex = 9;
			okButton.Text = "Ok";
			okButton.UseVisualStyleBackColor = true;
			okButton.Click += new System.EventHandler(okButton_Click);
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(143, 20);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(47, 13);
			label3.TabIndex = 10;
			label3.Text = "seconds";
			label4.AutoSize = true;
			label4.Location = new System.Drawing.Point(144, 43);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(47, 13);
			label4.TabIndex = 11;
			label4.Text = "seconds";
			label5.AutoSize = true;
			label5.Location = new System.Drawing.Point(307, 65);
			label5.Name = "label5";
			label5.Size = new System.Drawing.Size(47, 13);
			label5.TabIndex = 12;
			label5.Text = "seconds";
			channelDetectMetodCheckBox.AutoSize = true;
			channelDetectMetodCheckBox.Location = new System.Drawing.Point(6, 89);
			channelDetectMetodCheckBox.Name = "channelDetectMetodCheckBox";
			channelDetectMetodCheckBox.Size = new System.Drawing.Size(310, 17);
			channelDetectMetodCheckBox.TabIndex = 13;
			channelDetectMetodCheckBox.Text = "Detect channel only center frequency (default all bandwidth)";
			channelDetectMetodCheckBox.UseVisualStyleBackColor = true;
			useMuteCheckBox.AutoSize = true;
			useMuteCheckBox.Location = new System.Drawing.Point(6, 135);
			useMuteCheckBox.Name = "useMuteCheckBox";
			useMuteCheckBox.Size = new System.Drawing.Size(100, 17);
			useMuteCheckBox.TabIndex = 16;
			useMuteCheckBox.Text = "Use audio mute";
			useMuteCheckBox.UseVisualStyleBackColor = true;
			useMuteCheckBox.CheckedChanged += new System.EventHandler(useMuteCheckBox_CheckedChanged);
			unMuteNumericUpDown.Location = new System.Drawing.Point(230, 134);
			unMuteNumericUpDown.Maximum = new decimal(new int[4]
			{
				20,
				0,
				0,
				0
			});
			unMuteNumericUpDown.Name = "unMuteNumericUpDown";
			unMuteNumericUpDown.Size = new System.Drawing.Size(48, 20);
			unMuteNumericUpDown.TabIndex = 17;
			unMuteNumericUpDown.Value = new decimal(new int[4]
			{
				5,
				0,
				0,
				0
			});
			unMuteNumericUpDown.ValueChanged += new System.EventHandler(unMuteNumericUpDown_ValueChanged);
			label7.AutoSize = true;
			label7.Location = new System.Drawing.Point(112, 136);
			label7.Name = "label7";
			label7.Size = new System.Drawing.Size(112, 13);
			label7.TabIndex = 18;
			label7.Text = "Noise protection delay";
			maximumChannelCheckBox.AutoSize = true;
			maximumChannelCheckBox.Location = new System.Drawing.Point(6, 112);
			maximumChannelCheckBox.Name = "maximumChannelCheckBox";
			maximumChannelCheckBox.Size = new System.Drawing.Size(190, 17);
			maximumChannelCheckBox.TabIndex = 23;
			maximumChannelCheckBox.Text = "Select channel with maximum level";
			maximumChannelCheckBox.UseVisualStyleBackColor = true;
			groupBox2.Controls.Add(label10);
			groupBox2.Controls.Add(label9);
			groupBox2.Controls.Add(pluginPositionComboBox);
			groupBox2.Controls.Add(label8);
			groupBox2.Controls.Add(maxAlphaNumericUpDown);
			groupBox2.Controls.Add(minAlphaNumericUpDown);
			groupBox2.Controls.Add(label6);
			groupBox2.Controls.Add(debugCheckBox);
			groupBox2.Controls.Add(autoSkipCheckBox);
			groupBox2.Controls.Add(autoLockCheckBox);
			groupBox2.Controls.Add(channelDetectMetodCheckBox);
			groupBox2.Controls.Add(maximumChannelCheckBox);
			groupBox2.Controls.Add(autoClearCheckBox);
			groupBox2.Controls.Add(useMuteCheckBox);
			groupBox2.Controls.Add(LockIntervalNumericUpDown);
			groupBox2.Controls.Add(unMuteNumericUpDown);
			groupBox2.Controls.Add(activityNumericUpDown);
			groupBox2.Controls.Add(label4);
			groupBox2.Controls.Add(label7);
			groupBox2.Controls.Add(autoClearIntervalNumericUpDown);
			groupBox2.Controls.Add(label2);
			groupBox2.Controls.Add(label5);
			groupBox2.Controls.Add(label1);
			groupBox2.Controls.Add(skipIntervalNumericUpDown);
			groupBox2.Controls.Add(label3);
			groupBox2.Location = new System.Drawing.Point(12, 12);
			groupBox2.Name = "groupBox2";
			groupBox2.Size = new System.Drawing.Size(370, 235);
			groupBox2.TabIndex = 13;
			groupBox2.TabStop = false;
			groupBox2.Text = "Scanner settings";
			label9.AutoSize = true;
			label9.Location = new System.Drawing.Point(80, 182);
			label9.Name = "label9";
			label9.Size = new System.Drawing.Size(26, 13);
			label9.TabIndex = 31;
			label9.Text = "min.";
			label8.AutoSize = true;
			label8.Location = new System.Drawing.Point(6, 182);
			label8.Name = "label8";
			label8.Size = new System.Drawing.Size(72, 13);
			label8.TabIndex = 30;
			label8.Text = "Buttons alpha";
			maxAlphaNumericUpDown.Location = new System.Drawing.Point(208, 180);
			maxAlphaNumericUpDown.Maximum = new decimal(new int[4]
			{
				255,
				0,
				0,
				0
			});
			maxAlphaNumericUpDown.Minimum = new decimal(new int[4]
			{
				1,
				0,
				0,
				0
			});
			maxAlphaNumericUpDown.Name = "maxAlphaNumericUpDown";
			maxAlphaNumericUpDown.Size = new System.Drawing.Size(55, 20);
			maxAlphaNumericUpDown.TabIndex = 27;
			maxAlphaNumericUpDown.Value = new decimal(new int[4]
			{
				150,
				0,
				0,
				0
			});
			maxAlphaNumericUpDown.ValueChanged += new System.EventHandler(maxAlphaNumericUpDown_ValueChanged);
			minAlphaNumericUpDown.Location = new System.Drawing.Point(112, 180);
			minAlphaNumericUpDown.Maximum = new decimal(new int[4]
			{
				255,
				0,
				0,
				0
			});
			minAlphaNumericUpDown.Minimum = new decimal(new int[4]
			{
				1,
				0,
				0,
				0
			});
			minAlphaNumericUpDown.Name = "minAlphaNumericUpDown";
			minAlphaNumericUpDown.Size = new System.Drawing.Size(55, 20);
			minAlphaNumericUpDown.TabIndex = 28;
			minAlphaNumericUpDown.Value = new decimal(new int[4]
			{
				10,
				0,
				0,
				0
			});
			minAlphaNumericUpDown.ValueChanged += new System.EventHandler(minAlphaNumericUpDown_ValueChanged);
			label6.AutoSize = true;
			label6.Location = new System.Drawing.Point(173, 182);
			label6.Name = "label6";
			label6.Size = new System.Drawing.Size(29, 13);
			label6.TabIndex = 29;
			label6.Text = "max.";
			debugCheckBox.AutoSize = true;
			debugCheckBox.Location = new System.Drawing.Point(6, 158);
			debugCheckBox.Name = "debugCheckBox";
			debugCheckBox.Size = new System.Drawing.Size(106, 17);
			debugCheckBox.TabIndex = 24;
			debugCheckBox.Text = "Show debug info";
			debugCheckBox.UseVisualStyleBackColor = true;
			debugCheckBox.CheckedChanged += new System.EventHandler(debugCheckBox_CheckedChanged);
			label10.AutoSize = true;
			label10.Location = new System.Drawing.Point(6, 209);
			label10.Name = "label10";
			label10.Size = new System.Drawing.Size(87, 13);
			label10.TabIndex = 37;
			label10.Text = "Panview position";
			label10.Click += new System.EventHandler(label10_Click);
			pluginPositionComboBox.AutoCompleteCustomSource.AddRange(new string[3]
			{
				"Plugin panel",
				"Main screen left",
				"Main screen right"
			});
			pluginPositionComboBox.FormattingEnabled = true;
			pluginPositionComboBox.Items.AddRange(new object[4]
			{
				"Main screen up",
				"Main screen down",
				"Main screen left",
				"Main screen right"
			});
			pluginPositionComboBox.Location = new System.Drawing.Point(97, 206);
			pluginPositionComboBox.Name = "pluginPositionComboBox";
			pluginPositionComboBox.Size = new System.Drawing.Size(105, 21);
			pluginPositionComboBox.TabIndex = 36;
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			AutoSize = true;
			base.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			base.ClientSize = new System.Drawing.Size(391, 285);
			base.ControlBox = false;
			base.Controls.Add(groupBox2);
			base.Controls.Add(okButton);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			base.Name = "DialogConfigure";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			Text = "Configure scanner";
			((System.ComponentModel.ISupportInitialize)LockIntervalNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)activityNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)autoClearIntervalNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)skipIntervalNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)unMuteNumericUpDown).EndInit();
			groupBox2.ResumeLayout(performLayout: false);
			groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)maxAlphaNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)minAlphaNumericUpDown).EndInit();
			ResumeLayout(performLayout: false);
		}
	}
}

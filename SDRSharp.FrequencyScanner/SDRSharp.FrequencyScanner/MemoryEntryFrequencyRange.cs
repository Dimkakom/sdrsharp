using SDRSharp.Radio;
using System.Drawing;

namespace SDRSharp.FrequencyScanner
{
	public class MemoryEntryFrequencyRange
	{
		private long _startFrequency;

		private long _endFrequency;

		private string _rangeName;

		private DetectorType _detectorType;

		private int _stepSize;

		private int _filterBandwidth;

		private Point _lastLocation;

		private Size _lastSize;

		public int StepSize
		{
			get
			{
				return _stepSize;
			}
			set
			{
				_stepSize = value;
			}
		}

		public int FilterBandwidth
		{
			get
			{
				return _filterBandwidth;
			}
			set
			{
				_filterBandwidth = value;
			}
		}

		public Size LastSize
		{
			get
			{
				return _lastSize;
			}
			set
			{
				_lastSize = value;
			}
		}

		public Point LastLocation
		{
			get
			{
				return _lastLocation;
			}
			set
			{
				_lastLocation = value;
			}
		}

		public long StartFrequency
		{
			get
			{
				return _startFrequency;
			}
			set
			{
				_startFrequency = value;
			}
		}

		public long EndFrequency
		{
			get
			{
				return _endFrequency;
			}
			set
			{
				_endFrequency = value;
			}
		}

		public string RangeName
		{
			get
			{
				return _rangeName;
			}
			set
			{
				_rangeName = value;
			}
		}

		public DetectorType RangeDetectorType
		{
			get
			{
				return _detectorType;
			}
			set
			{
				_detectorType = value;
			}
		}

		public MemoryEntryFrequencyRange()
		{
		}

		public MemoryEntryFrequencyRange(MemoryEntryFrequencyRange memoryEntryFrequencyRange)
		{
			_startFrequency = memoryEntryFrequencyRange._startFrequency;
			_endFrequency = memoryEntryFrequencyRange._endFrequency;
			_rangeName = memoryEntryFrequencyRange._rangeName;
			_detectorType = memoryEntryFrequencyRange._detectorType;
			_lastLocation = memoryEntryFrequencyRange._lastLocation;
			_lastSize = memoryEntryFrequencyRange._lastSize;
			_stepSize = memoryEntryFrequencyRange._stepSize;
			_filterBandwidth = memoryEntryFrequencyRange._filterBandwidth;
		}
	}
}

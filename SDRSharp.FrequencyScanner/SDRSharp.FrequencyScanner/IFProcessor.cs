using SDRSharp.Radio;

namespace SDRSharp.FrequencyScanner
{
	public class IFProcessor : IIQProcessor, IStreamProcessor, IBaseProcessor
	{
		public unsafe delegate void IQReadyDelegate(Complex* buffer, int length);

		private double _sampleRate;

		private bool _enabled;

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				_sampleRate = value;
			}
		}

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public event IQReadyDelegate IQReady;

		public unsafe void Process(Complex* buffer, int length)
		{
			if (this.IQReady != null)
			{
				this.IQReady(buffer, length);
			}
		}
	}
}

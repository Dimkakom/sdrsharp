using System.Collections.Generic;
using System.ComponentModel;

namespace SDRSharp.FrequencyScanner
{
	public class SortableBindingList<T> : BindingList<T>
	{
		private bool _isSorted;

		private PropertyDescriptor _sortProperty;

		private ListSortDirection _sortDirection;

		protected override bool SupportsSortingCore => true;

		protected override ListSortDirection SortDirectionCore => _sortDirection;

		protected override PropertyDescriptor SortPropertyCore => _sortProperty;

		protected override bool IsSortedCore => _isSorted;

		protected override void ApplySortCore(PropertyDescriptor property, ListSortDirection direction)
		{
			List<T> list = (List<T>)base.Items;
			if (list != null)
			{
				SortableBindingListComparer<T> comparer = new SortableBindingListComparer<T>(property.Name, direction);
				list.Sort(comparer);
				_isSorted = true;
			}
			else
			{
				_isSorted = false;
			}
			_sortProperty = property;
			_sortDirection = direction;
			OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
		}
	}
}

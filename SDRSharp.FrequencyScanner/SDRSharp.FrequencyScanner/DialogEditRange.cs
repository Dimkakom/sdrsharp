using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.FrequencyScanner
{
	public class DialogEditRange : Form
	{
		private List<MemoryEntryFrequencyRange> _memoryEntryFrequencyRange;

		private readonly SortableBindingList<MemoryEntryFrequencyRange> _displayedEntries = new SortableBindingList<MemoryEntryFrequencyRange>();

		private IContainer components;

		private DataGridView editRangeDataGridView;

		private Button cancelButton;

		private Button okButton;

		private Button deleteRangeButton;

		private BindingSource editRangeBindingSource;

		private DataGridViewTextBoxColumn rangeNameDataGridViewTextBoxColumn;

		private DataGridViewTextBoxColumn startFrequencyDataGridViewTextBoxColumn;

		private DataGridViewTextBoxColumn endFrequencyDataGridViewTextBoxColumn;

		private DataGridViewTextBoxColumn rangeDetectorTypeDataGridViewTextBoxColumn;

		private DataGridViewTextBoxColumn FilterBandwidth;

		private DataGridViewTextBoxColumn StepSize;

		public DialogEditRange()
		{
			InitializeComponent();
			editRangeBindingSource.DataSource = _displayedEntries;
			editRangeDataGridView.CellEndEdit += ValidateForm;
			editRangeDataGridView.CellBeginEdit += FormBegiEdit;
		}

		private void FormBegiEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			okButton.Enabled = false;
		}

		public List<MemoryEntryFrequencyRange> EditRange(List<MemoryEntryFrequencyRange> memoryEntry)
		{
			_memoryEntryFrequencyRange = memoryEntry;
			if (_memoryEntryFrequencyRange != null)
			{
				foreach (MemoryEntryFrequencyRange item in _memoryEntryFrequencyRange)
				{
					_displayedEntries.Add(item);
				}
			}
			return _memoryEntryFrequencyRange;
		}

		private void editRangeDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
		}

		private string GetFrequencyDisplay(long frequency)
		{
			long num = Math.Abs(frequency);
			if (num == 0L)
			{
				return "DC";
			}
			if (num > 1500000000)
			{
				return $"{(double)frequency / 1000000000.0:#,0.000 000} GHz";
			}
			if (num > 30000000)
			{
				return $"{(double)frequency / 1000000.0:0,0.000###} MHz";
			}
			if (num > 1000)
			{
				return $"{(double)frequency / 1000.0:#,#.###} kHz";
			}
			return frequency.ToString();
		}

		private void okButton_Click(object sender, EventArgs e)
		{
			if (editRangeBindingSource != null)
			{
				_memoryEntryFrequencyRange.Clear();
				foreach (MemoryEntryFrequencyRange item in editRangeBindingSource)
				{
					_memoryEntryFrequencyRange.Add(item);
				}
			}
			base.DialogResult = DialogResult.OK;
		}

		private void ValidateForm(object sender, DataGridViewCellEventArgs e)
		{
			bool flag = true;
			foreach (MemoryEntryFrequencyRange item in editRangeBindingSource)
			{
				int row = editRangeBindingSource.IndexOf(item);
				bool flag2 = item.RangeName != null && !"".Equals(item.RangeName.Trim());
				IndicateErrorCells(0, row, flag2);
				bool flag3 = item.StartFrequency < item.EndFrequency && item.StartFrequency != 0L && item.EndFrequency != 0;
				IndicateErrorCells(1, row, flag3);
				IndicateErrorCells(2, row, flag3);
				bool flag4 = item.FilterBandwidth != 0;
				IndicateErrorCells(4, row, flag4);
				bool flag5 = item.StepSize != 0;
				IndicateErrorCells(5, row, flag5);
				flag = ((flag && flag2) & flag3 & flag4 & flag5);
			}
			okButton.Enabled = flag;
		}

		private void IndicateErrorCells(int collumn, int row, bool valid)
		{
			if (valid)
			{
				editRangeDataGridView[collumn, row].Style.BackColor = editRangeDataGridView.DefaultCellStyle.BackColor;
			}
			else
			{
				editRangeDataGridView[collumn, row].Style.BackColor = Color.LightPink;
			}
		}

		private void cancelButton_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
		}

		private void deleteRangeButton_Click(object sender, EventArgs e)
		{
			if (editRangeBindingSource.IndexOf(editRangeBindingSource.Current) > -1)
			{
				editRangeBindingSource.RemoveCurrent();
				ValidateForm(null, null);
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			editRangeDataGridView = new System.Windows.Forms.DataGridView();
			rangeNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			startFrequencyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			endFrequencyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			rangeDetectorTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			FilterBandwidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
			StepSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
			editRangeBindingSource = new System.Windows.Forms.BindingSource(components);
			cancelButton = new System.Windows.Forms.Button();
			okButton = new System.Windows.Forms.Button();
			deleteRangeButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)editRangeDataGridView).BeginInit();
			((System.ComponentModel.ISupportInitialize)editRangeBindingSource).BeginInit();
			SuspendLayout();
			editRangeDataGridView.AutoGenerateColumns = false;
			editRangeDataGridView.Columns.AddRange(rangeNameDataGridViewTextBoxColumn, startFrequencyDataGridViewTextBoxColumn, endFrequencyDataGridViewTextBoxColumn, rangeDetectorTypeDataGridViewTextBoxColumn, FilterBandwidth, StepSize);
			editRangeDataGridView.DataSource = editRangeBindingSource;
			editRangeDataGridView.Location = new System.Drawing.Point(15, 15);
			editRangeDataGridView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			editRangeDataGridView.MultiSelect = false;
			editRangeDataGridView.Name = "editRangeDataGridView";
			editRangeDataGridView.RowHeadersVisible = false;
			editRangeDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			editRangeDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			editRangeDataGridView.ShowEditingIcon = false;
			editRangeDataGridView.ShowRowErrors = false;
			editRangeDataGridView.Size = new System.Drawing.Size(912, 351);
			editRangeDataGridView.TabIndex = 0;
			rangeNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			rangeNameDataGridViewTextBoxColumn.DataPropertyName = "RangeName";
			dataGridViewCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			rangeNameDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle;
			rangeNameDataGridViewTextBoxColumn.HeaderText = "Name";
			rangeNameDataGridViewTextBoxColumn.MinimumWidth = 110;
			rangeNameDataGridViewTextBoxColumn.Name = "rangeNameDataGridViewTextBoxColumn";
			startFrequencyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			startFrequencyDataGridViewTextBoxColumn.DataPropertyName = "StartFrequency";
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle2.Format = "N0";
			dataGridViewCellStyle2.NullValue = null;
			startFrequencyDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
			startFrequencyDataGridViewTextBoxColumn.HeaderText = "Start";
			startFrequencyDataGridViewTextBoxColumn.MinimumWidth = 80;
			startFrequencyDataGridViewTextBoxColumn.Name = "startFrequencyDataGridViewTextBoxColumn";
			startFrequencyDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			endFrequencyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			endFrequencyDataGridViewTextBoxColumn.DataPropertyName = "EndFrequency";
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle3.Format = "N0";
			dataGridViewCellStyle3.NullValue = null;
			endFrequencyDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
			endFrequencyDataGridViewTextBoxColumn.HeaderText = "End";
			endFrequencyDataGridViewTextBoxColumn.MinimumWidth = 80;
			endFrequencyDataGridViewTextBoxColumn.Name = "endFrequencyDataGridViewTextBoxColumn";
			rangeDetectorTypeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			rangeDetectorTypeDataGridViewTextBoxColumn.DataPropertyName = "RangeDetectorType";
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle4.NullValue = null;
			rangeDetectorTypeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
			rangeDetectorTypeDataGridViewTextBoxColumn.FillWeight = 10f;
			rangeDetectorTypeDataGridViewTextBoxColumn.HeaderText = "Detector";
			rangeDetectorTypeDataGridViewTextBoxColumn.MinimumWidth = 60;
			rangeDetectorTypeDataGridViewTextBoxColumn.Name = "rangeDetectorTypeDataGridViewTextBoxColumn";
			rangeDetectorTypeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			rangeDetectorTypeDataGridViewTextBoxColumn.Width = 60;
			FilterBandwidth.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			FilterBandwidth.DataPropertyName = "FilterBandwidth";
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle5.Format = "N0";
			dataGridViewCellStyle5.NullValue = null;
			FilterBandwidth.DefaultCellStyle = dataGridViewCellStyle5;
			FilterBandwidth.HeaderText = "Bandwidth";
			FilterBandwidth.Name = "FilterBandwidth";
			FilterBandwidth.Width = 80;
			StepSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			StepSize.DataPropertyName = "StepSize";
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle6.Format = "N0";
			dataGridViewCellStyle6.NullValue = null;
			StepSize.DefaultCellStyle = dataGridViewCellStyle6;
			StepSize.HeaderText = "Step size";
			StepSize.Name = "StepSize";
			StepSize.Width = 80;
			editRangeBindingSource.DataSource = typeof(SDRSharp.FrequencyScanner.MemoryEntryFrequencyRange);
			cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			cancelButton.Location = new System.Drawing.Point(833, 372);
			cancelButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			cancelButton.Name = "cancelButton";
			cancelButton.Size = new System.Drawing.Size(100, 28);
			cancelButton.TabIndex = 1;
			cancelButton.Text = "Cancel";
			cancelButton.UseVisualStyleBackColor = true;
			cancelButton.Click += new System.EventHandler(cancelButton_Click);
			okButton.Location = new System.Drawing.Point(725, 372);
			okButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			okButton.Name = "okButton";
			okButton.Size = new System.Drawing.Size(100, 28);
			okButton.TabIndex = 2;
			okButton.Text = "Ok";
			okButton.UseVisualStyleBackColor = true;
			okButton.Click += new System.EventHandler(okButton_Click);
			deleteRangeButton.Location = new System.Drawing.Point(15, 370);
			deleteRangeButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			deleteRangeButton.Name = "deleteRangeButton";
			deleteRangeButton.Size = new System.Drawing.Size(100, 28);
			deleteRangeButton.TabIndex = 3;
			deleteRangeButton.Text = "Delete rows";
			deleteRangeButton.UseVisualStyleBackColor = true;
			deleteRangeButton.Click += new System.EventHandler(deleteRangeButton_Click);
			base.AcceptButton = okButton;
			base.AutoScaleDimensions = new System.Drawing.SizeF(8f, 16f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			AutoSize = true;
			base.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			BackColor = System.Drawing.SystemColors.Control;
			base.CancelButton = cancelButton;
			base.ClientSize = new System.Drawing.Size(932, 407);
			base.ControlBox = false;
			base.Controls.Add(deleteRangeButton);
			base.Controls.Add(okButton);
			base.Controls.Add(cancelButton);
			base.Controls.Add(editRangeDataGridView);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			base.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "DialogEditRange";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			Text = "Edit Range";
			((System.ComponentModel.ISupportInitialize)editRangeDataGridView).EndInit();
			((System.ComponentModel.ISupportInitialize)editRangeBindingSource).EndInit();
			ResumeLayout(performLayout: false);
		}
	}
}

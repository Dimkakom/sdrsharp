using SDRSharp.Common;
using SDRSharp.Radio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace SDRSharp.FrequencyScanner
{
	public class FrequencyScannerPanel : UserControl
	{
		private enum ScanState
		{
			SetScreen,
			PauseToSet,
			ScanScreen,
			ScanStop
		}

		private class CenterFrequencyEntry
		{
			public long CenterFrequency
			{
				get;
				set;
			}

			public int StartFrequencyIndex
			{
				get;
				set;
			}

			public int EndFrequencyIndex
			{
				get;
				set;
			}
		}

		private class IControlParameters
		{
			private bool _audioIsMuted;

			private bool _audioNeedUpdate;

			private long _centerFrequency;

			private bool _centerNeedUpdate;

			private long _frequency;

			private bool _frequencyNeedUpdate;

			private DetectorType _detectorType;

			private bool _detectorNeedUpdate;

			private int _filterBandwidth;

			private bool _filterNeedUpdate;

			private int _rfBandwidth;

			private bool _isChanged;

			public bool AudioIsMuted
			{
				get
				{
					return _audioIsMuted;
				}
				set
				{
					_audioIsMuted = value;
					_audioNeedUpdate = true;
					_isChanged = true;
				}
			}

			public bool AudioNeedUpdate => _audioNeedUpdate;

			public long CenterFrequency
			{
				get
				{
					return _centerFrequency;
				}
				set
				{
					_centerFrequency = value;
					_centerNeedUpdate = true;
					_isChanged = true;
				}
			}

			public bool CenterNeedUpdate => _centerNeedUpdate;

			public long Frequency
			{
				get
				{
					return _frequency;
				}
				set
				{
					_frequency = value;
					_frequencyNeedUpdate = true;
					_isChanged = true;
				}
			}

			public bool FrequencyNeedUpdate => _frequencyNeedUpdate;

			public DetectorType DetectorType
			{
				get
				{
					return _detectorType;
				}
				set
				{
					_detectorType = value;
					_detectorNeedUpdate = true;
					_isChanged = true;
				}
			}

			public bool DetectorNeedUpdate => _detectorNeedUpdate;

			public int FilterBandwidth
			{
				get
				{
					return _filterBandwidth;
				}
				set
				{
					_filterBandwidth = value;
					_filterNeedUpdate = true;
					_isChanged = true;
				}
			}

			public bool FilterNeedUpdate => _filterNeedUpdate;

			public int RfBandwidth
			{
				get
				{
					return _rfBandwidth;
				}
				set
				{
					_rfBandwidth = value;
				}
			}

			public bool IsChanged => _isChanged;

			public void ResetFlags()
			{
				_audioNeedUpdate = false;
				_centerNeedUpdate = false;
				_detectorNeedUpdate = false;
				_filterNeedUpdate = false;
				_frequencyNeedUpdate = false;
				_isChanged = false;
			}
		}

		private enum Buttons
		{
			None,
			SNRUp,
			SNRDown,
			HystUp,
			HystDown
		}

		private ISharpControl _controlInterface;

		private IFProcessor _ifProcessor;

		private ChannelAnalyzer _channelAnalyzer;

		private readonly SortableBindingList<MemoryEntryNewFrequency> _displayedEntries = new SortableBindingList<MemoryEntryNewFrequency>();

		private readonly List<MemoryEntryNewFrequency> _entriesNewFrequency;

		private List<MemoryEntry> _entriesInManager;

		private List<MemoryEntryFrequencyRange> _enteriesFrequencyRange;

		private MemoryEntryNewSkipAndRangeFrequency _newSkipAndRange;

		private List<long> _skipFrequencyList;

		private readonly SettingsPersister _settingsPersister;

		private List<long> _newActiveFrequencies;

		private List<ChannelAnalizerMemoryEntry> _channelList;

		private int _currentStartIndex;

		private int _currentEndIndex;

		private int _currentScreenIndex;

		private bool _isPlayed;

		private bool _isActivePlaedFrequency;

		private bool _scanMemory;

		private bool _scanWidthStoreNew;

		private bool _scanExceptMemory;

		private ScanState _state;

		private float _interval;

		private float _pauseAfter;

		private bool _changeFrequencyInScanner;

		private bool _timeToClean;

		private System.Timers.Timer _autoSkipTimer;

		private System.Timers.Timer _autoLockTimer;

		private const int FFTBins = 32768;

		private int _fftBins;

		private UnsafeBuffer _fftBuffer;

		private unsafe Complex* _fftPtr;

		private UnsafeBuffer _fftWindow;

		private unsafe float* _fftWindowPtr;

		private UnsafeBuffer _unScaledFFTSpectrum;

		private unsafe float* _unScaledFFTSpectrumPtr;

		private UnsafeBuffer _scaledFFTSpectrum;

		private unsafe float* _scaledFFTSpectrumPtr;

		private readonly SharpEvent _bufferEvent = new SharpEvent(initialState: false);

		private readonly SharpEvent _scannerEvent = new SharpEvent(initialState: false);

		private float _timeConstant;

		private int _squelchCount;

		private int _count;

		private bool _scanProcessIsWork;

		private List<CenterFrequencyEntry> _centerFrequncyList;

		private float _usableSpectrum;

		private bool _requestToStopScanner;

		private bool _directionReverse;

		private List<int> _activeFrequencies;

		private IControlParameters _controlParameters = new IControlParameters();

		private int _writePos;

		private Thread _scannerThread;

		private bool _scannerRunning;

		private bool _needUpdateParametrs;

		private int _scanLevel;

		private int _hysteresis;

		private bool _pauseScan;

		private bool _nextButtonPress;

		private bool _direction;

		private int _detect;

		private int _pauseToNextScreen;

		private int _debugCounter;

		private int _debugTime;

		private double _bufferLengthInMs;

		private TuningStyle _tuningStyleTemp;

		private float _tuningLimitTemp;

		private bool _hotTrackNeeded;

		private bool _selectFrequency;

		private int _trackingX;

		private int _trackingY;

		private int _startSelectX;

		private int _endSelectX;

		private int _oldX;

		private int _trackingIndex;

		private long _trackingFrequency;

		private int _oldIndex;

		private long _oldFrequency;

		private int _startSelectIndex;

		private int _endSelectIndex;

		public bool AutoSkipEnabled;

		public int AutoSkipInterval;

		public bool AutoLockEnabled;

		public int AutoLockInterval;

		public bool AutoClearEnabled;

		public float AutoClearActivityLevel;

		public int AutoClearInterval;

		public bool ChannelDetectMetod;

		public bool UseMute;

		public int UnMuteDelay;

		public float UsableSpectrum;

		public bool MaxLevelSelect;

		public bool ShowDebugInfo;

		public int MaxButtonsAlpha;

		public int MinButtonsAlpha;

		public int ScannerPluginPosition;

		private Rectangle _reverseRectangle;

		private Rectangle _forwardRectangle;

		private Rectangle _pauseRectangle;

		private Rectangle _lockRectangle;

		private Rectangle _unlockRectangle;

		private Rectangle _snrPlusRectangle;

		private Rectangle _snrMinusRectangle;

		private Rectangle _histPlusRectangle;

		private Rectangle _histMinusRectangle;

		private int _bright;

		private string _infoText;

		private int _filterWidth;

		private float _iavg;

		private Buttons _buttons;

		private bool _frequencyIsChanged;

		private bool _centerFrequencyIsChanged;

		private IContainer components;

		private ToolStrip mainToolStrip;

		private ToolStripButton btnNewEntry;

		private ToolStripButton btnDelete;

		private DataGridView frequencyDataGridView;

		private ComboBox scanModeComboBox;

		private BindingSource memoryEntryBindingSource;

		private Button ScanButton;

		private Button FrequencyRangeEditButton;

		private System.Windows.Forms.Timer newFrequencyDisplayUpdateTimer;

		private Button configureButton;

		private System.Windows.Forms.Timer autoCleanTimer;

		private Label timeConstantLabel;

		private System.Windows.Forms.Timer testDisplayTimer;

		private DataGridViewTextBoxColumn Activity;

		private DataGridViewTextBoxColumn frequencyDataGridViewTextBoxColumn;

		private ListBox frequencyRangeListBox;

		private TableLayoutPanel tableLayoutPanel1;

		private System.Windows.Forms.Timer iControlsUpdateTimer;

		private System.Windows.Forms.Timer refreshTimer;

		private Label label1;

		private Label label2;

		private NumericUpDown detectNumericUpDown;

		private NumericUpDown waitNumericUpDown;

		private System.Windows.Forms.Timer buttonsRepeaterTimer;

		public unsafe FrequencyScannerPanel(ISharpControl control)
		{
			InitializeComponent();
			_controlInterface = control;
			_controlInterface.PropertyChanged += PropertyChangedHandler;
			_ifProcessor = new IFProcessor();
			_controlInterface.RegisterStreamHook(_ifProcessor, ProcessorType.RawIQ);
			_ifProcessor.Enabled = false;
			_ifProcessor.IQReady += FastBufferAvailable;
			_fftBins = 32768;
			InitFFTBuffers();
			BuildFFTWindow();
			_settingsPersister = new SettingsPersister();
			_newSkipAndRange = _settingsPersister.ReadStored();
			_entriesInManager = _settingsPersister.ReadStoredFrequencies();
			_entriesNewFrequency = _newSkipAndRange.NewFrequency;
			_enteriesFrequencyRange = _newSkipAndRange.FrequencyRange;
			_skipFrequencyList = _newSkipAndRange.SkipFrequencyArray;
			memoryEntryBindingSource.DataSource = _displayedEntries;
			ProcessRangeName();
			int[] intArraySetting = Utils.GetIntArraySetting("FrequencyRangeIndexes", null);
			if (intArraySetting != null && intArraySetting.Length != 0)
			{
				for (int i = 0; i < intArraySetting.Length; i++)
				{
					if (intArraySetting[i] < _enteriesFrequencyRange.Count)
					{
						frequencyRangeListBox.SetSelected(intArraySetting[i], value: true);
					}
				}
			}
			int num = Utils.GetIntSetting("ScanModeIndex", 0);
			if (num >= scanModeComboBox.Items.Count)
			{
				num = 0;
			}
			scanModeComboBox.SelectedIndex = num;
			scanModeComboBox_SelectedIndexChanged(null, null);
			InitDisplayEntry();
			ScanButton.Enabled = false;
			AutoSkipEnabled = Utils.GetBooleanSetting("AutoSkipEnabled");
			AutoSkipInterval = Utils.GetIntSetting("AutoSkipIntervalInSec", 30);
			AutoLockEnabled = Utils.GetBooleanSetting("AutoLockEnabled");
			AutoLockInterval = Utils.GetIntSetting("AutoLockIntervalInSec", 60);
			AutoClearEnabled = Utils.GetBooleanSetting("AutoClearEnabled");
			AutoClearActivityLevel = (float)Utils.GetDoubleSetting("AutoClearActivityLevel", 1.0);
			AutoClearInterval = Utils.GetIntSetting("AutoClearIntervalInSec", 10);
			ChannelDetectMetod = Utils.GetBooleanSetting("ChannelDetectOnCenterFrequency");
			UseMute = Utils.GetBooleanSetting("ScannerUseAudioMute");
			UnMuteDelay = Utils.GetIntSetting("ScannerUnMuteDelay", 5);
			_detect = Utils.GetIntSetting("ScannerDetect", 100);
			detectNumericUpDown.Value = _detect;
			MaxLevelSelect = Utils.GetBooleanSetting("ScannerMaxSelect");
			_pauseToNextScreen = Utils.GetIntSetting("PauseToNextScreenInMs", 2000);
			waitNumericUpDown.Value = (decimal)((float)_pauseToNextScreen / 1000f);
			_hysteresis = Utils.GetIntSetting("ScannerHysteresis", 10);
			_scanLevel = Utils.GetIntSetting("ScanLevel", 30);
			MaxButtonsAlpha = Utils.GetIntSetting("ScannerButtonsMaxAlpha", 150);
			MinButtonsAlpha = Utils.GetIntSetting("ScannerButtonsMinAlpha", 40);
			ScannerPluginPosition = Utils.GetIntSetting("ScannerPluginPosition", 1);
			_autoLockTimer = new System.Timers.Timer();
			_autoLockTimer.AutoReset = true;
			_autoLockTimer.Elapsed += AutoLock_Tick;
			_autoSkipTimer = new System.Timers.Timer();
			_autoSkipTimer.AutoReset = true;
			_autoSkipTimer.Elapsed += AutoSkip_Tick;
			_state = ScanState.ScanStop;
			_channelAnalyzer = new ChannelAnalyzer();
			_controlInterface.RegisterFrontControl(_channelAnalyzer, (PluginPosition)ScannerPluginPosition);
			_channelAnalyzer.Visible = false;
			_channelAnalyzer.CustomPaint += _channelAnalyzer_CustomPaint;
			_channelAnalyzer.MouseMove += _channelAnalyzer_MouseMove;
			_channelAnalyzer.MouseDown += _channelAnalyzer_MouseDown;
			_channelAnalyzer.MouseUp += _channelAnalyzer_MouseUp;
			_channelAnalyzer.MouseEnter += _channelAnalyzer_MouseEnter;
			_channelAnalyzer.MouseLeave += _channelAnalyzer_MouseLeave;
			_channelAnalyzer.MouseWheel += _channelAnalyzer_MouseWheel;
		}

		public void SaveSettings()
		{
			Utils.SaveSetting("ScanModeIndex", scanModeComboBox.SelectedIndex);
			int[] array = new int[frequencyRangeListBox.SelectedIndices.Count];
			frequencyRangeListBox.SelectedIndices.CopyTo(array, 0);
			Utils.SaveSetting("FrequencyRangeIndexes", Utils.IntArrayToString(array));
			Utils.SaveSetting("AutoSkipEnabled", AutoSkipEnabled);
			Utils.SaveSetting("AutoSkipIntervalInSec", AutoSkipInterval);
			Utils.SaveSetting("AutoLockEnabled", AutoLockEnabled);
			Utils.SaveSetting("AutoLockIntervalInSec", AutoLockInterval);
			Utils.SaveSetting("AutoClearEnabled", AutoClearEnabled);
			Utils.SaveSetting("AutoClearActivityLevel", AutoClearActivityLevel);
			Utils.SaveSetting("AutoClearIntervalInSec", AutoClearInterval);
			Utils.SaveSetting("ChannelDetectOnCenterFrequency", ChannelDetectMetod);
			Utils.SaveSetting("ScannerUseAudioMute", UseMute);
			Utils.SaveSetting("ScannerUnMuteDelay", UnMuteDelay);
			Utils.SaveSetting("ScannerDetect", _detect);
			Utils.SaveSetting("ScanLevel", _scanLevel);
			Utils.SaveSetting("PauseToNextScreenInMs", _pauseToNextScreen);
			Utils.SaveSetting("ScannerHysteresis", _hysteresis);
			Utils.SaveSetting("ScannerMaxSelect", MaxLevelSelect);
			Utils.SaveSetting("ScannerButtonsMaxAlpha", MaxButtonsAlpha);
			Utils.SaveSetting("ScannerButtonsMinAlpha", MinButtonsAlpha);
			Utils.SaveSetting("ScannerPluginPosition", ScannerPluginPosition);
		}

		private void StoreEntry()
		{
			if (memoryEntryBindingSource != null)
			{
				_entriesNewFrequency.Clear();
				foreach (MemoryEntryNewFrequency item in memoryEntryBindingSource)
				{
					_entriesNewFrequency.Add(item);
				}
				_entriesNewFrequency.Sort((MemoryEntryNewFrequency e1, MemoryEntryNewFrequency e2) => e1.Frequency.CompareTo(e2.Frequency));
				InitDisplayEntry();
			}
			if (_channelList != null)
			{
				for (int i = 0; i < _channelList.Count; i++)
				{
					bool flag = _channelList[i].Skipped;
					for (int j = 0; j < _skipFrequencyList.Count; j++)
					{
						if (_channelList[i].Frequency == _skipFrequencyList[j])
						{
							if (!_channelList[i].Skipped)
							{
								_skipFrequencyList.RemoveAt(j);
							}
							flag = false;
							break;
						}
					}
					if (flag)
					{
						_skipFrequencyList.Add(_channelList[i].Frequency);
					}
				}
			}
			_newSkipAndRange.NewFrequency = _entriesNewFrequency;
			_newSkipAndRange.SkipFrequencyArray = _skipFrequencyList;
			_newSkipAndRange.FrequencyRange = _enteriesFrequencyRange;
			_settingsPersister.PersistStored(_newSkipAndRange);
		}

		private void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			string propertyName = e.PropertyName;
			switch (propertyName)
			{
			default:
				if (propertyName == "CenterFrequency")
				{
					_centerFrequencyIsChanged = true;
				}
				break;
			case "StartRadio":
				ScanButton.Enabled = true;
				break;
			case "StopRadio":
				if (_state != ScanState.ScanStop)
				{
					ScanStop();
				}
				ScanButton.Enabled = false;
				break;
			case "Frequency":
				if (!_changeFrequencyInScanner && _state != ScanState.ScanStop)
				{
					_channelAnalyzer.Frequency = _controlInterface.Frequency;
				}
				_frequencyIsChanged = true;
				break;
			}
		}

		private void configureButton_Click(object sender, EventArgs e)
		{
			new DialogConfigure(this).ShowDialog();
		}

		private void detectNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_detect = (int)detectNumericUpDown.Value;
		}

		private void waitNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_pauseToNextScreen = (int)(waitNumericUpDown.Value * 1000m);
		}

		private void scanModeComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (scanModeComboBox.SelectedIndex)
			{
			case 0:
				_scanWidthStoreNew = true;
				_scanMemory = false;
				_scanExceptMemory = false;
				break;
			case 1:
				_scanWidthStoreNew = false;
				_scanMemory = false;
				_scanExceptMemory = false;
				break;
			case 2:
				_scanWidthStoreNew = false;
				_scanMemory = true;
				_scanExceptMemory = false;
				break;
			case 3:
				_scanWidthStoreNew = true;
				_scanMemory = false;
				_scanExceptMemory = true;
				break;
			}
		}

		private static string GetFrequencyDisplay(long frequency)
		{
			long num = Math.Abs(frequency);
			if (num == 0L)
			{
				return "DC";
			}
			if (num > 1500000000)
			{
				return $"{(double)frequency / 1000000000.0:#,0.000 000} GHz";
			}
			if (num > 30000000)
			{
				return $"{(double)frequency / 1000000.0:0,0.000###} MHz";
			}
			if (num > 1000)
			{
				return $"{(double)frequency / 1000.0:#,#.###} kHz";
			}
			return frequency.ToString();
		}

		private void InitDisplayEntry()
		{
			memoryEntryBindingSource.Clear();
			_displayedEntries.Clear();
			if (_entriesNewFrequency != null)
			{
				foreach (MemoryEntryNewFrequency item in _entriesNewFrequency)
				{
					_displayedEntries.Add(item);
				}
			}
		}

		private void AddNewFrequencyEntry(long frequency)
		{
			MemoryEntryNewFrequency memoryEntryNewFrequency = new MemoryEntryNewFrequency();
			memoryEntryNewFrequency.Frequency = frequency;
			memoryEntryNewFrequency.Activity = _timeConstant * 0.001f;
			memoryEntryNewFrequency.CenterFrequency = _controlInterface.CenterFrequency;
			_displayedEntries.Add(memoryEntryNewFrequency);
		}

		private void frequencyDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			if (frequencyDataGridView.Columns[e.ColumnIndex].DataPropertyName == "Frequency" && e.Value != null)
			{
				long frequency = (long)e.Value;
				e.Value = GetFrequencyDisplay(frequency);
				e.FormattingApplied = true;
			}
		}

		private void frequencyDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			Navigate();
		}

		private void frequencyDataGridView_SelectionChanged(object sender, EventArgs e)
		{
			btnDelete.Enabled = (frequencyDataGridView.SelectedRows.Count > 0);
		}

		private void frequencyDataGridView_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				Navigate();
				e.Handled = true;
			}
		}

		private void btnClearEntry_Click(object sender, EventArgs e)
		{
			_entriesNewFrequency.Clear();
			_newSkipAndRange.NewFrequency = _entriesNewFrequency;
			_displayedEntries.Clear();
			if (_state == ScanState.ScanStop)
			{
				_settingsPersister.PersistStored(_newSkipAndRange);
			}
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			int num = frequencyDataGridView.SelectedRows.Count;
			while (num > 0 && memoryEntryBindingSource.Count > 0)
			{
				MemoryEntryNewFrequency item = (MemoryEntryNewFrequency)memoryEntryBindingSource.Current;
				_displayedEntries.Remove(item);
				num--;
			}
		}

		public void Navigate()
		{
			if (_controlInterface.IsPlaying)
			{
				int num = (frequencyDataGridView.SelectedCells.Count > 0) ? frequencyDataGridView.SelectedCells[0].RowIndex : (-1);
				if (num != -1)
				{
					try
					{
						MemoryEntryNewFrequency memoryEntryNewFrequency = (MemoryEntryNewFrequency)memoryEntryBindingSource.List[num];
						_changeFrequencyInScanner = true;
						_controlInterface.CenterFrequency = memoryEntryNewFrequency.CenterFrequency;
						_controlInterface.Frequency = memoryEntryNewFrequency.Frequency;
						_changeFrequencyInScanner = false;
					}
					catch (Exception ex)
					{
						MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
			}
		}

		private void UpdateNewFrequencyDisplay()
		{
			if (_newActiveFrequencies.Count > 0)
			{
				do
				{
					bool flag = true;
					if (memoryEntryBindingSource.Count > 0)
					{
						foreach (MemoryEntryNewFrequency item in memoryEntryBindingSource)
						{
							if (item.Frequency == _newActiveFrequencies[0])
							{
								item.Activity += _timeConstant * 0.001f;
								memoryEntryBindingSource.ResetItem(memoryEntryBindingSource.IndexOf(item));
								flag = false;
								break;
							}
						}
					}
					if (flag)
					{
						AddNewFrequencyEntry(_newActiveFrequencies[0]);
					}
					_newActiveFrequencies.RemoveAt(0);
				}
				while (_newActiveFrequencies.Count > 0);
			}
			if (memoryEntryBindingSource.Count <= 0)
			{
				return;
			}
			for (int i = 0; i < _channelList.Count; i++)
			{
				if (_channelList[i].Skipped)
				{
					foreach (MemoryEntryNewFrequency item2 in memoryEntryBindingSource)
					{
						if (item2.Frequency == _channelList[i].Frequency)
						{
							memoryEntryBindingSource.Remove(item2);
							break;
						}
					}
					if (memoryEntryBindingSource.Count == 0)
					{
						break;
					}
				}
			}
			if (_timeToClean && memoryEntryBindingSource.Count > 0)
			{
				int position = memoryEntryBindingSource.Position;
				_timeToClean = false;
				int num = 0;
				while (num < memoryEntryBindingSource.Count)
				{
					if (((MemoryEntryNewFrequency)memoryEntryBindingSource[num]).Activity < AutoClearActivityLevel)
					{
						memoryEntryBindingSource.RemoveAt(num);
					}
					else
					{
						num++;
					}
				}
				if (position < memoryEntryBindingSource.Count)
				{
					memoryEntryBindingSource.Position = position;
				}
				else if (memoryEntryBindingSource.Count > 0)
				{
					memoryEntryBindingSource.Position = memoryEntryBindingSource.Count - 1;
				}
			}
			autoCleanTimer.Interval = AutoClearInterval * 1000;
			autoCleanTimer.Enabled = AutoClearEnabled;
		}

		private void ScanButton_Click(object sender, EventArgs e)
		{
			if (ScanButton.Text == "Scan" && _controlInterface.IsPlaying)
			{
				ScanStart();
			}
			else
			{
				ScanStop();
			}
		}

		private void ScanStart()
		{
			//IL_0010: Unknown result type (might be due to invalid IL or missing references)
			//IL_0015: Unknown result type (might be due to invalid IL or missing references)
			if (CheckConditions())
			{
				_tuningStyleTemp = _controlInterface.TuningStyle;
				_controlInterface.TuningStyle = 0;
				_tuningLimitTemp = _controlInterface.TuningLimit;
				_controlInterface.TuningLimit = 0.5f;
				_entriesInManager = _settingsPersister.ReadStoredFrequencies();
				frequencyRangeListBox.Enabled = false;
				FrequencyRangeEditButton.Enabled = false;
				scanModeComboBox.Enabled = false;
				ScanButton.Text = "Stop scan";
				_state = ScanState.SetScreen;
				_pauseScan = false;
				_usableSpectrum = (float)_controlInterface.RFDisplayBandwidth / (float)_controlInterface.RFBandwidth;
				CreateChannelList();
				_currentScreenIndex = -1;
				_pauseAfter = 0f;
				_newActiveFrequencies = new List<long>();
				_activeFrequencies = new List<int>();
				if (_scanWidthStoreNew)
				{
					newFrequencyDisplayUpdateTimer.Enabled = true;
				}
				_channelAnalyzer.Zoom = 0;
				_channelAnalyzer.ZoomPosition = 0;
				_channelAnalyzer.Visible = true;
				_autoSkipTimer.Interval = AutoSkipInterval * 1000;
				_autoSkipTimer.Enabled = AutoSkipEnabled;
				_autoLockTimer.Interval = AutoLockInterval * 1000;
				_autoLockTimer.Enabled = AutoLockEnabled;
				autoCleanTimer.Interval = AutoClearInterval * 1000;
				autoCleanTimer.Enabled = AutoClearEnabled;
				if (UseMute)
				{
					_controlInterface.AudioIsMuted = true;
				}
				_scanProcessIsWork = false;
				_scannerThread = new Thread(ScanProcess);
				_scannerThread.Name = "Scanner Process";
				_bufferEvent.Reset();
				_scannerRunning = true;
				_scannerThread.Start();
				iControlsUpdateTimer.Enabled = true;
				_ifProcessor.Enabled = true;
			}
		}

		public void ScanStop()
		{
			//IL_00d0: Unknown result type (might be due to invalid IL or missing references)
			if (!(ScanButton.Text == "Scan"))
			{
				_state = ScanState.ScanStop;
				_ifProcessor.Enabled = false;
				_autoSkipTimer.Enabled = false;
				_autoLockTimer.Enabled = false;
				autoCleanTimer.Enabled = false;
				newFrequencyDisplayUpdateTimer.Enabled = false;
				_scannerRunning = false;
				_bufferEvent.Set();
				_scannerThread.Join();
				if (_scannerThread != null)
				{
					_scannerThread = null;
				}
				iControlsUpdateTimer.Enabled = false;
				StoreEntry();
				_channelAnalyzer.Visible = false;
				frequencyRangeListBox.Enabled = true;
				FrequencyRangeEditButton.Enabled = true;
				scanModeComboBox.Enabled = true;
				_controlInterface.TuningStyle = _tuningStyleTemp;
				_controlInterface.TuningLimit = _tuningLimitTemp;
				if (UseMute)
				{
					_controlInterface.AudioIsMuted = false;
				}
				ScanButton.Text = "Scan";
			}
		}

		private void PanViewClose(object sender, FormClosingEventArgs e)
		{
			e.Cancel = true;
			if (_scannerRunning)
			{
				_requestToStopScanner = true;
			}
		}

		private void CreateChannelList()
		{
			_channelList = new List<ChannelAnalizerMemoryEntry>();
			_centerFrequncyList = new List<CenterFrequencyEntry>();
			int rFBandwidth = _controlInterface.RFBandwidth;
			int num = (int)((float)_controlInterface.RFBandwidth * _usableSpectrum);
			int count = frequencyRangeListBox.SelectedIndices.Count;
			int num2 = 0;
			for (int i = 0; i < count; i++)
			{
				MemoryEntryFrequencyRange memoryEntryFrequencyRange = new MemoryEntryFrequencyRange();
				int num3 = frequencyRangeListBox.SelectedIndices[i];
				if (num3 < 1)
				{
					long num4 = _controlInterface.CenterFrequency / _controlInterface.StepSize * _controlInterface.StepSize;
					int stepSize = _controlInterface.StepSize;
					int num5 = num / stepSize / 2 * 2;
					memoryEntryFrequencyRange.StartFrequency = num4 - num5 / 2 * stepSize;
					memoryEntryFrequencyRange.EndFrequency = num4 + num5 / 2 * stepSize;
					memoryEntryFrequencyRange.FilterBandwidth = _controlInterface.FilterBandwidth;
					memoryEntryFrequencyRange.StepSize = stepSize;
					memoryEntryFrequencyRange.RangeDetectorType = _controlInterface.DetectorType;
				}
				else
				{
					memoryEntryFrequencyRange = _enteriesFrequencyRange[num3 - 1];
				}
				long startFrequency = memoryEntryFrequencyRange.StartFrequency;
				long endFrequency = memoryEntryFrequencyRange.EndFrequency;
				int num6 = memoryEntryFrequencyRange.FilterBandwidth;
				int stepSize2 = memoryEntryFrequencyRange.StepSize;
				if (num6 > stepSize2)
				{
					num6 = stepSize2;
				}
				int num7 = num / stepSize2 / 2 * 2;
				long num8 = startFrequency;
				int num9 = 0;
				long centerFrequency = 0L;
				for (; num8 <= endFrequency; num8 += stepSize2)
				{
					if (num9 > num7 || num9 == 0)
					{
						centerFrequency = (((endFrequency - num8) / stepSize2 > num7) ? (num8 + num7 / 2 * stepSize2) : ((num8 + endFrequency) / 2));
						CenterFrequencyEntry centerFrequencyEntry = new CenterFrequencyEntry();
						centerFrequencyEntry.CenterFrequency = centerFrequency;
						centerFrequencyEntry.StartFrequencyIndex = num2;
						_centerFrequncyList.Add(centerFrequencyEntry);
						num9 = 0;
					}
					int iFOffset = _controlInterface.IFOffset;
					ChannelAnalizerMemoryEntry channelAnalizerMemoryEntry = new ChannelAnalizerMemoryEntry();
					channelAnalizerMemoryEntry.Frequency = num8;
					channelAnalizerMemoryEntry.CenterFrequency = centerFrequency;
					channelAnalizerMemoryEntry.FilterBandwidth = memoryEntryFrequencyRange.FilterBandwidth;
					channelAnalizerMemoryEntry.StepSize = memoryEntryFrequencyRange.StepSize;
					channelAnalizerMemoryEntry.DetectorType = memoryEntryFrequencyRange.RangeDetectorType;
					channelAnalizerMemoryEntry.StartBins = FrequencyToBins(num8 - num6 / 2 - iFOffset, centerFrequency, rFBandwidth);
					channelAnalizerMemoryEntry.EndBins = FrequencyToBins(num8 + num6 / 2 - iFOffset, centerFrequency, rFBandwidth);
					_channelList.Add(channelAnalizerMemoryEntry);
					num2++;
					num9++;
				}
			}
			for (int j = 0; j < _centerFrequncyList.Count - 1; j++)
			{
				_centerFrequncyList[j].EndFrequencyIndex = _centerFrequncyList[j + 1].StartFrequencyIndex - 1;
			}
			_centerFrequncyList[_centerFrequncyList.Count - 1].EndFrequencyIndex = _channelList.Count - 1;
			for (int k = 0; k < _channelList.Count; k++)
			{
				long frequency = _channelList[k].Frequency;
				int filterBandwidth = _channelList[k].FilterBandwidth;
				if (_skipFrequencyList != null)
				{
					for (int l = 0; l < _skipFrequencyList.Count; l++)
					{
						if (_skipFrequencyList[l] == frequency)
						{
							_channelList[k].Skipped = true;
							break;
						}
					}
				}
				if (_entriesInManager != null)
				{
					foreach (MemoryEntry item in _entriesInManager)
					{
						if (item.Frequency == frequency)
						{
							_channelList[k].IsStore = true;
							_channelList[k].StoreName = item.GroupName + " " + item.Name;
							break;
						}
					}
				}
			}
			_filterWidth = (_channelList[0].EndBins - _channelList[0].StartBins) * 2;
			_filterWidth |= 1;
		}

		private bool CheckConditions()
		{
			if (frequencyRangeListBox.SelectedIndices.Count == 0)
			{
				frequencyRangeListBox.SelectedIndex = 0;
			}
			if (frequencyRangeListBox.SelectedIndices.Count > 1 && frequencyRangeListBox.SelectedIndices.IndexOf(0) != -1)
			{
				frequencyRangeListBox.SelectedIndices.Remove(frequencyRangeListBox.SelectedIndices.IndexOf(0));
			}
			if (!_controlInterface.SourceIsTunable && frequencyRangeListBox.SelectedIndex != 0)
			{
				if (CultureInfo.CurrentCulture.Name == "ru-RU")
				{
					MessageBox.Show("Невозможно управлять настройкой частоты источника сигнала. Доступно только сканирование в пределах экрана.", "Scan conditions Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					MessageBox.Show("Unable to control the frequency settings for the source. Available only scan within the screen.", "Scan conditions Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				return false;
			}
			for (int i = 0; i < frequencyRangeListBox.SelectedIndices.Count; i++)
			{
				int num = frequencyRangeListBox.SelectedIndices[i];
				int num2 = 0;
				int num3 = 0;
				if (num == 0)
				{
					num2 = _controlInterface.StepSize;
					num3 = _controlInterface.FilterBandwidth;
				}
				else
				{
					num2 = _enteriesFrequencyRange[num - 1].StepSize;
					num3 = _enteriesFrequencyRange[num - 1].FilterBandwidth;
				}
				if (num2 == 0 || num3 == 0)
				{
					if (CultureInfo.CurrentCulture.Name == "ru-RU")
					{
						MessageBox.Show("Параметр Step size или Filter bandwidth равны нулю! Для работы сканера параметр Step Size и Filter Bandwidth не равны нулю.", "Scan conditions Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					}
					else
					{
						MessageBox.Show("Step size or Filter bandwidth equal to zero! For the scanner must be Step Size and Filter Bandwidth not equal to zero.", "Scan conditions Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					}
					return false;
				}
			}
			return true;
		}

		private void ScanProcess()
		{
			while (_scannerRunning)
			{
				_bufferEvent.WaitOne();
				if (!_scannerRunning)
				{
					break;
				}
				GetControlParameters();
				_directionReverse = _direction;
				_interval += _timeConstant;
				switch (_state)
				{
				case ScanState.SetScreen:
				{
					int currentScreenIndex = _currentScreenIndex;
					SetCurrentScreen();
					if (_currentScreenIndex != currentScreenIndex)
					{
						_state = ScanState.PauseToSet;
						_interval = 0f;
						goto case ScanState.PauseToSet;
					}
					_state = ScanState.ScanScreen;
					_interval = 0f;
					goto case ScanState.ScanScreen;
				}
				case ScanState.PauseToSet:
					if (!_centerFrequencyIsChanged)
					{
						_interval = 0f;
					}
					if (!(_interval >= (float)_detect))
					{
						break;
					}
					_centerFrequencyIsChanged = false;
					_state = ScanState.ScanScreen;
					_interval = 0f;
					goto case ScanState.ScanScreen;
				case ScanState.ScanScreen:
				{
					ProcessFFT();
					int num = 1;
					if (!_directionReverse)
					{
						num = 1;
						_currentStartIndex = _centerFrequncyList[_currentScreenIndex].StartFrequencyIndex;
						_currentEndIndex = _centerFrequncyList[_currentScreenIndex].EndFrequencyIndex;
					}
					else
					{
						num = -1;
						_currentEndIndex = _centerFrequncyList[_currentScreenIndex].StartFrequencyIndex;
						_currentStartIndex = _centerFrequncyList[_currentScreenIndex].EndFrequencyIndex;
					}
					for (int i = _currentStartIndex; i != _currentEndIndex + num; i += num)
					{
						_channelList[i].Level = MaxLevelOnChannel(_channelList[i].StartBins, _channelList[i].EndBins);
					}
					bool flag = true;
					_activeFrequencies.Clear();
					for (int j = _currentStartIndex; j != _currentEndIndex + num; j += num)
					{
						if (_channelList[j].Skipped)
						{
							if (_controlParameters.Frequency == _channelList[j].Frequency && !_pauseScan)
							{
								int num2 = j + num;
								if ((!_directionReverse && num2 > _currentEndIndex) || (_directionReverse && num2 < _currentEndIndex))
								{
									num2 = _currentStartIndex;
								}
								_controlParameters.Frequency = _channelList[num2].Frequency;
								_isPlayed = false;
								if (UseMute)
								{
									AudioMuteProcess(unmute: false);
								}
							}
						}
						else if ((!_scanMemory || _channelList[j].IsStore) && (!_scanExceptMemory || !_channelList[j].IsStore) && _channelList[j].Level > (float)_scanLevel)
						{
							flag = false;
							if (_scanWidthStoreNew && !_channelList[j].IsStore)
							{
								_newActiveFrequencies.Add(_channelList[j].Frequency);
							}
							_activeFrequencies.Add(j);
						}
					}
					if (!_isPlayed && _activeFrequencies.Count > 0)
					{
						if (_activeFrequencies.Count == 1 || !MaxLevelSelect)
						{
							ChangeFrequencyInScanner(_channelList[_activeFrequencies[0]], outToPanView: true);
						}
						else
						{
							float num3 = 0f;
							int index = 0;
							foreach (int activeFrequency in _activeFrequencies)
							{
								if (_channelList[activeFrequency].Level > num3)
								{
									num3 = _channelList[activeFrequency].Level;
									index = activeFrequency;
								}
							}
							ChangeFrequencyInScanner(_channelList[index], outToPanView: true);
						}
						_isPlayed = true;
					}
					if (_nextButtonPress)
					{
						_nextButtonPress = false;
						flag = true;
						_isPlayed = false;
						if (_activeFrequencies.Count > 0)
						{
							foreach (int activeFrequency2 in _activeFrequencies)
							{
								if ((_channelList[activeFrequency2].Frequency > _controlParameters.Frequency && !_directionReverse) || (_channelList[activeFrequency2].Frequency < _controlParameters.Frequency && _directionReverse))
								{
									ChangeFrequencyInScanner(_channelList[activeFrequency2], outToPanView: true);
									_isPlayed = true;
									break;
								}
							}
						}
						if (UseMute)
						{
							AudioMuteProcess(_isPlayed);
						}
					}
					if (_isPlayed)
					{
						flag = false;
						_isActivePlaedFrequency = SquelchActiveFrequency(_isActivePlaedFrequency);
						if (_isActivePlaedFrequency)
						{
							_pauseAfter = _pauseToNextScreen;
						}
						else
						{
							_pauseAfter -= _interval;
						}
						_isPlayed = (_pauseAfter > 0f || _isActivePlaedFrequency);
						if (UseMute)
						{
							AudioMuteProcess(_isActivePlaedFrequency);
						}
					}
					if (!_isPlayed || _pauseScan)
					{
						ResetTimers();
					}
					if (_pauseScan)
					{
						_isPlayed = true;
						flag = false;
						_pauseAfter = 0f;
					}
					if (flag)
					{
						_state = ScanState.SetScreen;
					}
					_interval = 0f;
					break;
				}
				case ScanState.ScanStop:
					_interval = 0f;
					break;
				}
				if (_controlParameters.IsChanged)
				{
					_needUpdateParametrs = true;
					continue;
				}
				if (_state == ScanState.ScanScreen || _centerFrequncyList.Count == 1)
				{
					Thread.Sleep(20);
				}
				_scanProcessIsWork = false;
			}
		}

		private void GetControlParameters()
		{
			_controlParameters.AudioIsMuted = _controlInterface.AudioIsMuted;
			_controlParameters.CenterFrequency = _controlInterface.CenterFrequency;
			_controlParameters.Frequency = _controlInterface.Frequency;
			_controlParameters.FilterBandwidth = _controlInterface.FilterBandwidth;
			_controlParameters.DetectorType = _controlInterface.DetectorType;
			_controlParameters.RfBandwidth = _controlInterface.RFBandwidth;
			_controlParameters.ResetFlags();
		}

		private unsafe float MaxLevelOnChannel(int startBins, int endBins)
		{
			float num = 0f;
			if (ChannelDetectMetod)
			{
				int num2 = (startBins + endBins) / 2;
				num = _scaledFFTSpectrumPtr[num2];
			}
			else
			{
				for (int i = startBins; i <= endBins; i++)
				{
					int num3 = i;
					if (num3 < 0)
					{
						num3 = 0;
					}
					if (num3 >= _fftBins)
					{
						num3 = _fftBins - 1;
					}
					num = Math.Max(num, _scaledFFTSpectrumPtr[num3]);
				}
			}
			return num;
		}

		private void ChangeFrequencyInScanner(ChannelAnalizerMemoryEntry entry, bool outToPanView)
		{
			_controlParameters.DetectorType = entry.DetectorType;
			_controlParameters.FilterBandwidth = entry.FilterBandwidth;
			_controlParameters.Frequency = entry.Frequency;
			if (outToPanView)
			{
				_channelAnalyzer.Frequency = entry.Frequency;
			}
		}

		private bool SquelchActiveFrequency(bool useHisteresis)
		{
			int startBins = FrequencyToBins(_controlParameters.Frequency - _controlParameters.FilterBandwidth / 4 - _controlInterface.IFOffset, _controlParameters.CenterFrequency, _controlParameters.RfBandwidth);
			int endBins = FrequencyToBins(_controlParameters.Frequency + _controlParameters.FilterBandwidth / 4 - _controlInterface.IFOffset, _controlParameters.CenterFrequency, _controlParameters.RfBandwidth);
			if (useHisteresis)
			{
				return MaxLevelOnChannel(startBins, endBins) > (float)_hysteresis;
			}
			return MaxLevelOnChannel(startBins, endBins) > (float)_scanLevel;
		}

		private void AudioMuteProcess(bool unmute)
		{
			bool flag = _controlParameters.AudioIsMuted;
			if (unmute)
			{
				_squelchCount++;
				if (_squelchCount > UnMuteDelay)
				{
					flag = false;
					_squelchCount = UnMuteDelay;
				}
			}
			else
			{
				flag = true;
				_squelchCount = 0;
			}
			if (_controlParameters.AudioIsMuted != flag)
			{
				_controlParameters.AudioIsMuted = flag;
			}
		}

		private void SetCurrentScreen()
		{
			if (_currentScreenIndex == 0)
			{
				_debugTime = _debugCounter;
				_debugCounter = 0;
			}
			if (_centerFrequncyList.Count == 1 && _controlParameters.CenterFrequency == _centerFrequncyList[0].CenterFrequency)
			{
				_currentScreenIndex = 0;
				return;
			}
			if (!_directionReverse)
			{
				_currentScreenIndex++;
				if (_currentScreenIndex >= _centerFrequncyList.Count)
				{
					_currentScreenIndex = 0;
				}
			}
			else
			{
				_currentScreenIndex--;
				if (_currentScreenIndex < 0)
				{
					_currentScreenIndex = _centerFrequncyList.Count - 1;
				}
			}
			_controlParameters.CenterFrequency = _centerFrequncyList[_currentScreenIndex].CenterFrequency;
		}

		private int FrequencyToBins(long frequency, long centerFrequency, int rfBandwidth)
		{
			long num = centerFrequency - rfBandwidth / 2;
			float num2 = (float)rfBandwidth / (float)_fftBins;
			return (int)((float)(frequency - num) / num2);
		}

		private long BinsToFrequency(int bins)
		{
			float num = (float)_controlParameters.RfBandwidth / (float)_fftBins;
			return _controlParameters.CenterFrequency - _controlParameters.RfBandwidth / 2 + (int)((float)bins * num);
		}

		private List<string> GetRangeNameFromEntries()
		{
			List<string> list = new List<string>();
			list.Add("Screen");
			foreach (MemoryEntryFrequencyRange item in _enteriesFrequencyRange)
			{
				list.Add(item.RangeName);
			}
			return list;
		}

		private void ProcessRangeName()
		{
			frequencyRangeListBox.Items.Clear();
			frequencyRangeListBox.Items.AddRange(GetRangeNameFromEntries().ToArray());
		}

		private void FrequencyRangeEditButton_Click(object sender, EventArgs e)
		{
			DialogEditRange dialogEditRange = new DialogEditRange();
			_enteriesFrequencyRange = dialogEditRange.EditRange(_enteriesFrequencyRange);
			if (dialogEditRange.ShowDialog() == DialogResult.OK)
			{
				StoreEntry();
				ProcessRangeName();
			}
		}

		private unsafe void BuildFFTWindow()
		{
			float[] array = FilterBuilder.MakeWindow(WindowType.BlackmanHarris4, _fftBins);
			fixed (float* src = array)
			{
				Utils.Memcpy(_fftWindow, src, _fftBins * 4);
			}
		}

		private unsafe void InitFFTBuffers()
		{
			_fftBuffer = UnsafeBuffer.Create(_fftBins, sizeof(Complex));
			_fftWindow = UnsafeBuffer.Create(_fftBins, 4);
			_unScaledFFTSpectrum = UnsafeBuffer.Create(_fftBins, 4);
			_scaledFFTSpectrum = UnsafeBuffer.Create(_fftBins, 4);
			_fftPtr = (Complex*)(void*)_fftBuffer;
			_fftWindowPtr = (float*)(void*)_fftWindow;
			_unScaledFFTSpectrumPtr = (float*)(void*)_unScaledFFTSpectrum;
			_scaledFFTSpectrumPtr = (float*)(void*)_scaledFFTSpectrum;
		}

		private unsafe void FastBufferAvailable(Complex* buffer, int length)
		{
			if (!_scannerRunning || !_controlInterface.IsPlaying)
			{
				return;
			}
			_count++;
			_debugCounter++;
			if (!_scanProcessIsWork)
			{
				int num = Math.Min(length, _fftBins - _writePos);
				Utils.Memcpy(_fftPtr + _writePos, buffer, num * sizeof(Complex));
				_writePos += num;
				if (_writePos >= _fftBins)
				{
					_writePos = 0;
					_bufferLengthInMs = (double)length / _ifProcessor.SampleRate * 1000.0;
					_timeConstant = (float)(_bufferLengthInMs * (double)_count);
					_scanProcessIsWork = true;
					_count = 0;
					_bufferEvent.Set();
				}
			}
			else
			{
				_writePos = 0;
			}
		}

		private unsafe void ProcessFFT()
		{
			Fourier.ApplyFFTWindow(_fftPtr, _fftWindowPtr, _fftBins);
			Fourier.ForwardTransform(_fftPtr, _fftBins);
			Fourier.SpectrumPower(_fftPtr, _unScaledFFTSpectrumPtr, _fftBins);
			ScaleSpectrum(_unScaledFFTSpectrumPtr, _scaledFFTSpectrumPtr, _fftBins);
		}

		private unsafe void ScaleSpectrum(float* source, float* dest, int length)
		{
			bool swapIq = _controlInterface.SwapIq;
			float num = 1f / (float)_filterWidth;
			int num2 = 0;
			int num3 = 0;
			for (int i = 0; i < length; i++)
			{
				num3 = (swapIq ? (length - i) : i);
				num2 = i;
				_iavg += num * (source[num2] - _iavg);
				dest[num3] = source[num2] - _iavg;
			}
		}

		private void iControlsUpdateTimer_Tick(object sender, EventArgs e)
		{
			//IL_0038: Unknown result type (might be due to invalid IL or missing references)
			if (_controlParameters == null || !_needUpdateParametrs)
			{
				return;
			}
			_needUpdateParametrs = false;
			if (_controlParameters.IsChanged)
			{
				_changeFrequencyInScanner = true;
				if ((int)_controlInterface.TuningStyle != 0)
				{
					_controlInterface.TuningStyle = 0;
				}
				if (_controlParameters.AudioNeedUpdate && _controlInterface.AudioIsMuted != _controlParameters.AudioIsMuted)
				{
					_controlInterface.AudioIsMuted = _controlParameters.AudioIsMuted;
				}
				if (_controlParameters.DetectorNeedUpdate && _controlInterface.DetectorType != _controlParameters.DetectorType)
				{
					_controlInterface.DetectorType = _controlParameters.DetectorType;
				}
				if (_controlParameters.FilterNeedUpdate && _controlInterface.FilterBandwidth != _controlParameters.FilterBandwidth)
				{
					_controlInterface.FilterBandwidth = _controlParameters.FilterBandwidth;
				}
				if (_controlParameters.CenterNeedUpdate && _controlInterface.CenterFrequency != _controlParameters.CenterFrequency)
				{
					_controlInterface.CenterFrequency = _controlParameters.CenterFrequency;
				}
				if (_controlParameters.FrequencyNeedUpdate && _controlInterface.Frequency != _controlParameters.Frequency)
				{
					_controlInterface.Frequency = _controlParameters.Frequency;
				}
				_changeFrequencyInScanner = false;
				_controlParameters.ResetFlags();
			}
			_scanProcessIsWork = false;
		}

		private void testDisplayTimer_Tick(object sender, EventArgs e)
		{
			timeConstantLabel.Text = $"{_timeConstant:F} ms";
			if (_requestToStopScanner)
			{
				_requestToStopScanner = false;
				ScanStop();
			}
			string str = "Scan";
			str = ((!_directionReverse) ? (str + " >>>") : (str + " <<<"));
			if (_isPlayed)
			{
				str = ((_pauseAfter != (float)_pauseToNextScreen) ? $"Wait {_pauseAfter / 1000f:F1} s" : "Play");
			}
			if (_pauseScan)
			{
				str = "Pause";
			}
			if (_channelAnalyzer.Zoom != 0 && _scannerRunning)
			{
				str = $"Zoom x{_channelAnalyzer.Zoom + 1:N0} " + str;
			}
			if (ShowDebugInfo && _scannerRunning)
			{
				int count = _centerFrequncyList.Count;
				float num = (float)_controlInterface.RFBandwidth * _usableSpectrum;
				float num2 = (float)(_bufferLengthInMs * (double)_debugTime * 0.0010000000474974513);
				float num3 = num * (float)count / num2 * 1E-06f;
				str = $"Info: screens {count:N0} x {num * 1E-06f:F2} MHz, time {num2:F3} s, speed {num3:F1} MHz/s. " + str;
			}
			_infoText = str;
		}

		private void ResetTimers()
		{
			_autoSkipTimer.Enabled = false;
			_autoSkipTimer.Interval = AutoSkipInterval * 1000;
			_autoSkipTimer.Enabled = AutoSkipEnabled;
			_autoLockTimer.Enabled = false;
			_autoLockTimer.Interval = AutoLockInterval * 1000;
			_autoLockTimer.Enabled = AutoLockEnabled;
		}

		private void newFrequencyDisplayUpdateTimer_Tick(object sender, EventArgs e)
		{
			UpdateNewFrequencyDisplay();
		}

		private void AutoSkip_Tick(object source, ElapsedEventArgs e)
		{
			_nextButtonPress = true;
		}

		private void AutoLock_Tick(object source, ElapsedEventArgs e)
		{
			SetSkipSelectedChannel(setUnSet: true);
		}

		private void autoCleanTimer_Tick(object sender, EventArgs e)
		{
			_timeToClean = true;
		}

		private void refreshTimer_Tick(object sender, EventArgs e)
		{
			if (_channelList != null)
			{
				_channelAnalyzer.Perform(_channelList);
			}
		}

		private void _channelAnalyzer_MouseUp(object sender, MouseEventArgs e)
		{
			if (_channelList == null)
			{
				return;
			}
			buttonsRepeaterTimer.Enabled = false;
			_buttons = Buttons.None;
			if (_selectFrequency)
			{
				_startSelectX = _oldX;
				_endSelectX = _trackingX;
				_startSelectIndex = _oldIndex;
				_endSelectIndex = _trackingIndex;
				if (_startSelectIndex > _endSelectIndex)
				{
					int startSelectX = _startSelectX;
					_startSelectX = _endSelectX;
					_endSelectX = startSelectX;
					startSelectX = _startSelectIndex;
					_startSelectIndex = _endSelectIndex;
					_endSelectIndex = startSelectX;
				}
				if (_startSelectX == _endSelectX)
				{
					_channelList[_startSelectIndex].Skipped = !_channelList[_startSelectIndex].Skipped;
					_selectFrequency = false;
				}
			}
		}

		private void _channelAnalyzer_MouseDown(object sender, MouseEventArgs e)
		{
			Point location = e.Location;
			if (_channelList == null)
			{
				return;
			}
			if (e.Button == MouseButtons.Left)
			{
				if (_reverseRectangle.Contains(location))
				{
					_nextButtonPress = true;
					_direction = true;
				}
				else if (_forwardRectangle.Contains(location))
				{
					_nextButtonPress = true;
					_direction = false;
				}
				else if (_pauseRectangle.Contains(location))
				{
					_pauseScan = !_pauseScan;
				}
				else if (_lockRectangle.Contains(location))
				{
					SetSkipSelectedChannel(setUnSet: true);
				}
				else if (_unlockRectangle.Contains(location))
				{
					SetSkipSelectedChannel(setUnSet: false);
				}
				else if (_snrPlusRectangle.Contains(location))
				{
					_scanLevel++;
					_buttons = Buttons.SNRUp;
					buttonsRepeaterTimer.Enabled = true;
				}
				else if (_snrMinusRectangle.Contains(location))
				{
					if (_scanLevel > _hysteresis)
					{
						_scanLevel--;
					}
					_buttons = Buttons.SNRDown;
					buttonsRepeaterTimer.Enabled = true;
				}
				else if (_histPlusRectangle.Contains(location))
				{
					if (_hysteresis < _scanLevel)
					{
						_hysteresis++;
					}
					_buttons = Buttons.HystUp;
					buttonsRepeaterTimer.Enabled = true;
				}
				else if (_histMinusRectangle.Contains(location))
				{
					if (_hysteresis > 0)
					{
						_hysteresis--;
					}
					_buttons = Buttons.HystDown;
					buttonsRepeaterTimer.Enabled = true;
				}
				else if (_selectFrequency && _trackingX > _startSelectX && _trackingX < _endSelectX)
				{
					SetSkipSelectedChannel(setUnSet: true);
				}
				else
				{
					_oldX = _trackingX;
					_oldIndex = _trackingIndex;
					_oldFrequency = _trackingFrequency;
					_selectFrequency = true;
					_startSelectX = 0;
					_endSelectX = 0;
				}
			}
			else if (e.Button == MouseButtons.Right && _selectFrequency && _trackingX > _startSelectX && _trackingX < _endSelectX)
			{
				SetSkipSelectedChannel(setUnSet: false);
			}
		}

		private void _channelAnalyzer_MouseMove(object sender, MouseEventArgs e)
		{
			if (_channelList != null)
			{
				_trackingX = e.X;
				_trackingY = e.Y;
				_trackingIndex = _channelAnalyzer.PointToChannel(_trackingX);
				_trackingFrequency = _channelList[_trackingIndex].Frequency;
			}
		}

		private void _channelAnalyzer_MouseWheel(object sender, MouseEventArgs e)
		{
			if (_channelAnalyzer.Zoom > 0 && e.Delta < 0)
			{
				_channelAnalyzer.Zoom--;
				if (_channelAnalyzer.Zoom == 0)
				{
					_channelAnalyzer.ZoomPosition = 0;
				}
			}
			if (_channelAnalyzer.Zoom < 100 && e.Delta > 0)
			{
				if (_channelAnalyzer.Zoom == 0)
				{
					_channelAnalyzer.ZoomPosition = _trackingX;
				}
				_channelAnalyzer.Zoom++;
			}
		}

		private void _channelAnalyzer_MouseLeave(object sender, EventArgs e)
		{
			_hotTrackNeeded = false;
		}

		private void _channelAnalyzer_MouseEnter(object sender, EventArgs e)
		{
			_hotTrackNeeded = true;
		}

		public void SetSkipSelectedChannel(bool setUnSet)
		{
			if (_selectFrequency)
			{
				if (_startSelectX >= _endSelectX)
				{
					return;
				}
				for (int i = _startSelectIndex; i <= _endSelectIndex; i++)
				{
					if (i < _channelList.Count && i >= 0)
					{
						_channelList[i].Skipped = setUnSet;
					}
				}
				_selectFrequency = false;
			}
			else
			{
				int num = _channelAnalyzer.CurrentChannelIndex();
				if (num < _channelList.Count && num >= 0)
				{
					_channelList[num].Skipped = setUnSet;
				}
			}
		}

		private void _channelAnalyzer_CustomPaint(object sender, CustomPaintEventArgs e)
		{
			ChannelAnalyzer channelAnalyzer = (ChannelAnalyzer)sender;
			Graphics graphics = e.Graphics;
			if (_hotTrackNeeded)
			{
				if (_bright < MaxButtonsAlpha)
				{
					_bright += 10;
				}
				else
				{
					_bright = MaxButtonsAlpha;
				}
			}
			else if (_bright > MinButtonsAlpha)
			{
				_bright--;
			}
			else
			{
				_bright = MinButtonsAlpha;
			}
			using (SolidBrush brush7 = new SolidBrush(Color.FromArgb(80, Color.DarkGray)))
			{
				using (Pen pen = new Pen(Color.Red))
				{
					using (Pen pen2 = new Pen(Color.Yellow))
					{
						using (Pen pen3 = new Pen(Color.Green))
						{
							using (Font font3 = new Font("Lucida Console", 9f))
							{
								using (Font font = new Font("Webdings", 32f))
								{
									using (Font font2 = new Font("Lucida Console", 8f))
									{
										using (new Font("Lucida Console", 32f))
										{
											using (SolidBrush brush5 = new SolidBrush(Color.FromArgb(150, Color.Black)))
											{
												using (SolidBrush brush6 = new SolidBrush(Color.FromArgb(255, Color.White)))
												{
													using (SolidBrush brush = new SolidBrush(Color.FromArgb(_bright, Color.White)))
													{
														using (SolidBrush brush3 = new SolidBrush(Color.FromArgb(_bright, Color.Red)))
														{
															using (SolidBrush brush4 = new SolidBrush(Color.FromArgb(_bright, Color.Yellow)))
															{
																using (SolidBrush brush2 = new SolidBrush(Color.FromArgb(_bright, Color.Black)))
																{
																	using (StringFormat format = new StringFormat(StringFormat.GenericTypographic))
																	{
																		using (new Pen(Color.Black))
																		{
																			Point p = default(Point);
																			SizeF sizeF = default(SizeF);
																			string empty = string.Empty;
																			empty = "7";
																			sizeF = graphics.MeasureString(empty, font, channelAnalyzer.Width, format);
																			p.X = 37;
																			p.Y = 7;
																			_reverseRectangle = new Rectangle(p.X, p.Y, (int)sizeF.Width, (int)sizeF.Height);
																			graphics.FillRectangle(brush, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 4f);
																			graphics.DrawString(empty, font, brush2, p, format);
																			empty = "8";
																			p.X = p.X + (int)sizeF.Width + 10;
																			sizeF = graphics.MeasureString(empty, font, channelAnalyzer.Width, format);
																			_forwardRectangle = new Rectangle(p.X, p.Y, (int)sizeF.Width, (int)sizeF.Height);
																			graphics.FillRectangle(brush, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 4f);
																			graphics.DrawString(empty, font, brush2, p, format);
																			empty = (_pauseScan ? "4" : ";");
																			p.X = p.X + (int)sizeF.Width + 10;
																			sizeF = graphics.MeasureString(empty, font, channelAnalyzer.Width, format);
																			_pauseRectangle = new Rectangle(p.X, p.Y, (int)sizeF.Width, (int)sizeF.Height);
																			graphics.FillRectangle(brush, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 4f);
																			graphics.DrawString(empty, font, brush2, p, format);
																			empty = "r";
																			p.X = p.X + (int)sizeF.Width + 10;
																			sizeF = graphics.MeasureString(empty, font, channelAnalyzer.Width, format);
																			_lockRectangle = new Rectangle(p.X, p.Y, (int)sizeF.Width, (int)sizeF.Height);
																			graphics.FillRectangle(brush, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 4f);
																			graphics.DrawString(empty, font, brush2, p, format);
																			empty = "q";
																			p.X = p.X + (int)sizeF.Width + 10;
																			sizeF = graphics.MeasureString(empty, font, channelAnalyzer.Width, format);
																			_unlockRectangle = new Rectangle(p.X, p.Y, (int)sizeF.Width, (int)sizeF.Height);
																			graphics.FillRectangle(brush, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 4f);
																			graphics.DrawString(empty, font, brush2, p, format);
																			empty = "5";
																			p.X = 37;
																			p.Y = 62;
																			sizeF = graphics.MeasureString(empty, font, channelAnalyzer.Width, format);
																			_snrPlusRectangle = new Rectangle(p.X, p.Y, (int)sizeF.Width, (int)sizeF.Height);
																			graphics.FillRectangle(brush3, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 4f);
																			graphics.DrawString(empty, font, brush2, p, format);
																			empty = "6";
																			p.Y = (int)((float)p.Y + sizeF.Height + 10f);
																			sizeF = graphics.MeasureString(empty, font, channelAnalyzer.Width, format);
																			_snrMinusRectangle = new Rectangle(p.X, p.Y, (int)sizeF.Width, (int)sizeF.Height);
																			graphics.FillRectangle(brush3, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 4f);
																			graphics.DrawString(empty, font, brush2, p, format);
																			empty = "5";
																			p.X = channelAnalyzer.Width - 35 - (int)sizeF.Width;
																			p.Y = 62;
																			sizeF = graphics.MeasureString(empty, font, channelAnalyzer.Width, format);
																			_histPlusRectangle = new Rectangle(p.X, p.Y, (int)sizeF.Width, (int)sizeF.Height);
																			graphics.FillRectangle(brush4, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 4f);
																			graphics.DrawString(empty, font, brush2, p, format);
																			empty = "6";
																			p.Y = (int)((float)p.Y + sizeF.Height + 10f);
																			sizeF = graphics.MeasureString(empty, font, channelAnalyzer.Width, format);
																			_histMinusRectangle = new Rectangle(p.X, p.Y, (int)sizeF.Width, (int)sizeF.Height);
																			graphics.FillRectangle(brush4, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 4f);
																			graphics.DrawString(empty, font, brush2, p, format);
																			empty = _infoText;
																			sizeF = graphics.MeasureString(empty, font2, channelAnalyzer.Width, format);
																			p.Y = 7;
																			p.X = channelAnalyzer.Width - 2 - 5 - (int)sizeF.Width;
																			graphics.FillRectangle(brush5, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 4f);
																			graphics.DrawString(empty, font2, brush6, p, format);
																			int num = Math.Max(2, channelAnalyzer.Height - 2 - _scanLevel * 2);
																			graphics.DrawLine(pen, 2, num, channelAnalyzer.Width - 2, num);
																			int num2 = Math.Max(2, channelAnalyzer.Height - 2 - _hysteresis * 2);
																			graphics.DrawLine(pen2, 2, num2, channelAnalyzer.Width - 2, num2);
																			if (_selectFrequency)
																			{
																				int num3 = _oldX;
																				int num4 = _trackingX;
																				if (_startSelectX != 0 && _endSelectX != 0)
																				{
																					num3 = _startSelectX;
																					num4 = _endSelectX;
																				}
																				if (num3 > num4)
																				{
																					int num5 = num3;
																					num3 = num4;
																					num4 = num5;
																				}
																				graphics.FillRectangle(brush7, num3, 2, num4 - num3, channelAnalyzer.Height - 4);
																			}
																			if (_hotTrackNeeded && _trackingX >= 2 && _trackingX <= channelAnalyzer.Width - 2 && _trackingY >= 2 && _trackingY <= channelAnalyzer.Height - 2)
																			{
																				graphics.DrawLine(pen3, _trackingX, 0, _trackingX, channelAnalyzer.Height);
																				string text;
																				if (_selectFrequency && _oldFrequency != _trackingFrequency)
																				{
																					text = $"{GetFrequencyDisplay(_oldFrequency)}-{GetFrequencyDisplay(_trackingFrequency)}";
																				}
																				else
																				{
																					text = $"{GetFrequencyDisplay(_trackingFrequency)}";
																					if (_channelList[_trackingIndex].IsStore)
																					{
																						text = text + " " + _channelList[_trackingIndex].StoreName;
																					}
																				}
																				sizeF = graphics.MeasureString(text, font3, channelAnalyzer.Width, format);
																				p.X = _trackingX + 5;
																				if ((float)p.X + sizeF.Width + 2f > (float)channelAnalyzer.Width)
																				{
																					p.X = (int)((float)(_trackingX - 5) - sizeF.Width);
																				}
																				p.Y = 52;
																				graphics.FillRectangle(brush5, p.X - 1, p.Y - 2, sizeF.Width + 2f, sizeF.Height + 2f);
																				graphics.DrawString(text, font3, brush6, p, format);
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		private void buttonsRepeaterTimer_Tick(object sender, EventArgs e)
		{
			switch (_buttons)
			{
			case Buttons.None:
				break;
			case Buttons.SNRUp:
				if (_scanLevel < 120)
				{
					_scanLevel++;
				}
				break;
			case Buttons.SNRDown:
				if (_scanLevel > _hysteresis)
				{
					_scanLevel--;
				}
				break;
			case Buttons.HystUp:
				if (_hysteresis < _scanLevel)
				{
					_hysteresis++;
				}
				break;
			case Buttons.HystDown:
				if (_hysteresis > 0)
				{
					_hysteresis--;
				}
				break;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager componentResourceManager = new System.ComponentModel.ComponentResourceManager(typeof(SDRSharp.FrequencyScanner.FrequencyScannerPanel));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			mainToolStrip = new System.Windows.Forms.ToolStrip();
			btnNewEntry = new System.Windows.Forms.ToolStripButton();
			btnDelete = new System.Windows.Forms.ToolStripButton();
			frequencyDataGridView = new System.Windows.Forms.DataGridView();
			Activity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			scanModeComboBox = new System.Windows.Forms.ComboBox();
			ScanButton = new System.Windows.Forms.Button();
			FrequencyRangeEditButton = new System.Windows.Forms.Button();
			newFrequencyDisplayUpdateTimer = new System.Windows.Forms.Timer(components);
			configureButton = new System.Windows.Forms.Button();
			autoCleanTimer = new System.Windows.Forms.Timer(components);
			timeConstantLabel = new System.Windows.Forms.Label();
			testDisplayTimer = new System.Windows.Forms.Timer(components);
			frequencyRangeListBox = new System.Windows.Forms.ListBox();
			tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			iControlsUpdateTimer = new System.Windows.Forms.Timer(components);
			refreshTimer = new System.Windows.Forms.Timer(components);
			label1 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			detectNumericUpDown = new System.Windows.Forms.NumericUpDown();
			waitNumericUpDown = new System.Windows.Forms.NumericUpDown();
			buttonsRepeaterTimer = new System.Windows.Forms.Timer(components);
			frequencyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			memoryEntryBindingSource = new System.Windows.Forms.BindingSource(components);
			mainToolStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)frequencyDataGridView).BeginInit();
			tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)detectNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)waitNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)memoryEntryBindingSource).BeginInit();
			SuspendLayout();
			mainToolStrip.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			mainToolStrip.AutoSize = false;
			mainToolStrip.Dock = System.Windows.Forms.DockStyle.None;
			mainToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[2]
			{
				btnNewEntry,
				btnDelete
			});
			mainToolStrip.Location = new System.Drawing.Point(0, 204);
			mainToolStrip.Name = "mainToolStrip";
			mainToolStrip.Size = new System.Drawing.Size(217, 23);
			mainToolStrip.TabIndex = 7;
			btnNewEntry.Image = (System.Drawing.Image)componentResourceManager.GetObject("btnNewEntry.Image");
			btnNewEntry.ImageTransparentColor = System.Drawing.Color.Magenta;
			btnNewEntry.Name = "btnNewEntry";
			btnNewEntry.Size = new System.Drawing.Size(54, 20);
			btnNewEntry.Text = "Clear";
			btnNewEntry.ToolTipText = "Clear";
			btnNewEntry.Click += new System.EventHandler(btnClearEntry_Click);
			btnDelete.Image = (System.Drawing.Image)componentResourceManager.GetObject("btnDelete.Image");
			btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
			btnDelete.Name = "btnDelete";
			btnDelete.Size = new System.Drawing.Size(60, 20);
			btnDelete.Text = "Delete";
			btnDelete.Click += new System.EventHandler(btnDelete_Click);
			frequencyDataGridView.AllowUserToAddRows = false;
			frequencyDataGridView.AllowUserToDeleteRows = false;
			frequencyDataGridView.AllowUserToResizeRows = false;
			dataGridViewCellStyle.BackColor = System.Drawing.Color.WhiteSmoke;
			frequencyDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle;
			frequencyDataGridView.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			frequencyDataGridView.AutoGenerateColumns = false;
			frequencyDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			frequencyDataGridView.Columns.AddRange(Activity, frequencyDataGridViewTextBoxColumn);
			frequencyDataGridView.DataSource = memoryEntryBindingSource;
			frequencyDataGridView.Location = new System.Drawing.Point(0, 229);
			frequencyDataGridView.Margin = new System.Windows.Forms.Padding(2);
			frequencyDataGridView.Name = "frequencyDataGridView";
			frequencyDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
			frequencyDataGridView.RowHeadersVisible = false;
			frequencyDataGridView.RowTemplate.Height = 24;
			frequencyDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			frequencyDataGridView.ShowCellErrors = false;
			frequencyDataGridView.ShowCellToolTips = false;
			frequencyDataGridView.ShowEditingIcon = false;
			frequencyDataGridView.ShowRowErrors = false;
			frequencyDataGridView.Size = new System.Drawing.Size(215, 186);
			frequencyDataGridView.TabIndex = 6;
			frequencyDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(frequencyDataGridView_CellDoubleClick);
			frequencyDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(frequencyDataGridView_CellFormatting);
			frequencyDataGridView.SelectionChanged += new System.EventHandler(frequencyDataGridView_SelectionChanged);
			frequencyDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(frequencyDataGridView_KeyDown);
			Activity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			Activity.DataPropertyName = "Activity";
			dataGridViewCellStyle2.Format = "N2";
			dataGridViewCellStyle2.NullValue = null;
			Activity.DefaultCellStyle = dataGridViewCellStyle2;
			Activity.HeaderText = "Activity time s.";
			Activity.Name = "Activity";
			Activity.ReadOnly = true;
			scanModeComboBox.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			scanModeComboBox.AutoCompleteCustomSource.AddRange(new string[2]
			{
				"screen with playback",
				"screen without playback"
			});
			scanModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			scanModeComboBox.FormattingEnabled = true;
			scanModeComboBox.IntegralHeight = false;
			scanModeComboBox.Items.AddRange(new object[4]
			{
				"Scan all with save new",
				"Scan all without save new",
				"Scan only memorized except new",
				"Scan only new except memorized"
			});
			scanModeComboBox.Location = new System.Drawing.Point(3, 4);
			scanModeComboBox.Margin = new System.Windows.Forms.Padding(2);
			scanModeComboBox.Name = "scanModeComboBox";
			scanModeComboBox.Size = new System.Drawing.Size(211, 21);
			scanModeComboBox.TabIndex = 4;
			scanModeComboBox.SelectedIndexChanged += new System.EventHandler(scanModeComboBox_SelectedIndexChanged);
			ScanButton.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			ScanButton.Cursor = System.Windows.Forms.Cursors.Default;
			ScanButton.FlatAppearance.BorderSize = 0;
			ScanButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
			ScanButton.Location = new System.Drawing.Point(89, 3);
			ScanButton.Name = "ScanButton";
			ScanButton.Size = new System.Drawing.Size(125, 21);
			ScanButton.TabIndex = 22;
			ScanButton.Text = "Scan";
			ScanButton.UseCompatibleTextRendering = true;
			ScanButton.UseMnemonic = false;
			ScanButton.UseVisualStyleBackColor = false;
			ScanButton.Click += new System.EventHandler(ScanButton_Click);
			FrequencyRangeEditButton.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			FrequencyRangeEditButton.Location = new System.Drawing.Point(3, 119);
			FrequencyRangeEditButton.Name = "FrequencyRangeEditButton";
			FrequencyRangeEditButton.Size = new System.Drawing.Size(211, 23);
			FrequencyRangeEditButton.TabIndex = 32;
			FrequencyRangeEditButton.Text = "Edit scan ranges";
			FrequencyRangeEditButton.UseVisualStyleBackColor = true;
			FrequencyRangeEditButton.Click += new System.EventHandler(FrequencyRangeEditButton_Click);
			newFrequencyDisplayUpdateTimer.Interval = 500;
			newFrequencyDisplayUpdateTimer.Tick += new System.EventHandler(newFrequencyDisplayUpdateTimer_Tick);
			configureButton.Cursor = System.Windows.Forms.Cursors.Default;
			configureButton.FlatAppearance.BorderSize = 0;
			configureButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
			configureButton.Location = new System.Drawing.Point(3, 3);
			configureButton.Name = "configureButton";
			configureButton.Size = new System.Drawing.Size(78, 21);
			configureButton.TabIndex = 22;
			configureButton.Text = "Configure";
			configureButton.UseCompatibleTextRendering = true;
			configureButton.UseMnemonic = false;
			configureButton.UseVisualStyleBackColor = false;
			configureButton.Click += new System.EventHandler(configureButton_Click);
			autoCleanTimer.Interval = 100000;
			autoCleanTimer.Tick += new System.EventHandler(autoCleanTimer_Tick);
			timeConstantLabel.AutoSize = true;
			timeConstantLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			timeConstantLabel.Location = new System.Drawing.Point(167, 211);
			timeConstantLabel.Name = "timeConstantLabel";
			timeConstantLabel.Size = new System.Drawing.Size(35, 13);
			timeConstantLabel.TabIndex = 36;
			timeConstantLabel.Text = "00 ms";
			timeConstantLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			testDisplayTimer.Enabled = true;
			testDisplayTimer.Tick += new System.EventHandler(testDisplayTimer_Tick);
			frequencyRangeListBox.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			frequencyRangeListBox.BackColor = System.Drawing.SystemColors.Menu;
			frequencyRangeListBox.FormattingEnabled = true;
			frequencyRangeListBox.Location = new System.Drawing.Point(3, 30);
			frequencyRangeListBox.Name = "frequencyRangeListBox";
			frequencyRangeListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			frequencyRangeListBox.Size = new System.Drawing.Size(211, 82);
			frequencyRangeListBox.TabIndex = 37;
			frequencyRangeListBox.Tag = "";
			tableLayoutPanel1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			tableLayoutPanel1.ColumnCount = 2;
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			tableLayoutPanel1.Controls.Add(ScanButton, 1, 0);
			tableLayoutPanel1.Controls.Add(configureButton, 0, 0);
			tableLayoutPanel1.Location = new System.Drawing.Point(0, 141);
			tableLayoutPanel1.Name = "tableLayoutPanel1";
			tableLayoutPanel1.RowCount = 1;
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100f));
			tableLayoutPanel1.Size = new System.Drawing.Size(217, 28);
			tableLayoutPanel1.TabIndex = 38;
			iControlsUpdateTimer.Interval = 1;
			iControlsUpdateTimer.Tick += new System.EventHandler(iControlsUpdateTimer_Tick);
			refreshTimer.Enabled = true;
			refreshTimer.Interval = 50;
			refreshTimer.Tick += new System.EventHandler(refreshTimer_Tick);
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(5, 177);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(39, 13);
			label1.TabIndex = 39;
			label1.Text = "Detect";
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(112, 177);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(29, 13);
			label2.TabIndex = 40;
			label2.Text = "Wait";
			detectNumericUpDown.Location = new System.Drawing.Point(50, 175);
			detectNumericUpDown.Maximum = new decimal(new int[4]
			{
				1000,
				0,
				0,
				0
			});
			detectNumericUpDown.Name = "detectNumericUpDown";
			detectNumericUpDown.Size = new System.Drawing.Size(48, 20);
			detectNumericUpDown.TabIndex = 41;
			detectNumericUpDown.ValueChanged += new System.EventHandler(detectNumericUpDown_ValueChanged);
			waitNumericUpDown.DecimalPlaces = 1;
			waitNumericUpDown.Location = new System.Drawing.Point(147, 175);
			waitNumericUpDown.Maximum = new decimal(new int[4]
			{
				10000,
				0,
				0,
				0
			});
			waitNumericUpDown.Name = "waitNumericUpDown";
			waitNumericUpDown.Size = new System.Drawing.Size(55, 20);
			waitNumericUpDown.TabIndex = 42;
			waitNumericUpDown.ValueChanged += new System.EventHandler(waitNumericUpDown_ValueChanged);
			buttonsRepeaterTimer.Tick += new System.EventHandler(buttonsRepeaterTimer_Tick);
			frequencyDataGridViewTextBoxColumn.DataPropertyName = "Frequency";
			frequencyDataGridViewTextBoxColumn.HeaderText = "Frequency";
			frequencyDataGridViewTextBoxColumn.Name = "frequencyDataGridViewTextBoxColumn";
			frequencyDataGridViewTextBoxColumn.ReadOnly = true;
			memoryEntryBindingSource.DataSource = typeof(SDRSharp.FrequencyScanner.MemoryEntry);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
			base.Controls.Add(waitNumericUpDown);
			base.Controls.Add(detectNumericUpDown);
			base.Controls.Add(label2);
			base.Controls.Add(label1);
			base.Controls.Add(tableLayoutPanel1);
			base.Controls.Add(frequencyRangeListBox);
			base.Controls.Add(timeConstantLabel);
			base.Controls.Add(FrequencyRangeEditButton);
			base.Controls.Add(scanModeComboBox);
			base.Controls.Add(mainToolStrip);
			base.Controls.Add(frequencyDataGridView);
			base.Margin = new System.Windows.Forms.Padding(2);
			base.Name = "FrequencyScannerPanel";
			base.Size = new System.Drawing.Size(217, 417);
			mainToolStrip.ResumeLayout(performLayout: false);
			mainToolStrip.PerformLayout();
			((System.ComponentModel.ISupportInitialize)frequencyDataGridView).EndInit();
			tableLayoutPanel1.ResumeLayout(performLayout: false);
			((System.ComponentModel.ISupportInitialize)detectNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)waitNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)memoryEntryBindingSource).EndInit();
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

using System;
using System.Collections.Generic;

namespace SDRSharp.FrequencyScanner
{
	[Serializable]
	public class SerializableDictionary<TKey, TValue>
	{
		private List<TKey> _keys = new List<TKey>();

		private List<TValue> _values = new List<TValue>();

		public TValue this[TKey index]
		{
			get
			{
				int num = _keys.IndexOf(index);
				if (num != -1)
				{
					return _values[num];
				}
				throw new KeyNotFoundException();
			}
			set
			{
				int num = _keys.IndexOf(index);
				if (num == -1)
				{
					_keys.Add(index);
					_values.Add(value);
				}
				else
				{
					_values[num] = value;
				}
			}
		}

		public List<TKey> Keys
		{
			get
			{
				return _keys;
			}
			set
			{
				_keys = value;
			}
		}

		public List<TValue> Values
		{
			get
			{
				return _values;
			}
			set
			{
				_values = value;
			}
		}

		public bool ContainsKey(TKey key)
		{
			return _keys.IndexOf(key) != -1;
		}

		public void Clear()
		{
			_keys.Clear();
			_values.Clear();
		}
	}
}

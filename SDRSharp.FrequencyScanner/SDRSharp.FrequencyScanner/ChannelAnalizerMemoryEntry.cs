using SDRSharp.Radio;

namespace SDRSharp.FrequencyScanner
{
	public class ChannelAnalizerMemoryEntry
	{
		private long _frequency;

		private long _centerFrequency;

		private float _level;

		private int _activity;

		private bool _skipped;

		private bool _isStore;

		private string _storeName;

		private int _filterBandwidth;

		private DetectorType _detectorType;

		private int _stepSize;

		private int _startBins;

		private int _endBins;

		public long Frequency
		{
			get
			{
				return _frequency;
			}
			set
			{
				_frequency = value;
			}
		}

		public long CenterFrequency
		{
			get
			{
				return _centerFrequency;
			}
			set
			{
				_centerFrequency = value;
			}
		}

		public string StoreName
		{
			get
			{
				return _storeName;
			}
			set
			{
				_storeName = value;
			}
		}

		public bool IsStore
		{
			get
			{
				return _isStore;
			}
			set
			{
				_isStore = value;
			}
		}

		public float Level
		{
			get
			{
				return _level;
			}
			set
			{
				_level = value;
			}
		}

		public bool Skipped
		{
			get
			{
				return _skipped;
			}
			set
			{
				_skipped = value;
			}
		}

		public int Activity
		{
			get
			{
				return _activity;
			}
			set
			{
				_activity = value;
			}
		}

		public int StartBins
		{
			get
			{
				return _startBins;
			}
			set
			{
				_startBins = value;
			}
		}

		public int EndBins
		{
			get
			{
				return _endBins;
			}
			set
			{
				_endBins = value;
			}
		}

		public int FilterBandwidth
		{
			get
			{
				return _filterBandwidth;
			}
			set
			{
				_filterBandwidth = value;
			}
		}

		public int StepSize
		{
			get
			{
				return _stepSize;
			}
			set
			{
				_stepSize = value;
			}
		}

		public DetectorType DetectorType
		{
			get
			{
				return _detectorType;
			}
			set
			{
				_detectorType = value;
			}
		}

		public ChannelAnalizerMemoryEntry()
		{
		}

		public ChannelAnalizerMemoryEntry(ChannelAnalizerMemoryEntry ChannelAnalizerMemoryEntry)
		{
			_frequency = ChannelAnalizerMemoryEntry._frequency;
			_centerFrequency = ChannelAnalizerMemoryEntry._centerFrequency;
			_level = ChannelAnalizerMemoryEntry._level;
			_activity = ChannelAnalizerMemoryEntry._activity;
			_skipped = ChannelAnalizerMemoryEntry._skipped;
			_isStore = ChannelAnalizerMemoryEntry._isStore;
			_storeName = ChannelAnalizerMemoryEntry._storeName;
			_filterBandwidth = ChannelAnalizerMemoryEntry._filterBandwidth;
			_detectorType = ChannelAnalizerMemoryEntry._detectorType;
			_stepSize = ChannelAnalizerMemoryEntry._stepSize;
			_startBins = ChannelAnalizerMemoryEntry._startBins;
			_endBins = ChannelAnalizerMemoryEntry._endBins;
		}
	}
}

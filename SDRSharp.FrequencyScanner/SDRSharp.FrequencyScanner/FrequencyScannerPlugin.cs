using SDRSharp.Common;
using System.Windows.Forms;

namespace SDRSharp.FrequencyScanner
{
	public class FrequencyScannerPlugin : ISharpPlugin
	{
		private const string _displayName = "Frequency Scanner";

		private ISharpControl _controlInterface;

		private FrequencyScannerPanel _frequencyScannerPanel;

		public UserControl Gui => _frequencyScannerPanel;

		public string DisplayName => "Frequency Scanner";

		public void Close()
		{
			if (_frequencyScannerPanel != null)
			{
				_frequencyScannerPanel.ScanStop();
				_frequencyScannerPanel.SaveSettings();
			}
		}

		public void Initialize(ISharpControl control)
		{
			_controlInterface = control;
			_frequencyScannerPanel = new FrequencyScannerPanel(_controlInterface);
		}
	}
}

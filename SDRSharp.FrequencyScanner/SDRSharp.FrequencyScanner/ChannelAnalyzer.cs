using SDRSharp.Radio;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace SDRSharp.FrequencyScanner
{
	public class ChannelAnalyzer : UserControl
	{
		private const float TrackingFontSize = 16f;

		private const int CarrierPenWidth = 1;

		private const int GradientAlpha = 180;

		public const int AxisMargin = 2;

		public const float DefaultCursorHeight = 32f;

		private bool _drawBackgroundNeeded;

		private Bitmap _bkgBuffer;

		private Bitmap _buffer;

		private Graphics _graphics;

		private float _xIncrement;

		private float _yIncrement;

		private long _playFrequency;

		private int _playFrequencyIndex;

		private List<ChannelAnalizerMemoryEntry> _channelArray;

		private int _zoom;

		private int _zoomPosition;

		private float _defXIncrement;

		public long Frequency
		{
			set
			{
				_playFrequency = value;
			}
		}

		public int Zoom
		{
			get
			{
				return _zoom;
			}
			set
			{
				_zoom = value;
			}
		}

		public int ZoomPosition
		{
			get
			{
				return _zoomPosition;
			}
			set
			{
				_zoomPosition = value;
			}
		}

		public event CustomPaintEventHandler CustomPaint;

		public ChannelAnalyzer()
		{
			_bkgBuffer = new Bitmap(base.ClientRectangle.Width, base.ClientRectangle.Height, PixelFormat.Format32bppPArgb);
			_buffer = new Bitmap(base.ClientRectangle.Width, base.ClientRectangle.Height, PixelFormat.Format32bppPArgb);
			_graphics = Graphics.FromImage(_buffer);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.DoubleBuffer, value: true);
			SetStyle(ControlStyles.UserPaint, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			UpdateStyles();
		}

		~ChannelAnalyzer()
		{
			_buffer.Dispose();
			_graphics.Dispose();
		}

		public void Perform(List<ChannelAnalizerMemoryEntry> channelArray)
		{
			_channelArray = channelArray;
			if (_drawBackgroundNeeded)
			{
				DrawBackground();
			}
			DrawForeground();
			Invalidate();
			_drawBackgroundNeeded = false;
		}

		private void DrawBackground()
		{
			using (SolidBrush brush = new SolidBrush(Color.Silver))
			{
				using (Pen pen = new Pen(Color.FromArgb(80, 80, 80)))
				{
					using (new Pen(Color.DarkGray))
					{
						using (Font font = new Font("Arial", 8f))
						{
							using (Graphics graphics = Graphics.FromImage(_bkgBuffer))
							{
								ConfigureGraphics(graphics);
								graphics.Clear(Color.Black);
								pen.DashStyle = DashStyle.Dash;
								int num = 20;
								int num2 = (base.ClientRectangle.Height - 4) / num;
								for (int i = 1; i <= num2; i++)
								{
									graphics.DrawLine(pen, 2, base.ClientRectangle.Height - 2 - i * num, base.ClientRectangle.Width - 2, base.ClientRectangle.Height - 2 - i * num);
								}
								for (int j = 1; j <= num2; j++)
								{
									string text = (j * num / 2 - 10).ToString();
									SizeF sizeF = graphics.MeasureString(text, font);
									float width = sizeF.Width;
									float height = sizeF.Height;
									graphics.DrawString(text, font, brush, 4f, (float)(base.ClientRectangle.Height - 2 - j * num) - height + 1f);
								}
							}
						}
					}
				}
			}
		}

		private string GetFrequencyDisplay(long frequency)
		{
			if (frequency == 0L)
			{
				return "DC";
			}
			if (Math.Abs(frequency) > 1500000000)
			{
				return $"{(double)frequency / 1000000000.0:#,0.000 000}GHz";
			}
			if (Math.Abs(frequency) > 30000000)
			{
				return $"{(double)frequency / 1000000.0:0,0.000###}MHz";
			}
			if (Math.Abs(frequency) > 1000)
			{
				return $"{(double)frequency / 1000.0:#,#.###}kHz";
			}
			return $"{frequency}Hz";
		}

		public static void ConfigureGraphics(Graphics graphics)
		{
			graphics.CompositingMode = CompositingMode.SourceOver;
			graphics.CompositingQuality = CompositingQuality.HighSpeed;
			graphics.SmoothingMode = SmoothingMode.None;
			graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
			graphics.InterpolationMode = InterpolationMode.High;
		}

		private void DrawForeground()
		{
			if (base.ClientRectangle.Width > 2 && base.ClientRectangle.Height > 2)
			{
				CopyBackground();
				DrawSpectrum();
				OnCustomPaint(new CustomPaintEventArgs(_graphics));
			}
		}

		private unsafe void CopyBackground()
		{
			BitmapData bitmapData = _buffer.LockBits(base.ClientRectangle, ImageLockMode.WriteOnly, _buffer.PixelFormat);
			BitmapData bitmapData2 = _bkgBuffer.LockBits(base.ClientRectangle, ImageLockMode.ReadOnly, _bkgBuffer.PixelFormat);
			Utils.Memcpy((void*)bitmapData.Scan0, (void*)bitmapData2.Scan0, Math.Abs(bitmapData.Stride) * bitmapData.Height);
			_buffer.UnlockBits(bitmapData);
			_bkgBuffer.UnlockBits(bitmapData2);
		}

		private void DrawSpectrum()
		{
			if (_channelArray != null && _channelArray.Count != 0)
			{
				_xIncrement = (float)(base.ClientRectangle.Width - 4) / (float)_channelArray.Count * (float)(_zoom + 1);
				_yIncrement = 2f;
				_defXIncrement = (float)(base.ClientRectangle.Width - 4) / (float)_channelArray.Count;
				float num = 0f;
				float num2 = 0f;
				_playFrequencyIndex = -1;
				using (Pen pen = new Pen(Color.SkyBlue))
				{
					using (Pen pen4 = new Pen(Color.DarkRed))
					{
						using (Pen pen5 = new Pen(Color.Yellow))
						{
							using (Pen pen3 = new Pen(Color.Green))
							{
								for (int i = 0; i < _channelArray.Count; i++)
								{
									float num3 = (float)Math.Max((int)_channelArray[i].Level, 4) * _yIncrement;
									float num4 = _xIncrement - 1f;
									if (_xIncrement < 3f)
									{
										num4 = _xIncrement;
									}
									float num5 = 2f + (float)i * _xIncrement - (float)(_zoomPosition * (_zoom + 1)) + (float)_zoomPosition + num4 * 0.5f;
									float num6 = Math.Max(2, (int)((float)(base.ClientRectangle.Height - 2) - num3));
									num3 = Math.Min(num3, base.ClientRectangle.Height - 4);
									if (!(num5 < 2f) && !(num5 > (float)(base.ClientRectangle.Width - 2)))
									{
										Pen pen2 = pen;
										if (_playFrequency > _channelArray[i].Frequency - _channelArray[i].StepSize / 2 && _playFrequency < _channelArray[i].Frequency + _channelArray[i].StepSize / 2)
										{
											num = num5;
											num2 = num6;
											_playFrequencyIndex = i;
										}
										if (_channelArray[i].IsStore)
										{
											pen2 = pen3;
										}
										if (_channelArray[i].Skipped)
										{
											pen2 = ((!_channelArray[i].IsStore) ? pen4 : pen5);
										}
										pen2.Width = num4;
										_graphics.DrawLine(pen2, num5, num6, num5, num6 + num3);
									}
								}
							}
						}
					}
				}
				using (SolidBrush brush = new SolidBrush(Color.White))
				{
					using (Pen pen6 = new Pen(Color.White))
					{
						using (Font font = new Font("Arial", 8f))
						{
							if (_playFrequencyIndex >= 0 && _playFrequencyIndex < _channelArray.Count)
							{
								string str = GetFrequencyDisplay(_channelArray[_playFrequencyIndex].Frequency) + " ";
								str = ((!_channelArray[_playFrequencyIndex].IsStore) ? (str + "Unknown") : (str + _channelArray[_playFrequencyIndex].StoreName));
								SizeF sizeF = _graphics.MeasureString(str, font);
								float width = sizeF.Width;
								float height = sizeF.Height;
								float num7 = 0f;
								int num8 = 62;
								_graphics.DrawLine(pen6, num, num2 - 10f, num, (float)num8 + height + 4f);
								num7 = ((!(num + width + 10f > (float)base.ClientRectangle.Width)) ? num : (num - width));
								_graphics.DrawLine(pen6, num7, (float)num8 + height + 4f, num7 + width, (float)num8 + height + 4f);
								_graphics.DrawString(str, font, brush, num7, num8);
							}
						}
					}
				}
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			ConfigureGraphics(e.Graphics);
			e.Graphics.DrawImageUnscaled(_buffer, 0, 0);
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			if (base.ClientRectangle.Width > 0 && base.ClientRectangle.Height > 0)
			{
				_buffer.Dispose();
				_graphics.Dispose();
				_bkgBuffer.Dispose();
				_buffer = new Bitmap(base.ClientRectangle.Width, base.ClientRectangle.Height, PixelFormat.Format32bppPArgb);
				_graphics = Graphics.FromImage(_buffer);
				ConfigureGraphics(_graphics);
				_bkgBuffer = new Bitmap(base.ClientRectangle.Width, base.ClientRectangle.Height, PixelFormat.Format32bppPArgb);
				_drawBackgroundNeeded = true;
				Perform(_channelArray);
			}
		}

		protected virtual void OnCustomPaint(CustomPaintEventArgs e)
		{
			this.CustomPaint?.Invoke(this, e);
		}

		public int CurrentChannelIndex()
		{
			return _playFrequencyIndex;
		}

		public int PointToChannel(float point)
		{
			int num = 0;
			num = (int)(((float)(_zoomPosition * (_zoom + 1)) + point - (float)_zoomPosition - 2f) / _xIncrement);
			if (num < 0)
			{
				num = 0;
			}
			if (num >= _channelArray.Count)
			{
				num = _channelArray.Count - 1;
			}
			return num;
		}

		private void InitializeComponent()
		{
			SuspendLayout();
			AutoSize = true;
			DoubleBuffered = true;
			base.Name = "ChannelAnalyzer";
			ResumeLayout(performLayout: false);
		}
	}
}

using SDRSharp.Common;
using SDRSharp.PanView;
using SDRSharp.Radio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.FreqMan
{
	[Description("RF Memory Management Panel")]
	[DesignTimeVisible(true)]
	[Category("SDRSharp")]
	public class FrequencyManagerPanel : UserControl
	{
		private enum ScanState
		{
			FreqChange,
			SquelchPause,
			SquelchOpen,
			PauseAfter
		}

		private const string AllGroups = "[All Groups]";

		private const string FavouriteGroup = "[Favourites]";

		private KeyboardPanel _keyboardPanel;

		private readonly SortableBindingList<MemoryEntry> _displayedEntries = new SortableBindingList<MemoryEntry>();

		private readonly List<MemoryEntry> _entries;

		private readonly SettingsPersister _settingsPersister;

		private readonly List<string> _groups = new List<string>();

		private List<VisibleEntry> _visibleEntries;

		private ISharpControl _controlInterface;

		private ScanState _state;

		private int _scanIndex;

		private int _interval;

		private bool _changeFrequencyInMenedger;

		private bool _nextButtonClick;

		private bool _requestToStopScanner;

		private IContainer components;

		private ToolStrip mainToolStrip;

		private ToolStripButton btnNewEntry;

		private ToolStripButton btnEdit;

		private ToolStripButton btnDelete;

		private DataGridView frequencyDataGridView;

		private ComboBox comboGroups;

		private BindingSource memoryEntryBindingSource;

		private Timer ScanTimer;

		private Button ScanButton;

		private Button configureButton;

		private DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;

		private DataGridViewTextBoxColumn frequencyDataGridViewTextBoxColumn;

		private DataGridViewCheckBoxColumn ScanCheckBox;

		private TableLayoutPanel tableLayoutPanel1;

		public float Pause
		{
			get;
			set;
		}

		public float DetectLevel
		{
			get;
			set;
		}

		public float Hysteresis
		{
			get;
			set;
		}

		public int Detect
		{
			get;
			set;
		}

		public bool EnableOutToPanView
		{
			get;
			set;
		}

		public bool ToSpectrumOrWaterfall
		{
			get;
			set;
		}

		public Font TextFont
		{
			get;
			set;
		}

		public int RowsNumber
		{
			get;
			set;
		}

		public bool TrackingFrequency
		{
			get;
			set;
		}

		public int ManagerPluginPosition
		{
			get;
			set;
		}

		public bool ScanPause
		{
			get;
			set;
		}

		public string SelectedGroup
		{
			get
			{
				return (string)comboGroups.SelectedItem;
			}
			set
			{
				if (value != null && comboGroups.Items.IndexOf(value) != -1)
				{
					comboGroups.SelectedIndex = comboGroups.Items.IndexOf(value);
				}
			}
		}

		public FrequencyManagerPanel(ISharpControl control)
		{
			InitializeComponent();
			_controlInterface = control;
			_controlInterface.PropertyChanged += PropertyChangedHandler;
			_visibleEntries = new List<VisibleEntry>();
			if (LicenseManager.UsageMode == LicenseUsageMode.Runtime)
			{
				_settingsPersister = new SettingsPersister();
				_entries = _settingsPersister.ReadStoredFrequencies();
				_groups = GetGroupsFromEntries();
				ProcessGroups(Utils.GetStringSetting("SelectedFreqManagerGroup", null));
			}
			memoryEntryBindingSource.DataSource = _displayedEntries;
			FontConverter fontConverter = new FontConverter();
			TextFont = (Font)fontConverter.ConvertFromString(Utils.GetStringSetting("configureWord_Font", fontConverter.ConvertToString(Font)));
			EnableOutToPanView = Utils.GetBooleanSetting("configureWord_panviewEnable");
			ToSpectrumOrWaterfall = Utils.GetBooleanSetting("configureWord_toSpectrum");
			TrackingFrequency = Utils.GetBooleanSetting("CheckFrequencyOnEntry");
			RowsNumber = Utils.GetIntSetting("AdditionalRowsInTable", 10);
			ManagerPluginPosition = Utils.GetIntSetting("FrequencyManagerPluginPosition", 0);
			ConfigurePanelHeight(RowsNumber);
			ConfigureOutToPanView();
			Detect = Utils.GetIntSetting("ChangeFreqPauseInMs", 100);
			Pause = (float)Utils.GetDoubleSetting("NextFreqPauseInSecond", 2.0);
			ScanButton.Enabled = false;
		}

		private void _keyboardPanel_PauseButtonClick(object sender, EventArgs e)
		{
			ScanPause = !ScanPause;
		}

		private void _keyboardPanel_NextButtonClick(object sender, EventArgs e)
		{
			_nextButtonClick = true;
		}

		private void ConfigurePanelHeight(int addRows)
		{
			if (ManagerPluginPosition == 0)
			{
				base.Height += (addRows - 5) * 24;
			}
		}

		private void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			string propertyName;
			if ((propertyName = e.PropertyName) == null)
			{
				return;
			}
			if (!(propertyName == "StartRadio"))
			{
				if (!(propertyName == "StopRadio"))
				{
					if (!(propertyName == "Frequency"))
					{
						if (propertyName == "CenterFrequency" && EnableOutToPanView)
						{
							UpdateVisibleFrequencies();
						}
					}
					else if (!_changeFrequencyInMenedger && TrackingFrequency)
					{
						CheckFrequencyOnEntry(_controlInterface.Frequency);
					}
				}
				else
				{
					if (ScanButton.Text == "Stop")
					{
						ScanStartStop();
					}
					ScanButton.Enabled = false;
				}
			}
			else
			{
				ScanButton.Enabled = true;
				if (EnableOutToPanView)
				{
					UpdateVisibleFrequencies();
				}
			}
		}

		private void btnNewEntry_Click(object sender, EventArgs e)
		{
			Bookmark();
		}

		private void btnEdit_Click(object sender, EventArgs e)
		{
			if (memoryEntryBindingSource.Current != null)
			{
				DoEdit((MemoryEntry)memoryEntryBindingSource.Current, isNew: false);
			}
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			MemoryEntry memoryEntry = (MemoryEntry)memoryEntryBindingSource.Current;
			if (memoryEntry != null && MessageBox.Show("Are you sure that you want to delete '" + memoryEntry.Name + "'?", "Delete Entry", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				_entries.Remove(memoryEntry);
				_settingsPersister.PersistStoredFrequencies(_entries);
				_displayedEntries.Remove(memoryEntry);
			}
		}

		private void DoEdit(MemoryEntry memoryEntry, bool isNew)
		{
			DialogEntryInfo dialogEntryInfo = new DialogEntryInfo(memoryEntry, _groups);
			if (dialogEntryInfo.ShowDialog() != DialogResult.OK)
			{
				return;
			}
			if (isNew)
			{
				_entries.Add(memoryEntry);
				_entries.Sort((MemoryEntry e1, MemoryEntry e2) => e1.Frequency.CompareTo(e2.Frequency));
			}
			_settingsPersister.PersistStoredFrequencies(_entries);
			if (!_groups.Contains(memoryEntry.GroupName))
			{
				_groups.Add(memoryEntry.GroupName);
				ProcessGroups(memoryEntry.GroupName);
			}
			else if ((string)comboGroups.SelectedItem == "[All Groups]" || (string)comboGroups.SelectedItem == memoryEntry.GroupName || ((string)comboGroups.SelectedItem == "[Favourites]" && memoryEntry.IsFavourite))
			{
				if (isNew)
				{
					_displayedEntries.Add(memoryEntry);
				}
			}
			else
			{
				comboGroups.SelectedItem = memoryEntry.GroupName;
			}
		}

		private List<string> GetGroupsFromEntries()
		{
			List<string> list = new List<string>();
			foreach (MemoryEntry entry in _entries)
			{
				if (!list.Contains(entry.GroupName))
				{
					list.Add(entry.GroupName);
				}
			}
			return list;
		}

		private void frequencyDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			if (frequencyDataGridView.Columns[e.ColumnIndex].DataPropertyName == "Frequency" && e.Value != null)
			{
				long frequency = (long)e.Value;
				e.Value = GetFrequencyDisplay(frequency);
				e.FormattingApplied = true;
			}
		}

		private void frequencyDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			Navigate();
		}

		private void ProcessGroups(string selectedGroupName)
		{
			_groups.Sort();
			comboGroups.Items.Clear();
			comboGroups.Items.Add("[All Groups]");
			comboGroups.Items.Add("[Favourites]");
			comboGroups.Items.AddRange(_groups.ToArray());
			if (selectedGroupName != null)
			{
				comboGroups.SelectedItem = selectedGroupName;
			}
			else
			{
				comboGroups.SelectedIndex = 0;
			}
		}

		private void comboGroups_SelectedIndexChanged(object sender, EventArgs e)
		{
			memoryEntryBindingSource.Clear();
			_displayedEntries.Clear();
			if (comboGroups.SelectedIndex != -1)
			{
				string text = (string)comboGroups.SelectedItem;
				foreach (MemoryEntry entry in _entries)
				{
					if (text == "[All Groups]" || entry.GroupName == text || (text == "[Favourites]" && entry.IsFavourite))
					{
						_displayedEntries.Add(entry);
					}
				}
			}
		}

		private void frequencyDataGridView_SelectionChanged(object sender, EventArgs e)
		{
			btnDelete.Enabled = (frequencyDataGridView.SelectedRows.Count > 0);
			btnEdit.Enabled = (frequencyDataGridView.SelectedRows.Count > 0);
		}

		public void Bookmark()
		{
			if (!_controlInterface.IsPlaying)
			{
				return;
			}
			MemoryEntry memoryEntry = new MemoryEntry();
			memoryEntry.DetectorType = _controlInterface.DetectorType;
			memoryEntry.Frequency = _controlInterface.Frequency;
			memoryEntry.CenterFrequency = _controlInterface.CenterFrequency;
			memoryEntry.FilterBandwidth = _controlInterface.FilterBandwidth;
			memoryEntry.Shift = (_controlInterface.FrequencyShiftEnabled ? _controlInterface.FrequencyShift : 0);
			memoryEntry.GroupName = "Misc";
			if (comboGroups.SelectedIndex != -1)
			{
				string text = (string)comboGroups.SelectedItem;
				if (text != "[All Groups]" && text != "[Favourites]")
				{
					memoryEntry.GroupName = text;
				}
			}
			if (_controlInterface.DetectorType == DetectorType.WFM)
			{
				string text2 = _controlInterface.RdsProgramService.Trim();
				memoryEntry.Name = string.Empty;
				if (!string.IsNullOrEmpty(text2))
				{
					memoryEntry.Name = text2;
				}
				else
				{
					memoryEntry.Name = GetFrequencyDisplay(_controlInterface.Frequency) + " " + memoryEntry.DetectorType;
				}
			}
			else
			{
				memoryEntry.Name = GetFrequencyDisplay(_controlInterface.Frequency) + " " + memoryEntry.DetectorType;
			}
			memoryEntry.IsFavourite = true;
			DoEdit(memoryEntry, isNew: true);
		}

		public void Navigate()
		{
			if (_controlInterface.IsPlaying)
			{
				int num = (frequencyDataGridView.SelectedCells.Count > 0) ? frequencyDataGridView.SelectedCells[0].RowIndex : (-1);
				_changeFrequencyInMenedger = true;
				if (num != -1)
				{
					try
					{
						MemoryEntry memoryEntry = (MemoryEntry)memoryEntryBindingSource.List[num];
						_controlInterface.FrequencyShiftEnabled = (memoryEntry.Shift != 0);
						if (_controlInterface.FrequencyShiftEnabled)
						{
							_controlInterface.FrequencyShift = memoryEntry.Shift;
						}
						if (_controlInterface.DetectorType != memoryEntry.DetectorType)
						{
							_controlInterface.DetectorType = memoryEntry.DetectorType;
						}
						if (_controlInterface.FilterBandwidth != (int)memoryEntry.FilterBandwidth)
						{
							_controlInterface.FilterBandwidth = (int)memoryEntry.FilterBandwidth;
						}
						if (_controlInterface.CenterFrequency != memoryEntry.CenterFrequency)
						{
							_controlInterface.CenterFrequency = memoryEntry.CenterFrequency;
						}
						if (_controlInterface.Frequency != memoryEntry.Frequency)
						{
							_controlInterface.Frequency = memoryEntry.Frequency;
						}
					}
					catch (Exception ex)
					{
						MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				_changeFrequencyInMenedger = false;
			}
		}

		private void CheckFrequencyOnEntry(long frequency)
		{
			string text = null;
			string text2 = null;
			long num = 0L;
			foreach (MemoryEntry entry in _entries)
			{
				if (Math.Abs(entry.Frequency - frequency) <= _controlInterface.FilterBandwidth / 2)
				{
					text = entry.GroupName;
					text2 = entry.Name;
					num = entry.Frequency;
					break;
				}
			}
			if (text != null && text2 != null && num != 0)
			{
				if (text != (string)comboGroups.SelectedItem)
				{
					comboGroups.SelectedIndex = comboGroups.Items.IndexOf(text);
				}
				int num2 = 0;
				foreach (MemoryEntry item in memoryEntryBindingSource)
				{
					if (item.Name == text2 && item.Frequency == num)
					{
						frequencyDataGridView.ClearSelection();
						frequencyDataGridView.Rows[num2].Selected = true;
						if (!frequencyDataGridView.Rows[num2].Displayed)
						{
							frequencyDataGridView.FirstDisplayedScrollingRowIndex = num2;
						}
						break;
					}
					num2++;
				}
			}
			else
			{
				frequencyDataGridView.ClearSelection();
			}
		}

		private static string GetFrequencyDisplay(long frequency)
		{
			long num = Math.Abs(frequency);
			if (num == 0)
			{
				return "DC";
			}
			if (num > 1500000000)
			{
				return $"{(double)frequency / 1000000000.0:#,0.000 000} GHz";
			}
			if (num > 30000000)
			{
				return $"{(double)frequency / 1000000.0:0,0.000###} MHz";
			}
			if (num > 1000)
			{
				return $"{(double)frequency / 1000.0:#,#.###} kHz";
			}
			return frequency.ToString();
		}

		private void frequencyDataGridView_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				Navigate();
				e.Handled = true;
			}
		}

		private void ScanStartStop()
		{
			if (ScanButton.Text == "Scan" && _controlInterface.IsPlaying && frequencyDataGridView.RowCount > 0)
			{
				_requestToStopScanner = false;
				ScanButton.Text = "Stop";
				_keyboardPanel = new KeyboardPanel(this);
				_keyboardPanel.Visible = true;
				_keyboardPanel.NextButtonClick += _keyboardPanel_NextButtonClick;
				_keyboardPanel.PauseButtonClick += _keyboardPanel_PauseButtonClick;
				_keyboardPanel.FormClosing += _keyboardPanel_FormClosing;
				_state = ScanState.FreqChange;
				_interval = 0;
				ScanPause = false;
				_nextButtonClick = false;
				ScanTimer.Interval = 10;
				ScanTimer.Enabled = true;
			}
			else
			{
				ScanButton.Text = "Scan";
				ScanTimer.Enabled = false;
				_keyboardPanel.Visible = false;
				_keyboardPanel.Close();
				_keyboardPanel.Dispose();
				_keyboardPanel = null;
			}
		}

		private void _keyboardPanel_FormClosing(object sender, FormClosingEventArgs e)
		{
			e.Cancel = true;
			_requestToStopScanner = true;
		}

		private void ScanProcess()
		{
			if (frequencyDataGridView.RowCount <= 0 || _requestToStopScanner)
			{
				_requestToStopScanner = false;
				ScanStartStop();
				return;
			}
			if (ScanPause)
			{
				_keyboardPanel.Text = "Pause";
				return;
			}
			ScanTimer.Enabled = false;
			if (_scanIndex >= frequencyDataGridView.RowCount)
			{
				_scanIndex = 0;
			}
			_interval += ScanTimer.Interval;
			string text = string.Empty;
			switch (_state)
			{
			case ScanState.FreqChange:
			{
				text = "Scan >>>";
				frequencyDataGridView.ClearSelection();
				int num = frequencyDataGridView.RowCount;
				do
				{
					_scanIndex++;
					num--;
					if (_scanIndex == frequencyDataGridView.RowCount)
					{
						_scanIndex = 0;
					}
				}
				while (!Convert.ToBoolean(frequencyDataGridView.Rows[_scanIndex].Cells[2].Value) && num > 0);
				frequencyDataGridView.Rows[_scanIndex].Selected = true;
				Navigate();
				_state = ScanState.SquelchPause;
				_interval = 0;
				break;
			}
			case ScanState.SquelchPause:
				if (_interval >= Detect)
				{
					if (_controlInterface.IsSquelchOpen)
					{
						_state = ScanState.SquelchOpen;
						_interval = 0;
					}
					else
					{
						_state = ScanState.FreqChange;
						_interval = 0;
					}
				}
				break;
			case ScanState.SquelchOpen:
				if (!_controlInterface.IsSquelchOpen)
				{
					_state = ScanState.PauseAfter;
					_interval = 0;
				}
				else
				{
					text = "Play";
				}
				break;
			case ScanState.PauseAfter:
				if ((float)_interval <= Pause * 1000f)
				{
					text = $"Wait {Pause - (float)_interval * 0.001f:F1} s";
					if (_controlInterface.IsSquelchOpen)
					{
						_state = ScanState.SquelchOpen;
						_interval = 0;
					}
				}
				else
				{
					_state = ScanState.FreqChange;
					_interval = 0;
				}
				break;
			}
			if (_nextButtonClick)
			{
				_nextButtonClick = false;
				_state = ScanState.FreqChange;
			}
			if (text != string.Empty)
			{
				_keyboardPanel.Text = text;
			}
			ScanTimer.Enabled = true;
		}

		private void ScanButton_Click(object sender, EventArgs e)
		{
			ScanStartStop();
		}

		private void ScanTimer_Tick(object sender, EventArgs e)
		{
			ScanProcess();
		}

		private void frequencyDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
		{
			if (frequencyDataGridView.IsCurrentCellDirty)
			{
				frequencyDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
			}
			_settingsPersister.PersistStoredFrequencies(_entries);
		}

		private void frequencyDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.ColumnIndex == frequencyDataGridView.Columns["ScanCheckBox"].Index)
			{
				bool flag = true;
				if (frequencyDataGridView.Columns["ScanCheckBox"].HeaderText == "s")
				{
					frequencyDataGridView.Columns["ScanCheckBox"].HeaderText = "r";
					flag = true;
				}
				else
				{
					frequencyDataGridView.Columns["ScanCheckBox"].HeaderText = "s";
					flag = false;
				}
				foreach (MemoryEntry item in memoryEntryBindingSource)
				{
					item.IsScanned = flag;
				}
				frequencyDataGridView.Refresh();
			}
		}

		private void UpdateVisibleFrequencies()
		{
			long num = _controlInterface.CenterFrequency - _controlInterface.RFBandwidth / 2;
			long num2 = _controlInterface.CenterFrequency + _controlInterface.RFBandwidth / 2;
			if (_controlInterface.FrequencyShiftEnabled)
			{
				num += _controlInterface.FrequencyShift;
				num2 += _controlInterface.FrequencyShift;
			}
			_visibleEntries.Clear();
			foreach (MemoryEntry entry in _entries)
			{
				long num3 = entry.Frequency;
				long num4 = entry.Frequency;
				switch (entry.DetectorType)
				{
				case DetectorType.NFM:
				case DetectorType.WFM:
				case DetectorType.AM:
				case DetectorType.DSB:
				case DetectorType.CW:
				case DetectorType.RAW:
					num3 += entry.FilterBandwidth / 2;
					num4 -= entry.FilterBandwidth / 2;
					break;
				case DetectorType.USB:
					num3 += entry.FilterBandwidth;
					break;
				case DetectorType.LSB:
					num4 -= entry.FilterBandwidth;
					break;
				}
				if (num4 >= num && num3 <= num2)
				{
					VisibleEntry visibleEntry = new VisibleEntry();
					visibleEntry.Description = entry.Name;
					visibleEntry.MinFreq = num4;
					visibleEntry.MaxFreq = num3;
					VisibleEntry item = visibleEntry;
					_visibleEntries.Add(item);
				}
			}
		}

		private void controlInterface_WaterfallCustomPaint(object sender, CustomPaintEventArgs e)
		{
			Waterfall waterfall = (Waterfall)sender;
			using (SolidBrush brush = new SolidBrush(Color.FromArgb(50, Color.Silver)))
			{
				using (SolidBrush brush2 = new SolidBrush(Color.FromArgb(200, Color.White)))
				{
					using (StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic))
					{
						stringFormat.Alignment = StringAlignment.Near;
						Rectangle rect = default(Rectangle);
						Point p = default(Point);
						List<float> list = new List<float>();
						float width = e.Graphics.MeasureString("AAA", TextFont, waterfall.Width, stringFormat).Width;
						list.Add(0f);
						foreach (VisibleEntry visibleEntry in _visibleEntries)
						{
							rect.X = Math.Max(30, (int)waterfall.FrequencyToPoint(visibleEntry.MinFreq));
							rect.Y = 0;
							rect.Width = Math.Min(waterfall.Width - 30, (int)waterfall.FrequencyToPoint(visibleEntry.MaxFreq)) - rect.X;
							rect.Height = waterfall.Height;
							string description = visibleEntry.Description;
							description = ((visibleEntry.MaxFreq <= waterfall.CenterFrequency) ? ("< " + description) : (description + " >"));
							SizeF sizeF = e.Graphics.MeasureString(description, TextFont, waterfall.Width, stringFormat);
							if (visibleEntry.MaxFreq > waterfall.CenterFrequency)
							{
								p.X = (int)((float)(rect.X + rect.Width / 2) - sizeF.Width);
							}
							else
							{
								p.X = rect.X + rect.Width / 2;
							}
							for (int i = 0; i < list.Count; i++)
							{
								if ((float)p.X > list[i] + width)
								{
									p.Y = (int)Math.Min(30.0 + (double)(sizeF.Height * (float)i) * 1.25, waterfall.Height - 30);
									list[i] = (float)p.X + sizeF.Width;
									break;
								}
								if (i == list.Count - 1)
								{
									p.Y = (int)Math.Min(30f + sizeF.Height * (float)(i + 1) * 1.25f, waterfall.Height - 30);
									list.Add((float)p.X + sizeF.Width);
									break;
								}
							}
							e.Graphics.FillRectangle(brush, rect);
							e.Graphics.DrawString(description, TextFont, brush2, p, stringFormat);
						}
					}
				}
			}
		}

		private void controlInterface_SpectrumCustomPaint(object sender, CustomPaintEventArgs e)
		{
			SpectrumAnalyzer spectrumAnalyzer = (SpectrumAnalyzer)sender;
			using (SolidBrush brush = new SolidBrush(Color.FromArgb(50, Color.Silver)))
			{
				using (SolidBrush brush2 = new SolidBrush(Color.FromArgb(200, Color.White)))
				{
					using (StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic))
					{
						stringFormat.Alignment = StringAlignment.Near;
						Rectangle rect = default(Rectangle);
						Point p = default(Point);
						List<float> list = new List<float>();
						float width = e.Graphics.MeasureString("AAA", TextFont, spectrumAnalyzer.Width, stringFormat).Width;
						list.Add(0f);
						foreach (VisibleEntry visibleEntry in _visibleEntries)
						{
							rect.X = Math.Max(30, (int)spectrumAnalyzer.FrequencyToPoint(visibleEntry.MinFreq));
							rect.Y = 30;
							rect.Width = Math.Min(spectrumAnalyzer.Width - 30, (int)spectrumAnalyzer.FrequencyToPoint(visibleEntry.MaxFreq)) - rect.X;
							rect.Height = spectrumAnalyzer.Height - 60;
							string description = visibleEntry.Description;
							description = ((visibleEntry.MaxFreq <= spectrumAnalyzer.CenterFrequency) ? ("< " + description) : (description + " >"));
							SizeF sizeF = e.Graphics.MeasureString(description, TextFont, spectrumAnalyzer.Width, stringFormat);
							if (visibleEntry.MaxFreq > spectrumAnalyzer.CenterFrequency)
							{
								p.X = (int)((float)(rect.X + rect.Width / 2) - sizeF.Width);
							}
							else
							{
								p.X = rect.X + rect.Width / 2;
							}
							for (int i = 0; i < list.Count; i++)
							{
								if ((float)p.X > list[i] + width)
								{
									p.Y = (int)Math.Min(30.0 + (double)(sizeF.Height * (float)i) * 1.25, spectrumAnalyzer.Height - 30);
									list[i] = (float)p.X + sizeF.Width;
									break;
								}
								if (i == list.Count - 1)
								{
									p.Y = (int)Math.Min(30f + sizeF.Height * (float)(i + 1) * 1.25f, spectrumAnalyzer.Height - 30);
									list.Add((float)p.X + sizeF.Width);
									break;
								}
							}
							e.Graphics.FillRectangle(brush, rect);
							e.Graphics.DrawString(description, TextFont, brush2, p, stringFormat);
						}
					}
				}
			}
		}

		private void configureButton_Click(object sender, EventArgs e)
		{
			DialogConfigure dialogConfigure = new DialogConfigure(this);
			dialogConfigure.ShowDialog();
			ConfigureOutToPanView();
		}

		private void ConfigureOutToPanView()
		{
			_controlInterface.SpectrumAnalyzerCustomPaint -= controlInterface_SpectrumCustomPaint;
			_controlInterface.WaterfallCustomPaint -= controlInterface_WaterfallCustomPaint;
			if (EnableOutToPanView)
			{
				UpdateVisibleFrequencies();
				if (ToSpectrumOrWaterfall)
				{
					_controlInterface.SpectrumAnalyzerCustomPaint += controlInterface_SpectrumCustomPaint;
				}
				else
				{
					_controlInterface.WaterfallCustomPaint += controlInterface_WaterfallCustomPaint;
				}
			}
		}

		public void SaveSettings()
		{
			Utils.SaveSetting("SelectedFreqManagerGroup", (string)comboGroups.SelectedItem);
			Utils.SaveSetting("ChangeFreqPauseInMs", Detect);
			Utils.SaveSetting("NextFreqPauseInSecond", Pause);
			FontConverter fontConverter = new FontConverter();
			Utils.SaveSetting("configureWord_Font", fontConverter.ConvertToString(TextFont));
			Utils.SaveSetting("configureWord_panviewEnable", EnableOutToPanView);
			Utils.SaveSetting("configureWord_toSpectrum", ToSpectrumOrWaterfall);
			Utils.SaveSetting("AdditionalRowsInTable", RowsNumber);
			Utils.SaveSetting("CheckFrequencyOnEntry", TrackingFrequency);
			Utils.SaveSetting("FrequencyManagerPluginPosition", ManagerPluginPosition);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager componentResourceManager = new System.ComponentModel.ComponentResourceManager(typeof(SDRSharp.FreqMan.FrequencyManagerPanel));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle = new System.Windows.Forms.DataGridViewCellStyle();
			mainToolStrip = new System.Windows.Forms.ToolStrip();
			btnNewEntry = new System.Windows.Forms.ToolStripButton();
			btnEdit = new System.Windows.Forms.ToolStripButton();
			btnDelete = new System.Windows.Forms.ToolStripButton();
			frequencyDataGridView = new System.Windows.Forms.DataGridView();
			ScanCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			comboGroups = new System.Windows.Forms.ComboBox();
			ScanTimer = new System.Windows.Forms.Timer(components);
			configureButton = new System.Windows.Forms.Button();
			ScanButton = new System.Windows.Forms.Button();
			tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			frequencyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			memoryEntryBindingSource = new System.Windows.Forms.BindingSource(components);
			mainToolStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)frequencyDataGridView).BeginInit();
			tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)memoryEntryBindingSource).BeginInit();
			SuspendLayout();
			mainToolStrip.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			mainToolStrip.AutoSize = false;
			mainToolStrip.Dock = System.Windows.Forms.DockStyle.None;
			mainToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[3]
			{
				btnNewEntry,
				btnEdit,
				btnDelete
			});
			mainToolStrip.Location = new System.Drawing.Point(0, 39);
			mainToolStrip.Name = "mainToolStrip";
			mainToolStrip.Size = new System.Drawing.Size(210, 22);
			mainToolStrip.TabIndex = 7;
			mainToolStrip.Text = "toolStrip1";
			btnNewEntry.Image = (System.Drawing.Image)componentResourceManager.GetObject("btnNewEntry.Image");
			btnNewEntry.ImageTransparentColor = System.Drawing.Color.Magenta;
			btnNewEntry.Name = "btnNewEntry";
			btnNewEntry.Size = new System.Drawing.Size(51, 19);
			btnNewEntry.Text = "New";
			btnNewEntry.Click += new System.EventHandler(btnNewEntry_Click);
			btnEdit.Image = (System.Drawing.Image)componentResourceManager.GetObject("btnEdit.Image");
			btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
			btnEdit.Name = "btnEdit";
			btnEdit.Size = new System.Drawing.Size(47, 19);
			btnEdit.Text = "Edit";
			btnEdit.Click += new System.EventHandler(btnEdit_Click);
			btnDelete.Image = (System.Drawing.Image)componentResourceManager.GetObject("btnDelete.Image");
			btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
			btnDelete.Name = "btnDelete";
			btnDelete.Size = new System.Drawing.Size(60, 19);
			btnDelete.Text = "Delete";
			btnDelete.Click += new System.EventHandler(btnDelete_Click);
			frequencyDataGridView.AllowUserToAddRows = false;
			frequencyDataGridView.AllowUserToDeleteRows = false;
			frequencyDataGridView.AllowUserToResizeRows = false;
			dataGridViewCellStyle.BackColor = System.Drawing.Color.WhiteSmoke;
			frequencyDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle;
			frequencyDataGridView.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			frequencyDataGridView.AutoGenerateColumns = false;
			frequencyDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
			frequencyDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			frequencyDataGridView.Columns.AddRange(nameDataGridViewTextBoxColumn, frequencyDataGridViewTextBoxColumn, ScanCheckBox);
			frequencyDataGridView.DataSource = memoryEntryBindingSource;
			frequencyDataGridView.EnableHeadersVisualStyles = false;
			frequencyDataGridView.Location = new System.Drawing.Point(0, 63);
			frequencyDataGridView.Margin = new System.Windows.Forms.Padding(0);
			frequencyDataGridView.MultiSelect = false;
			frequencyDataGridView.Name = "frequencyDataGridView";
			frequencyDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
			frequencyDataGridView.RowHeadersVisible = false;
			frequencyDataGridView.RowTemplate.Height = 24;
			frequencyDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			frequencyDataGridView.ShowEditingIcon = false;
			frequencyDataGridView.Size = new System.Drawing.Size(210, 239);
			frequencyDataGridView.TabIndex = 6;
			frequencyDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(frequencyDataGridView_CellDoubleClick);
			frequencyDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(frequencyDataGridView_CellFormatting);
			frequencyDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(frequencyDataGridView_ColumnHeaderMouseClick);
			frequencyDataGridView.CurrentCellDirtyStateChanged += new System.EventHandler(frequencyDataGridView_CurrentCellDirtyStateChanged);
			frequencyDataGridView.SelectionChanged += new System.EventHandler(frequencyDataGridView_SelectionChanged);
			frequencyDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(frequencyDataGridView_KeyDown);
			ScanCheckBox.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
			ScanCheckBox.DataPropertyName = "IsScanned";
			ScanCheckBox.FalseValue = "false";
			ScanCheckBox.HeaderText = "s";
			ScanCheckBox.Name = "ScanCheckBox";
			ScanCheckBox.TrueValue = "true";
			ScanCheckBox.Width = 5;
			comboGroups.Anchor = (System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			comboGroups.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			comboGroups.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			comboGroups.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			comboGroups.FormattingEnabled = true;
			comboGroups.Location = new System.Drawing.Point(2, 6);
			comboGroups.Margin = new System.Windows.Forms.Padding(2);
			comboGroups.Name = "comboGroups";
			comboGroups.Size = new System.Drawing.Size(114, 21);
			comboGroups.TabIndex = 4;
			comboGroups.SelectedIndexChanged += new System.EventHandler(comboGroups_SelectedIndexChanged);
			ScanTimer.Tick += new System.EventHandler(ScanTimer_Tick);
			configureButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
			configureButton.Font = new System.Drawing.Font("Webdings", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 2);
			configureButton.Location = new System.Drawing.Point(121, 5);
			configureButton.Name = "configureButton";
			configureButton.Size = new System.Drawing.Size(26, 23);
			configureButton.TabIndex = 25;
			configureButton.Text = "@";
			configureButton.UseCompatibleTextRendering = true;
			configureButton.UseVisualStyleBackColor = true;
			configureButton.Click += new System.EventHandler(configureButton_Click);
			ScanButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
			ScanButton.Cursor = System.Windows.Forms.Cursors.Default;
			ScanButton.FlatAppearance.BorderSize = 0;
			ScanButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
			ScanButton.Location = new System.Drawing.Point(154, 5);
			ScanButton.Name = "ScanButton";
			ScanButton.Size = new System.Drawing.Size(50, 23);
			ScanButton.TabIndex = 22;
			ScanButton.Text = "Scan";
			ScanButton.UseCompatibleTextRendering = true;
			ScanButton.UseMnemonic = false;
			ScanButton.UseVisualStyleBackColor = false;
			ScanButton.Click += new System.EventHandler(ScanButton_Click);
			tableLayoutPanel1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			tableLayoutPanel1.AutoSize = true;
			tableLayoutPanel1.ColumnCount = 3;
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100f));
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33f));
			tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56f));
			tableLayoutPanel1.Controls.Add(ScanButton, 2, 0);
			tableLayoutPanel1.Controls.Add(configureButton, 1, 0);
			tableLayoutPanel1.Controls.Add(comboGroups, 0, 0);
			tableLayoutPanel1.Location = new System.Drawing.Point(0, 3);
			tableLayoutPanel1.Name = "tableLayoutPanel1";
			tableLayoutPanel1.RowCount = 1;
			tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33f));
			tableLayoutPanel1.Size = new System.Drawing.Size(207, 33);
			tableLayoutPanel1.TabIndex = 26;
			nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
			nameDataGridViewTextBoxColumn.HeaderText = "Name";
			nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
			nameDataGridViewTextBoxColumn.ReadOnly = true;
			frequencyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			frequencyDataGridViewTextBoxColumn.DataPropertyName = "Frequency";
			frequencyDataGridViewTextBoxColumn.HeaderText = "Frequency";
			frequencyDataGridViewTextBoxColumn.Name = "frequencyDataGridViewTextBoxColumn";
			frequencyDataGridViewTextBoxColumn.ReadOnly = true;
			memoryEntryBindingSource.DataSource = typeof(SDRSharp.FreqMan.MemoryEntry);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
			base.Controls.Add(tableLayoutPanel1);
			base.Controls.Add(mainToolStrip);
			base.Controls.Add(frequencyDataGridView);
			base.Margin = new System.Windows.Forms.Padding(2);
			MinimumSize = new System.Drawing.Size(214, 310);
			base.Name = "FrequencyManagerPanel";
			base.Size = new System.Drawing.Size(214, 310);
			mainToolStrip.ResumeLayout(performLayout: false);
			mainToolStrip.PerformLayout();
			((System.ComponentModel.ISupportInitialize)frequencyDataGridView).EndInit();
			tableLayoutPanel1.ResumeLayout(performLayout: false);
			((System.ComponentModel.ISupportInitialize)memoryEntryBindingSource).EndInit();
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

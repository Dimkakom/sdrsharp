using SDRSharp.Common;
using SDRSharp.Radio;
using System;

namespace SDRSharp.FreqMan
{
	public class Scanner
	{
		private ISharpControl _control;

		private IFProcessor _ifProcessor;

		private UnsafeBuffer _fftBuffer;

		private unsafe Complex* _fftPtr;

		private UnsafeBuffer _fftWindow;

		private unsafe float* _fftWindowPtr;

		private UnsafeBuffer _unScaledFFTSpectrum;

		private unsafe float* _unScaledFFTSpectrumPtr;

		private UnsafeBuffer _scaledFFTSpectrum;

		private unsafe float* _scaledFFTSpectrumPtr;

		private SharpEvent _bufferEvent;

		private bool _scanProcessIsWork;

		private int _writePos;

		private int _fftBins;

		private int _count;

		private int _timeConstant;

		private bool _fftRunning;

		private int _usableSpectrumLength;

		private float _range;

		private float _offset;

		private bool autoCorrectNoiseLevel;

		public Scanner(ISharpControl control)
		{
			_control = control;
			_ifProcessor = new IFProcessor();
			_control.RegisterStreamHook(_ifProcessor, ProcessorType.RawIQ);
			_ifProcessor.Enabled = false;
			InitFFTBuffers();
			BuildFFTWindow();
		}

		private unsafe void BuildFFTWindow()
		{
			float[] array = FilterBuilder.MakeWindow(WindowType.BlackmanHarris7, _fftBins);
			float[] array2 = array;
			fixed (float* src = array2)
			{
				Utils.Memcpy(_fftWindow, src, _fftBins * 4);
			}
		}

		private unsafe void InitFFTBuffers()
		{
			_fftBuffer = UnsafeBuffer.Create(_fftBins, sizeof(Complex));
			_fftWindow = UnsafeBuffer.Create(_fftBins, 4);
			_unScaledFFTSpectrum = UnsafeBuffer.Create(_fftBins, 4);
			_scaledFFTSpectrum = UnsafeBuffer.Create(_fftBins, 4);
			_writePos = 0;
			_fftPtr = (Complex*)(void*)_fftBuffer;
			_fftWindowPtr = (float*)(void*)_fftWindow;
			_unScaledFFTSpectrumPtr = (float*)(void*)_unScaledFFTSpectrum;
			_scaledFFTSpectrumPtr = (float*)(void*)_scaledFFTSpectrum;
		}

		private unsafe void FastBufferAvailable(Complex* buffer, int length)
		{
			_count++;
			if (!_scanProcessIsWork)
			{
				int num = Math.Min(length, _fftBins - _writePos);
				Utils.Memcpy(_fftPtr + _writePos, buffer, num * sizeof(Complex));
				_writePos += num;
				if (_writePos >= _fftBins)
				{
					_writePos = 0;
					_timeConstant = (int)((double)length / _ifProcessor.SampleRate * 1000.0) * _count;
					_scanProcessIsWork = true;
					_count = 0;
					_bufferEvent.Set();
				}
			}
			else
			{
				_writePos = 0;
			}
		}

		private unsafe void ProcessFFT()
		{
			_ifProcessor.IQReady += FastBufferAvailable;
			while (_fftRunning)
			{
				_bufferEvent.WaitOne();
				Fourier.ApplyFFTWindow(_fftPtr, _fftWindowPtr, _fftBins);
				Fourier.ForwardTransform(_fftPtr, _fftBins);
				Fourier.SpectrumPower(_fftPtr, _unScaledFFTSpectrumPtr, _fftBins);
				Fourier.SmoothMaxCopy(_unScaledFFTSpectrumPtr, _scaledFFTSpectrumPtr, _fftBins, _fftBins);
			}
			_ifProcessor.IQReady -= FastBufferAvailable;
		}

		private unsafe void ScaleSpectrum(float* source, float* dest, int sourceLength, int destLength)
		{
			float num = 0f;
			if (autoCorrectNoiseLevel)
			{
				int num2 = sourceLength / 2 - destLength / 2;
				float num3 = 0f;
				for (int i = 0; i < destLength; i++)
				{
					num3 += source[num2 + i];
				}
				num = _range - num3 / (float)destLength - _offset;
			}
			else
			{
				num = -55f;
			}
			for (int j = 0; j < sourceLength; j++)
			{
				dest[j] = source[j] + num;
			}
		}
	}
}

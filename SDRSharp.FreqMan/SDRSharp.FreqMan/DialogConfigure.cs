using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.FreqMan
{
	public class DialogConfigure : Form
	{
		private FrequencyManagerPanel _managerPanel;

		private IContainer components;

		private Button btnOk;

		private CheckBox outToPanViewCheckBox;

		private RadioButton toSpectrumRadioButton;

		private RadioButton toWaterfallRadioButton;

		private FontDialog selectFontDialog;

		private GroupBox groupBox1;

		private Button fontSelectButton;

		private NumericUpDown addRowsNumericUpDown;

		private Label label1;

		private CheckBox trackingFrequencyCheckBox;

		private ComboBox pluginPositionComboBox;

		private GroupBox groupBox2;

		public DialogConfigure()
		{
			InitializeComponent();
		}

		public DialogConfigure(FrequencyManagerPanel managerPanel)
		{
			_managerPanel = managerPanel;
			InitializeComponent();
			outToPanViewCheckBox.Checked = _managerPanel.EnableOutToPanView;
			toSpectrumRadioButton.Checked = _managerPanel.ToSpectrumOrWaterfall;
			toWaterfallRadioButton.Checked = !_managerPanel.ToSpectrumOrWaterfall;
			selectFontDialog.Font = _managerPanel.TextFont;
			addRowsNumericUpDown.Value = _managerPanel.RowsNumber;
			trackingFrequencyCheckBox.Checked = _managerPanel.TrackingFrequency;
			pluginPositionComboBox.SelectedIndex = _managerPanel.ManagerPluginPosition;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			_managerPanel.TextFont = selectFontDialog.Font;
			_managerPanel.ToSpectrumOrWaterfall = toSpectrumRadioButton.Checked;
			_managerPanel.EnableOutToPanView = outToPanViewCheckBox.Checked;
			_managerPanel.TrackingFrequency = trackingFrequencyCheckBox.Checked;
			_managerPanel.RowsNumber = (int)addRowsNumericUpDown.Value;
			_managerPanel.ManagerPluginPosition = pluginPositionComboBox.SelectedIndex;
			base.DialogResult = DialogResult.OK;
		}

		private void fontSelectButton_Click(object sender, EventArgs e)
		{
			selectFontDialog.ShowDialog();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			btnOk = new System.Windows.Forms.Button();
			outToPanViewCheckBox = new System.Windows.Forms.CheckBox();
			toSpectrumRadioButton = new System.Windows.Forms.RadioButton();
			toWaterfallRadioButton = new System.Windows.Forms.RadioButton();
			selectFontDialog = new System.Windows.Forms.FontDialog();
			groupBox1 = new System.Windows.Forms.GroupBox();
			fontSelectButton = new System.Windows.Forms.Button();
			addRowsNumericUpDown = new System.Windows.Forms.NumericUpDown();
			label1 = new System.Windows.Forms.Label();
			trackingFrequencyCheckBox = new System.Windows.Forms.CheckBox();
			pluginPositionComboBox = new System.Windows.Forms.ComboBox();
			groupBox2 = new System.Windows.Forms.GroupBox();
			groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)addRowsNumericUpDown).BeginInit();
			groupBox2.SuspendLayout();
			SuspendLayout();
			btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			btnOk.Location = new System.Drawing.Point(143, 208);
			btnOk.Margin = new System.Windows.Forms.Padding(2);
			btnOk.Name = "btnOk";
			btnOk.Size = new System.Drawing.Size(56, 23);
			btnOk.TabIndex = 7;
			btnOk.Text = "O&K";
			btnOk.UseVisualStyleBackColor = true;
			btnOk.Click += new System.EventHandler(btnOk_Click);
			outToPanViewCheckBox.AutoSize = true;
			outToPanViewCheckBox.Location = new System.Drawing.Point(6, 0);
			outToPanViewCheckBox.Name = "outToPanViewCheckBox";
			outToPanViewCheckBox.Size = new System.Drawing.Size(89, 17);
			outToPanViewCheckBox.TabIndex = 9;
			outToPanViewCheckBox.Text = "Show Names";
			outToPanViewCheckBox.UseVisualStyleBackColor = true;
			toSpectrumRadioButton.AutoSize = true;
			toSpectrumRadioButton.Location = new System.Drawing.Point(6, 23);
			toSpectrumRadioButton.Name = "toSpectrumRadioButton";
			toSpectrumRadioButton.Size = new System.Drawing.Size(113, 17);
			toSpectrumRadioButton.TabIndex = 10;
			toSpectrumRadioButton.TabStop = true;
			toSpectrumRadioButton.Text = "Spectrum Analyzer";
			toSpectrumRadioButton.UseVisualStyleBackColor = true;
			toWaterfallRadioButton.AutoSize = true;
			toWaterfallRadioButton.Location = new System.Drawing.Point(6, 46);
			toWaterfallRadioButton.Name = "toWaterfallRadioButton";
			toWaterfallRadioButton.Size = new System.Drawing.Size(67, 17);
			toWaterfallRadioButton.TabIndex = 11;
			toWaterfallRadioButton.TabStop = true;
			toWaterfallRadioButton.Text = "Waterfall";
			toWaterfallRadioButton.UseVisualStyleBackColor = true;
			groupBox1.Controls.Add(fontSelectButton);
			groupBox1.Controls.Add(outToPanViewCheckBox);
			groupBox1.Controls.Add(toSpectrumRadioButton);
			groupBox1.Controls.Add(toWaterfallRadioButton);
			groupBox1.Location = new System.Drawing.Point(12, 12);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new System.Drawing.Size(193, 79);
			groupBox1.TabIndex = 12;
			groupBox1.TabStop = false;
			groupBox1.Text = "groupBox1";
			fontSelectButton.Location = new System.Drawing.Point(112, 46);
			fontSelectButton.Name = "fontSelectButton";
			fontSelectButton.Size = new System.Drawing.Size(75, 23);
			fontSelectButton.TabIndex = 12;
			fontSelectButton.Text = "Select Font";
			fontSelectButton.UseVisualStyleBackColor = true;
			fontSelectButton.Click += new System.EventHandler(fontSelectButton_Click);
			addRowsNumericUpDown.Location = new System.Drawing.Point(146, 52);
			addRowsNumericUpDown.Maximum = new decimal(new int[4]
			{
				50,
				0,
				0,
				0
			});
			addRowsNumericUpDown.Minimum = new decimal(new int[4]
			{
				5,
				0,
				0,
				0
			});
			addRowsNumericUpDown.Name = "addRowsNumericUpDown";
			addRowsNumericUpDown.Size = new System.Drawing.Size(41, 20);
			addRowsNumericUpDown.TabIndex = 13;
			addRowsNumericUpDown.Value = new decimal(new int[4]
			{
				5,
				0,
				0,
				0
			});
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(6, 54);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(71, 13);
			label1.TabIndex = 14;
			label1.Text = "Rows in table";
			trackingFrequencyCheckBox.AutoSize = true;
			trackingFrequencyCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			trackingFrequencyCheckBox.Location = new System.Drawing.Point(8, 187);
			trackingFrequencyCheckBox.Name = "trackingFrequencyCheckBox";
			trackingFrequencyCheckBox.Size = new System.Drawing.Size(128, 17);
			trackingFrequencyCheckBox.TabIndex = 15;
			trackingFrequencyCheckBox.Text = "Track VFO frequency";
			trackingFrequencyCheckBox.UseVisualStyleBackColor = true;
			pluginPositionComboBox.AutoCompleteCustomSource.AddRange(new string[3]
			{
				"Plugin panel",
				"Main screen left",
				"Main screen right"
			});
			pluginPositionComboBox.FormattingEnabled = true;
			pluginPositionComboBox.Items.AddRange(new object[5]
			{
				"Plugin panel",
				"Main screen up",
				"Main screen down",
				"Main screen left",
				"Main screen right"
			});
			pluginPositionComboBox.Location = new System.Drawing.Point(6, 19);
			pluginPositionComboBox.Name = "pluginPositionComboBox";
			pluginPositionComboBox.Size = new System.Drawing.Size(181, 21);
			pluginPositionComboBox.TabIndex = 17;
			groupBox2.Controls.Add(pluginPositionComboBox);
			groupBox2.Controls.Add(label1);
			groupBox2.Controls.Add(addRowsNumericUpDown);
			groupBox2.Location = new System.Drawing.Point(12, 97);
			groupBox2.Name = "groupBox2";
			groupBox2.Size = new System.Drawing.Size(193, 84);
			groupBox2.TabIndex = 18;
			groupBox2.TabStop = false;
			groupBox2.Text = "Plugin position (restart needed)";
			base.AcceptButton = btnOk;
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(214, 240);
			base.Controls.Add(groupBox2);
			base.Controls.Add(trackingFrequencyCheckBox);
			base.Controls.Add(groupBox1);
			base.Controls.Add(btnOk);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			base.Margin = new System.Windows.Forms.Padding(2);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "DialogConfigure";
			base.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			base.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			Text = "Configure Frequency Manager";
			groupBox1.ResumeLayout(performLayout: false);
			groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)addRowsNumericUpDown).EndInit();
			groupBox2.ResumeLayout(performLayout: false);
			groupBox2.PerformLayout();
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.FreqMan
{
	public class KeyboardPanel : Form
	{
		public delegate void NextButtonClickHandler(object sender, EventArgs e);

		public delegate void PauseButtonClickHandler(object sender, EventArgs e);

		private FrequencyManagerPanel _managerPanel;

		private IContainer components;

		private Button nextButton;

		private Button pauseButton;

		private NumericUpDown pauseNumericUpDown;

		private NumericUpDown detectNumericUpDown;

		private Label label1;

		private Label label2;

		public event NextButtonClickHandler NextButtonClick;

		public event PauseButtonClickHandler PauseButtonClick;

		public KeyboardPanel(FrequencyManagerPanel managerPanel)
		{
			InitializeComponent();
			_managerPanel = managerPanel;
			pauseNumericUpDown.Value = (decimal)_managerPanel.Pause;
			detectNumericUpDown.Value = _managerPanel.Detect;
		}

		private void nextButton_Click(object sender, EventArgs e)
		{
			if (this.NextButtonClick != null)
			{
				this.NextButtonClick(sender, e);
			}
		}

		private void pauseButton_Click(object sender, EventArgs e)
		{
			if (this.PauseButtonClick != null)
			{
				this.PauseButtonClick(sender, e);
			}
			if (_managerPanel.ScanPause)
			{
				pauseButton.Text = "4";
			}
			else
			{
				pauseButton.Text = ";";
			}
		}

		private void pauseNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_managerPanel.Pause = (float)pauseNumericUpDown.Value;
		}

		private void detectNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_managerPanel.Detect = (int)detectNumericUpDown.Value;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			nextButton = new System.Windows.Forms.Button();
			pauseButton = new System.Windows.Forms.Button();
			pauseNumericUpDown = new System.Windows.Forms.NumericUpDown();
			detectNumericUpDown = new System.Windows.Forms.NumericUpDown();
			label1 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)pauseNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)detectNumericUpDown).BeginInit();
			SuspendLayout();
			nextButton.Font = new System.Drawing.Font("Webdings", 21.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 2);
			nextButton.Location = new System.Drawing.Point(116, 7);
			nextButton.Name = "nextButton";
			nextButton.Size = new System.Drawing.Size(100, 33);
			nextButton.TabIndex = 0;
			nextButton.Text = "8";
			nextButton.UseCompatibleTextRendering = true;
			nextButton.UseVisualStyleBackColor = true;
			nextButton.Click += new System.EventHandler(nextButton_Click);
			pauseButton.Font = new System.Drawing.Font("Webdings", 21.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 2);
			pauseButton.Location = new System.Drawing.Point(6, 7);
			pauseButton.Name = "pauseButton";
			pauseButton.Size = new System.Drawing.Size(100, 33);
			pauseButton.TabIndex = 0;
			pauseButton.Text = ";";
			pauseButton.UseCompatibleTextRendering = true;
			pauseButton.UseVisualStyleBackColor = true;
			pauseButton.Click += new System.EventHandler(pauseButton_Click);
			pauseNumericUpDown.DecimalPlaces = 1;
			pauseNumericUpDown.Location = new System.Drawing.Point(170, 48);
			pauseNumericUpDown.Maximum = new decimal(new int[4]
			{
				3600,
				0,
				0,
				0
			});
			pauseNumericUpDown.Name = "pauseNumericUpDown";
			pauseNumericUpDown.Size = new System.Drawing.Size(46, 20);
			pauseNumericUpDown.TabIndex = 1;
			pauseNumericUpDown.ValueChanged += new System.EventHandler(pauseNumericUpDown_ValueChanged);
			detectNumericUpDown.Increment = new decimal(new int[4]
			{
				10,
				0,
				0,
				0
			});
			detectNumericUpDown.Location = new System.Drawing.Point(67, 48);
			detectNumericUpDown.Maximum = new decimal(new int[4]
			{
				1000,
				0,
				0,
				0
			});
			detectNumericUpDown.Name = "detectNumericUpDown";
			detectNumericUpDown.Size = new System.Drawing.Size(46, 20);
			detectNumericUpDown.TabIndex = 2;
			detectNumericUpDown.ValueChanged += new System.EventHandler(detectNumericUpDown_ValueChanged);
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(119, 50);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(45, 13);
			label1.TabIndex = 3;
			label1.Text = "Pause s";
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(6, 50);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(55, 13);
			label2.TabIndex = 4;
			label2.Text = "Detect ms";
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			AutoSize = true;
			base.ClientSize = new System.Drawing.Size(224, 74);
			base.Controls.Add(label2);
			base.Controls.Add(label1);
			base.Controls.Add(detectNumericUpDown);
			base.Controls.Add(pauseNumericUpDown);
			base.Controls.Add(pauseButton);
			base.Controls.Add(nextButton);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			base.MaximizeBox = false;
			base.Name = "KeyboardPanel";
			base.ShowIcon = false;
			base.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			base.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			Text = "   ";
			base.TopMost = true;
			((System.ComponentModel.ISupportInitialize)pauseNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)detectNumericUpDown).EndInit();
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

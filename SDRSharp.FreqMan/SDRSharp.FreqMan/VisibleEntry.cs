namespace SDRSharp.FreqMan
{
	public class VisibleEntry
	{
		private string _description;

		private long _minFreq;

		private long _maxFreq;

		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		public long MinFreq
		{
			get
			{
				return _minFreq;
			}
			set
			{
				_minFreq = value;
			}
		}

		public long MaxFreq
		{
			get
			{
				return _maxFreq;
			}
			set
			{
				_maxFreq = value;
			}
		}

		public VisibleEntry()
		{
		}

		public VisibleEntry(VisibleEntry visibleEntry)
		{
			_description = visibleEntry._description;
			_minFreq = visibleEntry._minFreq;
			_maxFreq = visibleEntry._maxFreq;
		}
	}
}

using System;

namespace SDRSharp.FreqMan
{
	public class MemoryInfoEventArgs : EventArgs
	{
		private readonly MemoryEntry _memoryEntry;

		public MemoryEntry MemoryEntry => _memoryEntry;

		public MemoryInfoEventArgs(MemoryEntry entry)
		{
			_memoryEntry = entry;
		}
	}
}

using SDRSharp.Common;
using SDRSharp.Radio;
using System.Windows.Forms;

namespace SDRSharp.FreqMan
{
	public class FreqManPlugin : ISharpPlugin
	{
		private const string _displayName = "Frequency Manager*";

		private ISharpControl _controlInterface;

		private FrequencyManagerPanel _frequencyManagerPanel;

		private int _pluginPosition;

		public bool HasGui
		{
			get
			{
				if (_pluginPosition == 0)
				{
					return true;
				}
				return false;
			}
		}

		public UserControl GuiControl
		{
			get
			{
				if (_pluginPosition == 0)
				{
					return _frequencyManagerPanel;
				}
				return null;
			}
		}

		public string DisplayName => "Frequency Manager*";

        public UserControl Gui => _frequencyManagerPanel;

        public void Close()
		{
			_frequencyManagerPanel.SaveSettings();
		}

		public void Initialize(ISharpControl control)
		{
			_pluginPosition = Utils.GetIntSetting("FrequencyManagerPluginPosition", 0);
			_controlInterface = control;
			_frequencyManagerPanel = new FrequencyManagerPanel(_controlInterface);
			if (_pluginPosition > 0)
			{
				_controlInterface.RegisterFrontControl(_frequencyManagerPanel, (PluginPosition)(_pluginPosition - 1));
			}
		}
	}
}

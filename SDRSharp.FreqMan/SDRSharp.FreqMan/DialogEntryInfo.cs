using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.FreqMan
{
	public class DialogEntryInfo : Form
	{
		private MemoryEntry _memoryEntry;

		private IContainer components;

		private Label label1;

		private Label label2;

		private Label label3;

		private Label label4;

		private Label label5;

		private Label lblMode;

		private ComboBox comboGroupName;

		private TextBox textBoxName;

		private NumericUpDown frequencyNumericUpDown;

		private Button btnOk;

		private Button btnCancel;

		private NumericUpDown shiftNumericUpDown;

		private Label label6;

		private NumericUpDown nudFilterBandwidth;

		private Label label7;

		private CheckBox favouriteCb;

		private NumericUpDown CenterFrequencyNumericUpDown;

		private Label label8;

		public DialogEntryInfo()
		{
			InitializeComponent();
			ValidateForm();
		}

		public DialogEntryInfo(MemoryEntry memoryEntry, List<string> groups)
		{
			_memoryEntry = memoryEntry;
			InitializeComponent();
			textBoxName.Text = memoryEntry.Name;
			comboGroupName.Text = memoryEntry.GroupName;
			frequencyNumericUpDown.Value = memoryEntry.Frequency;
			CenterFrequencyNumericUpDown.Value = memoryEntry.CenterFrequency;
			shiftNumericUpDown.Value = memoryEntry.Shift;
			lblMode.Text = memoryEntry.DetectorType.ToString();
			comboGroupName.Items.AddRange(groups.ToArray());
			nudFilterBandwidth.Value = memoryEntry.FilterBandwidth;
			favouriteCb.Checked = memoryEntry.IsFavourite;
			ValidateForm();
		}

		private void Control_TextChanged(object sender, EventArgs e)
		{
			ValidateForm();
		}

		private void ValidateForm()
		{
			bool enabled = textBoxName.Text != null && !"".Equals(textBoxName.Text.Trim()) && comboGroupName.Text != null && !"".Equals(comboGroupName.Text.Trim()) && frequencyNumericUpDown.Value != 0m && nudFilterBandwidth.Value != 0m;
			btnOk.Enabled = enabled;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			_memoryEntry.Name = textBoxName.Text.Trim();
			_memoryEntry.GroupName = comboGroupName.Text.Trim();
			_memoryEntry.Frequency = (long)frequencyNumericUpDown.Value;
			_memoryEntry.CenterFrequency = (long)CenterFrequencyNumericUpDown.Value;
			_memoryEntry.Shift = (long)shiftNumericUpDown.Value;
			_memoryEntry.FilterBandwidth = (long)nudFilterBandwidth.Value;
			_memoryEntry.IsFavourite = favouriteCb.Checked;
			base.DialogResult = DialogResult.OK;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			label1 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			label4 = new System.Windows.Forms.Label();
			label5 = new System.Windows.Forms.Label();
			lblMode = new System.Windows.Forms.Label();
			comboGroupName = new System.Windows.Forms.ComboBox();
			textBoxName = new System.Windows.Forms.TextBox();
			frequencyNumericUpDown = new System.Windows.Forms.NumericUpDown();
			btnOk = new System.Windows.Forms.Button();
			btnCancel = new System.Windows.Forms.Button();
			shiftNumericUpDown = new System.Windows.Forms.NumericUpDown();
			label6 = new System.Windows.Forms.Label();
			nudFilterBandwidth = new System.Windows.Forms.NumericUpDown();
			label7 = new System.Windows.Forms.Label();
			favouriteCb = new System.Windows.Forms.CheckBox();
			CenterFrequencyNumericUpDown = new System.Windows.Forms.NumericUpDown();
			label8 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)frequencyNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)shiftNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)nudFilterBandwidth).BeginInit();
			((System.ComponentModel.ISupportInitialize)CenterFrequencyNumericUpDown).BeginInit();
			SuspendLayout();
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(10, 11);
			label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(250, 13);
			label1.TabIndex = 0;
			label1.Text = "Select an existing group or enter a new group name";
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(10, 37);
			label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(39, 13);
			label2.TabIndex = 1;
			label2.Text = "Group:";
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(10, 67);
			label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(38, 13);
			label3.TabIndex = 2;
			label3.Text = "Name:";
			label4.AutoSize = true;
			label4.Location = new System.Drawing.Point(10, 98);
			label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(60, 13);
			label4.TabIndex = 3;
			label4.Text = "Frequency:";
			label5.AutoSize = true;
			label5.Location = new System.Drawing.Point(11, 217);
			label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			label5.Name = "label5";
			label5.Size = new System.Drawing.Size(37, 13);
			label5.TabIndex = 4;
			label5.Text = "Mode:";
			lblMode.Location = new System.Drawing.Point(89, 217);
			lblMode.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			lblMode.Name = "lblMode";
			lblMode.Size = new System.Drawing.Size(120, 17);
			lblMode.TabIndex = 5;
			comboGroupName.FormattingEnabled = true;
			comboGroupName.Location = new System.Drawing.Point(85, 34);
			comboGroupName.Margin = new System.Windows.Forms.Padding(2);
			comboGroupName.Name = "comboGroupName";
			comboGroupName.Size = new System.Drawing.Size(178, 21);
			comboGroupName.TabIndex = 0;
			comboGroupName.TextChanged += new System.EventHandler(Control_TextChanged);
			textBoxName.Location = new System.Drawing.Point(85, 64);
			textBoxName.Margin = new System.Windows.Forms.Padding(2);
			textBoxName.Name = "textBoxName";
			textBoxName.Size = new System.Drawing.Size(178, 20);
			textBoxName.TabIndex = 1;
			textBoxName.TextChanged += new System.EventHandler(Control_TextChanged);
			frequencyNumericUpDown.Increment = new decimal(new int[4]
			{
				10,
				0,
				0,
				0
			});
			frequencyNumericUpDown.Location = new System.Drawing.Point(85, 96);
			frequencyNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
			frequencyNumericUpDown.Maximum = new decimal(new int[4]
			{
				-727379969,
				232,
				0,
				0
			});
			frequencyNumericUpDown.Minimum = new decimal(new int[4]
			{
				-727379969,
				232,
				0,
				-2147483648
			});
			frequencyNumericUpDown.Name = "frequencyNumericUpDown";
			frequencyNumericUpDown.Size = new System.Drawing.Size(124, 20);
			frequencyNumericUpDown.TabIndex = 2;
			frequencyNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			frequencyNumericUpDown.ThousandsSeparator = true;
			frequencyNumericUpDown.ValueChanged += new System.EventHandler(Control_TextChanged);
			btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			btnOk.Enabled = false;
			btnOk.Location = new System.Drawing.Point(149, 275);
			btnOk.Margin = new System.Windows.Forms.Padding(2);
			btnOk.Name = "btnOk";
			btnOk.Size = new System.Drawing.Size(56, 23);
			btnOk.TabIndex = 7;
			btnOk.Text = "O&K";
			btnOk.UseVisualStyleBackColor = true;
			btnOk.Click += new System.EventHandler(btnOk_Click);
			btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			btnCancel.Location = new System.Drawing.Point(209, 275);
			btnCancel.Margin = new System.Windows.Forms.Padding(2);
			btnCancel.Name = "btnCancel";
			btnCancel.Size = new System.Drawing.Size(56, 23);
			btnCancel.TabIndex = 8;
			btnCancel.Text = "&Cancel";
			btnCancel.UseVisualStyleBackColor = true;
			shiftNumericUpDown.Increment = new decimal(new int[4]
			{
				10,
				0,
				0,
				0
			});
			shiftNumericUpDown.Location = new System.Drawing.Point(85, 156);
			shiftNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
			shiftNumericUpDown.Maximum = new decimal(new int[4]
			{
				-727379969,
				232,
				0,
				0
			});
			shiftNumericUpDown.Minimum = new decimal(new int[4]
			{
				-727379969,
				232,
				0,
				-2147483648
			});
			shiftNumericUpDown.Name = "shiftNumericUpDown";
			shiftNumericUpDown.Size = new System.Drawing.Size(124, 20);
			shiftNumericUpDown.TabIndex = 4;
			shiftNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			shiftNumericUpDown.ThousandsSeparator = true;
			label6.AutoSize = true;
			label6.Location = new System.Drawing.Point(10, 159);
			label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			label6.Name = "label6";
			label6.Size = new System.Drawing.Size(31, 13);
			label6.TabIndex = 12;
			label6.Text = "Shift:";
			nudFilterBandwidth.Increment = new decimal(new int[4]
			{
				10,
				0,
				0,
				0
			});
			nudFilterBandwidth.Location = new System.Drawing.Point(85, 186);
			nudFilterBandwidth.Margin = new System.Windows.Forms.Padding(2);
			nudFilterBandwidth.Maximum = new decimal(new int[4]
			{
				1410065407,
				2,
				0,
				0
			});
			nudFilterBandwidth.Name = "nudFilterBandwidth";
			nudFilterBandwidth.Size = new System.Drawing.Size(124, 20);
			nudFilterBandwidth.TabIndex = 5;
			nudFilterBandwidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			nudFilterBandwidth.ThousandsSeparator = true;
			label7.AutoSize = true;
			label7.Location = new System.Drawing.Point(10, 190);
			label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			label7.Name = "label7";
			label7.Size = new System.Drawing.Size(53, 13);
			label7.TabIndex = 14;
			label7.Text = "Filter BW:";
			favouriteCb.AutoSize = true;
			favouriteCb.Location = new System.Drawing.Point(85, 237);
			favouriteCb.Name = "favouriteCb";
			favouriteCb.Size = new System.Drawing.Size(70, 17);
			favouriteCb.TabIndex = 6;
			favouriteCb.Text = "Favourite";
			favouriteCb.UseVisualStyleBackColor = true;
			CenterFrequencyNumericUpDown.Increment = new decimal(new int[4]
			{
				10,
				0,
				0,
				0
			});
			CenterFrequencyNumericUpDown.Location = new System.Drawing.Point(85, 125);
			CenterFrequencyNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
			CenterFrequencyNumericUpDown.Maximum = new decimal(new int[4]
			{
				-727379969,
				232,
				0,
				0
			});
			CenterFrequencyNumericUpDown.Minimum = new decimal(new int[4]
			{
				-727379969,
				232,
				0,
				-2147483648
			});
			CenterFrequencyNumericUpDown.Name = "CenterFrequencyNumericUpDown";
			CenterFrequencyNumericUpDown.Size = new System.Drawing.Size(124, 20);
			CenterFrequencyNumericUpDown.TabIndex = 3;
			CenterFrequencyNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			CenterFrequencyNumericUpDown.ThousandsSeparator = true;
			label8.AutoSize = true;
			label8.Location = new System.Drawing.Point(10, 127);
			label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			label8.Name = "label8";
			label8.Size = new System.Drawing.Size(41, 13);
			label8.TabIndex = 18;
			label8.Text = "Center:";
			base.AcceptButton = btnOk;
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.CancelButton = btnCancel;
			base.ClientSize = new System.Drawing.Size(276, 313);
			base.Controls.Add(CenterFrequencyNumericUpDown);
			base.Controls.Add(label8);
			base.Controls.Add(favouriteCb);
			base.Controls.Add(nudFilterBandwidth);
			base.Controls.Add(label7);
			base.Controls.Add(shiftNumericUpDown);
			base.Controls.Add(label6);
			base.Controls.Add(btnCancel);
			base.Controls.Add(btnOk);
			base.Controls.Add(frequencyNumericUpDown);
			base.Controls.Add(textBoxName);
			base.Controls.Add(comboGroupName);
			base.Controls.Add(lblMode);
			base.Controls.Add(label5);
			base.Controls.Add(label4);
			base.Controls.Add(label3);
			base.Controls.Add(label2);
			base.Controls.Add(label1);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			base.Margin = new System.Windows.Forms.Padding(2);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "DialogEntryInfo";
			base.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			base.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			Text = "Edit Entry Information";
			((System.ComponentModel.ISupportInitialize)frequencyNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)shiftNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)nudFilterBandwidth).EndInit();
			((System.ComponentModel.ISupportInitialize)CenterFrequencyNumericUpDown).EndInit();
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

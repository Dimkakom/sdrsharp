using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace SDRSharp.FreqMan
{
	public class SortableBindingListComparer<T> : IComparer<T>
	{
		private PropertyInfo _sortProperty;

		private ListSortDirection _sortDirection;

		public SortableBindingListComparer(string sortProperty, ListSortDirection sortDirection)
		{
			_sortProperty = typeof(T).GetProperty(sortProperty);
			_sortDirection = sortDirection;
		}

		public int Compare(T x, T y)
		{
			IComparable comparable = (IComparable)_sortProperty.GetValue(x, null);
			IComparable comparable2 = (IComparable)_sortProperty.GetValue(y, null);
			if (_sortDirection == ListSortDirection.Ascending)
			{
				return comparable.CompareTo(comparable2);
			}
			return comparable2.CompareTo(comparable);
		}
	}
}

using SDRSharp.Common;
using SDRSharp.Radio;
using System.ComponentModel;
using System.Windows.Forms;

namespace SDRSharp.DCSDecoder
{
	public class DCSDecoderPlugin : ISharpPlugin
	{
		private const string _displayName = "DCS Decoder";

		private ISharpControl _control;

		private AudioProcessor _audioProcessor;

		private DCSDecoderPanel _guiControl;

		public string DisplayName => "DCS Decoder";

		public UserControl Gui => _guiControl;

		public void Initialize(ISharpControl control)
		{
			_control = control;
			_control.PropertyChanged += PropertyChangedHandler;
			_audioProcessor = new AudioProcessor();
			_control.RegisterStreamHook(_audioProcessor, ProcessorType.DemodulatorOutput);
			_guiControl = new DCSDecoderPanel(_audioProcessor, _control);
		}

		private void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			string propertyName = e.PropertyName;
			switch (propertyName)
			{
			default:
				if (propertyName == "Frequency" && _guiControl != null)
				{
					_guiControl.Reset();
				}
				break;
			case "StartRadio":
				if (_guiControl != null)
				{
					_guiControl.EnableControls();
				}
				break;
			case "StopRadio":
				if (_guiControl != null)
				{
					_guiControl.DisableControls();
				}
				break;
			case "DetectorType":
				if (_guiControl != null)
				{
					_guiControl.SetGain();
					if (_control.DetectorType == DetectorType.NFM)
					{
						_guiControl.EnableControls();
					}
					else
					{
						_guiControl.DisableControls();
					}
				}
				break;
			}
		}

		public void Close()
		{
			_guiControl.StoreSettings();
		}
	}
}

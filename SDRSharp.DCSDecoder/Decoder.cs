using SDRSharp.Radio;
using System;

namespace SDRSharp.DCSDecoder
{
	public class Decoder
	{
		private const int SubToneFilterOrder = 500;

		private const double BitRate = 134.3;

		private const int BitPerWord = 23;

		private FirFilter _toneFilter;

		private unsafe float* _filteredBufferPtr;

		private UnsafeBuffer _filteredBuffer;

		private float _samplerate;

		private float _average;

		private float _center;

		private int _sampleCount;

		private byte[] _recivedCode;

		private float _max;

		private float _min;

		private float _samplesPerBit;

		private float _previousAverage;

		private int _bitCounter;

		private bool _codeAvailable;

		public byte[] RecivedCode
		{
			get;
			set;
		}

		public bool CodeAvailable
		{
			get
			{
				bool codeAvailable = _codeAvailable;
				_codeAvailable = false;
				return codeAvailable;
			}
		}

		public Decoder(double sampleRate)
		{
			if (_toneFilter != null)
			{
				_toneFilter.Dispose();
			}
			_toneFilter = new FirFilter(FilterBuilder.MakeLowPassKernel(sampleRate, 500, 134.3, WindowType.BlackmanHarris4), 1);
			_samplesPerBit = (float)(sampleRate / 134.3);
			RecivedCode = new byte[23];
			_recivedCode = new byte[23];
			_samplerate = (float)sampleRate;
		}

		public unsafe void Process(float* buffer, int length)
		{
			if (_filteredBuffer == null || _filteredBuffer.Length != length)
			{
				_filteredBuffer = UnsafeBuffer.Create(length, 4);
				_filteredBufferPtr = (float*)(void*)_filteredBuffer;
			}
			Utils.Memcpy(_filteredBufferPtr, buffer, length * 4);
			_toneFilter.Process(_filteredBufferPtr, length);
			DetectCode(_filteredBufferPtr, length);
		}

		private unsafe void DetectCode(float* filteredBuffer, int length)
		{
			_max = float.MinValue;
			_min = float.MaxValue;
			for (int i = 0; i < length; i++)
			{
				float val = filteredBuffer[i];
				_max = Math.Max(_max, val);
				_min = Math.Min(_min, val);
			}
			_center = (_max + _min) * 0.5f;
			for (int j = 0; j < length; j++)
			{
				SampleAdd(filteredBuffer[j]);
			}
		}

		private void SampleAdd(float sample)
		{
			_average = (_average + sample) * 0.5f;
			_sampleCount++;
			if (_previousAverage > _center && _average <= _center)
			{
				_sampleCount = (int)_samplesPerBit / 2;
			}
			_previousAverage = _average;
			if ((float)_sampleCount >= _samplesPerBit)
			{
				_sampleCount = 0;
				if (_average < _center)
				{
					AddBit(bit: true);
				}
				else
				{
					AddBit(bit: false);
				}
				_average = _center;
			}
		}

		private void AddBit(bool bit)
		{
			if (_bitCounter >= 23)
			{
				_bitCounter = 0;
				_recivedCode.CopyTo(RecivedCode, 0);
				_codeAvailable = true;
			}
			if (bit)
			{
				_recivedCode[_bitCounter] = 1;
			}
			else
			{
				_recivedCode[_bitCounter] = 0;
			}
			_bitCounter++;
		}
	}
}

using SDRSharp.Radio;

namespace SDRSharp.DCSDecoder
{
	public class AudioProcessor : IRealProcessor, IStreamProcessor, IBaseProcessor
	{
		private double _sampleRate;

		private bool _enabled;

		private Decoder _decoder;

		private bool _needNewDecoder;

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				_sampleRate = value;
				_needNewDecoder = true;
			}
		}

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public bool Mute
		{
			get;
			set;
		}

		public byte[] RecivedCode
		{
			get
			{
				if (_decoder != null)
				{
					return _decoder.RecivedCode;
				}
				return null;
			}
		}

		public bool CodeAvailable
		{
			get
			{
				if (_decoder != null)
				{
					return _decoder.CodeAvailable;
				}
				return false;
			}
		}

		public unsafe void Process(float* buffer, int length)
		{
			if (_needNewDecoder || _decoder == null)
			{
				_decoder = new Decoder(_sampleRate);
				_needNewDecoder = false;
			}
			_decoder.Process(buffer, length);
			if (Mute)
			{
				MuteBuffer(buffer, length);
			}
		}

		private unsafe void MuteBuffer(float* buffer, int length)
		{
			for (int i = 0; i < length; i++)
			{
				buffer[i] = 0f;
			}
		}
	}
}

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.DCSDecoder
{
	public class AuxWindow : Form
	{
		private bool _setCode;

		private IContainer components;

		private Button setToneButton;

		private CheckBox squelchEnableCheckBox;

		private Label label1;

		private Label plusLabel;

		private Label label3;

		private Label minusLabel;

		private Label detectLabel;

		private Label muteLabel;

		public string PlusCode
		{
			set
			{
				plusLabel.Text = value;
			}
		}

		public string MinusCode
		{
			set
			{
				minusLabel.Text = value;
			}
		}

		public bool Detected
		{
			set
			{
				detectLabel.Visible = value;
			}
		}

		public bool SquelchEnable
		{
			get
			{
				return squelchEnableCheckBox.Checked;
			}
			set
			{
				squelchEnableCheckBox.Checked = value;
			}
		}

		public bool Mute
		{
			set
			{
				muteLabel.Visible = value;
			}
		}

		public bool SetThisCode
		{
			get
			{
				bool setCode = _setCode;
				_setCode = false;
				return setCode;
			}
		}

		public AuxWindow()
		{
			InitializeComponent();
		}

		private void setToneButton_Click(object sender, EventArgs e)
		{
			_setCode = true;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			setToneButton = new System.Windows.Forms.Button();
			squelchEnableCheckBox = new System.Windows.Forms.CheckBox();
			label1 = new System.Windows.Forms.Label();
			plusLabel = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			minusLabel = new System.Windows.Forms.Label();
			detectLabel = new System.Windows.Forms.Label();
			muteLabel = new System.Windows.Forms.Label();
			SuspendLayout();
			setToneButton.Location = new System.Drawing.Point(83, 86);
			setToneButton.Name = "setToneButton";
			setToneButton.Size = new System.Drawing.Size(84, 23);
			setToneButton.TabIndex = 2;
			setToneButton.Text = "Set this code";
			setToneButton.UseVisualStyleBackColor = true;
			setToneButton.Click += new System.EventHandler(setToneButton_Click);
			squelchEnableCheckBox.AutoSize = true;
			squelchEnableCheckBox.Location = new System.Drawing.Point(11, 90);
			squelchEnableCheckBox.Name = "squelchEnableCheckBox";
			squelchEnableCheckBox.Size = new System.Drawing.Size(65, 17);
			squelchEnableCheckBox.TabIndex = 4;
			squelchEnableCheckBox.Text = "Squelch";
			squelchEnableCheckBox.UseVisualStyleBackColor = true;
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(9, 5);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(41, 13);
			label1.TabIndex = 5;
			label1.Text = "Code +";
			plusLabel.AutoSize = true;
			plusLabel.Font = new System.Drawing.Font("Consolas", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			plusLabel.Location = new System.Drawing.Point(8, 21);
			plusLabel.Name = "plusLabel";
			plusLabel.Size = new System.Drawing.Size(140, 15);
			plusLabel.TabIndex = 6;
			plusLabel.Text = "000 000 000 000 000";
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(9, 45);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(38, 13);
			label3.TabIndex = 7;
			label3.Text = "Code -";
			minusLabel.AutoSize = true;
			minusLabel.Font = new System.Drawing.Font("Consolas", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			minusLabel.Location = new System.Drawing.Point(8, 62);
			minusLabel.Name = "minusLabel";
			minusLabel.Size = new System.Drawing.Size(140, 15);
			minusLabel.TabIndex = 8;
			minusLabel.Text = "000 000 000 000 000";
			detectLabel.AutoSize = true;
			detectLabel.Location = new System.Drawing.Point(89, 5);
			detectLabel.Name = "detectLabel";
			detectLabel.Size = new System.Drawing.Size(42, 13);
			detectLabel.TabIndex = 9;
			detectLabel.Text = "Detect.";
			muteLabel.AutoSize = true;
			muteLabel.ForeColor = System.Drawing.Color.Red;
			muteLabel.Location = new System.Drawing.Point(137, 5);
			muteLabel.Name = "muteLabel";
			muteLabel.Size = new System.Drawing.Size(34, 13);
			muteLabel.TabIndex = 10;
			muteLabel.Text = "Mute.";
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(171, 108);
			base.ControlBox = false;
			base.Controls.Add(muteLabel);
			base.Controls.Add(detectLabel);
			base.Controls.Add(minusLabel);
			base.Controls.Add(label3);
			base.Controls.Add(plusLabel);
			base.Controls.Add(label1);
			base.Controls.Add(squelchEnableCheckBox);
			base.Controls.Add(setToneButton);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "AuxWindow";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			Text = "DCS code";
			base.TopMost = true;
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

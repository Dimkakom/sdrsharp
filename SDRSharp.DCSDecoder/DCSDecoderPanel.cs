using SDRSharp.Common;
using SDRSharp.PanView;
using SDRSharp.Radio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.DCSDecoder
{
	public class DCSDecoderPanel : UserControl
	{
		private const int CodeWordLengthInBit = 23;

		private AudioProcessor _audioProcessor;

		private ISharpControl _control;

		private AuxWindow _auxWindow;

		private int _codeToSet;

		private bool _reset;

		private byte[] _previousBinaryCode;

		private byte[] _recivedBinaryCode;

		private List<int> _allRecivedCodes;

		private IContainer components;

		private CheckBox squelchEnableCheckBox;

		private NumericUpDown squelchCodeNumericUpDown;

		private GroupBox audioFilterGroupBox;

		private Label label2;

		private Timer displayUpdateTimer;

		private GroupBox groupBox1;

		private CheckBox detectEnableCheckBox;

		private CheckBox auxWindowCheckBox;

		private Button setCodeButton;

		private Label minusLabel;

		private Label plusLabel;

		private Label label5;

		private Label label3;

		private Label recivedCodeLabel;

		private Label muteLabel;

		private CheckBox showOnSpectrumCheckBox;

		public DCSDecoderPanel(AudioProcessor audioProcessor, ISharpControl control)
		{
			InitializeComponent();
			_audioProcessor = audioProcessor;
			_control = control;
			_auxWindow = new AuxWindow();
			_recivedBinaryCode = new byte[23];
			_previousBinaryCode = new byte[23];
			_allRecivedCodes = new List<int>();
			squelchCodeNumericUpDown.Value = (decimal)Utils.GetDoubleSetting("DCSSquelchCode", 0.0);
			detectEnableCheckBox.Checked = Utils.GetBooleanSetting("DCSDetectEnable");
			squelchEnableCheckBox.Checked = Utils.GetBooleanSetting("DCSSquelchEnable");
			auxWindowCheckBox.Checked = Utils.GetBooleanSetting("DCSAuxWindowEnable");
			showOnSpectrumCheckBox.Checked = Utils.GetBooleanSetting("DCSShowOnSpectrum");
			ConfigureOutToPanView();
			DisableControls();
			SetGain();
		}

		public void StoreSettings()
		{
			Utils.SaveSetting("DCSSquelchCode", squelchCodeNumericUpDown.Value);
			Utils.SaveSetting("DCSSquelchEnable", squelchEnableCheckBox.Checked);
			Utils.SaveSetting("DCSDetectEnable", detectEnableCheckBox.Checked);
			Utils.SaveSetting("DCSAuxWindowEnable", auxWindowCheckBox.Checked);
			Utils.SaveSetting("DCSShowOnSpectrum", showOnSpectrumCheckBox.Checked);
		}

		public void SetGain()
		{
			_audioProcessor.Enabled = ((squelchEnableCheckBox.Checked || detectEnableCheckBox.Checked) && _control.DetectorType == DetectorType.NFM);
			ConfigureOutToPanView();
		}

		public void DisableControls()
		{
			detectEnableCheckBox.Enabled = false;
			squelchEnableCheckBox.Enabled = false;
			squelchCodeNumericUpDown.Enabled = false;
			auxWindowCheckBox.Enabled = false;
			showOnSpectrumCheckBox.Enabled = false;
			_auxWindow.Visible = false;
			setCodeButton.Enabled = false;
		}

		public void EnableControls()
		{
			detectEnableCheckBox.Enabled = true;
			squelchEnableCheckBox.Enabled = true;
			squelchCodeNumericUpDown.Enabled = true;
			showOnSpectrumCheckBox.Enabled = true;
			auxWindowCheckBox.Enabled = true;
			auxWindowCheckBox_CheckedChanged(null, null);
			setCodeButton.Enabled = true;
		}

		public void Reset()
		{
			_reset = true;
			string empty = string.Empty;
			plusLabel.Text = empty;
			minusLabel.Text = empty;
			recivedCodeLabel.Visible = false;
			muteLabel.Visible = false;
			_auxWindow.PlusCode = empty;
			_auxWindow.MinusCode = empty;
			_codeToSet = 0;
		}

		private void detectEnableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			SetGain();
			_auxWindow.Visible = (detectEnableCheckBox.Checked && auxWindowCheckBox.Checked);
		}

		private void SquelchEnableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			SetGain();
			_auxWindow.SquelchEnable = squelchEnableCheckBox.Checked;
			_audioProcessor.Mute = squelchEnableCheckBox.Checked;
		}

		private void DisplayUpdateTimer_Tick(object sender, EventArgs e)
		{
			if (_reset)
			{
				_reset = false;
				return;
			}
			if (_audioProcessor.CodeAvailable)
			{
				_audioProcessor.RecivedCode.CopyTo(_recivedBinaryCode, 0);
				if (!ArrayCompare(_recivedBinaryCode, _previousBinaryCode))
				{
					_recivedBinaryCode.CopyTo(_previousBinaryCode, 0);
					List<int> list = BinaryToOctal(_recivedBinaryCode);
					List<int> list2 = BinaryToOctal(InvertedCode(_recivedBinaryCode));
					_allRecivedCodes.Clear();
					_allRecivedCodes.AddRange(list);
					_allRecivedCodes.AddRange(list2);
					if (_allRecivedCodes.Count > 0)
					{
						_codeToSet = _allRecivedCodes[0];
						Label label = recivedCodeLabel;
						bool visible = _auxWindow.Detected = true;
						label.Visible = visible;
						string text3 = _auxWindow.PlusCode = (plusLabel.Text = ListToString(list));
						text3 = (_auxWindow.MinusCode = (minusLabel.Text = ListToString(list2)));
					}
					else
					{
						Label label2 = recivedCodeLabel;
						bool visible = _auxWindow.Detected = false;
						label2.Visible = visible;
					}
				}
			}
			bool flag3 = squelchEnableCheckBox.Checked;
			if (_allRecivedCodes.Count > 0)
			{
				flag3 &= (!(squelchCodeNumericUpDown.Value == decimal.Zero) && _allRecivedCodes.IndexOf((int)squelchCodeNumericUpDown.Value) == -1);
			}
			_audioProcessor.Mute = flag3;
			muteLabel.Visible = (flag3 && squelchEnableCheckBox.Checked);
			_auxWindow.Mute = (flag3 && squelchEnableCheckBox.Checked);
			if (_auxWindow.SquelchEnable != squelchEnableCheckBox.Checked)
			{
				squelchEnableCheckBox.Checked = _auxWindow.SquelchEnable;
			}
			if (_auxWindow.SetThisCode)
			{
				setCodeButton_Click(null, null);
			}
		}

		private bool ArrayCompare(byte[] arrayLeft, byte[] arrayRight)
		{
			if (arrayLeft.Length != arrayRight.Length)
			{
				return false;
			}
			for (int i = 0; i < arrayRight.Length; i++)
			{
				if (arrayRight[i] != arrayLeft[i])
				{
					return false;
				}
			}
			return true;
		}

		private string ListToString(List<int> code)
		{
			string text = string.Empty;
			foreach (int item in code)
			{
				text = text + item.ToString("D3") + " ";
			}
			return text;
		}

		private List<int> BinaryToOctal(byte[] binary)
		{
			List<int> list = new List<int>();
			for (int i = 0; i < binary.Length; i++)
			{
				if (binary[i] == 1 && binary[CalculateIndex(i - 1)] == 0 && binary[CalculateIndex(i - 2)] == 0 && ParityCheck(binary, i))
				{
					int num = binary[CalculateIndex(i - 3)] * 4 + binary[CalculateIndex(i - 4)] * 2 + binary[CalculateIndex(i - 5)] * 1;
					int num2 = binary[CalculateIndex(i - 6)] * 4 + binary[CalculateIndex(i - 7)] * 2 + binary[CalculateIndex(i - 8)] * 1;
					int num3 = binary[CalculateIndex(i - 9)] * 4 + binary[CalculateIndex(i - 10)] * 2 + binary[CalculateIndex(i - 11)] * 1;
					int item = num * 100 + num2 * 10 + num3;
					list.Add(item);
				}
			}
			list.Sort();
			return list;
		}

		private bool ParityCheck(byte[] binary, int startIndex)
		{
			byte b = binary[CalculateIndex(startIndex - 11)];
			byte b2 = binary[CalculateIndex(startIndex - 10)];
			byte b3 = binary[CalculateIndex(startIndex - 9)];
			byte b4 = binary[CalculateIndex(startIndex - 8)];
			byte b5 = binary[CalculateIndex(startIndex - 7)];
			byte b6 = binary[CalculateIndex(startIndex - 6)];
			byte b7 = binary[CalculateIndex(startIndex - 5)];
			byte b8 = binary[CalculateIndex(startIndex - 4)];
			byte b9 = binary[CalculateIndex(startIndex - 3)];
			byte num = binary[CalculateIndex(startIndex + 1)];
			byte b10 = binary[CalculateIndex(startIndex + 2)];
			byte b11 = binary[CalculateIndex(startIndex + 3)];
			byte b12 = binary[CalculateIndex(startIndex + 4)];
			byte b13 = binary[CalculateIndex(startIndex + 5)];
			byte b14 = binary[CalculateIndex(startIndex + 6)];
			byte b15 = binary[CalculateIndex(startIndex + 7)];
			byte b16 = binary[CalculateIndex(startIndex + 8)];
			byte b17 = binary[CalculateIndex(startIndex + 9)];
			byte b18 = binary[CalculateIndex(startIndex + 10)];
			byte b19 = binary[CalculateIndex(startIndex + 11)];
			if (num != (b ^ b2 ^ b3 ^ b4 ^ b5 ^ b8))
			{
				return false;
			}
			if (b10 == (b2 ^ b3 ^ b4 ^ b5 ^ b6 ^ b9))
			{
				return false;
			}
			if (b11 != (b ^ b2 ^ b6 ^ b7 ^ b8))
			{
				return false;
			}
			if (b12 == (b2 ^ b3 ^ b7 ^ b8 ^ b9))
			{
				return false;
			}
			if (b13 == (b ^ b2 ^ b5 ^ b9))
			{
				return false;
			}
			if (b14 == (b ^ b4 ^ b5 ^ b6 ^ b8))
			{
				return false;
			}
			if (b15 != (b ^ b3 ^ b4 ^ b6 ^ b7 ^ b8 ^ b9))
			{
				return false;
			}
			if (b16 != (b2 ^ b4 ^ b5 ^ b7 ^ b8 ^ b9))
			{
				return false;
			}
			if (b17 != (b3 ^ b5 ^ b6 ^ b8 ^ b9))
			{
				return false;
			}
			if (b18 == (b4 ^ b6 ^ b7 ^ b9))
			{
				return false;
			}
			if (b19 == (b ^ b2 ^ b3 ^ b4 ^ b7))
			{
				return false;
			}
			return true;
		}

		private byte[] InvertedCode(byte[] code)
		{
			byte[] array = new byte[code.Length];
			for (int i = 0; i < array.Length; i++)
			{
				if (code[i] == 1)
				{
					array[i] = 0;
				}
				else
				{
					array[i] = 1;
				}
			}
			return array;
		}

		private int CalculateIndex(int index)
		{
			if (index < 0)
			{
				index = 23 + index;
			}
			return index % 23;
		}

		private void auxWindowCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_auxWindow.Visible = (auxWindowCheckBox.Checked && detectEnableCheckBox.Checked);
		}

		private void setCodeButton_Click(object sender, EventArgs e)
		{
			squelchCodeNumericUpDown.Value = _codeToSet;
		}

		private void CustomPaint(object sender, CustomPaintEventArgs e)
		{
			SpectrumAnalyzer spectrumAnalyzer = (SpectrumAnalyzer)sender;
			using (SolidBrush brush = new SolidBrush(Color.FromArgb(100, Color.Black)))
			{
				using (SolidBrush solidBrush = new SolidBrush(Color.FromArgb(200, Color.White)))
				{
					using (SolidBrush solidBrush2 = new SolidBrush(Color.FromArgb(200, Color.LightGreen)))
					{
						using (StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic))
						{
							using (Font font = new Font("Arial", 10f))
							{
								stringFormat.Alignment = StringAlignment.Near;
								Rectangle rect = default(Rectangle);
								Point p = default(Point);
								string text = "DCS + " + plusLabel.Text + " - " + minusLabel.Text;
								if (squelchEnableCheckBox.Checked)
								{
									text = text + " Squelch " + ((int)squelchCodeNumericUpDown.Value).ToString("D3");
									if (_audioProcessor.Mute)
									{
										text += " Mute";
									}
								}
								SizeF sizeF = e.Graphics.MeasureString(text, font);
								rect.X = 40;
								rect.Y = 3;
								rect.Width = (int)sizeF.Width;
								rect.Height = (int)sizeF.Height;
								p.X = rect.X;
								p.Y = rect.Y;
								e.Graphics.FillRectangle(brush, rect);
								e.Graphics.DrawString(text, font, solidBrush, p, stringFormat);
								if (_codeToSet != 0)
								{
									text = "+" + _codeToSet.ToString("D3");
									text = ((_control.Frequency <= spectrumAnalyzer.CenterFrequency) ? ("< " + text) : (text + " >"));
									sizeF = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
									rect.X = (int)spectrumAnalyzer.FrequencyToPoint(_control.Frequency);
									rect.Y += rect.Height;
									rect.Width = (int)sizeF.Width;
									rect.Height = (int)sizeF.Height;
									if (_control.Frequency > spectrumAnalyzer.CenterFrequency)
									{
										rect.X = (int)((float)rect.X - sizeF.Width);
									}
									p.X = rect.X;
									p.Y = rect.Y;
									SolidBrush brush2 = solidBrush;
									if (recivedCodeLabel.Visible)
									{
										brush2 = solidBrush2;
									}
									e.Graphics.FillRectangle(brush, rect);
									e.Graphics.DrawString(text, font, brush2, p, stringFormat);
								}
							}
						}
					}
				}
			}
		}

		private void ConfigureOutToPanView()
		{
			_control.SpectrumAnalyzerCustomPaint -= CustomPaint;
			if (showOnSpectrumCheckBox.Checked && _audioProcessor.Enabled)
			{
				_control.SpectrumAnalyzerCustomPaint += CustomPaint;
			}
		}

		private void showOnSpectrumCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			ConfigureOutToPanView();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			squelchEnableCheckBox = new System.Windows.Forms.CheckBox();
			squelchCodeNumericUpDown = new System.Windows.Forms.NumericUpDown();
			audioFilterGroupBox = new System.Windows.Forms.GroupBox();
			label2 = new System.Windows.Forms.Label();
			muteLabel = new System.Windows.Forms.Label();
			displayUpdateTimer = new System.Windows.Forms.Timer(components);
			groupBox1 = new System.Windows.Forms.GroupBox();
			showOnSpectrumCheckBox = new System.Windows.Forms.CheckBox();
			label5 = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			recivedCodeLabel = new System.Windows.Forms.Label();
			minusLabel = new System.Windows.Forms.Label();
			setCodeButton = new System.Windows.Forms.Button();
			auxWindowCheckBox = new System.Windows.Forms.CheckBox();
			plusLabel = new System.Windows.Forms.Label();
			detectEnableCheckBox = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)squelchCodeNumericUpDown).BeginInit();
			audioFilterGroupBox.SuspendLayout();
			groupBox1.SuspendLayout();
			SuspendLayout();
			squelchEnableCheckBox.AutoSize = true;
			squelchEnableCheckBox.Location = new System.Drawing.Point(9, 0);
			squelchEnableCheckBox.Name = "squelchEnableCheckBox";
			squelchEnableCheckBox.Size = new System.Drawing.Size(65, 17);
			squelchEnableCheckBox.TabIndex = 7;
			squelchEnableCheckBox.Text = "Squelch";
			squelchEnableCheckBox.UseVisualStyleBackColor = true;
			squelchEnableCheckBox.CheckedChanged += new System.EventHandler(SquelchEnableCheckBox_CheckedChanged);
			squelchCodeNumericUpDown.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			squelchCodeNumericUpDown.Location = new System.Drawing.Point(86, 19);
			squelchCodeNumericUpDown.Maximum = new decimal(new int[4]
			{
				777,
				0,
				0,
				0
			});
			squelchCodeNumericUpDown.Name = "squelchCodeNumericUpDown";
			squelchCodeNumericUpDown.Size = new System.Drawing.Size(47, 20);
			squelchCodeNumericUpDown.TabIndex = 8;
			squelchCodeNumericUpDown.Value = new decimal(new int[4]
			{
				110,
				0,
				0,
				0
			});
			audioFilterGroupBox.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			audioFilterGroupBox.Controls.Add(label2);
			audioFilterGroupBox.Controls.Add(muteLabel);
			audioFilterGroupBox.Controls.Add(squelchCodeNumericUpDown);
			audioFilterGroupBox.Controls.Add(squelchEnableCheckBox);
			audioFilterGroupBox.Location = new System.Drawing.Point(4, 168);
			audioFilterGroupBox.Name = "audioFilterGroupBox";
			audioFilterGroupBox.Size = new System.Drawing.Size(210, 49);
			audioFilterGroupBox.TabIndex = 0;
			audioFilterGroupBox.TabStop = false;
			audioFilterGroupBox.Text = "Squelch";
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(7, 21);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(73, 13);
			label2.TabIndex = 10;
			label2.Text = "Code (0 - any)";
			muteLabel.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			muteLabel.AutoSize = true;
			muteLabel.ForeColor = System.Drawing.Color.Red;
			muteLabel.Location = new System.Drawing.Point(170, 21);
			muteLabel.Name = "muteLabel";
			muteLabel.Size = new System.Drawing.Size(34, 13);
			muteLabel.TabIndex = 20;
			muteLabel.Text = "Mute.";
			displayUpdateTimer.Enabled = true;
			displayUpdateTimer.Tick += new System.EventHandler(DisplayUpdateTimer_Tick);
			groupBox1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			groupBox1.Controls.Add(showOnSpectrumCheckBox);
			groupBox1.Controls.Add(label5);
			groupBox1.Controls.Add(label3);
			groupBox1.Controls.Add(recivedCodeLabel);
			groupBox1.Controls.Add(minusLabel);
			groupBox1.Controls.Add(setCodeButton);
			groupBox1.Controls.Add(auxWindowCheckBox);
			groupBox1.Controls.Add(plusLabel);
			groupBox1.Controls.Add(detectEnableCheckBox);
			groupBox1.Location = new System.Drawing.Point(4, 3);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new System.Drawing.Size(210, 159);
			groupBox1.TabIndex = 12;
			groupBox1.TabStop = false;
			groupBox1.Text = "Detect";
			showOnSpectrumCheckBox.AutoSize = true;
			showOnSpectrumCheckBox.Location = new System.Drawing.Point(9, 19);
			showOnSpectrumCheckBox.Name = "showOnSpectrumCheckBox";
			showOnSpectrumCheckBox.Size = new System.Drawing.Size(114, 17);
			showOnSpectrumCheckBox.TabIndex = 11;
			showOnSpectrumCheckBox.Text = "Show on spectrum";
			showOnSpectrumCheckBox.UseVisualStyleBackColor = true;
			showOnSpectrumCheckBox.CheckedChanged += new System.EventHandler(showOnSpectrumCheckBox_CheckedChanged);
			label5.AutoSize = true;
			label5.Location = new System.Drawing.Point(6, 80);
			label5.Name = "label5";
			label5.Size = new System.Drawing.Size(38, 13);
			label5.TabIndex = 19;
			label5.Text = "Code -";
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(6, 39);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(41, 13);
			label3.TabIndex = 18;
			label3.Text = "Code +";
			recivedCodeLabel.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			recivedCodeLabel.AutoSize = true;
			recivedCodeLabel.Location = new System.Drawing.Point(150, 131);
			recivedCodeLabel.Name = "recivedCodeLabel";
			recivedCodeLabel.Size = new System.Drawing.Size(54, 13);
			recivedCodeLabel.TabIndex = 17;
			recivedCodeLabel.Text = "Detected.";
			recivedCodeLabel.Visible = false;
			minusLabel.AutoSize = true;
			minusLabel.Font = new System.Drawing.Font("Consolas", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			minusLabel.Location = new System.Drawing.Point(6, 96);
			minusLabel.Name = "minusLabel";
			minusLabel.Size = new System.Drawing.Size(180, 19);
			minusLabel.TabIndex = 13;
			minusLabel.Text = "000 000 000 000 000";
			setCodeButton.Location = new System.Drawing.Point(10, 126);
			setCodeButton.Name = "setCodeButton";
			setCodeButton.Size = new System.Drawing.Size(84, 23);
			setCodeButton.TabIndex = 15;
			setCodeButton.Text = "Set this code";
			setCodeButton.UseVisualStyleBackColor = true;
			setCodeButton.Click += new System.EventHandler(setCodeButton_Click);
			auxWindowCheckBox.AutoSize = true;
			auxWindowCheckBox.Location = new System.Drawing.Point(127, 19);
			auxWindowCheckBox.Name = "auxWindowCheckBox";
			auxWindowCheckBox.Size = new System.Drawing.Size(83, 17);
			auxWindowCheckBox.TabIndex = 16;
			auxWindowCheckBox.Text = "Aux window";
			auxWindowCheckBox.UseVisualStyleBackColor = true;
			auxWindowCheckBox.CheckedChanged += new System.EventHandler(auxWindowCheckBox_CheckedChanged);
			plusLabel.AutoSize = true;
			plusLabel.Font = new System.Drawing.Font("Consolas", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
			plusLabel.Location = new System.Drawing.Point(5, 55);
			plusLabel.Name = "plusLabel";
			plusLabel.Size = new System.Drawing.Size(180, 19);
			plusLabel.TabIndex = 12;
			plusLabel.Text = "000 000 000 000 000";
			detectEnableCheckBox.AutoSize = true;
			detectEnableCheckBox.Location = new System.Drawing.Point(9, 0);
			detectEnableCheckBox.Name = "detectEnableCheckBox";
			detectEnableCheckBox.Size = new System.Drawing.Size(58, 17);
			detectEnableCheckBox.TabIndex = 13;
			detectEnableCheckBox.Text = "Detect";
			detectEnableCheckBox.UseVisualStyleBackColor = true;
			detectEnableCheckBox.CheckedChanged += new System.EventHandler(detectEnableCheckBox_CheckedChanged);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(groupBox1);
			base.Controls.Add(audioFilterGroupBox);
			base.Name = "DCSDecoderPanel";
			base.Size = new System.Drawing.Size(217, 247);
			((System.ComponentModel.ISupportInitialize)squelchCodeNumericUpDown).EndInit();
			audioFilterGroupBox.ResumeLayout(performLayout: false);
			audioFilterGroupBox.PerformLayout();
			groupBox1.ResumeLayout(performLayout: false);
			groupBox1.PerformLayout();
			ResumeLayout(performLayout: false);
		}
	}
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.BasebandRecorder
{
	public class DialogEditTimer : Form
	{
		private readonly SortableBindingList<StartTimerEntry> _displayedEntries = new SortableBindingList<StartTimerEntry>();

		private RecordingPanel _recorderPanel;

		private IContainer components;

		private Button cancelButton;

		private Button okButton;

		private Button deleteRangeButton;

		private BindingSource editTimeBindingSource;

		private DataGridView editTimeDataGridView;

		private Label label1;

		private DataGridViewTextBoxColumn startTimeDataGridViewTextBoxColumn;

		private DataGridViewTextBoxColumn endTimeDataGridViewTextBoxColumn;

		public DialogEditTimer(RecordingPanel panel)
		{
			_recorderPanel = panel;
			InitializeComponent();
			editTimeBindingSource.DataSource = _displayedEntries;
			editTimeDataGridView.CellEndEdit += ValidateForm;
			editTimeDataGridView.CellBeginEdit += FormBegiEdit;
			label1.Text = "Date and time format - " + DateTime.Now.ToString();
			if (_recorderPanel.TimerEntry != null)
			{
				foreach (StartTimerEntry item in _recorderPanel.TimerEntry)
				{
					_displayedEntries.Add(item);
				}
			}
		}

		private void FormBegiEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			okButton.Enabled = false;
		}

		private void okButton_Click(object sender, EventArgs e)
		{
			if (editTimeBindingSource != null)
			{
				if (_recorderPanel.TimerEntry == null)
				{
					_recorderPanel.TimerEntry = new List<StartTimerEntry>();
				}
				_recorderPanel.TimerEntry.Clear();
				foreach (StartTimerEntry item in editTimeBindingSource)
				{
					_recorderPanel.TimerEntry.Add(item);
				}
			}
			base.DialogResult = DialogResult.OK;
		}

		private void ValidateForm(object sender, DataGridViewCellEventArgs e)
		{
			bool flag = true;
			foreach (StartTimerEntry item in editTimeBindingSource)
			{
				int row = editTimeBindingSource.IndexOf(item);
				DateTime startTime = item.StartTime;
				bool flag2 = item.StartTime > DateTime.Now;
				IndicateErrorCells(0, row, flag2);
				DateTime endTime = item.EndTime;
				bool flag3 = item.EndTime > DateTime.Now && item.EndTime > item.StartTime;
				IndicateErrorCells(1, row, flag3);
				flag = (flag && flag2 && flag3);
			}
			okButton.Enabled = flag;
		}

		private void IndicateErrorCells(int collumn, int row, bool valid)
		{
			if (valid)
			{
				editTimeDataGridView[collumn, row].Style.BackColor = editTimeDataGridView.DefaultCellStyle.BackColor;
			}
			else
			{
				editTimeDataGridView[collumn, row].Style.BackColor = Color.LightPink;
			}
		}

		private void cancelButton_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
		}

		private void deleteRangeButton_Click(object sender, EventArgs e)
		{
			if (editTimeBindingSource.IndexOf(editTimeBindingSource.Current) > -1)
			{
				editTimeBindingSource.RemoveCurrent();
				ValidateForm(null, null);
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			cancelButton = new System.Windows.Forms.Button();
			okButton = new System.Windows.Forms.Button();
			deleteRangeButton = new System.Windows.Forms.Button();
			editTimeDataGridView = new System.Windows.Forms.DataGridView();
			startTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			endTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			editTimeBindingSource = new System.Windows.Forms.BindingSource(components);
			label1 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)editTimeDataGridView).BeginInit();
			((System.ComponentModel.ISupportInitialize)editTimeBindingSource).BeginInit();
			SuspendLayout();
			cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			cancelButton.Location = new System.Drawing.Point(261, 278);
			cancelButton.Name = "cancelButton";
			cancelButton.Size = new System.Drawing.Size(75, 23);
			cancelButton.TabIndex = 1;
			cancelButton.Text = "Cancel";
			cancelButton.UseVisualStyleBackColor = true;
			cancelButton.Click += new System.EventHandler(cancelButton_Click);
			okButton.Location = new System.Drawing.Point(180, 278);
			okButton.Name = "okButton";
			okButton.Size = new System.Drawing.Size(75, 23);
			okButton.TabIndex = 2;
			okButton.Text = "Ok";
			okButton.UseVisualStyleBackColor = true;
			okButton.Click += new System.EventHandler(okButton_Click);
			deleteRangeButton.Location = new System.Drawing.Point(3, 278);
			deleteRangeButton.Name = "deleteRangeButton";
			deleteRangeButton.Size = new System.Drawing.Size(75, 23);
			deleteRangeButton.TabIndex = 3;
			deleteRangeButton.Text = "Delete rows";
			deleteRangeButton.UseVisualStyleBackColor = true;
			deleteRangeButton.Click += new System.EventHandler(deleteRangeButton_Click);
			editTimeDataGridView.AutoGenerateColumns = false;
			editTimeDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			editTimeDataGridView.Columns.AddRange(startTimeDataGridViewTextBoxColumn, endTimeDataGridViewTextBoxColumn);
			editTimeDataGridView.DataSource = editTimeBindingSource;
			editTimeDataGridView.Location = new System.Drawing.Point(3, 3);
			editTimeDataGridView.Name = "editTimeDataGridView";
			editTimeDataGridView.RowHeadersVisible = false;
			editTimeDataGridView.Size = new System.Drawing.Size(333, 233);
			editTimeDataGridView.TabIndex = 4;
			startTimeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			startTimeDataGridViewTextBoxColumn.DataPropertyName = "StartTime";
			dataGridViewCellStyle.Format = "G";
			dataGridViewCellStyle.NullValue = null;
			startTimeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle;
			startTimeDataGridViewTextBoxColumn.HeaderText = "Start date and time";
			startTimeDataGridViewTextBoxColumn.Name = "startTimeDataGridViewTextBoxColumn";
			endTimeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			endTimeDataGridViewTextBoxColumn.DataPropertyName = "EndTime";
			dataGridViewCellStyle2.Format = "G";
			dataGridViewCellStyle2.NullValue = null;
			endTimeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
			endTimeDataGridViewTextBoxColumn.HeaderText = "Stop date and time";
			endTimeDataGridViewTextBoxColumn.Name = "endTimeDataGridViewTextBoxColumn";
			editTimeBindingSource.DataSource = typeof(SDRSharp.BasebandRecorder.StartTimerEntry);
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(12, 252);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(35, 13);
			label1.TabIndex = 5;
			label1.Text = "label1";
			base.AcceptButton = okButton;
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.SystemColors.Control;
			base.CancelButton = cancelButton;
			base.ClientSize = new System.Drawing.Size(337, 301);
			base.ControlBox = false;
			base.Controls.Add(label1);
			base.Controls.Add(editTimeDataGridView);
			base.Controls.Add(deleteRangeButton);
			base.Controls.Add(okButton);
			base.Controls.Add(cancelButton);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "DialogEditTimer";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			Text = "Edit Time";
			((System.ComponentModel.ISupportInitialize)editTimeDataGridView).EndInit();
			((System.ComponentModel.ISupportInitialize)editTimeBindingSource).EndInit();
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

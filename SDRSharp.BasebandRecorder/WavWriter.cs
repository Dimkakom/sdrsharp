using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace SDRSharp.BasebandRecorder
{
	public class WavWriter
	{
		private const long MaxStreamLength64 = long.MaxValue;

		private const long MaxStreamLength32 = 4294967295L;

		private const long MaxStreamLength31 = 2147483647L;

		private readonly string _filename;

		private readonly WavFormatHeader _format;

		private readonly WavSampleFormat _wavSampleFormat;

		private FileStream _outputStream;

		private long _fileSizeOffsInt;

		private long _dataSizeOffsInt;

		private long _fileSizeOffsLong;

		private long _dataSizeOffsLong;

		private long _sampleCountOffsLong;

		private long _length;

		private byte[] _outputBuffer;

		private bool _isStreamFull;

		private WaveFileFormat _wavFileFormat;

		private long _maxStreamLength;

		public WavSampleFormat SampleFormat => _wavSampleFormat;

		public WavFormatHeader WaveFormat => _format;

		public string FileName => _filename;

		public long Length => _length;

		public bool IsStreamFull => _isStreamFull;

		public WavWriter(string filename, WavSampleFormat recordingFormat, WaveFileFormat fileFormat, uint sampleRate)
		{
			_wavFileFormat = fileFormat;
			_filename = filename;
			_wavSampleFormat = recordingFormat;
			_format = new WavFormatHeader(recordingFormat, sampleRate);
			switch (_wavFileFormat)
			{
			case WaveFileFormat.RF64:
				_maxStreamLength = long.MaxValue;
				break;
			case WaveFileFormat.WAV32Full:
				_maxStreamLength = 4294967295L;
				break;
			case WaveFileFormat.WAV32Compatible:
				_maxStreamLength = 2147483647L;
				break;
			}
		}

		public void Open()
		{
			if (_outputStream == null)
			{
				_outputStream = new FileStream(_filename, FileMode.CreateNew);
				WriteHeader();
				return;
			}
			throw new InvalidOperationException("Stream already open");
		}

		public void Close()
		{
			if (_outputStream != null)
			{
				UpdateLength();
				_outputStream.Flush();
				_outputStream.Close();
				_outputStream = null;
				_outputBuffer = null;
				return;
			}
			throw new InvalidOperationException("Stream not open");
		}

		public unsafe void Write(float* data, int length)
		{
			if (_outputStream != null)
			{
				switch (_wavSampleFormat)
				{
				case WavSampleFormat.PCM8:
					length *= 2;
					WritePCM8(data, length);
					break;
				case WavSampleFormat.PCM16:
					length *= 2;
					WritePCM16(data, length);
					break;
				case WavSampleFormat.Float32:
					WriteFloat(data, length);
					break;
				}
				return;
			}
			throw new InvalidOperationException("Stream not open");
		}

		private unsafe void WritePCM8(float* data, int length)
		{
			if (_outputBuffer == null || _outputBuffer.Length != length)
			{
				_outputBuffer = null;
				_outputBuffer = new byte[length];
			}
			for (int i = 0; i < length; i++)
			{
				_outputBuffer[i] = (byte)Math.Round(data[i] * 127f + 128f);
			}
			WriteStream(_outputBuffer);
		}

		private unsafe void WritePCM16(float* data, int length)
		{
			if (_outputBuffer == null || _outputBuffer.Length != length * 2)
			{
				_outputBuffer = null;
				_outputBuffer = new byte[length * 2];
			}
			int num = 0;
			short num2 = 0;
			for (int i = 0; i < length; i++)
			{
				num2 = (short)(data[i] * 32767f);
				_outputBuffer[num] = (byte)(num2 & 0xFF);
				num++;
				_outputBuffer[num] = (byte)(num2 >> 8);
				num++;
			}
			WriteStream(_outputBuffer);
		}

		private unsafe void WriteFloat(float* data, int length)
		{
			if (_outputBuffer == null || _outputBuffer.Length != length * 4 * 2)
			{
				_outputBuffer = null;
				_outputBuffer = new byte[length * 4 * 2];
			}
			Marshal.Copy((IntPtr)(void*)data, _outputBuffer, 0, _outputBuffer.Length);
			WriteStream(_outputBuffer);
		}

		private void WriteStream(byte[] data)
		{
			if (_outputStream != null)
			{
				int num = (int)Math.Min(_maxStreamLength - _outputStream.Length, data.Length);
				_outputStream.Write(data, 0, num);
				_length += num;
				UpdateLength();
				_isStreamFull = (_outputStream.Length >= _maxStreamLength);
			}
		}

		private void WriteHeader()
		{
			if (_outputStream != null)
			{
				_outputStream.Seek(0L, SeekOrigin.Begin);
				switch (_wavFileFormat)
				{
				case WaveFileFormat.RF64:
					WriteTag("RF64");
					WriteUInt(uint.MaxValue);
					WriteTag("WAVE");
					WriteTag("ds64");
					WriteUInt(28u);
					_fileSizeOffsLong = _outputStream.Position;
					WriteLong(0uL);
					_dataSizeOffsLong = _outputStream.Position;
					WriteLong(0uL);
					_sampleCountOffsLong = _outputStream.Position;
					WriteLong(0uL);
					WriteUInt(0u);
					WriteTag("fmt ");
					WriteUInt(16u);
					WriteUShort(_format.FormatTag);
					WriteUShort(_format.Channels);
					WriteUInt(_format.SamplesPerSec);
					WriteUInt(_format.AvgBytesPerSec);
					WriteUShort(_format.BlockAlign);
					WriteUShort(_format.BitsPerSample);
					WriteTag("data");
					WriteUInt(uint.MaxValue);
					break;
				case WaveFileFormat.WAV32Full:
					WriteTag("RIFF");
					_fileSizeOffsInt = _outputStream.Position;
					WriteUInt(0u);
					WriteTag("WAVE");
					WriteTag("fmt ");
					WriteUInt(16u);
					WriteUShort(_format.FormatTag);
					WriteUShort(_format.Channels);
					WriteUInt(_format.SamplesPerSec);
					WriteUInt(_format.AvgBytesPerSec);
					WriteUShort(_format.BlockAlign);
					WriteUShort(_format.BitsPerSample);
					WriteTag("data");
					_dataSizeOffsInt = _outputStream.Position;
					WriteUInt(0u);
					break;
				case WaveFileFormat.WAV32Compatible:
					WriteTag("RIFF");
					_fileSizeOffsInt = _outputStream.Position;
					WriteUInt(0u);
					WriteTag("WAVE");
					WriteTag("fmt ");
					WriteUInt(16u);
					WriteUShort(_format.FormatTag);
					WriteUShort(_format.Channels);
					WriteUInt(_format.SamplesPerSec);
					WriteUInt(_format.AvgBytesPerSec);
					WriteUShort(_format.BlockAlign);
					WriteUShort(_format.BitsPerSample);
					WriteTag("data");
					_dataSizeOffsInt = _outputStream.Position;
					WriteUInt(0u);
					break;
				}
				_outputStream.Seek(0L, SeekOrigin.End);
			}
		}

		private void UpdateLength()
		{
			if (_outputStream != null)
			{
				if (_wavFileFormat == WaveFileFormat.RF64)
				{
					_outputStream.Seek(_fileSizeOffsLong, SeekOrigin.Begin);
					WriteLong((ulong)(_outputStream.Length - 8));
					_outputStream.Seek(_dataSizeOffsLong, SeekOrigin.Begin);
					WriteLong((ulong)_length);
					_outputStream.Seek(_sampleCountOffsLong, SeekOrigin.Begin);
					WriteLong((ulong)(_length / (long)_format.BlockAlign));
					_outputStream.Seek(0L, SeekOrigin.End);
				}
				else
				{
					_outputStream.Seek(_fileSizeOffsInt, SeekOrigin.Begin);
					WriteUInt((uint)(_outputStream.Length - 8));
					_outputStream.Seek(_dataSizeOffsInt, SeekOrigin.Begin);
					WriteUInt((uint)_length);
					_outputStream.Seek(0L, SeekOrigin.End);
				}
			}
		}

		private void WriteUShort(ushort shortData)
		{
			byte[] array = new byte[2];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = (byte)(shortData & 0xFF);
				shortData = (ushort)(shortData >> 8);
			}
			_outputStream.Write(array, 0, array.Length);
		}

		private void WriteUInt(uint intData)
		{
			byte[] array = new byte[4];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = (byte)(intData & 0xFF);
				intData >>= 8;
			}
			_outputStream.Write(array, 0, array.Length);
		}

		private void WriteLong(ulong longData)
		{
			byte[] array = new byte[8];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = (byte)(longData & 0xFF);
				longData >>= 8;
			}
			_outputStream.Write(array, 0, array.Length);
		}

		private void WriteTag(string tag)
		{
			byte[] bytes = Encoding.ASCII.GetBytes(tag);
			_outputStream.Write(bytes, 0, bytes.Length);
		}
	}
}

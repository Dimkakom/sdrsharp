using SDRSharp.Common;
using System.Windows.Forms;

namespace SDRSharp.BasebandRecorder
{
	public class BasebandRecorderPlugin : ISharpPlugin
	{
		private const string DefaultDisplayName = "Baseband Recorder";

		private ISharpControl _control;

		private RecordingPanel _guiControl;

		public UserControl Gui => _guiControl;

		public string DisplayName => "Baseband Recorder";

		public void Initialize(ISharpControl control)
		{
			_control = control;
			_guiControl = new RecordingPanel(_control);
		}

		public void Close()
		{
			if (_guiControl != null)
			{
				_guiControl.AbortRecording();
				_guiControl.SaveSettings();
			}
		}
	}
}

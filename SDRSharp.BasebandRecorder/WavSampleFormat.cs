namespace SDRSharp.BasebandRecorder
{
	public enum WavSampleFormat
	{
		PCM8,
		PCM16,
		Float32
	}
}

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.BasebandRecorder
{
	public class DialogConfigure : Form
	{
		private RecordingPanel _recordingPanel;

		private IContainer components;

		private Button OpenFolderDialog;

		private ComboBox fileFormatComboBox;

		private Label label1;

		private FolderBrowserDialog folderBrowserDialog;

		private Label maxLengthLabel;

		private NumericUpDown limitNumericUpDown;

		private CheckBox limiterCheckBox;

		private ComboBox sampleFormatComboBox;

		private Label sampleFormatLbl;

		private Label pathLabel;

		private Button okButton;

		public DialogConfigure(RecordingPanel owner)
		{
			InitializeComponent();
			_recordingPanel = owner;
			fileFormatComboBox.SelectedIndex = Convert.ToInt32(_recordingPanel.FileFormat);
			fileFormatComboBox_SelectedIndexChanged(null, null);
			sampleFormatComboBox.SelectedIndex = Convert.ToInt32(_recordingPanel.SampleFormat);
			folderBrowserDialog.SelectedPath = _recordingPanel.FilePath;
			pathLabel.Text = _recordingPanel.FilePath;
			limiterCheckBox.Checked = _recordingPanel.FileLimiterEnable;
			limitNumericUpDown.Value = (decimal)((double)_recordingPanel.FileLimitInByte / 1024.0 / 1024.0);
		}

		private void OpenFolderDialog_Click(object sender, EventArgs e)
		{
			folderBrowserDialog.ShowDialog();
			pathLabel.Text = folderBrowserDialog.SelectedPath;
		}

		private void fileFormatComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			long num = 0L;
			switch (fileFormatComboBox.SelectedIndex)
			{
			case 0:
				num = 2147483647L;
				break;
			case 1:
				num = 4294967295L;
				break;
			case 2:
				num = long.MaxValue;
				break;
			}
			maxLengthLabel.Text = $"Maximum file lenth {num / 1024 / 1024:N0} MB";
		}

		private void okButton_Click(object sender, EventArgs e)
		{
			_recordingPanel.FileFormat = (WaveFileFormat)fileFormatComboBox.SelectedIndex;
			_recordingPanel.SampleFormat = (WavSampleFormat)sampleFormatComboBox.SelectedIndex;
			_recordingPanel.FilePath = folderBrowserDialog.SelectedPath;
			_recordingPanel.FileLimiterEnable = limiterCheckBox.Checked;
			_recordingPanel.FileLimitInByte = (long)((double)limitNumericUpDown.Value * 1024.0 * 1024.0);
			base.DialogResult = DialogResult.OK;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			OpenFolderDialog = new System.Windows.Forms.Button();
			fileFormatComboBox = new System.Windows.Forms.ComboBox();
			label1 = new System.Windows.Forms.Label();
			folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			maxLengthLabel = new System.Windows.Forms.Label();
			limitNumericUpDown = new System.Windows.Forms.NumericUpDown();
			limiterCheckBox = new System.Windows.Forms.CheckBox();
			sampleFormatComboBox = new System.Windows.Forms.ComboBox();
			sampleFormatLbl = new System.Windows.Forms.Label();
			pathLabel = new System.Windows.Forms.Label();
			okButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)limitNumericUpDown).BeginInit();
			SuspendLayout();
			OpenFolderDialog.Location = new System.Drawing.Point(15, 123);
			OpenFolderDialog.Name = "OpenFolderDialog";
			OpenFolderDialog.Size = new System.Drawing.Size(215, 23);
			OpenFolderDialog.TabIndex = 13;
			OpenFolderDialog.Text = "Folder select";
			OpenFolderDialog.UseVisualStyleBackColor = true;
			OpenFolderDialog.Click += new System.EventHandler(OpenFolderDialog_Click);
			fileFormatComboBox.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			fileFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			fileFormatComboBox.DropDownWidth = 120;
			fileFormatComboBox.FormattingEnabled = true;
			fileFormatComboBox.Items.AddRange(new object[3]
			{
				"WAV SDR# Compatible",
				"WAV FULL",
				"WAV RF64"
			});
			fileFormatComboBox.Location = new System.Drawing.Point(134, 12);
			fileFormatComboBox.Name = "fileFormatComboBox";
			fileFormatComboBox.Size = new System.Drawing.Size(96, 21);
			fileFormatComboBox.TabIndex = 16;
			fileFormatComboBox.SelectedIndexChanged += new System.EventHandler(fileFormatComboBox_SelectedIndexChanged);
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(12, 15);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(58, 13);
			label1.TabIndex = 17;
			label1.Text = "File Format";
			maxLengthLabel.AutoSize = true;
			maxLengthLabel.Location = new System.Drawing.Point(12, 42);
			maxLengthLabel.Name = "maxLengthLabel";
			maxLengthLabel.Size = new System.Drawing.Size(145, 13);
			maxLengthLabel.TabIndex = 18;
			maxLengthLabel.Text = "Maximum file length 2048 MB";
			limitNumericUpDown.Location = new System.Drawing.Point(134, 66);
			limitNumericUpDown.Maximum = new decimal(new int[4]
			{
				100000000,
				0,
				0,
				0
			});
			limitNumericUpDown.Minimum = new decimal(new int[4]
			{
				1,
				0,
				0,
				0
			});
			limitNumericUpDown.Name = "limitNumericUpDown";
			limitNumericUpDown.Size = new System.Drawing.Size(96, 20);
			limitNumericUpDown.TabIndex = 20;
			limitNumericUpDown.Value = new decimal(new int[4]
			{
				2000,
				0,
				0,
				0
			});
			limiterCheckBox.AutoSize = true;
			limiterCheckBox.Location = new System.Drawing.Point(15, 67);
			limiterCheckBox.Name = "limiterCheckBox";
			limiterCheckBox.Size = new System.Drawing.Size(113, 17);
			limiterCheckBox.TabIndex = 21;
			limiterCheckBox.Text = "File length limit MB";
			limiterCheckBox.UseVisualStyleBackColor = true;
			sampleFormatComboBox.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			sampleFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			sampleFormatComboBox.DropDownWidth = 120;
			sampleFormatComboBox.FormattingEnabled = true;
			sampleFormatComboBox.Items.AddRange(new object[3]
			{
				"8 Bit PCM IQ",
				"16 Bit PCM IQ",
				"32 Bit IEEE Float IQ"
			});
			sampleFormatComboBox.Location = new System.Drawing.Point(134, 92);
			sampleFormatComboBox.Name = "sampleFormatComboBox";
			sampleFormatComboBox.Size = new System.Drawing.Size(96, 21);
			sampleFormatComboBox.TabIndex = 22;
			sampleFormatLbl.AutoSize = true;
			sampleFormatLbl.Location = new System.Drawing.Point(12, 95);
			sampleFormatLbl.Name = "sampleFormatLbl";
			sampleFormatLbl.Size = new System.Drawing.Size(77, 13);
			sampleFormatLbl.TabIndex = 23;
			sampleFormatLbl.Text = "Sample Format";
			pathLabel.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			pathLabel.Location = new System.Drawing.Point(12, 158);
			pathLabel.Name = "pathLabel";
			pathLabel.Size = new System.Drawing.Size(218, 18);
			pathLabel.TabIndex = 24;
			pathLabel.Text = "Path ";
			okButton.Location = new System.Drawing.Point(155, 182);
			okButton.Name = "okButton";
			okButton.Size = new System.Drawing.Size(75, 23);
			okButton.TabIndex = 25;
			okButton.Text = "Ok";
			okButton.UseVisualStyleBackColor = true;
			okButton.Click += new System.EventHandler(okButton_Click);
			base.AcceptButton = okButton;
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(242, 217);
			base.Controls.Add(okButton);
			base.Controls.Add(pathLabel);
			base.Controls.Add(sampleFormatComboBox);
			base.Controls.Add(sampleFormatLbl);
			base.Controls.Add(limiterCheckBox);
			base.Controls.Add(limitNumericUpDown);
			base.Controls.Add(maxLengthLabel);
			base.Controls.Add(fileFormatComboBox);
			base.Controls.Add(label1);
			base.Controls.Add(OpenFolderDialog);
			base.Name = "DialogConfigure";
			Text = "Configure";
			((System.ComponentModel.ISupportInitialize)limitNumericUpDown).EndInit();
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

namespace SDRSharp.BasebandRecorder
{
	public enum WaveFileFormat
	{
		WAV32Compatible,
		WAV32Full,
		RF64
	}
}

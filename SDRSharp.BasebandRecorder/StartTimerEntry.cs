using System;

namespace SDRSharp.BasebandRecorder
{
	public class StartTimerEntry
	{
		public DateTime StartTime
		{
			get;
			set;
		}

		public DateTime EndTime
		{
			get;
			set;
		}
	}
}

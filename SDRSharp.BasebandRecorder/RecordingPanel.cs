using SDRSharp.Common;
using SDRSharp.PluginsCom;
using SDRSharp.Radio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SDRSharp.BasebandRecorder
{
	public class RecordingPanel : UserControl
	{
		private const string recorderStartCom = "baseband_recorder_Start";

		private const string recorderStopCom = "baseband_recorder_Stop";

		private const double BytesToMb = 9.5367431640625E-07;

		private readonly ISharpControl _control;

		private readonly RecordingIQProcessor _iqProcessor = new RecordingIQProcessor();

		private readonly Recorder _simpleRecorder;

		private PluginsComProxy _pluginsCom;

		private DateTime _startTime;

		private long _oldFrequency;

		public List<StartTimerEntry> TimerEntry;

		private IContainer components;

		private Button recBtn;

		private Timer recDisplayTimer;

		private Label sizeAllLbl;

		private Label label2;

		private Panel panel1;

		private Button openFolderButton;

		private Button timerButton;

		private Timer sheduleTimer;

		private Label scheduleLabel;

		private Label dropLabel;

		private ProgressBar progressBar;

		private Button configureButton;

		public WavSampleFormat SampleFormat
		{
			get;
			set;
		}

		public WaveFileFormat FileFormat
		{
			get;
			set;
		}

		public string FilePath
		{
			get;
			set;
		}

		public bool FileLimiterEnable
		{
			get;
			set;
		}

		public long FileLimitInByte
		{
			get;
			set;
		}

		public RecordingPanel(ISharpControl control)
		{
			InitializeComponent();
			_control = control;
			_control.PropertyChanged += PropertyChangedHandler;
			_iqProcessor.Enabled = false;
			_control.RegisterStreamHook(_iqProcessor, ProcessorType.RawIQ);
			_simpleRecorder = new Recorder(_iqProcessor);
			SampleFormat = (WavSampleFormat)Utils.GetIntSetting("BasebandRecordSampleFormat", 1);
			FileFormat = (WaveFileFormat)Utils.GetIntSetting("BasebandRecorderFileFormat", 0);
			FilePath = Utils.GetStringSetting("BasebandWriteFolder", Path.GetDirectoryName(Application.ExecutablePath));
			FileLimiterEnable = Utils.GetBooleanSetting("BasebandRecorderFileLimiterEnable");
			FileLimitInByte = Utils.GetLongSetting("BasebandRecorderFileLimit", 2147483648L);
			string stringSetting = Utils.GetStringSetting("BasebandSchedule", null);
			if (!string.IsNullOrEmpty(stringSetting))
			{
				TimerEntry = new List<StartTimerEntry>();
				char[] separator = new char[1]
				{
					'|'
				};
				string[] array = stringSetting.Split(separator, StringSplitOptions.RemoveEmptyEntries);
				if (array.Length > 1)
				{
					for (int i = 0; i < array.Length; i += 2)
					{
						StartTimerEntry item = new StartTimerEntry
						{
							StartTime = Convert.ToDateTime(array[i]),
							EndTime = Convert.ToDateTime(array[i + 1])
						};
						TimerEntry.Add(item);
					}
				}
			}
			ConfigureGUI();
			_pluginsCom = new PluginsComProxy(NewCommandAvailable);
			_pluginsCom.AddAvailableCommands("baseband_recorder_Start");
			_pluginsCom.AddAvailableCommands("baseband_recorder_Stop");
		}

		public void SaveSettings()
		{
			Utils.SaveSetting("BasebandWriteFolder", FilePath);
			Utils.SaveSetting("BasebandRecordSampleFormat", Convert.ToInt32(SampleFormat));
			Utils.SaveSetting("BasebandRecorderFileFormat", Convert.ToInt32(FileFormat));
			Utils.SaveSetting("BasebandRecorderFileLimiterEnable", FileLimiterEnable);
			Utils.SaveSetting("BasebandRecorderFileLimit", FileLimitInByte);
			if (TimerEntry != null && TimerEntry.Count > 0)
			{
				string text = string.Empty;
				foreach (StartTimerEntry item in TimerEntry)
				{
					text = text + item.StartTime.ToShortDateString() + " ";
					text = text + item.StartTime.ToShortTimeString() + "|";
					text = text + item.EndTime.ToShortDateString() + " ";
					text = text + item.EndTime.ToShortTimeString() + "|";
				}
				Utils.SaveSetting("BasebandSchedule", text);
			}
			else
			{
				Utils.SaveSetting("BasebandSchedule", string.Empty);
			}
		}

		private void NewCommandAvailable(string command)
		{
			int num = command.IndexOf('<');
			int num2 = command.IndexOf('>');
			string text = command;
			if (num > 0)
			{
				text = command.Substring(0, num + 1).Trim();
			}
			string empty = string.Empty;
			if (num2 > 0)
			{
				command.Substring(num + 1, num2 - num - 1).Trim();
			}
			try
			{
				string a;
				if ((a = text) != null)
				{
					if (!(a == "baseband_recorder_Start"))
					{
						if (a == "baseband_recorder_Stop" && _simpleRecorder.IsRecording)
						{
							RecBtn_Click(null, null);
						}
					}
					else if (!_simpleRecorder.IsRecording)
					{
						RecBtn_Click(null, null);
					}
				}
			}
			catch
			{
				MessageBox.Show("Error command translate - " + command);
			}
		}

		private void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			string propertyName;
			if ((propertyName = e.PropertyName) == null)
			{
				return;
			}
			if (!(propertyName == "StartRadio"))
			{
				if (propertyName == "StopRadio")
				{
					if (_simpleRecorder.IsRecording)
					{
						RecordStop();
					}
					ConfigureGUI();
				}
			}
			else
			{
				ConfigureGUI();
			}
		}

		private void RecordStart()
		{
			_simpleRecorder.FileFormat = FileFormat;
			_simpleRecorder.SampleFormat = SampleFormat;
			_simpleRecorder.FileName = MakeFileName();
			_simpleRecorder.SampleRate = _iqProcessor.SampleRate;
			try
			{
				_simpleRecorder.StartRecording();
			}
			catch
			{
				MessageBox.Show("Unable to start recording", "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				return;
			}
			_startTime = DateTime.Now;
			_oldFrequency = _control.Frequency;
			recDisplayTimer.Enabled = true;
		}

		private void RecordStop()
		{
			_simpleRecorder.StopRecording();
			recDisplayTimer.Enabled = false;
		}

		private void RecBtn_Click(object sender, EventArgs e)
		{
			if (!_simpleRecorder.IsRecording)
			{
				RecordStart();
			}
			else
			{
				RecordStop();
			}
			ConfigureGUI();
		}

		private void RecDisplayTimer_Tick(object sender, EventArgs e)
		{
			double num = (double)_simpleRecorder.BytesWritten * 9.5367431640625E-07;
			if (num > 1024.0)
			{
				sizeAllLbl.Text = $"{num / 1024.0:f3} GB";
			}
			else
			{
				sizeAllLbl.Text = $"{num:f2} MB";
			}
			if (_simpleRecorder.IsStreamFull || (_simpleRecorder.BytesWritten >= FileLimitInByte && FileLimiterEnable))
			{
				RecordStop();
				RecordStart();
			}
			int bufferUsage = _simpleRecorder.BufferUsage;
			progressBar.Value = ((bufferUsage < progressBar.Minimum) ? progressBar.Minimum : ((bufferUsage > progressBar.Maximum) ? progressBar.Maximum : bufferUsage));
			dropLabel.Text = "Dropped buffers: " + _simpleRecorder.LostBuffers.ToString();
		}

		private void ConfigureGUI()
		{
			if (_control.IsPlaying)
			{
				recBtn.Enabled = true;
				recBtn.Text = (_simpleRecorder.IsRecording ? "Stop" : "Record");
			}
			else
			{
				recBtn.Enabled = false;
				recBtn.Text = "Record";
			}
			configureButton.Enabled = !_simpleRecorder.IsRecording;
		}

		private string MakeFileName()
		{
			long num = Math.Abs(_control.CenterFrequency + _control.IFOffset);
			string arg = "Hz";
			string str = DateTime.Now.ToString("yyyy_MM_dd");
			string arg2 = DateTime.Now.ToString("HH-mm-ss");
			string text = FilePath + "\\IQ\\" + str;
			string result = text + "\\" + $"{arg2}_{num}{arg}.wav";
			try
			{
				Directory.CreateDirectory(text);
				return result;
			}
			catch
			{
				MessageBox.Show("Unable to create directory", text, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				return result;
			}
		}

		public void AbortRecording()
		{
			if (_simpleRecorder != null)
			{
				_simpleRecorder.StopRecording();
			}
		}

		private void OpenFolderButton_Click(object sender, EventArgs e)
		{
			Process.Start("explorer.exe", FilePath + "\\IQ");
		}

		private void TimerButton_Click(object sender, EventArgs e)
		{
			DialogEditTimer dialogEditTimer = new DialogEditTimer(this);
			dialogEditTimer.ShowDialog();
		}

		private void SheduleTimer_Tick(object sender, EventArgs e)
		{
			if (TimerEntry == null)
			{
				return;
			}
			timerButton.Text = "Schedule - " + TimerEntry.Count;
			for (int i = 0; i < TimerEntry.Count; i++)
			{
				StartTimerEntry startTimerEntry = TimerEntry[i];
				if (startTimerEntry.StartTime <= DateTime.Now)
				{
					if (!_simpleRecorder.IsRecording)
					{
						RecordStart();
						ConfigureGUI();
					}
					scheduleLabel.Visible = !scheduleLabel.Visible;
				}
				if (startTimerEntry.EndTime <= DateTime.Now)
				{
					if (_simpleRecorder.IsRecording)
					{
						RecordStop();
						ConfigureGUI();
					}
					scheduleLabel.Visible = false;
					TimerEntry.RemoveAt(i);
					i--;
				}
			}
		}

		private void ConfigureButton_Click(object sender, EventArgs e)
		{
			DialogConfigure dialogConfigure = new DialogConfigure(this);
			dialogConfigure.ShowDialog();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			recBtn = new System.Windows.Forms.Button();
			recDisplayTimer = new System.Windows.Forms.Timer(components);
			sizeAllLbl = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			panel1 = new System.Windows.Forms.Panel();
			dropLabel = new System.Windows.Forms.Label();
			progressBar = new System.Windows.Forms.ProgressBar();
			scheduleLabel = new System.Windows.Forms.Label();
			timerButton = new System.Windows.Forms.Button();
			openFolderButton = new System.Windows.Forms.Button();
			sheduleTimer = new System.Windows.Forms.Timer(components);
			configureButton = new System.Windows.Forms.Button();
			panel1.SuspendLayout();
			SuspendLayout();
			recBtn.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			recBtn.Enabled = false;
			recBtn.Location = new System.Drawing.Point(111, 3);
			recBtn.Name = "recBtn";
			recBtn.Size = new System.Drawing.Size(103, 23);
			recBtn.TabIndex = 0;
			recBtn.Text = "Record";
			recBtn.UseVisualStyleBackColor = true;
			recBtn.Click += new System.EventHandler(RecBtn_Click);
			recDisplayTimer.Tick += new System.EventHandler(RecDisplayTimer_Tick);
			sizeAllLbl.AutoSize = true;
			sizeAllLbl.Location = new System.Drawing.Point(47, 8);
			sizeAllLbl.Name = "sizeAllLbl";
			sizeAllLbl.Size = new System.Drawing.Size(32, 13);
			sizeAllLbl.TabIndex = 3;
			sizeAllLbl.Text = "0 MB";
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(6, 8);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(35, 13);
			label2.TabIndex = 4;
			label2.Text = "Write:";
			panel1.Controls.Add(configureButton);
			panel1.Controls.Add(dropLabel);
			panel1.Controls.Add(progressBar);
			panel1.Controls.Add(scheduleLabel);
			panel1.Controls.Add(timerButton);
			panel1.Controls.Add(openFolderButton);
			panel1.Controls.Add(label2);
			panel1.Controls.Add(sizeAllLbl);
			panel1.Controls.Add(recBtn);
			panel1.Location = new System.Drawing.Point(0, 0);
			panel1.Name = "panel1";
			panel1.Size = new System.Drawing.Size(217, 146);
			panel1.TabIndex = 7;
			dropLabel.AutoSize = true;
			dropLabel.Location = new System.Drawing.Point(6, 37);
			dropLabel.Name = "dropLabel";
			dropLabel.Size = new System.Drawing.Size(95, 13);
			dropLabel.TabIndex = 23;
			dropLabel.Text = "Dropped buffers: 0";
			progressBar.Location = new System.Drawing.Point(5, 61);
			progressBar.Name = "progressBar";
			progressBar.Size = new System.Drawing.Size(100, 23);
			progressBar.TabIndex = 22;
			scheduleLabel.AutoSize = true;
			scheduleLabel.ForeColor = System.Drawing.Color.Red;
			scheduleLabel.Location = new System.Drawing.Point(6, 95);
			scheduleLabel.Name = "scheduleLabel";
			scheduleLabel.Size = new System.Drawing.Size(99, 13);
			scheduleLabel.TabIndex = 8;
			scheduleLabel.Text = "Schedule recording";
			scheduleLabel.Visible = false;
			timerButton.Location = new System.Drawing.Point(111, 61);
			timerButton.Name = "timerButton";
			timerButton.Size = new System.Drawing.Size(103, 23);
			timerButton.TabIndex = 21;
			timerButton.Text = "Schedule";
			timerButton.UseVisualStyleBackColor = true;
			timerButton.Click += new System.EventHandler(TimerButton_Click);
			openFolderButton.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			openFolderButton.Location = new System.Drawing.Point(111, 32);
			openFolderButton.Name = "openFolderButton";
			openFolderButton.Size = new System.Drawing.Size(103, 23);
			openFolderButton.TabIndex = 20;
			openFolderButton.Text = "Open folder";
			openFolderButton.UseVisualStyleBackColor = true;
			openFolderButton.Click += new System.EventHandler(OpenFolderButton_Click);
			sheduleTimer.Enabled = true;
			sheduleTimer.Interval = 1000;
			sheduleTimer.Tick += new System.EventHandler(SheduleTimer_Tick);
			configureButton.Location = new System.Drawing.Point(111, 90);
			configureButton.Name = "configureButton";
			configureButton.Size = new System.Drawing.Size(103, 23);
			configureButton.TabIndex = 24;
			configureButton.Text = "Configure";
			configureButton.UseVisualStyleBackColor = true;
			configureButton.Click += new System.EventHandler(ConfigureButton_Click);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(panel1);
			base.Name = "RecordingPanel";
			base.Size = new System.Drawing.Size(217, 146);
			panel1.ResumeLayout(performLayout: false);
			panel1.PerformLayout();
			ResumeLayout(performLayout: false);
		}
	}
}

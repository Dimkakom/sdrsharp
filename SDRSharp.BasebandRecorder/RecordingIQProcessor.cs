using SDRSharp.Radio;

namespace SDRSharp.BasebandRecorder
{
	public class RecordingIQProcessor : IIQProcessor, IStreamProcessor, IBaseProcessor
	{
		public unsafe delegate void IQReadyDelegate(Complex* buffer, int length);

		private volatile bool _enabled;

		private double _sampleRate;

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				_sampleRate = value;
			}
		}

		public event IQReadyDelegate IQReady;

		public unsafe void Process(Complex* buffer, int length)
		{
			IQReadyDelegate iQReady = this.IQReady;
			if (iQReady != null)
			{
				this.IQReady(buffer, length);
			}
		}
	}
}

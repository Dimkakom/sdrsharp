using SDRSharp.Radio;
using System;
using System.Threading;

namespace SDRSharp.BasebandRecorder
{
	public class Recorder : IDisposable
	{
		private const int BlockSizeToRecord = 131072;

		private ComplexFifoStream _iqBuffer;

		private UnsafeBuffer _buffer;

		private unsafe Complex* _bufferPtrComplex;

		private unsafe float* _bufferPtrFloat;

		private bool _diskWriterRunning;

		private string _fileName;

		private double _sampleRate;

		private string _infoName;

		private WavSampleFormat _wavSampleFormat;

		private WavWriter _wavWriter;

		private Thread _diskWriter;

		private readonly RecordingIQProcessor _iqProcessor;

		private int _lostBuffers;

		private int _bufferUsage;

		private WaveFileFormat _fileFormat;

		public bool IsRecording => _diskWriterRunning;

		public bool IsStreamFull
		{
			get
			{
				if (_wavWriter != null)
				{
					return _wavWriter.IsStreamFull;
				}
				return false;
			}
		}

		public long BytesWritten
		{
			get
			{
				if (_wavWriter != null)
				{
					return _wavWriter.Length;
				}
				return 0L;
			}
		}

		public WavSampleFormat SampleFormat
		{
			get
			{
				return _wavSampleFormat;
			}
			set
			{
				if (_diskWriterRunning)
				{
					throw new ArgumentException("Format cannot be set while recording");
				}
				_wavSampleFormat = value;
			}
		}

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				if (_diskWriterRunning)
				{
					throw new ArgumentException("SampleRate cannot be set while recording");
				}
				_sampleRate = value;
			}
		}

		public string FileName
		{
			get
			{
				return _fileName;
			}
			set
			{
				if (_diskWriterRunning)
				{
					throw new ArgumentException("FileName cannot be set while recording");
				}
				_fileName = value;
			}
		}

		public WaveFileFormat FileFormat
		{
			get
			{
				return _fileFormat;
			}
			set
			{
				if (_diskWriterRunning)
				{
					throw new ArgumentException("FileName cannot be set while recording");
				}
				_fileFormat = value;
			}
		}

		public int BufferUsage => _bufferUsage;

		public int LostBuffers => _lostBuffers;

		public unsafe Recorder(RecordingIQProcessor iqProcessor)
		{
			_iqProcessor = iqProcessor;
			_iqProcessor.IQReady += IQSamplesIn;
			_iqProcessor.Enabled = false;
			_iqBuffer = new ComplexFifoStream(BlockMode.None);
		}

		~Recorder()
		{
			Dispose();
		}

		public void Dispose()
		{
			FreeBuffers();
		}

		private unsafe void IQSamplesIn(Complex* buffer, int length)
		{
			double num = Math.Max(_sampleRate, 262144.0);
			if ((double)_iqBuffer.Length < num)
			{
				_iqBuffer.Write(buffer, length);
			}
			else
			{
				_lostBuffers++;
			}
			_bufferUsage = (int)((double)_iqBuffer.Length / num * 100.0);
		}

		private unsafe void DiskWriterThread()
		{
			while (_diskWriterRunning && !_wavWriter.IsStreamFull)
			{
				if (_iqBuffer == null || _iqBuffer.Length < 131072)
				{
					Thread.Sleep(10);
					continue;
				}
				if (_buffer == null || _buffer.Length != 131072)
				{
					if (_buffer != null)
					{
						_buffer.Dispose();
						_buffer = null;
						_bufferPtrComplex = null;
					}
					_buffer = UnsafeBuffer.Create(131072, sizeof(Complex));
					_bufferPtrComplex = (Complex*)(void*)_buffer;
					_bufferPtrFloat = (float*)(void*)_buffer;
				}
				_iqBuffer.Read(_bufferPtrComplex, 131072);
				_wavWriter.Write(_bufferPtrFloat, 131072);
			}
			while (_iqBuffer.Length > 0 && !_wavWriter.IsStreamFull)
			{
				int num = Math.Min(131072, _iqBuffer.Length);
				_iqBuffer.Read(_bufferPtrComplex, num);
				_wavWriter.Write(_bufferPtrFloat, num);
			}
			_diskWriterRunning = false;
		}

		private void Flush()
		{
			if (_wavWriter != null)
			{
				_wavWriter.Close();
			}
			_iqBuffer.Flush();
		}

		private unsafe void FreeBuffers()
		{
			if (_iqBuffer != null)
			{
				_iqBuffer.Close();
				_iqBuffer.Dispose();
				_iqBuffer = null;
			}
			if (_buffer != null)
			{
				_buffer.Dispose();
				_buffer = null;
				_bufferPtrComplex = null;
				_bufferPtrFloat = null;
			}
		}

		public void StartRecording()
		{
			if (_diskWriter == null)
			{
				_wavWriter = new WavWriter(_fileName, _wavSampleFormat, _fileFormat, (uint)_sampleRate);
				_wavWriter.Open();
				_diskWriter = new Thread(DiskWriterThread);
				_diskWriterRunning = true;
				_diskWriter.Start();
				_iqProcessor.Enabled = true;
				_lostBuffers = 0;
			}
		}

		public void StopRecording()
		{
			_iqProcessor.Enabled = false;
			_diskWriterRunning = false;
			if (_diskWriter != null)
			{
				_diskWriter.Join();
			}
			Flush();
			_diskWriter = null;
			_wavWriter = null;
			_bufferUsage = 0;
		}
	}
}

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.CTCSSDecoder
{
	public class AuxWindow : Form
	{
		private bool _setTone;

		private IContainer components;

		private Label toneDetectedLabel;

		private Label lastToneLabel;

		private Button setToneButton;

		private Label toneLabel;

		private CheckBox squelchEnableCheckBox;

		public string LastTone
		{
			set
			{
				lastToneLabel.Text = value;
			}
		}

		public string Tone
		{
			set
			{
				toneLabel.Text = value;
			}
		}

		public bool ToneDetected
		{
			set
			{
				toneDetectedLabel.Visible = value;
			}
		}

		public bool SquelchEnable
		{
			get
			{
				return squelchEnableCheckBox.Checked;
			}
			set
			{
				squelchEnableCheckBox.Checked = value;
			}
		}

		public bool SetThisTone
		{
			get
			{
				bool setTone = _setTone;
				_setTone = false;
				return setTone;
			}
		}

		public AuxWindow()
		{
			InitializeComponent();
		}

		private void setToneButton_Click(object sender, EventArgs e)
		{
			_setTone = true;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			toneDetectedLabel = new System.Windows.Forms.Label();
			lastToneLabel = new System.Windows.Forms.Label();
			setToneButton = new System.Windows.Forms.Button();
			toneLabel = new System.Windows.Forms.Label();
			squelchEnableCheckBox = new System.Windows.Forms.CheckBox();
			SuspendLayout();
			toneDetectedLabel.AutoSize = true;
			toneDetectedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			toneDetectedLabel.Location = new System.Drawing.Point(2, 8);
			toneDetectedLabel.Name = "toneDetectedLabel";
			toneDetectedLabel.Size = new System.Drawing.Size(109, 16);
			toneDetectedLabel.TabIndex = 0;
			toneDetectedLabel.Text = "Tone detected";
			toneDetectedLabel.Visible = false;
			lastToneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lastToneLabel.Location = new System.Drawing.Point(12, 24);
			lastToneLabel.Name = "lastToneLabel";
			lastToneLabel.Size = new System.Drawing.Size(163, 39);
			lastToneLabel.TabIndex = 1;
			lastToneLabel.Text = "000.0 Hz";
			lastToneLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			setToneButton.Location = new System.Drawing.Point(83, 66);
			setToneButton.Name = "setToneButton";
			setToneButton.Size = new System.Drawing.Size(84, 23);
			setToneButton.TabIndex = 2;
			setToneButton.Text = "Set this tone";
			setToneButton.UseVisualStyleBackColor = true;
			setToneButton.Click += new System.EventHandler(setToneButton_Click);
			toneLabel.Location = new System.Drawing.Point(117, 10);
			toneLabel.Name = "toneLabel";
			toneLabel.Size = new System.Drawing.Size(50, 13);
			toneLabel.TabIndex = 3;
			toneLabel.Text = "000,0 Hz";
			toneLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			squelchEnableCheckBox.AutoSize = true;
			squelchEnableCheckBox.Location = new System.Drawing.Point(12, 70);
			squelchEnableCheckBox.Name = "squelchEnableCheckBox";
			squelchEnableCheckBox.Size = new System.Drawing.Size(65, 17);
			squelchEnableCheckBox.TabIndex = 4;
			squelchEnableCheckBox.Text = "Squelch";
			squelchEnableCheckBox.UseVisualStyleBackColor = true;
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(176, 91);
			base.ControlBox = false;
			base.Controls.Add(squelchEnableCheckBox);
			base.Controls.Add(toneLabel);
			base.Controls.Add(setToneButton);
			base.Controls.Add(lastToneLabel);
			base.Controls.Add(toneDetectedLabel);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "AuxWindow";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			Text = "CTCSS tone";
			base.TopMost = true;
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

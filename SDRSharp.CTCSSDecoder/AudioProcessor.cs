using SDRSharp.Radio;

namespace SDRSharp.CTCSSDecoder
{
	public class AudioProcessor : IRealProcessor, IStreamProcessor, IBaseProcessor
	{
		private double _sampleRate;

		private bool _enabled;

		private float _squelchLevel;

		private Decoder _decoder;

		private bool _needNewDecoder;

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				_sampleRate = value;
				_needNewDecoder = true;
			}
		}

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public float SquelchLevel
		{
			set
			{
				_squelchLevel = value;
				_needNewDecoder = true;
			}
		}

		public float DecodeTone
		{
			get
			{
				if (_decoder != null)
				{
					return _decoder.DetectedTone;
				}
				return 0f;
			}
		}

		public bool ToneIsDetected
		{
			get
			{
				if (_decoder != null)
				{
					return _decoder.ToneIsDetected;
				}
				return false;
			}
		}

		public bool IsSquelchOpen
		{
			get
			{
				if (_decoder != null)
				{
					return _decoder.IsSquelchOpen;
				}
				return true;
			}
		}

		public unsafe void Process(float* buffer, int length)
		{
			if (_needNewDecoder || _decoder == null)
			{
				_decoder = new Decoder(_sampleRate, _squelchLevel);
				_needNewDecoder = false;
			}
			_decoder.Process(buffer, length);
		}
	}
}

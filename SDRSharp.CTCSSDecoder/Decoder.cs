using SDRSharp.Radio;
using System;

namespace SDRSharp.CTCSSDecoder
{
	public class Decoder
	{
		private const int SubToneFilterOrder = 1000;

		private const int SubToneLow = 60;

		private const int SubToneHigh = 260;

		private FirFilter _subToneFilter;

		private FirFilter _squelchToneFilter;

		private unsafe float* _filteredBufferPtr;

		private UnsafeBuffer _filteredBuffer;

		private float _samplerate;

		private float _squelchTone;

		private bool _isSquelchOpen;

		private float _tone;

		private bool _toneIsDetected;

		public bool IsSquelchOpen => _isSquelchOpen;

		public float DetectedTone => _tone;

		public bool ToneIsDetected => _toneIsDetected;

		public Decoder(double sampleRate, float squelchTone)
		{
			if (_squelchToneFilter != null)
			{
				_squelchToneFilter.Dispose();
			}
			_squelchToneFilter = new FirFilter(FilterBuilder.MakeBandPassKernel(sampleRate, 1000, squelchTone - 2f, squelchTone + 2f, WindowType.BlackmanHarris4), 1);
			if (_subToneFilter != null)
			{
				_subToneFilter.Dispose();
			}
			_subToneFilter = new FirFilter(FilterBuilder.MakeBandPassKernel(sampleRate, 1000, 60.0, 260.0, WindowType.BlackmanHarris4), 1);
			_squelchTone = squelchTone;
			_samplerate = (float)sampleRate;
		}

		public unsafe void Process(float* buffer, int length)
		{
			if (_filteredBuffer == null || _filteredBuffer.Length != length)
			{
				_filteredBuffer = UnsafeBuffer.Create(length, 4);
				_filteredBufferPtr = (float*)(void*)_filteredBuffer;
			}
			Utils.Memcpy(_filteredBufferPtr, buffer, length * 4);
			DetectSubTone(_filteredBufferPtr, length);
			if (_squelchTone > 0f)
			{
				ProcessSquelch(_filteredBufferPtr, length);
				if (!_isSquelchOpen)
				{
					MuteBuffer(buffer, length);
				}
			}
		}

		private unsafe void DetectSubTone(float* filteredBuffer, int length)
		{
			_subToneFilter.Process(filteredBuffer, length);
			ToneDetect(filteredBuffer, length, out _tone, out _toneIsDetected);
		}

		private unsafe void ProcessSquelch(float* filteredBuffer, int length)
		{
			float tone = 0f;
			bool toneIsDetected = false;
			_squelchToneFilter.Process(filteredBuffer, length);
			ToneDetect(filteredBuffer, length, out tone, out toneIsDetected);
			_isSquelchOpen = (Math.Abs(tone - _squelchTone) < 2f && toneIsDetected);
		}

		private unsafe void ToneDetect(float* buffer, int length, out float tone, out bool toneIsDetected)
		{
			int num = 0;
			int num2 = 0;
			int num3 = -1;
			int num4 = 0;
			int num5 = 0;
			int num6 = 3;
			float num7 = 0f;
			float num8 = 0f;
			tone = 0f;
			toneIsDetected = false;
			for (int i = 0; i < length - 1; i++)
			{
				num7 = Math.Max(num7, Math.Abs(_filteredBufferPtr[i]));
				num8 += Math.Abs(buffer[i]);
				if (!(buffer[i] > 0f) || !(buffer[i + 1] <= 0f))
				{
					continue;
				}
				int num9 = i;
				if (num3 > -1)
				{
					num9 -= num;
					num2 += num9;
					if (num3 > 0)
					{
						num4 += Math.Abs(num5 - num9);
					}
					num5 = num9;
				}
				num = i;
				num3++;
			}
			num8 /= (float)(length - 1);
			if (num3 >= num6)
			{
				num2 /= num3;
				num4 /= num3 - 1;
				if (num2 > 0 && num8 > 0f)
				{
					tone = _samplerate / (float)num2;
					toneIsDetected = ((double)num4 < (double)num2 * 0.2 && num7 / num8 < 2f);
				}
			}
		}

		private unsafe void MuteBuffer(float* buffer, int lengthOfStereo)
		{
			for (int i = 0; i < lengthOfStereo; i++)
			{
				buffer[i] = 0f;
			}
		}
	}
}

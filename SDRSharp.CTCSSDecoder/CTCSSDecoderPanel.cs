using SDRSharp.Common;
using SDRSharp.PanView;
using SDRSharp.Radio;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.CTCSSDecoder
{
	public class CTCSSDecoderPanel : UserControl
	{
		private AudioProcessor _audioProcessor;

		private ISharpControl _control;

		private AuxWindow _auxWindow;

		private readonly float[] _ctcssToneTable = new float[55]
		{
			67f,
			69.3f,
			71.9f,
			74.4f,
			77f,
			79.7f,
			82.5f,
			85.4f,
			88.5f,
			91.5f,
			94.8f,
			97.4f,
			100f,
			103.5f,
			107.2f,
			110.9f,
			114.8f,
			118.8f,
			123f,
			127.3f,
			131.8f,
			136.5f,
			141.3f,
			146.2f,
			150f,
			151.4f,
			156.7f,
			159.8f,
			162.2f,
			165.5f,
			167.9f,
			171.3f,
			173.8f,
			177.3f,
			179.9f,
			183.5f,
			186.2f,
			189.9f,
			192.8f,
			196.6f,
			199.5f,
			203.5f,
			206.5f,
			210.7f,
			213.8f,
			218.1f,
			221.3f,
			225.7f,
			229.1f,
			233.6f,
			237.1f,
			241.8f,
			245.5f,
			250.3f,
			254.1f
		};

		private int _count;

		private float _oldTone;

		private float _lastTone;

		private string _toneInText;

		private IContainer components;

		private CheckBox squelchEnableCheckBox;

		private NumericUpDown squelchLevelNumericUpDown;

		private GroupBox audioFilterGroupBox;

		private Label label4;

		private Label label2;

		private Label subToneLabel;

		private Timer displayUpdateTimer;

		private GroupBox groupBox1;

		private CheckBox detectEnableCheckBox;

		private Label lastToneLabel;

		private Label detectedLabel;

		private CheckBox auxWindowCheckBox;

		private Button setToneButton;

		private CheckBox showOnSpectrumCheckBox;

		private Label muteLabel;

		public CTCSSDecoderPanel(AudioProcessor audioProcessor, ISharpControl control)
		{
			InitializeComponent();
			_audioProcessor = audioProcessor;
			_control = control;
			_auxWindow = new AuxWindow();
			squelchLevelNumericUpDown.Value = (decimal)Utils.GetDoubleSetting("CTCSSSquelchTone", 110.9);
			detectEnableCheckBox.Checked = Utils.GetBooleanSetting("CTCSSDetectEnable");
			squelchEnableCheckBox.Checked = Utils.GetBooleanSetting("CTCSSSquelchEnable");
			auxWindowCheckBox.Checked = Utils.GetBooleanSetting("CTCSSAuxWindowEnable");
			showOnSpectrumCheckBox.Checked = Utils.GetBooleanSetting("CTCSSOnSpectrum");
			ConfigureOutToPanView();
			DisableControls();
			SetGain();
		}

		public void StoreSettings()
		{
			Utils.SaveSetting("CTCSSSquelchTone", squelchLevelNumericUpDown.Value);
			Utils.SaveSetting("CTCSSSquelchEnable", squelchEnableCheckBox.Checked);
			Utils.SaveSetting("CTCSSDetectEnable", detectEnableCheckBox.Checked);
			Utils.SaveSetting("CTCSSAuxWindowEnable", auxWindowCheckBox.Checked);
			Utils.SaveSetting("CTCSSOnSpectrum", showOnSpectrumCheckBox.Checked);
		}

		public void SetGain()
		{
			_audioProcessor.Enabled = ((squelchEnableCheckBox.Checked || detectEnableCheckBox.Checked) && _control.DetectorType == DetectorType.NFM);
			if (!squelchEnableCheckBox.Checked)
			{
				_audioProcessor.SquelchLevel = 0f;
			}
			else
			{
				_audioProcessor.SquelchLevel = (float)squelchLevelNumericUpDown.Value;
			}
			ConfigureOutToPanView();
		}

		public void DisableControls()
		{
			detectEnableCheckBox.Enabled = false;
			squelchEnableCheckBox.Enabled = false;
			squelchLevelNumericUpDown.Enabled = false;
			auxWindowCheckBox.Enabled = false;
			_auxWindow.Visible = false;
			setToneButton.Enabled = false;
			showOnSpectrumCheckBox.Enabled = false;
		}

		public void EnableControls()
		{
			detectEnableCheckBox.Enabled = true;
			squelchEnableCheckBox.Enabled = true;
			squelchLevelNumericUpDown.Enabled = true;
			auxWindowCheckBox.Enabled = true;
			auxWindowCheckBox_CheckedChanged(null, null);
			setToneButton.Enabled = true;
			showOnSpectrumCheckBox.Enabled = true;
		}

		public void Reset()
		{
			_toneInText = "---.- Hz";
			lastToneLabel.Text = _toneInText;
			_auxWindow.LastTone = _toneInText;
		}

		private void detectEnableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			SetGain();
			_auxWindow.Visible = (detectEnableCheckBox.Checked && auxWindowCheckBox.Checked);
		}

		private void SquelchEnableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			SetGain();
			_auxWindow.SquelchEnable = squelchEnableCheckBox.Checked;
		}

		private void SquelchLevelNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			SetGain();
		}

		private void DisplayUpdateTimer_Tick(object sender, EventArgs e)
		{
			string text = $"{_audioProcessor.DecodeTone:F1} Hz";
			subToneLabel.Text = text;
			_auxWindow.Tone = text;
			muteLabel.Visible = (!_audioProcessor.IsSquelchOpen && squelchEnableCheckBox.Checked);
			float num = ConvertTone(_audioProcessor.DecodeTone);
			if (num == _oldTone && _audioProcessor.ToneIsDetected)
			{
				_count++;
			}
			else
			{
				_count = 0;
			}
			_oldTone = num;
			if (_count > 1)
			{
				_toneInText = $"{num:f1} Hz";
				lastToneLabel.Text = _toneInText;
				_auxWindow.LastTone = _toneInText;
				detectedLabel.Visible = true;
				_auxWindow.ToneDetected = true;
				_lastTone = num;
			}
			else
			{
				detectedLabel.Visible = false;
				_auxWindow.ToneDetected = false;
			}
			if (_auxWindow.SetThisTone)
			{
				setToneButton_Click(null, null);
			}
			if (_auxWindow.SquelchEnable != squelchEnableCheckBox.Checked)
			{
				squelchEnableCheckBox.Checked = _auxWindow.SquelchEnable;
			}
		}

		private float ConvertTone(float tone)
		{
			if (tone < 65f || tone > 256f)
			{
				return tone;
			}
			float num = 300f;
			for (int i = 0; i < _ctcssToneTable.Length; i++)
			{
				float num2 = Math.Abs(tone - _ctcssToneTable[i]);
				if (num2 > num)
				{
					return _ctcssToneTable[i - 1];
				}
				num = num2;
			}
			return _ctcssToneTable[_ctcssToneTable.Length - 1];
		}

		private void auxWindowCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_auxWindow.Visible = (auxWindowCheckBox.Checked && detectEnableCheckBox.Checked);
		}

		private void setToneButton_Click(object sender, EventArgs e)
		{
			if (!(_lastTone < (float)squelchLevelNumericUpDown.Minimum) && !(_lastTone > (float)squelchLevelNumericUpDown.Maximum))
			{
				squelchLevelNumericUpDown.Value = (decimal)_lastTone;
				SquelchLevelNumericUpDown_ValueChanged(null, null);
				squelchEnableCheckBox.Checked = true;
			}
		}

		private void CustomPaint(object sender, CustomPaintEventArgs e)
		{
			SpectrumAnalyzer spectrumAnalyzer = (SpectrumAnalyzer)sender;
			using (SolidBrush brush = new SolidBrush(Color.FromArgb(100, Color.Black)))
			{
				using (SolidBrush solidBrush = new SolidBrush(Color.FromArgb(200, Color.White)))
				{
					using (SolidBrush solidBrush2 = new SolidBrush(Color.FromArgb(200, Color.LightGreen)))
					{
						using (StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic))
						{
							using (Font font = new Font("Arial", 10f))
							{
								stringFormat.Alignment = StringAlignment.Near;
								Rectangle rect = default(Rectangle);
								Point p = default(Point);
								string text = string.Format("CTCSS {0,6:F1} Hz", _audioProcessor.DecodeTone);
								if (squelchEnableCheckBox.Checked)
								{
									text += "\n";
									text += string.Format("Squelch {0,6:F1} Hz", squelchLevelNumericUpDown.Value);
									if (!_audioProcessor.IsSquelchOpen)
									{
										text += " Mute";
									}
								}
								SizeF sizeF = e.Graphics.MeasureString(text, font);
								rect.X = 50;
								rect.Y = (int)((float)(spectrumAnalyzer.Height - 30) - sizeF.Height);
								rect.Width = (int)sizeF.Width;
								rect.Height = (int)sizeF.Height;
								p.X = rect.X;
								p.Y = rect.Y;
								e.Graphics.FillRectangle(brush, rect);
								e.Graphics.DrawString(text, font, solidBrush, p, stringFormat);
								text = _toneInText;
								if (!(text == "---.- Hz"))
								{
									text = ((_control.Frequency <= spectrumAnalyzer.CenterFrequency) ? ("< " + text) : (text + " >"));
									sizeF = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
									rect.X = (int)spectrumAnalyzer.FrequencyToPoint(_control.Frequency);
									rect.Y -= (int)sizeF.Height;
									rect.Width = (int)sizeF.Width;
									rect.Height = (int)sizeF.Height;
									if (_control.Frequency > spectrumAnalyzer.CenterFrequency)
									{
										rect.X = (int)((float)rect.X - sizeF.Width);
									}
									p.X = rect.X;
									p.Y = rect.Y;
									SolidBrush brush2 = solidBrush;
									if (_audioProcessor.ToneIsDetected)
									{
										brush2 = solidBrush2;
									}
									e.Graphics.FillRectangle(brush, rect);
									e.Graphics.DrawString(text, font, brush2, p, stringFormat);
								}
							}
						}
					}
				}
			}
		}

		private void ConfigureOutToPanView()
		{
			_control.SpectrumAnalyzerCustomPaint -= CustomPaint;
			if (showOnSpectrumCheckBox.Checked && _audioProcessor.Enabled)
			{
				_control.SpectrumAnalyzerCustomPaint += CustomPaint;
			}
		}

		private void showOnSpectrumCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			ConfigureOutToPanView();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			squelchEnableCheckBox = new System.Windows.Forms.CheckBox();
			squelchLevelNumericUpDown = new System.Windows.Forms.NumericUpDown();
			audioFilterGroupBox = new System.Windows.Forms.GroupBox();
			muteLabel = new System.Windows.Forms.Label();
			label4 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			subToneLabel = new System.Windows.Forms.Label();
			displayUpdateTimer = new System.Windows.Forms.Timer(components);
			groupBox1 = new System.Windows.Forms.GroupBox();
			showOnSpectrumCheckBox = new System.Windows.Forms.CheckBox();
			auxWindowCheckBox = new System.Windows.Forms.CheckBox();
			setToneButton = new System.Windows.Forms.Button();
			detectedLabel = new System.Windows.Forms.Label();
			lastToneLabel = new System.Windows.Forms.Label();
			detectEnableCheckBox = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)squelchLevelNumericUpDown).BeginInit();
			audioFilterGroupBox.SuspendLayout();
			groupBox1.SuspendLayout();
			SuspendLayout();
			squelchEnableCheckBox.AutoSize = true;
			squelchEnableCheckBox.Location = new System.Drawing.Point(8, 0);
			squelchEnableCheckBox.Name = "squelchEnableCheckBox";
			squelchEnableCheckBox.Size = new System.Drawing.Size(65, 17);
			squelchEnableCheckBox.TabIndex = 7;
			squelchEnableCheckBox.Text = "Squelch";
			squelchEnableCheckBox.UseVisualStyleBackColor = true;
			squelchEnableCheckBox.CheckedChanged += new System.EventHandler(SquelchEnableCheckBox_CheckedChanged);
			squelchLevelNumericUpDown.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			squelchLevelNumericUpDown.DecimalPlaces = 1;
			squelchLevelNumericUpDown.Location = new System.Drawing.Point(96, 18);
			squelchLevelNumericUpDown.Maximum = new decimal(new int[4]
			{
				260,
				0,
				0,
				0
			});
			squelchLevelNumericUpDown.Minimum = new decimal(new int[4]
			{
				60,
				0,
				0,
				0
			});
			squelchLevelNumericUpDown.Name = "squelchLevelNumericUpDown";
			squelchLevelNumericUpDown.Size = new System.Drawing.Size(47, 20);
			squelchLevelNumericUpDown.TabIndex = 8;
			squelchLevelNumericUpDown.Value = new decimal(new int[4]
			{
				110,
				0,
				0,
				0
			});
			squelchLevelNumericUpDown.ValueChanged += new System.EventHandler(SquelchLevelNumericUpDown_ValueChanged);
			audioFilterGroupBox.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			audioFilterGroupBox.Controls.Add(muteLabel);
			audioFilterGroupBox.Controls.Add(label4);
			audioFilterGroupBox.Controls.Add(label2);
			audioFilterGroupBox.Controls.Add(squelchLevelNumericUpDown);
			audioFilterGroupBox.Controls.Add(squelchEnableCheckBox);
			audioFilterGroupBox.Location = new System.Drawing.Point(3, 116);
			audioFilterGroupBox.Name = "audioFilterGroupBox";
			audioFilterGroupBox.Size = new System.Drawing.Size(210, 49);
			audioFilterGroupBox.TabIndex = 0;
			audioFilterGroupBox.TabStop = false;
			muteLabel.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			muteLabel.AutoSize = true;
			muteLabel.ForeColor = System.Drawing.Color.Red;
			muteLabel.Location = new System.Drawing.Point(173, 20);
			muteLabel.Name = "muteLabel";
			muteLabel.Size = new System.Drawing.Size(31, 13);
			muteLabel.TabIndex = 11;
			muteLabel.Text = "Mute";
			muteLabel.Visible = false;
			label4.AutoSize = true;
			label4.Location = new System.Drawing.Point(44, 20);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(20, 13);
			label4.TabIndex = 10;
			label4.Text = "Hz";
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(6, 20);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(32, 13);
			label2.TabIndex = 10;
			label2.Text = "Tone";
			subToneLabel.Location = new System.Drawing.Point(6, 55);
			subToneLabel.Name = "subToneLabel";
			subToneLabel.Size = new System.Drawing.Size(50, 13);
			subToneLabel.TabIndex = 11;
			subToneLabel.Text = "000,0 Hz";
			subToneLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			displayUpdateTimer.Enabled = true;
			displayUpdateTimer.Tick += new System.EventHandler(DisplayUpdateTimer_Tick);
			groupBox1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			groupBox1.Controls.Add(showOnSpectrumCheckBox);
			groupBox1.Controls.Add(auxWindowCheckBox);
			groupBox1.Controls.Add(setToneButton);
			groupBox1.Controls.Add(detectedLabel);
			groupBox1.Controls.Add(subToneLabel);
			groupBox1.Controls.Add(lastToneLabel);
			groupBox1.Controls.Add(detectEnableCheckBox);
			groupBox1.Location = new System.Drawing.Point(3, 3);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new System.Drawing.Size(210, 107);
			groupBox1.TabIndex = 12;
			groupBox1.TabStop = false;
			showOnSpectrumCheckBox.AutoSize = true;
			showOnSpectrumCheckBox.Location = new System.Drawing.Point(6, 19);
			showOnSpectrumCheckBox.Name = "showOnSpectrumCheckBox";
			showOnSpectrumCheckBox.Size = new System.Drawing.Size(114, 17);
			showOnSpectrumCheckBox.TabIndex = 17;
			showOnSpectrumCheckBox.Text = "Show on spectrum";
			showOnSpectrumCheckBox.UseVisualStyleBackColor = true;
			showOnSpectrumCheckBox.CheckedChanged += new System.EventHandler(showOnSpectrumCheckBox_CheckedChanged);
			auxWindowCheckBox.AutoSize = true;
			auxWindowCheckBox.Location = new System.Drawing.Point(126, 19);
			auxWindowCheckBox.Name = "auxWindowCheckBox";
			auxWindowCheckBox.Size = new System.Drawing.Size(83, 17);
			auxWindowCheckBox.TabIndex = 16;
			auxWindowCheckBox.Text = "Aux window";
			auxWindowCheckBox.UseVisualStyleBackColor = true;
			auxWindowCheckBox.CheckedChanged += new System.EventHandler(auxWindowCheckBox_CheckedChanged);
			setToneButton.Location = new System.Drawing.Point(6, 78);
			setToneButton.Name = "setToneButton";
			setToneButton.Size = new System.Drawing.Size(84, 23);
			setToneButton.TabIndex = 15;
			setToneButton.Text = "Set this tone";
			setToneButton.UseVisualStyleBackColor = true;
			setToneButton.Click += new System.EventHandler(setToneButton_Click);
			detectedLabel.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			detectedLabel.AutoSize = true;
			detectedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			detectedLabel.Location = new System.Drawing.Point(96, 81);
			detectedLabel.Name = "detectedLabel";
			detectedLabel.Size = new System.Drawing.Size(109, 16);
			detectedLabel.TabIndex = 13;
			detectedLabel.Text = "Tone detected";
			detectedLabel.Visible = false;
			lastToneLabel.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			lastToneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
			lastToneLabel.Location = new System.Drawing.Point(80, 43);
			lastToneLabel.Name = "lastToneLabel";
			lastToneLabel.Size = new System.Drawing.Size(124, 29);
			lastToneLabel.TabIndex = 12;
			lastToneLabel.Text = "000,0 Hz";
			lastToneLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			detectEnableCheckBox.AutoSize = true;
			detectEnableCheckBox.Location = new System.Drawing.Point(6, 0);
			detectEnableCheckBox.Name = "detectEnableCheckBox";
			detectEnableCheckBox.Size = new System.Drawing.Size(58, 17);
			detectEnableCheckBox.TabIndex = 13;
			detectEnableCheckBox.Text = "Detect";
			detectEnableCheckBox.UseVisualStyleBackColor = true;
			detectEnableCheckBox.CheckedChanged += new System.EventHandler(detectEnableCheckBox_CheckedChanged);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(groupBox1);
			base.Controls.Add(audioFilterGroupBox);
			base.Name = "CTCSSDecoderPanel";
			base.Size = new System.Drawing.Size(217, 191);
			((System.ComponentModel.ISupportInitialize)squelchLevelNumericUpDown).EndInit();
			audioFilterGroupBox.ResumeLayout(performLayout: false);
			audioFilterGroupBox.PerformLayout();
			groupBox1.ResumeLayout(performLayout: false);
			groupBox1.PerformLayout();
			ResumeLayout(performLayout: false);
		}
	}
}

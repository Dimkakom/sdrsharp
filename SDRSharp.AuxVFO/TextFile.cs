using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SDRSharp.AuxVFO
{
	internal class TextFile
	{
		public void Write(string line, string path)
		{
			using (StreamWriter streamWriter = new StreamWriter(path, append: true, Encoding.Default))
			{
				streamWriter.WriteLine(line);
			}
		}

		public List<string> Read(string path)
		{
			List<string> list = new List<string>();
			using (StreamReader streamReader = new StreamReader(path, Encoding.Default))
			{
				while (streamReader.Peek() >= 0)
				{
					list.Add(streamReader.ReadLine());
				}
				return list;
			}
		}
	}
}

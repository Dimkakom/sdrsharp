using SDRSharp.Radio;

namespace SDRSharp.AuxVFO
{
	public class AudioProcessor : IRealProcessor, IStreamProcessor, IBaseProcessor
	{
		public unsafe delegate void AudioReadyDelegate(float* buffer, int length);

		private volatile bool _enabled;

		private double _sampleRate;

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				_sampleRate = value;
			}
		}

		public event AudioReadyDelegate AudioReady;

		public unsafe void Process(float* buffer, int length)
		{
			if (this.AudioReady != null)
			{
				this.AudioReady(buffer, length);
			}
		}
	}
}

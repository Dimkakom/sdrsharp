using SDRSharp.Common;
using System.Windows.Forms;

namespace SDRSharp.AuxVFO
{
	public class AuxVFOPlugin : ISharpPlugin
	{
		private const string DefaultDisplayName = "Aux VFO";

		private ISharpControl _control;

		private VFOPanel _guiControl;

		public static int VFOPluginsCounter;

		public UserControl Gui => _guiControl;

		public string DisplayName => "Aux VFO-" + VFOPluginsCounter;

		public void Initialize(ISharpControl control)
		{
			VFOPluginsCounter++;
			_control = control;
			_guiControl = new VFOPanel(_control, VFOPluginsCounter);
		}

		public void Close()
		{
			if (_guiControl != null)
			{
				_guiControl.SaveSettings();
			}
		}
	}
}

using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SDRSharp.AuxVFO
{
	public class DialogConfigure : Form
	{
		private VFOSettings _vfoSettings;

		private VFOPanel _vfo;

		private int _samplerateOut;

		private IContainer components;

		private Button btnOk;

		private TabControl tabControl1;

		private TabPage recorderTabPage;

		private NumericUpDown continueRecordTimeNumericUpDown;

		private CheckBox dontWritePauseCheckBox;

		private CheckBox newFileTimeEnableCheckBox;

		private Timer displayTimer;

		private NumericUpDown NewFileTimeNumericUpDown;

		private FolderBrowserDialog writeFolderBrowserDialog;

		private Label label3;

		private TabPage logPage;

		private Button logFolderButton;

		private Label label5;

		private Label label6;

		private TextBox logFileRulesTextBox;

		private Label label10;

		private NumericUpDown logRefreshNumericUpDown;

		private Label label7;

		private Label label8;

		private TextBox logEntriesRulesTextBox;

		private Label label11;

		private TextBox logSeparatorTextBox;

		private CheckBox logUseSquelchCheckBox;

		private CheckBox logEventsCheckBox;

		private FolderBrowserDialog logFolderBrowserDialog;

		private CheckBox logEnableCheckBox;

		private Label LogEntryLabel;

		private Label LogFileLabel;

		private Button folderButton;

		private Label label2;

		private Label resultLabel;

		private Label label4;

		private TextBox folderTextBox;

		private Label label1;

		private ComboBox sampleRateComboBox;

		private ComboBox sampleFormatComboBox;

		private Label sampleFormatLbl;

		public DialogConfigure(VFOSettings vfoSettings, VFOPanel vfo)
		{
			_vfoSettings = vfoSettings;
			_vfo = vfo;
			InitializeComponent();
			AddSamplerate(_vfoSettings.OutputSamplerateArray);
			_samplerateOut = _vfoSettings.SamplerateOut;
			sampleRateComboBox.SelectedIndex = SamplerateSelect(_samplerateOut);
			sampleRateComboBox_SelectedIndexChanged(null, null);
			sampleFormatComboBox.SelectedIndex = _vfoSettings.SampleFormatSelectedIndex;
			dontWritePauseCheckBox.Checked = _vfoSettings.DontWritePause;
			continueRecordTimeNumericUpDown.Value = _vfoSettings.ContinueRecordTime / 1000;
			newFileTimeEnableCheckBox.Checked = _vfoSettings.NewFileTimeEnable;
			NewFileTimeNumericUpDown.Value = _vfoSettings.NewFileTime / 1000;
			writeFolderBrowserDialog.SelectedPath = _vfoSettings.WriteFolder;
			folderTextBox.Text = _vfoSettings.FileNameRules;
			folderTextBox_TextChanged(null, null);
			logFolderBrowserDialog.SelectedPath = _vfoSettings.LogWriteFolder;
			logFileRulesTextBox.Text = _vfoSettings.LogFileNameRules;
			logFileRulesTextBox_TextChanged(null, null);
			logEntriesRulesTextBox.Text = _vfoSettings.LogEntryRules;
			logEntriesRulesTextBox_TextChanged(null, null);
			logSeparatorTextBox.Text = _vfoSettings.LogSeparator;
			logEnableCheckBox.Checked = _vfoSettings.LogEnabled;
			logEventsCheckBox.Checked = _vfoSettings.LogOnlyEvents;
			logRefreshNumericUpDown.Value = _vfoSettings.LogUpdateTime;
			logUseSquelchCheckBox.Checked = _vfoSettings.LogUseSquelch;
		}

		private void AddSamplerate(string outputSamplerate)
		{
			sampleRateComboBox.Items.Add("without resample");
			sampleRateComboBox.Items.AddRange(outputSamplerate.Split(','));
		}

		private void sampleRateComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			int samplerateOut = 0;
			Match match = Regex.Match(sampleRateComboBox.Text, "([0-9\\.]+) ([k]?)Hz", RegexOptions.IgnoreCase);
			if (match.Success)
			{
				samplerateOut = (int)(double.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture) * 1000.0);
			}
			_samplerateOut = samplerateOut;
		}

		private int SamplerateSelect(int samplerate)
		{
			int num = 0;
			foreach (object item in sampleRateComboBox.Items)
			{
				Match match = Regex.Match(item.ToString(), "([0-9\\.]+) ([k]?)Hz", RegexOptions.IgnoreCase);
				if (match.Success && (int)(double.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture) * 1000.0) == samplerate)
				{
					return num;
				}
				num++;
			}
			return 0;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			_vfoSettings.DontWritePause = dontWritePauseCheckBox.Checked;
			_vfoSettings.ContinueRecordTime = (int)continueRecordTimeNumericUpDown.Value * 1000;
			_vfoSettings.NewFileTimeEnable = newFileTimeEnableCheckBox.Checked;
			_vfoSettings.NewFileTime = (int)NewFileTimeNumericUpDown.Value * 1000;
			_vfoSettings.SampleFormatSelectedIndex = sampleFormatComboBox.SelectedIndex;
			_vfoSettings.SamplerateOut = _samplerateOut;
			_vfoSettings.FileNameRules = folderTextBox.Text;
			_vfoSettings.LogFileNameRules = logFileRulesTextBox.Text;
			_vfoSettings.LogEntryRules = logEntriesRulesTextBox.Text;
			_vfoSettings.LogSeparator = logSeparatorTextBox.Text;
			_vfoSettings.LogEnabled = logEnableCheckBox.Checked;
			_vfoSettings.LogOnlyEvents = logEventsCheckBox.Checked;
			_vfoSettings.LogUpdateTime = (int)logRefreshNumericUpDown.Value;
			_vfoSettings.LogUseSquelch = logUseSquelchCheckBox.Checked;
			base.DialogResult = DialogResult.OK;
		}

		private void folderTextBox_TextChanged(object sender, EventArgs e)
		{
			resultLabel.Text = _vfo.ParseStringToPath(folderTextBox.Text, ".wav");
		}

		private void displayTimer_Tick(object sender, EventArgs e)
		{
			NewFileTimeNumericUpDown.Enabled = newFileTimeEnableCheckBox.Checked;
			continueRecordTimeNumericUpDown.Enabled = dontWritePauseCheckBox.Checked;
			sampleRateComboBox.Enabled = (sampleFormatComboBox.SelectedIndex > 2);
		}

		private void folderButton_Click(object sender, EventArgs e)
		{
			if (writeFolderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				_vfoSettings.WriteFolder = writeFolderBrowserDialog.SelectedPath;
			}
		}

		private void logFolderButton_Click(object sender, EventArgs e)
		{
			if (logFolderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				_vfoSettings.LogWriteFolder = logFolderBrowserDialog.SelectedPath;
			}
		}

		private void logFileRulesTextBox_TextChanged(object sender, EventArgs e)
		{
			LogFileLabel.Text = _vfo.ParseStringToPath(logFileRulesTextBox.Text, ".csv");
		}

		private void logEntriesRulesTextBox_TextChanged(object sender, EventArgs e)
		{
			LogEntryLabel.Text = _vfo.ParseStringToEntries(logEntriesRulesTextBox.Text);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			btnOk = new System.Windows.Forms.Button();
			tabControl1 = new System.Windows.Forms.TabControl();
			recorderTabPage = new System.Windows.Forms.TabPage();
			folderButton = new System.Windows.Forms.Button();
			label2 = new System.Windows.Forms.Label();
			resultLabel = new System.Windows.Forms.Label();
			label4 = new System.Windows.Forms.Label();
			folderTextBox = new System.Windows.Forms.TextBox();
			label1 = new System.Windows.Forms.Label();
			sampleRateComboBox = new System.Windows.Forms.ComboBox();
			sampleFormatComboBox = new System.Windows.Forms.ComboBox();
			sampleFormatLbl = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			NewFileTimeNumericUpDown = new System.Windows.Forms.NumericUpDown();
			newFileTimeEnableCheckBox = new System.Windows.Forms.CheckBox();
			continueRecordTimeNumericUpDown = new System.Windows.Forms.NumericUpDown();
			dontWritePauseCheckBox = new System.Windows.Forms.CheckBox();
			logPage = new System.Windows.Forms.TabPage();
			LogEntryLabel = new System.Windows.Forms.Label();
			LogFileLabel = new System.Windows.Forms.Label();
			logEnableCheckBox = new System.Windows.Forms.CheckBox();
			logEventsCheckBox = new System.Windows.Forms.CheckBox();
			label11 = new System.Windows.Forms.Label();
			logSeparatorTextBox = new System.Windows.Forms.TextBox();
			logUseSquelchCheckBox = new System.Windows.Forms.CheckBox();
			label10 = new System.Windows.Forms.Label();
			logRefreshNumericUpDown = new System.Windows.Forms.NumericUpDown();
			label7 = new System.Windows.Forms.Label();
			label8 = new System.Windows.Forms.Label();
			logEntriesRulesTextBox = new System.Windows.Forms.TextBox();
			logFolderButton = new System.Windows.Forms.Button();
			label5 = new System.Windows.Forms.Label();
			label6 = new System.Windows.Forms.Label();
			logFileRulesTextBox = new System.Windows.Forms.TextBox();
			displayTimer = new System.Windows.Forms.Timer(components);
			writeFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			logFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			tabControl1.SuspendLayout();
			recorderTabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)NewFileTimeNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)continueRecordTimeNumericUpDown).BeginInit();
			logPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)logRefreshNumericUpDown).BeginInit();
			SuspendLayout();
			btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			btnOk.Location = new System.Drawing.Point(434, 259);
			btnOk.Margin = new System.Windows.Forms.Padding(2);
			btnOk.Name = "btnOk";
			btnOk.Size = new System.Drawing.Size(56, 23);
			btnOk.TabIndex = 7;
			btnOk.Text = "O&K";
			btnOk.UseVisualStyleBackColor = true;
			btnOk.Click += new System.EventHandler(btnOk_Click);
			tabControl1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			tabControl1.Controls.Add(recorderTabPage);
			tabControl1.Controls.Add(logPage);
			tabControl1.Location = new System.Drawing.Point(12, 3);
			tabControl1.Name = "tabControl1";
			tabControl1.SelectedIndex = 0;
			tabControl1.Size = new System.Drawing.Size(478, 251);
			tabControl1.TabIndex = 26;
			recorderTabPage.BackColor = System.Drawing.SystemColors.Menu;
			recorderTabPage.Controls.Add(folderButton);
			recorderTabPage.Controls.Add(label2);
			recorderTabPage.Controls.Add(resultLabel);
			recorderTabPage.Controls.Add(label4);
			recorderTabPage.Controls.Add(folderTextBox);
			recorderTabPage.Controls.Add(label1);
			recorderTabPage.Controls.Add(sampleRateComboBox);
			recorderTabPage.Controls.Add(sampleFormatComboBox);
			recorderTabPage.Controls.Add(sampleFormatLbl);
			recorderTabPage.Controls.Add(label3);
			recorderTabPage.Controls.Add(NewFileTimeNumericUpDown);
			recorderTabPage.Controls.Add(newFileTimeEnableCheckBox);
			recorderTabPage.Controls.Add(continueRecordTimeNumericUpDown);
			recorderTabPage.Controls.Add(dontWritePauseCheckBox);
			recorderTabPage.Location = new System.Drawing.Point(4, 22);
			recorderTabPage.Name = "recorderTabPage";
			recorderTabPage.Padding = new System.Windows.Forms.Padding(3);
			recorderTabPage.Size = new System.Drawing.Size(470, 225);
			recorderTabPage.TabIndex = 1;
			recorderTabPage.Text = "Recorder options";
			folderButton.Location = new System.Drawing.Point(389, 147);
			folderButton.Name = "folderButton";
			folderButton.Size = new System.Drawing.Size(75, 23);
			folderButton.TabIndex = 56;
			folderButton.Text = "Folder";
			folderButton.UseVisualStyleBackColor = true;
			folderButton.Click += new System.EventHandler(folderButton_Click);
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(6, 157);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(259, 13);
			label2.TabIndex = 55;
			label2.Text = "You can use: date, time, frequency, \"any text\", +, \\, /";
			resultLabel.AutoSize = true;
			resultLabel.Location = new System.Drawing.Point(6, 206);
			resultLabel.Name = "resultLabel";
			resultLabel.Size = new System.Drawing.Size(13, 13);
			resultLabel.TabIndex = 54;
			resultLabel.Text = "_";
			label4.AutoSize = true;
			label4.Location = new System.Drawing.Point(6, 138);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(143, 13);
			label4.TabIndex = 53;
			label4.Text = "Rules for creating file names.";
			folderTextBox.Location = new System.Drawing.Point(9, 176);
			folderTextBox.Name = "folderTextBox";
			folderTextBox.Size = new System.Drawing.Size(458, 20);
			folderTextBox.TabIndex = 52;
			folderTextBox.Text = "/ date / name + \"_\" + frequency / time + \"_\" + name + \"_\" + frequency";
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(4, 107);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(60, 13);
			label1.TabIndex = 51;
			label1.Text = "Samplerate";
			sampleRateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			sampleRateComboBox.FormattingEnabled = true;
			sampleRateComboBox.Location = new System.Drawing.Point(103, 105);
			sampleRateComboBox.Name = "sampleRateComboBox";
			sampleRateComboBox.Size = new System.Drawing.Size(175, 21);
			sampleRateComboBox.TabIndex = 50;
			sampleFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			sampleFormatComboBox.DropDownWidth = 120;
			sampleFormatComboBox.FormattingEnabled = true;
			sampleFormatComboBox.Items.AddRange(new object[5]
			{
				"8 Bit PCM Stereo",
				"16 Bit PCM Stereo",
				"32 Bit IEEE Float",
				"8 Bit PCM Mono",
				"16 Bit PCM Mono"
			});
			sampleFormatComboBox.Location = new System.Drawing.Point(103, 78);
			sampleFormatComboBox.Name = "sampleFormatComboBox";
			sampleFormatComboBox.Size = new System.Drawing.Size(175, 21);
			sampleFormatComboBox.TabIndex = 48;
			sampleFormatLbl.AutoSize = true;
			sampleFormatLbl.Location = new System.Drawing.Point(4, 81);
			sampleFormatLbl.Name = "sampleFormatLbl";
			sampleFormatLbl.Size = new System.Drawing.Size(77, 13);
			sampleFormatLbl.TabIndex = 49;
			sampleFormatLbl.Text = "Sample Format";
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(6, 53);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(300, 13);
			label3.TabIndex = 28;
			label3.Text = "Continue recording after the squelch has been closed, second";
			NewFileTimeNumericUpDown.Location = new System.Drawing.Point(280, 5);
			NewFileTimeNumericUpDown.Name = "NewFileTimeNumericUpDown";
			NewFileTimeNumericUpDown.Size = new System.Drawing.Size(63, 20);
			NewFileTimeNumericUpDown.TabIndex = 27;
			newFileTimeEnableCheckBox.AutoSize = true;
			newFileTimeEnableCheckBox.Location = new System.Drawing.Point(6, 6);
			newFileTimeEnableCheckBox.Name = "newFileTimeEnableCheckBox";
			newFileTimeEnableCheckBox.Size = new System.Drawing.Size(268, 17);
			newFileTimeEnableCheckBox.TabIndex = 23;
			newFileTimeEnableCheckBox.Text = "New file after the squelch has been closed, second";
			newFileTimeEnableCheckBox.UseVisualStyleBackColor = true;
			continueRecordTimeNumericUpDown.Location = new System.Drawing.Point(330, 51);
			continueRecordTimeNumericUpDown.Name = "continueRecordTimeNumericUpDown";
			continueRecordTimeNumericUpDown.Size = new System.Drawing.Size(57, 20);
			continueRecordTimeNumericUpDown.TabIndex = 22;
			continueRecordTimeNumericUpDown.Value = new decimal(new int[4]
			{
				5,
				0,
				0,
				0
			});
			dontWritePauseCheckBox.AutoSize = true;
			dontWritePauseCheckBox.Location = new System.Drawing.Point(6, 29);
			dontWritePauseCheckBox.Name = "dontWritePauseCheckBox";
			dontWritePauseCheckBox.Size = new System.Drawing.Size(108, 17);
			dontWritePauseCheckBox.TabIndex = 20;
			dontWritePauseCheckBox.Text = "Don't write pause";
			dontWritePauseCheckBox.UseVisualStyleBackColor = true;
			logPage.BackColor = System.Drawing.SystemColors.Menu;
			logPage.Controls.Add(LogEntryLabel);
			logPage.Controls.Add(LogFileLabel);
			logPage.Controls.Add(logEnableCheckBox);
			logPage.Controls.Add(logEventsCheckBox);
			logPage.Controls.Add(label11);
			logPage.Controls.Add(logSeparatorTextBox);
			logPage.Controls.Add(logUseSquelchCheckBox);
			logPage.Controls.Add(label10);
			logPage.Controls.Add(logRefreshNumericUpDown);
			logPage.Controls.Add(label7);
			logPage.Controls.Add(label8);
			logPage.Controls.Add(logEntriesRulesTextBox);
			logPage.Controls.Add(logFolderButton);
			logPage.Controls.Add(label5);
			logPage.Controls.Add(label6);
			logPage.Controls.Add(logFileRulesTextBox);
			logPage.Location = new System.Drawing.Point(4, 22);
			logPage.Name = "logPage";
			logPage.Size = new System.Drawing.Size(470, 225);
			logPage.TabIndex = 2;
			logPage.Text = "Log";
			LogEntryLabel.AutoSize = true;
			LogEntryLabel.Location = new System.Drawing.Point(12, 198);
			LogEntryLabel.Name = "LogEntryLabel";
			LogEntryLabel.Size = new System.Drawing.Size(13, 13);
			LogEntryLabel.TabIndex = 63;
			LogEntryLabel.Text = "_";
			LogFileLabel.AutoSize = true;
			LogFileLabel.Location = new System.Drawing.Point(11, 102);
			LogFileLabel.Name = "LogFileLabel";
			LogFileLabel.Size = new System.Drawing.Size(13, 13);
			LogFileLabel.TabIndex = 62;
			LogFileLabel.Text = "_";
			logEnableCheckBox.AutoSize = true;
			logEnableCheckBox.Location = new System.Drawing.Point(8, 10);
			logEnableCheckBox.Name = "logEnableCheckBox";
			logEnableCheckBox.Size = new System.Drawing.Size(59, 17);
			logEnableCheckBox.TabIndex = 61;
			logEnableCheckBox.Text = "Enable";
			logEnableCheckBox.UseVisualStyleBackColor = true;
			logEventsCheckBox.AutoSize = true;
			logEventsCheckBox.Location = new System.Drawing.Point(108, 10);
			logEventsCheckBox.Name = "logEventsCheckBox";
			logEventsCheckBox.Size = new System.Drawing.Size(101, 17);
			logEventsCheckBox.TabIndex = 60;
			logEventsCheckBox.Text = "Log only events";
			logEventsCheckBox.UseVisualStyleBackColor = true;
			label11.AutoSize = true;
			label11.Location = new System.Drawing.Point(333, 132);
			label11.Name = "label11";
			label11.Size = new System.Drawing.Size(97, 13);
			label11.TabIndex = 59;
			label11.Text = "Elements separator";
			logSeparatorTextBox.Location = new System.Drawing.Point(438, 129);
			logSeparatorTextBox.Name = "logSeparatorTextBox";
			logSeparatorTextBox.Size = new System.Drawing.Size(24, 20);
			logSeparatorTextBox.TabIndex = 58;
			logUseSquelchCheckBox.AutoSize = true;
			logUseSquelchCheckBox.Location = new System.Drawing.Point(222, 10);
			logUseSquelchCheckBox.Name = "logUseSquelchCheckBox";
			logUseSquelchCheckBox.Size = new System.Drawing.Size(85, 17);
			logUseSquelchCheckBox.TabIndex = 57;
			logUseSquelchCheckBox.Text = "Use squelch";
			logUseSquelchCheckBox.UseVisualStyleBackColor = true;
			label10.AutoSize = true;
			label10.Location = new System.Drawing.Point(314, 11);
			label10.Name = "label10";
			label10.Size = new System.Drawing.Size(85, 13);
			label10.TabIndex = 56;
			label10.Text = "Refresh time, ms";
			logRefreshNumericUpDown.Location = new System.Drawing.Point(405, 9);
			logRefreshNumericUpDown.Maximum = new decimal(new int[4]
			{
				3600,
				0,
				0,
				0
			});
			logRefreshNumericUpDown.Minimum = new decimal(new int[4]
			{
				1,
				0,
				0,
				0
			});
			logRefreshNumericUpDown.Name = "logRefreshNumericUpDown";
			logRefreshNumericUpDown.Size = new System.Drawing.Size(57, 20);
			logRefreshNumericUpDown.TabIndex = 55;
			logRefreshNumericUpDown.Value = new decimal(new int[4]
			{
				100,
				0,
				0,
				0
			});
			label7.AutoSize = true;
			label7.Location = new System.Drawing.Point(5, 151);
			label7.Name = "label7";
			label7.Size = new System.Drawing.Size(344, 13);
			label7.TabIndex = 54;
			label7.Text = "You can use: date, time, frequency, level, squelch, record, \"any text\", +";
			label8.AutoSize = true;
			label8.Location = new System.Drawing.Point(5, 132);
			label8.Name = "label8";
			label8.Size = new System.Drawing.Size(144, 13);
			label8.TabIndex = 53;
			label8.Text = "Rules for creating log entries.";
			logEntriesRulesTextBox.Location = new System.Drawing.Point(8, 170);
			logEntriesRulesTextBox.Name = "logEntriesRulesTextBox";
			logEntriesRulesTextBox.Size = new System.Drawing.Size(458, 20);
			logEntriesRulesTextBox.TabIndex = 52;
			logEntriesRulesTextBox.Text = "/ date / name + \"_\" + frequency / time + \"_\" + name + \"_\" + frequency";
			logEntriesRulesTextBox.TextChanged += new System.EventHandler(logEntriesRulesTextBox_TextChanged);
			logFolderButton.Location = new System.Drawing.Point(388, 43);
			logFolderButton.Name = "logFolderButton";
			logFolderButton.Size = new System.Drawing.Size(75, 23);
			logFolderButton.TabIndex = 51;
			logFolderButton.Text = "Folder";
			logFolderButton.UseVisualStyleBackColor = true;
			logFolderButton.Click += new System.EventHandler(logFolderButton_Click);
			label5.AutoSize = true;
			label5.Location = new System.Drawing.Point(5, 53);
			label5.Name = "label5";
			label5.Size = new System.Drawing.Size(259, 13);
			label5.TabIndex = 50;
			label5.Text = "You can use: date, time, frequency, \"any text\", +, \\, /";
			label6.AutoSize = true;
			label6.Location = new System.Drawing.Point(5, 34);
			label6.Name = "label6";
			label6.Size = new System.Drawing.Size(160, 13);
			label6.TabIndex = 49;
			label6.Text = "Rules for creating log file names.";
			logFileRulesTextBox.Location = new System.Drawing.Point(8, 72);
			logFileRulesTextBox.Name = "logFileRulesTextBox";
			logFileRulesTextBox.Size = new System.Drawing.Size(458, 20);
			logFileRulesTextBox.TabIndex = 48;
			logFileRulesTextBox.Text = "/ date / name + \"_\" + frequency / time + \"_\" + name + \"_\" + frequency";
			logFileRulesTextBox.TextChanged += new System.EventHandler(logFileRulesTextBox_TextChanged);
			displayTimer.Enabled = true;
			displayTimer.Tick += new System.EventHandler(displayTimer_Tick);
			base.AcceptButton = btnOk;
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(501, 293);
			base.Controls.Add(tabControl1);
			base.Controls.Add(btnOk);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			base.Margin = new System.Windows.Forms.Padding(2);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "DialogConfigure";
			base.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			base.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			Text = "Configure audio recorder";
			tabControl1.ResumeLayout(performLayout: false);
			recorderTabPage.ResumeLayout(performLayout: false);
			recorderTabPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)NewFileTimeNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)continueRecordTimeNumericUpDown).EndInit();
			logPage.ResumeLayout(performLayout: false);
			logPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)logRefreshNumericUpDown).EndInit();
			ResumeLayout(performLayout: false);
		}
	}
}

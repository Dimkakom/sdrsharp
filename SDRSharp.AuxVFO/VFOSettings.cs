using SDRSharp.Radio;

namespace SDRSharp.AuxVFO
{
	public class VFOSettings
	{
		public bool Enabled
		{
			get;
			set;
		}

		public long Frequency
		{
			get;
			set;
		}

		public DetectorType DetectorType
		{
			get;
			set;
		}

		public int Bandwidth
		{
			get;
			set;
		}

		public int IFOffset
		{
			get;
			set;
		}

		public bool SquelchEnabled
		{
			get;
			set;
		}

		public int SquelchThreshold
		{
			get;
			set;
		}

		public int CWToneShift
		{
			get;
			set;
		}

		public int FilterOrder
		{
			get;
			set;
		}

		public WindowType WindowType
		{
			get;
			set;
		}

		public bool LockCarrier
		{
			get;
			set;
		}

		public bool UseAntiFading
		{
			get;
			set;
		}

		public bool FMStereo
		{
			get;
			set;
		}

		public bool FilterAudio
		{
			get;
			set;
		}

		public int AudioGain
		{
			get;
			set;
		}

		public float AgcDecay
		{
			get;
			set;
		}

		public bool AgcHang
		{
			get;
			set;
		}

		public float AgcSlope
		{
			get;
			set;
		}

		public float AgcThreshold
		{
			get;
			set;
		}

		public bool UseAgc
		{
			get;
			set;
		}

		public string OutputDevice
		{
			get;
			set;
		}

		public bool LeftChannel
		{
			get;
			set;
		}

		public bool RightChannel
		{
			get;
			set;
		}

		public bool Mix
		{
			get;
			set;
		}

		public bool DontWritePause
		{
			get;
			set;
		}

		public int ContinueRecordTime
		{
			get;
			set;
		}

		public bool NewFileTimeEnable
		{
			get;
			set;
		}

		public int NewFileTime
		{
			get;
			set;
		}

		public int SampleFormatSelectedIndex
		{
			get;
			set;
		}

		public int SamplerateOut
		{
			get;
			set;
		}

		public string OutputSamplerateArray
		{
			get;
			set;
		}

		public string FileNameRules
		{
			get;
			set;
		}

		public string WriteFolder
		{
			get;
			set;
		}

		public string LogFileNameRules
		{
			get;
			set;
		}

		public string LogWriteFolder
		{
			get;
			set;
		}

		public string LogEntryRules
		{
			get;
			set;
		}

		public string LogSeparator
		{
			get;
			set;
		}

		public bool LogEnabled
		{
			get;
			set;
		}

		public int LogUpdateTime
		{
			get;
			set;
		}

		public bool LogUseSquelch
		{
			get;
			set;
		}

		public bool LogOnlyEvents
		{
			get;
			set;
		}
	}
}

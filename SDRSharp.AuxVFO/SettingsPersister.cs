using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SDRSharp.AuxVFO
{
	public class SettingsPersister
	{
		private string _filename;

		private readonly string _settingsFolder;

		public SettingsPersister(string fileName)
		{
			_filename = fileName;
			_settingsFolder = Path.GetDirectoryName(Application.ExecutablePath);
		}

		public VFOSettings ReadStored()
		{
			VFOSettings vFOSettings = ReadObject<VFOSettings>(_filename);
			if (vFOSettings != null)
			{
				return vFOSettings;
			}
			return new VFOSettings();
		}

		public void PersistStored(VFOSettings entries)
		{
			WriteObject(entries, _filename);
		}

		private T ReadObject<T>(string fileName)
		{
			string path = Path.Combine(_settingsFolder, fileName);
			if (File.Exists(path))
			{
				using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
				{
					return (T)new XmlSerializer(typeof(T)).Deserialize(stream);
				}
			}
			return default(T);
		}

		private void WriteObject<T>(T obj, string fileName)
		{
			using (FileStream stream = new FileStream(Path.Combine(_settingsFolder, fileName), FileMode.Create))
			{
				new XmlSerializer(obj.GetType()).Serialize(stream, obj);
			}
		}
	}
}

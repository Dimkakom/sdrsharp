using SDRSharp.Radio;
using SDRSharp.Radio.PortAudio;

namespace SDRSharp.AuxVFO
{
	public class Player
	{
		private const float OutputLatency = 0.1f;

		private FloatFifoStream _audioStream;

		private WavePlayer _wavePlayer;

		private int _deviceIndex;

		private int _outputLength;

		private int _lostBuffers;

		private int _maxBufferSize;

		private double _sampleRate;

		private bool _playerIsStarted;

		public bool Pause
		{
			get;
			set;
		}

		public int LostBuffers => _lostBuffers;

		public void Start(int deviceIndex, double samplerate)
		{
			_playerIsStarted = true;
			_sampleRate = samplerate;
			_deviceIndex = deviceIndex;
			_lostBuffers = 0;
			_maxBufferSize = (int)_sampleRate;
			_outputLength = (int)(_sampleRate * 0.10000000149011612);
			_audioStream = new FloatFifoStream(BlockMode.None);
            unsafe
			{
				_wavePlayer = new WavePlayer(_deviceIndex, _sampleRate, _outputLength, PlayerProcess);
			}
		}

		public void Stop()
		{
			_playerIsStarted = false;
			if (_wavePlayer != null)
			{
				_wavePlayer.Dispose();
				_wavePlayer = null;
			}
			if (_audioStream != null)
			{
				_audioStream.Close();
				_audioStream.Dispose();
				_audioStream = null;
			}
			_sampleRate = 0.0;
		}

		public unsafe void AudioSamplesIn(float* buffer, int length)
		{
			if (_audioStream != null && _playerIsStarted && _audioStream.Length <= _maxBufferSize)
			{
				_audioStream.Write(buffer, length);
			}
		}

		private unsafe void PlayerProcess(float* buffer, int length)
		{
			if (_audioStream == null || !_playerIsStarted)
			{
				return;
			}
			int num = length * 2;
			if (_audioStream.Length < num || Pause)
			{
				for (int i = 0; i < num; i++)
				{
					buffer[i] = 0f;
				}
			}
			else
			{
				_audioStream.Read(buffer, num);
				ScaleAudio(buffer, num);
			}
		}

		private unsafe void ScaleAudio(float* buffer, int length)
		{
			float num = 100000f;
			for (int i = 0; i < length; i++)
			{
				buffer[i] *= num;
			}
		}
	}
}

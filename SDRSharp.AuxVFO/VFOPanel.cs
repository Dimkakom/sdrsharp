using SDRSharp.Common;
using SDRSharp.PanView;
using SDRSharp.Radio;
using SDRSharp.Radio.PortAudio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace SDRSharp.AuxVFO
{
	public class VFOPanel : UserControl
	{
		private class IQLevelCalculator : IIQProcessor, IStreamProcessor, IBaseProcessor
		{
			private float _level;

			private float _peak;

			private double _sampleRate;

			public double SampleRate
			{
				get
				{
					return _sampleRate;
				}
				set
				{
					_sampleRate = value;
				}
			}

			public bool Enabled
			{
				get
				{
					return true;
				}
				set
				{
				}
			}

			public int Level => (int)_level;

			public unsafe void Process(Complex* buffer, int length)
			{
				float num = 1f - (float)Math.Exp(-1.0 / (_sampleRate * 0.0099999997764825821));
				float num2 = 1f - (float)Math.Exp(-1.0 / (_sampleRate * 0.5));
				float num3 = 0f;
				for (int i = 0; i < length; i++)
				{
					num3 = Math.Max(Math.Abs(buffer[i].Real), Math.Abs(buffer[i].Imag));
					if (num3 > _peak)
					{
						_peak = (1f - num) * _peak + num * num3;
					}
					else
					{
						_peak = (1f - num2) * _peak + num2 * num3;
					}
				}
				_level = (float)(20.0 * Math.Log10(1E-60 + (double)_peak));
			}
		}

		private static readonly int[] _defaultNFMState = new int[10]
		{
			8000,
			300,
			3,
			50,
			1,
			600,
			1,
			12,
			0,
			0
		};

		private static readonly int[] _defaultWFMState = new int[10]
		{
			180000,
			100,
			3,
			50,
			0,
			600,
			1,
			17,
			0,
			0
		};

		private static readonly int[] _defaultAMState = new int[10]
		{
			10000,
			450,
			3,
			50,
			0,
			600,
			1,
			4,
			0,
			0
		};

		private static readonly int[] _defaultSSBState = new int[10]
		{
			2400,
			500,
			3,
			50,
			0,
			600,
			1,
			1,
			0,
			0
		};

		private static readonly int[] _defaultDSBState = new int[10]
		{
			6000,
			500,
			3,
			50,
			0,
			600,
			1,
			1,
			0,
			0
		};

		private static readonly int[] _defaultCWState = new int[10]
		{
			300,
			800,
			3,
			50,
			0,
			600,
			1,
			1,
			0,
			0
		};

		private static readonly int _bufferAlignment = 8 * Environment.ProcessorCount;

		private readonly ISharpControl _control;

		private readonly IQProcessor _iqProcessor = new IQProcessor();

		private readonly HookManager _hookManager;

		private readonly IQLevelCalculator _iqLevelCalculator = new IQLevelCalculator();

		private Vfo _vfo;

		private SimpleWavWriter _wavWriter;

		private TextFile _textFile;

		private UnsafeBuffer _iqBuffer;

		private unsafe Complex* _iqPtr;

		private UnsafeBuffer _audioBuffer;

		private unsafe float* _audioBufferPtr;

		private int _audioBufferLength;

		private ComplexFifoStream _fifoBuffer;

		private SharpEvent _iqBufferReady = new SharpEvent(initialState: false);

		private SharpEvent _audioBufferReady = new SharpEvent(initialState: false);

		private Thread _dspThread;

		private readonly Dictionary<DetectorType, int[]> _modeStates = new Dictionary<DetectorType, int[]>();

		private VFOSettings _vfoSettings;

		private SettingsPersister _settingsPersister;

		private bool _dspRunning;

		private int _iqBufferLength;

		private long _frequency;

		private long _frequencyForVFO;

		private bool _outOfRange;

		private int _numberOfPlugin;

		private bool _needReInitVFO;

		private bool _displayInit;

		private Player _player;

		private int _selectedOutput;

		private double _iqSamplerate;

		private double _audioSamplerate;

		private string _fileName;

		private WavSampleFormat _wavSampleFormat;

		private int _samplerateFileOut;

		private const int DefaultAudioGain = 30;

		private readonly float _audioGain = (float)Math.Pow(3.0, 10.0);

		private int _bufferUse;

		private int _waitTime;

		private int _waitFileTime;

		private bool _prevSquelchOpen;

		private int _prevLevel;

		private int _prevFrequency;

		private bool _prevRecordState;

		private bool _recordState;

		private bool _logUseFrequency;

		private bool _logUseRecord;

		private bool _logUseSquelch;

		private bool _logUseLevel;

		private bool _writerBlocked;

		private int _fifoBufferLength;

		private int _inputBufferSize;

		private int _smallBufferSize;

		private int _outputBufferSize;

		private IContainer components;

		private Panel panel1;

		private CheckBox enableCheckBox;

		private Label bandwidthLabel;

		private RadioButton nfmRadioButton;

		private RadioButton amRadioButton;

		private RadioButton lsbRadioButton;

		private RadioButton usbRadioButton;

		private RadioButton wfmRadioButton;

		private RadioButton dsbRadioButton;

		private RadioButton cwRadioButton;

		private RadioButton rawRadioButton;

		private NumericUpDown bandwidtNumericUpDown;

		private Label frequencyLabel;

		private NumericUpDown squelchNumericUpDown;

		private CheckBox squelchCheckBox;

		private Button frequencySetButton;

		private CheckBox filterAudioCheckBox;

		private GroupBox groupBox1;

		private ComboBox outputSelectComboBox;

		private CheckBox rightCheckBox;

		private CheckBox leftCheckBox;

		private CheckBox lockCarrierCheckBox;

		private CheckBox antiFadingCheckBox;

		private CheckBox fmStereoCheckBox;

		private Label squelchOpenLabel;

		private Label stereoLabel;

		private System.Windows.Forms.Timer displayUpdateTimer;

		private TrackBar volumeTrackBar;

		private Button configButton;

		private ProgressBar bufferUseBar;

		private System.Windows.Forms.Timer logTimer;

		private Label recordLabel;

		private Label levelLabel;

		public unsafe VFOPanel(ISharpControl control, int pluginCounter)
		{
			InitializeComponent();
			_control = control;
			_control.PropertyChanged += PropertyChangedHandler;
			_numberOfPlugin = pluginCounter;
			_iqProcessor.Enabled = false;
			_control.RegisterStreamHook(_iqProcessor, ProcessorType.RawIQ);
			_iqProcessor.IQReady += _iqProcessor_IQReady;
			_settingsPersister = new SettingsPersister("AuxVFOSettings" + _numberOfPlugin);
			_hookManager = new HookManager();
			_hookManager.RegisterStreamHook(_iqLevelCalculator, ProcessorType.FrequencyTranslatedIQ);
			_vfo = new Vfo(_hookManager);
			_vfo.HookdEnabled = true;
			_vfoSettings = _settingsPersister.ReadStored();
			if (_vfoSettings.OutputSamplerateArray == string.Empty || _vfoSettings.OutputSamplerateArray == null)
			{
				_vfoSettings.OutputSamplerateArray = "48 kHz,32 kHz,16 kHz,8 kHz";
			}
			if (_vfoSettings.FileNameRules == string.Empty || _vfoSettings.FileNameRules == null)
			{
				_vfoSettings.FileNameRules = "/ date / frequency / time";
			}
			if (_vfoSettings.WriteFolder == string.Empty || _vfoSettings.WriteFolder == null)
			{
				_vfoSettings.WriteFolder = Path.GetDirectoryName(Application.ExecutablePath);
			}
			if (_vfoSettings.LogFileNameRules == string.Empty || _vfoSettings.LogFileNameRules == null)
			{
				_vfoSettings.LogFileNameRules = "/\"Log\" / date / frequency";
			}
			if (_vfoSettings.LogEntryRules == string.Empty || _vfoSettings.LogEntryRules == null)
			{
				_vfoSettings.LogEntryRules = "date + time + frequency + level + squelch + record";
			}
			if (_vfoSettings.LogWriteFolder == string.Empty || _vfoSettings.LogWriteFolder == null)
			{
				_vfoSettings.LogWriteFolder = Path.GetDirectoryName(Application.ExecutablePath);
			}
			if (_vfoSettings.LogSeparator == string.Empty || _vfoSettings.LogSeparator == null)
			{
				_vfoSettings.LogSeparator = ";";
			}
			if (_vfoSettings.LogUpdateTime == 0)
			{
				_vfoSettings.LogUpdateTime = 100;
			}
			logTimer.Interval = _vfoSettings.LogUpdateTime;
			_player = new Player();
			_textFile = new TextFile();
			AudioDeviceGet();
			outputSelectComboBox_SelectedIndexChanged(null, null);
			_modeStates[DetectorType.WFM] = Utils.GetIntArraySetting("wfmState", _defaultWFMState);
			_modeStates[DetectorType.NFM] = Utils.GetIntArraySetting("nfmState", _defaultNFMState);
			_modeStates[DetectorType.AM] = Utils.GetIntArraySetting("amState", _defaultAMState);
			_modeStates[DetectorType.LSB] = Utils.GetIntArraySetting("lsbState", _defaultSSBState);
			_modeStates[DetectorType.USB] = Utils.GetIntArraySetting("usbState", _defaultSSBState);
			_modeStates[DetectorType.DSB] = Utils.GetIntArraySetting("dsbState", _defaultDSBState);
			_modeStates[DetectorType.CW] = Utils.GetIntArraySetting("cwlState", _defaultCWState);
			_modeStates[DetectorType.RAW] = Utils.GetIntArraySetting("rawState", _defaultAMState);
			DisplayInit();
			ConfigureGUI();
			enableCheckBox.Checked = _vfoSettings.Enabled;
		}

		public void SaveSettings()
		{
			if (_dspRunning)
			{
				StopDSP();
			}
			_settingsPersister.PersistStored(_vfoSettings);
		}

		private void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			string propertyName = e.PropertyName;
			if (!(propertyName == "StartRadio"))
			{
				if (!(propertyName == "StopRadio"))
				{
					if (propertyName == "DetectorType")
					{
						_needReInitVFO = true;
					}
					return;
				}
				ConfigureGUI();
				if (_dspRunning)
				{
					StopDSP();
				}
			}
			else
			{
				ConfigureGUI();
				enableCheckBox_CheckedChanged(null, null);
			}
		}

		private void AudioDeviceGet()
		{
			int num = 0;
			int num2 = 0;
			new AudioDevice();
			AudioDevice audioDevice = new AudioDevice();
			List<AudioDevice> devices = AudioDevice.GetDevices(DeviceDirection.Output);
			string outputDevice = _vfoSettings.OutputDevice;
			audioDevice.Name = "Write то file";
			audioDevice.Index = -1;
			audioDevice.Direction = DeviceDirection.Output;
			audioDevice.Host = "File";
			audioDevice.IsDefault = false;
			outputSelectComboBox.Items.Add(audioDevice);
			for (int i = 0; i < devices.Count; i++)
			{
				outputSelectComboBox.Items.Add(devices[i]);
			}
			for (int j = 0; j < outputSelectComboBox.Items.Count; j++)
			{
				AudioDevice obj = (AudioDevice)outputSelectComboBox.Items[j];
				if (obj.IsDefault)
				{
					num = j;
				}
				if (obj.ToString() == outputDevice)
				{
					num2 = j;
				}
			}
			if (outputSelectComboBox.Items.Count > 0)
			{
				outputSelectComboBox.SelectedIndex = ((num2 >= 0) ? num2 : num);
			}
		}

		private unsafe void _iqProcessor_IQReady(Complex* buffer, int length)
		{
			if (!_dspRunning)
			{
				return;
			}
			if (_fifoBuffer == null || _fifoBufferLength != _inputBufferSize)
			{
				if (_fifoBuffer != null)
				{
					_fifoBuffer.Close();
					_fifoBuffer.Dispose();
					_fifoBuffer = null;
				}
				_fifoBufferLength = _inputBufferSize;
				_fifoBuffer = new ComplexFifoStream(BlockMode.None, _fifoBufferLength);
			}
			if (_iqBuffer == null || _iqBufferLength != _smallBufferSize)
			{
				if (_iqBuffer != null)
				{
					_iqBuffer.Dispose();
					_iqBuffer = null;
				}
				_iqBufferLength = _smallBufferSize;
				_iqBuffer = UnsafeBuffer.Create(_iqBufferLength, sizeof(Complex));
				_iqPtr = (Complex*)(void*)_iqBuffer;
			}
			if (_audioBuffer == null || _audioBufferLength != _outputBufferSize)
			{
				_audioBufferLength = _outputBufferSize;
				_audioBuffer = UnsafeBuffer.Create(_audioBufferLength, 4);
				_audioBufferPtr = (float*)(void*)_audioBuffer;
			}
			_fifoBuffer.Write(buffer, length);
			_bufferUse = (int)((double)((float)_fifoBuffer.Length / (float)_fifoBufferLength) * 100.0);
		}

		private unsafe void DSPProcess()
		{
			while (_dspRunning)
			{
				if (_iqBuffer == null || _audioBuffer == null)
				{
					Thread.Sleep(10);
					continue;
				}
				if (_fifoBuffer.Length < _iqBuffer.Length)
				{
					Thread.Sleep(20);
					continue;
				}
				_fifoBuffer.Read(_iqPtr, _iqBuffer.Length);
				if (_needReInitVFO)
				{
					_needReInitVFO = false;
					VfoInit();
				}
				long frequency = _control.Frequency;
				long centerFrequency = _control.CenterFrequency;
				int num = _control.RFDisplayBandwidth / 2;
				if (_frequency > centerFrequency + num || _frequency < centerFrequency - num)
				{
					_outOfRange = true;
					if (_player != null)
					{
						_player.Pause = true;
					}
					continue;
				}
				_outOfRange = false;
				if (_player != null)
				{
					_player.Pause = false;
				}
				_frequencyForVFO = _frequency - centerFrequency;
				if (_vfo.Frequency != _frequencyForVFO)
				{
					_vfo.Frequency = (int)_frequencyForVFO;
				}
				_vfo.ProcessBuffer(_iqPtr, _audioBufferPtr, _iqBuffer.Length);
				ScaleAudio(_audioBufferPtr, _audioBufferLength);
				int selectedOutput = _selectedOutput;
				if (selectedOutput == -1)
				{
					WriteToFile();
				}
				else
				{
					_player.AudioSamplesIn(_audioBufferPtr, _audioBufferLength);
				}
			}
		}

		private unsafe void WriteToFile()
		{
			if (_vfo.IsSquelchOpen || _waitTime > 0 || !_vfoSettings.DontWritePause)
			{
				if (_waitFileTime == 0 && _vfoSettings.NewFileTimeEnable && _wavWriter != null)
				{
					_wavWriter.Close();
					_wavWriter = null;
				}
				if (_vfo.IsSquelchOpen)
				{
					_waitTime = _vfoSettings.ContinueRecordTime;
					_waitFileTime = _vfoSettings.NewFileTime;
				}
				if (_wavWriter == null)
				{
					_fileName = MakeFileName(_vfoSettings.WriteFolder, _vfoSettings.FileNameRules, ".wav");
					_wavSampleFormat = (WavSampleFormat)_vfoSettings.SampleFormatSelectedIndex;
					if (_vfoSettings.SamplerateOut == 0 || _vfoSettings.SampleFormatSelectedIndex < 3)
					{
						_samplerateFileOut = (int)_audioSamplerate;
					}
					else
					{
						_samplerateFileOut = _vfoSettings.SamplerateOut;
					}
					_wavWriter = new SimpleWavWriter(_fileName, _wavSampleFormat, (uint)_audioSamplerate, _samplerateFileOut);
					_wavWriter.Open();
				}
				_wavWriter.Write(_audioBufferPtr, _audioBufferLength);
				_recordState = true;
			}
			else
			{
				_recordState = false;
			}
		}

		private unsafe void ScaleAudio(float* buffer, int length)
		{
			float num = (float)_vfoSettings.AudioGain / 50f;
			float num2 = num * (_vfoSettings.LeftChannel ? 1f : 0f);
			float num3 = num * (_vfoSettings.RightChannel ? 1f : 0f);
			if (_selectedOutput == -1)
			{
				num2 = _audioGain;
				num3 = _audioGain;
			}
			for (int i = 0; i < length; i += 2)
			{
				buffer[i] *= num2;
				buffer[i + 1] *= num3;
			}
		}

		private void StartDSP()
		{
			if (!_dspRunning)
			{
				_control.SpectrumAnalyzerCustomPaint += controlInterface_SpectrumCustomPaint;
				_audioBufferReady.Reset();
				_iqBufferReady.Reset();
				_iqProcessor.Enabled = true;
				_outOfRange = false;
				_iqSamplerate = _iqProcessor.SampleRate;
				VfoInit();
				_audioSamplerate = _iqSamplerate / (double)(1 << _vfo.DecimationStageCount);
				if (_selectedOutput > 0)
				{
					_player.Start(_selectedOutput, _audioSamplerate);
				}
				ParseStringToEntries(_vfoSettings.LogEntryRules);
				_dspRunning = true;
				_dspThread = new Thread(DSPProcess);
				_dspThread.Priority = ThreadPriority.Highest;
				_dspThread.Name = "SecondaryVFO";
				_dspThread.Start();
			}
		}

		private unsafe void StopDSP()
		{
			if (_dspRunning)
			{
				_control.SpectrumAnalyzerCustomPaint -= controlInterface_SpectrumCustomPaint;
				_iqProcessor.Enabled = false;
				_dspRunning = false;
				_iqBufferReady.Set();
				_audioBufferReady.Set();
				if (_dspThread != null)
				{
					_dspThread.Join();
					_dspThread = null;
				}
				if (_selectedOutput > 0)
				{
					_player.Stop();
				}
				if (_selectedOutput == -1 && _wavWriter != null)
				{
					_wavWriter.Close();
					_wavWriter = null;
				}
				_iqBuffer = null;
				_iqPtr = null;
				_audioBuffer = null;
				_audioBufferPtr = null;
			}
		}

		private void VfoInit()
		{
			_vfo.FilterOrder = _vfoSettings.FilterOrder;
			_vfo.WindowType = _vfoSettings.WindowType;
			_vfo.DetectorType = _vfoSettings.DetectorType;
			_frequency = _vfoSettings.Frequency;
			_vfo.Bandwidth = _vfoSettings.Bandwidth;
			_vfo.IFOffset = _vfoSettings.IFOffset;
			_vfo.CWToneShift = _vfoSettings.CWToneShift;
			if (_vfoSettings.SquelchEnabled)
			{
				_vfo.SquelchThreshold = _vfoSettings.SquelchThreshold;
			}
			else
			{
				_vfo.SquelchThreshold = 0;
			}
			_vfo.AgcDecay = _vfoSettings.AgcDecay;
			_vfo.AgcHang = _vfoSettings.AgcHang;
			_vfo.AgcSlope = _vfoSettings.AgcSlope;
			_vfo.AgcThreshold = _vfoSettings.AgcThreshold;
			_vfo.UseAGC = _vfoSettings.UseAgc;
			_vfo.LockCarrier = _vfoSettings.LockCarrier;
			_vfo.UseAntiFading = _vfoSettings.UseAntiFading;
			_vfo.FmStereo = _vfoSettings.FMStereo;
			_vfo.FilterAudio = _vfoSettings.FilterAudio;
			_vfo.SampleRate = _iqSamplerate;
			int decimationStageCount = StreamControl.GetDecimationStageCount(_vfo.SampleRate, DetectorType.AM);
			_vfo.DecimationStageCount = decimationStageCount;
			int num = 1 << decimationStageCount;
			int num2 = num * _bufferAlignment;
			_inputBufferSize = (int)_iqProcessor.SampleRate / num2 * num2;
			_smallBufferSize = _inputBufferSize / 10 / num2 * num2;
			_outputBufferSize = _smallBufferSize / num * 2;
		}

		private void DisplayInit()
		{
			_displayInit = true;
			frequencyLabel.Text = GetFrequencyDisplay(_vfoSettings.Frequency);
			ModulationSet(_vfoSettings.DetectorType);
			bandwidtNumericUpDown.Value = _vfoSettings.Bandwidth;
			squelchCheckBox.Checked = _vfoSettings.SquelchEnabled;
			squelchNumericUpDown.Value = _vfoSettings.SquelchThreshold;
			filterAudioCheckBox.Checked = _vfoSettings.FilterAudio;
			fmStereoCheckBox.Checked = _vfoSettings.FMStereo;
			lockCarrierCheckBox.Checked = _vfoSettings.LockCarrier;
			antiFadingCheckBox.Checked = _vfoSettings.UseAntiFading;
			volumeTrackBar.Value = _vfoSettings.AudioGain;
			leftCheckBox.Checked = _vfoSettings.LeftChannel;
			rightCheckBox.Checked = _vfoSettings.RightChannel;
			_displayInit = false;
		}

		private void outputSelectComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			AudioDevice audioDevice = (AudioDevice)outputSelectComboBox.SelectedItem;
			_vfoSettings.OutputDevice = audioDevice.ToString();
			_selectedOutput = audioDevice.Index;
			ConfigureGUI();
		}

		private void ConfigureGUI()
		{
			outputSelectComboBox.Enabled = (!_control.IsPlaying || !enableCheckBox.Checked);
			fmStereoCheckBox.Enabled = (_vfoSettings.DetectorType == DetectorType.WFM);
			lockCarrierCheckBox.Enabled = (_vfoSettings.DetectorType == DetectorType.AM || _vfoSettings.DetectorType == DetectorType.DSB);
			antiFadingCheckBox.Enabled = (lockCarrierCheckBox.Enabled && lockCarrierCheckBox.Checked);
			squelchCheckBox.Enabled = (_vfoSettings.DetectorType == DetectorType.NFM || _vfoSettings.DetectorType == DetectorType.AM);
			squelchNumericUpDown.Enabled = (squelchCheckBox.Enabled && squelchCheckBox.Checked);
			_recordState = false;
		}

		private static string GetFrequencyDisplay(long frequency)
		{
			long num = Math.Abs(frequency);
			if (num == 0L)
			{
				return "DC";
			}
			if (num > 1500000000)
			{
				return $"{(double)frequency / 1000000000.0:#,0.000 000} GHz";
			}
			if (num > 30000000)
			{
				return $"{(double)frequency / 1000000.0:0,0.000###} MHz";
			}
			if (num > 1000)
			{
				return $"{(double)frequency / 1000.0:#,#.###} kHz";
			}
			return frequency.ToString();
		}

		private string MakeFileName(string folder, string nameRules, string fileExtension)
		{
			string text = folder + ParseStringToPath(nameRules, fileExtension);
			string text2 = text.Substring(0, text.LastIndexOf("\\"));
			try
			{
				Directory.CreateDirectory(text2);
				return text;
			}
			catch
			{
				MessageBox.Show("Unable to create directory", text2, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				return text;
			}
		}

		public string ParseStringToPath(string nameString, string extension)
		{
			string frequencyDisplay = GetFrequencyDisplay(_frequency);
			string str = DateTime.Now.ToString("yyyy_MM_dd");
			string str2 = DateTime.Now.ToString("HH-mm-ss");
			string text = "";
			int num = 0;
			while (num < nameString.Length)
			{
				if (CompareString(nameString, "date", num))
				{
					num += "date".Length;
					text += str;
					continue;
				}
				if (CompareString(nameString, "time", num))
				{
					num += "time".Length;
					text += str2;
					continue;
				}
				if (CompareString(nameString, "start_time", num))
				{
					num += "start_time".Length;
					text += str2;
					continue;
				}
				if (CompareString(nameString, "frequency", num))
				{
					num += "frequency".Length;
					text += frequencyDisplay;
					continue;
				}
				if (CompareString(nameString, "\\", num) || CompareString(nameString, "/", num))
				{
					num++;
					text += "\\";
					continue;
				}
				if (CompareString(nameString, "+", num) || CompareString(nameString, " ", num))
				{
					num++;
					continue;
				}
				if (CompareString(nameString, "\"", num))
				{
					num++;
					int num2 = nameString.IndexOf('"', num);
					if (num2 <= 0)
					{
						return text + "-error!";
					}
					for (int i = num; i <= num2; i++)
					{
						char c = nameString[i];
						num++;
						if (c != '?' && c != '/' && c != '\\' && c != ':' && c != '<' && c != '>' && c != '|' && c != '*' && c != '"')
						{
							text += c.ToString();
						}
					}
					continue;
				}
				return text + "-error!";
			}
			if (text.Length > 5)
			{
				string text2 = text;
				if (text2.Substring(text2.Length - extension.Length, extension.Length) != extension)
				{
					text += extension;
				}
				if (text.Substring(0, 1) != "\\")
				{
					text = "\\" + text;
				}
			}
			return text;
		}

		private bool CompareString(string source, string compare, int index)
		{
			if (index + compare.Length > source.Length)
			{
				return false;
			}
			return source.Substring(index, compare.Length) == compare;
		}

		private void controlInterface_SpectrumCustomPaint(object sender, CustomPaintEventArgs e)
		{
			if (!_outOfRange)
			{
				SpectrumAnalyzer spectrumAnalyzer = (SpectrumAnalyzer)sender;
				long frequency = _vfoSettings.Frequency;
				long frequency2 = frequency;
				long frequency3 = frequency;
				int bandwidth = _vfoSettings.Bandwidth;
				switch (_vfoSettings.DetectorType)
				{
				case DetectorType.AM:
					frequency2 = frequency - bandwidth / 2;
					frequency3 = frequency + bandwidth / 2;
					break;
				case DetectorType.CW:
					frequency2 = frequency - bandwidth / 2;
					frequency3 = frequency + bandwidth / 2;
					break;
				case DetectorType.WFM:
					frequency2 = frequency - bandwidth / 2;
					frequency3 = frequency + bandwidth / 2;
					break;
				case DetectorType.RAW:
					frequency2 = frequency - bandwidth / 2;
					frequency3 = frequency + bandwidth / 2;
					break;
				case DetectorType.NFM:
					frequency2 = frequency - bandwidth / 2;
					frequency3 = frequency + bandwidth / 2;
					break;
				case DetectorType.DSB:
					frequency2 = frequency - bandwidth / 2;
					frequency3 = frequency + bandwidth / 2;
					break;
				case DetectorType.LSB:
					frequency2 = frequency - bandwidth;
					frequency3 = frequency;
					break;
				case DetectorType.USB:
					frequency2 = frequency;
					frequency3 = frequency + bandwidth;
					break;
				}
				using (SolidBrush brush = new SolidBrush(Color.FromArgb(50, Color.Silver)))
				{
					using (SolidBrush solidBrush2 = new SolidBrush(Color.FromArgb(255, Color.Yellow)))
					{
						using (SolidBrush solidBrush = new SolidBrush(Color.FromArgb(255, Color.Red)))
						{
							using (Pen pen = new Pen(Color.FromArgb(255, Color.Yellow), 1f))
							{
								using (StringFormat format = new StringFormat(StringFormat.GenericTypographic))
								{
									using (Font font = new Font("Arial", 10f))
									{
										float num = Math.Max(30f, spectrumAnalyzer.FrequencyToPoint(frequency2));
										float y = 30f;
										float width = Math.Min(spectrumAnalyzer.Width - 30, spectrumAnalyzer.FrequencyToPoint(frequency3)) - num;
										float height = (float)spectrumAnalyzer.Height - 60f;
										e.Graphics.FillRectangle(brush, num, y, width, height);
										float num2 = Math.Max(30f, spectrumAnalyzer.FrequencyToPoint(frequency));
										float y2 = 30f;
										float y3 = (float)spectrumAnalyzer.Height - 30f;
										e.Graphics.DrawLine(pen, num2, y2, num2, y3);
										string text = _numberOfPlugin.ToString();
										PointF point = default(PointF);
										SizeF sizeF = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, format);
										point.X = num2 - sizeF.Width / 2f;
										point.Y = 30f - sizeF.Height - 3f;
										e.Graphics.DrawString(text, font, _vfo.IsSquelchOpen ? solidBrush : solidBrush2, point, format);
									}
								}
							}
						}
					}
				}
			}
		}

		private void enableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_vfoSettings.Enabled = enableCheckBox.Checked;
			if (_control.IsPlaying)
			{
				if (enableCheckBox.Checked && !_dspRunning)
				{
					StartDSP();
				}
				if (!enableCheckBox.Checked && _dspRunning)
				{
					StopDSP();
				}
				ConfigureGUI();
			}
		}

		private void frequencySetButton_Click(object sender, EventArgs e)
		{
			_vfoSettings.Frequency = _control.Frequency;
			_vfoSettings.DetectorType = _control.DetectorType;
			_vfoSettings.Bandwidth = _control.FilterBandwidth;
			_vfoSettings.IFOffset = -_control.IFOffset;
			_vfoSettings.CWToneShift = _control.CWShift;
			_vfoSettings.SquelchEnabled = _control.SquelchEnabled;
			_vfoSettings.SquelchThreshold = _control.SquelchThreshold;
			_vfoSettings.AgcDecay = _control.AgcDecay;
			_vfoSettings.AgcHang = _control.AgcHang;
			_vfoSettings.AgcSlope = _control.AgcSlope;
			_vfoSettings.AgcThreshold = _control.AgcThreshold;
			_vfoSettings.UseAgc = _control.UseAgc;
			_vfoSettings.FilterOrder = _control.FilterOrder;
			_vfoSettings.WindowType = _control.FilterType;
			_vfoSettings.LockCarrier = lockCarrierCheckBox.Checked;
			_vfoSettings.UseAntiFading = antiFadingCheckBox.Checked;
			_vfoSettings.FMStereo = fmStereoCheckBox.Checked;
			_needReInitVFO = true;
			DisplayInit();
			ConfigureGUI();
		}

		private void ModulationSet(DetectorType modulation)
		{
			switch (modulation)
			{
			case DetectorType.AM:
				amRadioButton.Checked = true;
				break;
			case DetectorType.CW:
				cwRadioButton.Checked = true;
				break;
			case DetectorType.NFM:
				nfmRadioButton.Checked = true;
				break;
			case DetectorType.DSB:
				dsbRadioButton.Checked = true;
				break;
			case DetectorType.LSB:
				lsbRadioButton.Checked = true;
				break;
			case DetectorType.USB:
				usbRadioButton.Checked = true;
				break;
			case DetectorType.WFM:
				wfmRadioButton.Checked = true;
				break;
			case DetectorType.RAW:
				rawRadioButton.Checked = true;
				break;
			}
		}

		private void filterAudioCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_vfoSettings.FilterAudio = filterAudioCheckBox.Checked;
			_needReInitVFO = true;
		}

		private void bandwidtNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_vfoSettings.Bandwidth = (int)bandwidtNumericUpDown.Value;
			_needReInitVFO = true;
		}

		private void squelchNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_vfoSettings.SquelchThreshold = (int)squelchNumericUpDown.Value;
			_needReInitVFO = true;
		}

		private void squelchCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_vfoSettings.SquelchEnabled = squelchCheckBox.Checked;
			_needReInitVFO = true;
			ConfigureGUI();
		}

		private void nfmRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			SetModeState(DetectorType.NFM);
		}

		private void amRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			SetModeState(DetectorType.AM);
		}

		private void lsbRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			SetModeState(DetectorType.LSB);
		}

		private void usbRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			SetModeState(DetectorType.USB);
		}

		private void wfmRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			SetModeState(DetectorType.WFM);
		}

		private void dsbRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			SetModeState(DetectorType.DSB);
		}

		private void cwRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			SetModeState(DetectorType.CW);
		}

		private void rawRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			SetModeState(DetectorType.RAW);
		}

		private void SetModeState(DetectorType detector)
		{
			if (!_displayInit)
			{
				int[] array = _modeStates[detector];
				_vfoSettings.DetectorType = detector;
				_vfoSettings.Bandwidth = array[0];
				_vfoSettings.FilterOrder = array[1];
				_vfoSettings.WindowType = (WindowType)array[2];
				_vfoSettings.SquelchThreshold = array[3];
				_vfoSettings.SquelchEnabled = (array[4] == 1);
				_vfoSettings.CWToneShift = array[5];
				_vfoSettings.LockCarrier = (array[8] == 1);
				_vfoSettings.UseAntiFading = (array[9] == 1);
				_needReInitVFO = true;
				DisplayInit();
				ConfigureGUI();
			}
		}

		private void leftCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_vfoSettings.LeftChannel = leftCheckBox.Checked;
		}

		private void rightCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_vfoSettings.RightChannel = rightCheckBox.Checked;
		}

		private void fmStereoCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_vfoSettings.FMStereo = fmStereoCheckBox.Checked;
			_needReInitVFO = true;
		}

		private void antiFadingCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_vfoSettings.UseAntiFading = antiFadingCheckBox.Checked;
			_needReInitVFO = true;
		}

		private void lockCarrierCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_vfoSettings.LockCarrier = lockCarrierCheckBox.Checked;
			_needReInitVFO = true;
			ConfigureGUI();
		}

		private void displayUpdateTimer_Tick(object sender, EventArgs e)
		{
			stereoLabel.Visible = (_vfo.SignalIsStereo && !_outOfRange && _vfoSettings.DetectorType == DetectorType.WFM && _dspRunning);
			squelchOpenLabel.Visible = (_vfo.IsSquelchOpen && !_outOfRange && (_vfoSettings.DetectorType == DetectorType.NFM || _vfoSettings.DetectorType == DetectorType.AM) && _dspRunning);
			bufferUseBar.Value = Math.Min(_bufferUse, 100);
			recordLabel.Visible = (_recordState && !_outOfRange && _dspRunning && _selectedOutput == -1);
			levelLabel.Text = _iqLevelCalculator.Level.ToString() + " dbFS";
			int interval = displayUpdateTimer.Interval;
			if (_waitTime > interval)
			{
				_waitTime -= interval;
			}
			else
			{
				_waitTime = 0;
			}
			if (_waitFileTime > interval)
			{
				_waitFileTime -= interval;
			}
			else
			{
				_waitFileTime = 0;
			}
		}

		private void volumeTrackBar_Scroll(object sender, EventArgs e)
		{
			_vfoSettings.AudioGain = volumeTrackBar.Value;
		}

		private void configButton_Click(object sender, EventArgs e)
		{
			new DialogConfigure(_vfoSettings, this).ShowDialog();
			logTimer.Interval = _vfoSettings.LogUpdateTime;
		}

		private void logTimer_Tick(object sender, EventArgs e)
		{
			if (!_vfoSettings.LogEnabled || !_vfoSettings.Enabled || !_dspRunning || _outOfRange || (_vfoSettings.LogUseSquelch && !_vfo.IsSquelchOpen))
			{
				return;
			}
			if (_vfoSettings.LogOnlyEvents)
			{
				int level = _iqLevelCalculator.Level;
				if ((!_logUseSquelch || _vfo.IsSquelchOpen == _prevSquelchOpen) && (!_logUseLevel || level == _prevLevel) && (!_logUseFrequency || _prevFrequency == _vfo.Frequency) && (!_logUseRecord || _prevRecordState == _recordState))
				{
					return;
				}
				_prevSquelchOpen = _vfo.IsSquelchOpen;
				_prevLevel = level;
				_prevFrequency = _vfo.Frequency;
				_prevRecordState = _recordState;
			}
			string line = ParseStringToEntries(_vfoSettings.LogEntryRules);
			string text = MakeFileName(_vfoSettings.LogWriteFolder, _vfoSettings.LogFileNameRules, ".csv");
			try
			{
				_textFile.Write(line, text);
			}
			catch
			{
				if (!_writerBlocked)
				{
					_writerBlocked = true;
					if (MessageBox.Show("Unable to open file " + text, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand) == DialogResult.OK)
					{
						_writerBlocked = false;
					}
				}
			}
		}

		public string ParseStringToEntries(string entryString)
		{
			string str = _frequency.ToString();
			string str2 = DateTime.Now.ToString("yyyy_MM_dd");
			string str3 = DateTime.Now.ToString("HH:mm:ss.fff");
			string str4 = _vfo.IsSquelchOpen ? "Open" : "Close";
			string str5 = _recordState ? "Rec" : "Stop";
			string str6 = _iqLevelCalculator.Level.ToString();
			string text = "";
			int num = 0;
			_logUseLevel = false;
			_logUseFrequency = false;
			_logUseRecord = false;
			_logUseSquelch = false;
			while (num < entryString.Length)
			{
				if (CompareString(entryString, "date", num))
				{
					num += "date".Length;
					text += str2;
					text += _vfoSettings.LogSeparator;
					continue;
				}
				if (CompareString(entryString, "time", num))
				{
					num += "time".Length;
					text += str3;
					text += _vfoSettings.LogSeparator;
					continue;
				}
				if (CompareString(entryString, "level", num))
				{
					num += "level".Length;
					text += str6;
					text += _vfoSettings.LogSeparator;
					_logUseLevel = true;
					continue;
				}
				if (CompareString(entryString, "squelch", num))
				{
					num += "squelch".Length;
					text += str4;
					text += _vfoSettings.LogSeparator;
					_logUseSquelch = true;
					continue;
				}
				if (CompareString(entryString, "record", num))
				{
					num += "record".Length;
					text += str5;
					text += _vfoSettings.LogSeparator;
					_logUseRecord = true;
					continue;
				}
				if (CompareString(entryString, "frequency", num))
				{
					num += "frequency".Length;
					text += str;
					text += _vfoSettings.LogSeparator;
					_logUseFrequency = true;
					continue;
				}
				if (CompareString(entryString, "+", num) || CompareString(entryString, " ", num))
				{
					num++;
					continue;
				}
				if (CompareString(entryString, "\"", num))
				{
					num++;
					int num2 = entryString.IndexOf('"', num);
					if (num2 <= 0)
					{
						return text + "-error!";
					}
					for (int i = num; i <= num2; i++)
					{
						char c = entryString[i];
						num++;
						text += c.ToString();
					}
					text += _vfoSettings.LogSeparator;
					continue;
				}
				return text + "-error!";
			}
			return text;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			panel1 = new System.Windows.Forms.Panel();
			levelLabel = new System.Windows.Forms.Label();
			recordLabel = new System.Windows.Forms.Label();
			bufferUseBar = new System.Windows.Forms.ProgressBar();
			configButton = new System.Windows.Forms.Button();
			squelchOpenLabel = new System.Windows.Forms.Label();
			stereoLabel = new System.Windows.Forms.Label();
			lockCarrierCheckBox = new System.Windows.Forms.CheckBox();
			antiFadingCheckBox = new System.Windows.Forms.CheckBox();
			fmStereoCheckBox = new System.Windows.Forms.CheckBox();
			groupBox1 = new System.Windows.Forms.GroupBox();
			volumeTrackBar = new System.Windows.Forms.TrackBar();
			outputSelectComboBox = new System.Windows.Forms.ComboBox();
			rightCheckBox = new System.Windows.Forms.CheckBox();
			leftCheckBox = new System.Windows.Forms.CheckBox();
			filterAudioCheckBox = new System.Windows.Forms.CheckBox();
			frequencySetButton = new System.Windows.Forms.Button();
			squelchCheckBox = new System.Windows.Forms.CheckBox();
			bandwidthLabel = new System.Windows.Forms.Label();
			enableCheckBox = new System.Windows.Forms.CheckBox();
			nfmRadioButton = new System.Windows.Forms.RadioButton();
			amRadioButton = new System.Windows.Forms.RadioButton();
			lsbRadioButton = new System.Windows.Forms.RadioButton();
			usbRadioButton = new System.Windows.Forms.RadioButton();
			wfmRadioButton = new System.Windows.Forms.RadioButton();
			dsbRadioButton = new System.Windows.Forms.RadioButton();
			cwRadioButton = new System.Windows.Forms.RadioButton();
			rawRadioButton = new System.Windows.Forms.RadioButton();
			bandwidtNumericUpDown = new System.Windows.Forms.NumericUpDown();
			frequencyLabel = new System.Windows.Forms.Label();
			squelchNumericUpDown = new System.Windows.Forms.NumericUpDown();
			displayUpdateTimer = new System.Windows.Forms.Timer(components);
			logTimer = new System.Windows.Forms.Timer(components);
			panel1.SuspendLayout();
			groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)volumeTrackBar).BeginInit();
			((System.ComponentModel.ISupportInitialize)bandwidtNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)squelchNumericUpDown).BeginInit();
			SuspendLayout();
			panel1.Controls.Add(levelLabel);
			panel1.Controls.Add(recordLabel);
			panel1.Controls.Add(bufferUseBar);
			panel1.Controls.Add(configButton);
			panel1.Controls.Add(squelchOpenLabel);
			panel1.Controls.Add(stereoLabel);
			panel1.Controls.Add(lockCarrierCheckBox);
			panel1.Controls.Add(antiFadingCheckBox);
			panel1.Controls.Add(fmStereoCheckBox);
			panel1.Controls.Add(groupBox1);
			panel1.Controls.Add(filterAudioCheckBox);
			panel1.Controls.Add(frequencySetButton);
			panel1.Controls.Add(squelchCheckBox);
			panel1.Controls.Add(bandwidthLabel);
			panel1.Controls.Add(enableCheckBox);
			panel1.Controls.Add(nfmRadioButton);
			panel1.Controls.Add(amRadioButton);
			panel1.Controls.Add(lsbRadioButton);
			panel1.Controls.Add(usbRadioButton);
			panel1.Controls.Add(wfmRadioButton);
			panel1.Controls.Add(dsbRadioButton);
			panel1.Controls.Add(cwRadioButton);
			panel1.Controls.Add(rawRadioButton);
			panel1.Controls.Add(bandwidtNumericUpDown);
			panel1.Controls.Add(frequencyLabel);
			panel1.Controls.Add(squelchNumericUpDown);
			panel1.Location = new System.Drawing.Point(0, 0);
			panel1.Name = "panel1";
			panel1.Size = new System.Drawing.Size(217, 325);
			panel1.TabIndex = 7;
			levelLabel.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			levelLabel.Location = new System.Drawing.Point(163, 4);
			levelLabel.Name = "levelLabel";
			levelLabel.Size = new System.Drawing.Size(51, 14);
			levelLabel.TabIndex = 33;
			levelLabel.Text = "0 dbFS";
			levelLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			recordLabel.AutoSize = true;
			recordLabel.ForeColor = System.Drawing.Color.Red;
			recordLabel.Location = new System.Drawing.Point(160, 194);
			recordLabel.Name = "recordLabel";
			recordLabel.Size = new System.Drawing.Size(42, 13);
			recordLabel.TabIndex = 32;
			recordLabel.Text = "Record";
			recordLabel.Visible = false;
			bufferUseBar.Location = new System.Drawing.Point(85, 274);
			bufferUseBar.Name = "bufferUseBar";
			bufferUseBar.Size = new System.Drawing.Size(123, 23);
			bufferUseBar.TabIndex = 8;
			configButton.Location = new System.Drawing.Point(4, 274);
			configButton.Name = "configButton";
			configButton.Size = new System.Drawing.Size(75, 23);
			configButton.TabIndex = 31;
			configButton.Text = "Config";
			configButton.UseVisualStyleBackColor = true;
			configButton.Click += new System.EventHandler(configButton_Click);
			squelchOpenLabel.AutoSize = true;
			squelchOpenLabel.Location = new System.Drawing.Point(118, 4);
			squelchOpenLabel.Name = "squelchOpenLabel";
			squelchOpenLabel.Size = new System.Drawing.Size(46, 13);
			squelchOpenLabel.TabIndex = 30;
			squelchOpenLabel.Text = "Squelch";
			squelchOpenLabel.Visible = false;
			stereoLabel.AutoSize = true;
			stereoLabel.Location = new System.Drawing.Point(60, 4);
			stereoLabel.Name = "stereoLabel";
			stereoLabel.Size = new System.Drawing.Size(56, 13);
			stereoLabel.TabIndex = 29;
			stereoLabel.Text = "(((Stereo)))";
			stereoLabel.Visible = false;
			lockCarrierCheckBox.AutoSize = true;
			lockCarrierCheckBox.Location = new System.Drawing.Point(112, 147);
			lockCarrierCheckBox.Name = "lockCarrierCheckBox";
			lockCarrierCheckBox.Size = new System.Drawing.Size(82, 17);
			lockCarrierCheckBox.TabIndex = 28;
			lockCarrierCheckBox.Text = "Lock carrier";
			lockCarrierCheckBox.UseVisualStyleBackColor = true;
			lockCarrierCheckBox.CheckedChanged += new System.EventHandler(lockCarrierCheckBox_CheckedChanged);
			antiFadingCheckBox.AutoSize = true;
			antiFadingCheckBox.Location = new System.Drawing.Point(112, 170);
			antiFadingCheckBox.Name = "antiFadingCheckBox";
			antiFadingCheckBox.Size = new System.Drawing.Size(79, 17);
			antiFadingCheckBox.TabIndex = 8;
			antiFadingCheckBox.Text = "Anti-Fading";
			antiFadingCheckBox.UseVisualStyleBackColor = true;
			antiFadingCheckBox.CheckedChanged += new System.EventHandler(antiFadingCheckBox_CheckedChanged);
			fmStereoCheckBox.AutoSize = true;
			fmStereoCheckBox.Location = new System.Drawing.Point(5, 170);
			fmStereoCheckBox.Name = "fmStereoCheckBox";
			fmStereoCheckBox.Size = new System.Drawing.Size(75, 17);
			fmStereoCheckBox.TabIndex = 9;
			fmStereoCheckBox.Text = "FM Stereo";
			fmStereoCheckBox.UseVisualStyleBackColor = true;
			fmStereoCheckBox.CheckedChanged += new System.EventHandler(fmStereoCheckBox_CheckedChanged);
			groupBox1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			groupBox1.Controls.Add(volumeTrackBar);
			groupBox1.Controls.Add(outputSelectComboBox);
			groupBox1.Controls.Add(rightCheckBox);
			groupBox1.Controls.Add(leftCheckBox);
			groupBox1.Location = new System.Drawing.Point(5, 193);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new System.Drawing.Size(205, 77);
			groupBox1.TabIndex = 27;
			groupBox1.TabStop = false;
			groupBox1.Text = "Output";
			volumeTrackBar.AutoSize = false;
			volumeTrackBar.Location = new System.Drawing.Point(81, 46);
			volumeTrackBar.Maximum = 100;
			volumeTrackBar.Name = "volumeTrackBar";
			volumeTrackBar.Size = new System.Drawing.Size(118, 25);
			volumeTrackBar.TabIndex = 4;
			volumeTrackBar.TickFrequency = 10;
			volumeTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
			volumeTrackBar.Value = 50;
			volumeTrackBar.Scroll += new System.EventHandler(volumeTrackBar_Scroll);
			outputSelectComboBox.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			outputSelectComboBox.BackColor = System.Drawing.SystemColors.Menu;
			outputSelectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			outputSelectComboBox.DropDownWidth = 300;
			outputSelectComboBox.FormattingEnabled = true;
			outputSelectComboBox.Location = new System.Drawing.Point(7, 19);
			outputSelectComboBox.Name = "outputSelectComboBox";
			outputSelectComboBox.Size = new System.Drawing.Size(193, 21);
			outputSelectComboBox.TabIndex = 3;
			outputSelectComboBox.SelectedIndexChanged += new System.EventHandler(outputSelectComboBox_SelectedIndexChanged);
			rightCheckBox.AutoSize = true;
			rightCheckBox.Location = new System.Drawing.Point(41, 46);
			rightCheckBox.Name = "rightCheckBox";
			rightCheckBox.Size = new System.Drawing.Size(34, 17);
			rightCheckBox.TabIndex = 1;
			rightCheckBox.Text = "R";
			rightCheckBox.UseVisualStyleBackColor = true;
			rightCheckBox.CheckedChanged += new System.EventHandler(rightCheckBox_CheckedChanged);
			leftCheckBox.AutoSize = true;
			leftCheckBox.Location = new System.Drawing.Point(7, 46);
			leftCheckBox.Name = "leftCheckBox";
			leftCheckBox.Size = new System.Drawing.Size(32, 17);
			leftCheckBox.TabIndex = 0;
			leftCheckBox.Text = "L";
			leftCheckBox.UseVisualStyleBackColor = true;
			leftCheckBox.CheckedChanged += new System.EventHandler(leftCheckBox_CheckedChanged);
			filterAudioCheckBox.AutoSize = true;
			filterAudioCheckBox.Location = new System.Drawing.Point(5, 147);
			filterAudioCheckBox.Name = "filterAudioCheckBox";
			filterAudioCheckBox.Size = new System.Drawing.Size(77, 17);
			filterAudioCheckBox.TabIndex = 26;
			filterAudioCheckBox.Text = "Filter audio";
			filterAudioCheckBox.UseVisualStyleBackColor = true;
			filterAudioCheckBox.CheckedChanged += new System.EventHandler(filterAudioCheckBox_CheckedChanged);
			frequencySetButton.Location = new System.Drawing.Point(5, 23);
			frequencySetButton.Name = "frequencySetButton";
			frequencySetButton.Size = new System.Drawing.Size(48, 23);
			frequencySetButton.TabIndex = 25;
			frequencySetButton.Text = "Set";
			frequencySetButton.UseVisualStyleBackColor = true;
			frequencySetButton.Click += new System.EventHandler(frequencySetButton_Click);
			squelchCheckBox.AutoSize = true;
			squelchCheckBox.Location = new System.Drawing.Point(5, 98);
			squelchCheckBox.Name = "squelchCheckBox";
			squelchCheckBox.Size = new System.Drawing.Size(65, 17);
			squelchCheckBox.TabIndex = 24;
			squelchCheckBox.Text = "Squelch";
			squelchCheckBox.UseVisualStyleBackColor = true;
			squelchCheckBox.CheckedChanged += new System.EventHandler(squelchCheckBox_CheckedChanged);
			bandwidthLabel.AutoSize = true;
			bandwidthLabel.Location = new System.Drawing.Point(109, 99);
			bandwidthLabel.Name = "bandwidthLabel";
			bandwidthLabel.Size = new System.Drawing.Size(57, 13);
			bandwidthLabel.TabIndex = 19;
			bandwidthLabel.Text = "Bandwidth";
			enableCheckBox.AutoSize = true;
			enableCheckBox.Location = new System.Drawing.Point(5, 3);
			enableCheckBox.Name = "enableCheckBox";
			enableCheckBox.Size = new System.Drawing.Size(59, 17);
			enableCheckBox.TabIndex = 8;
			enableCheckBox.Text = "Enable";
			enableCheckBox.UseVisualStyleBackColor = true;
			enableCheckBox.CheckedChanged += new System.EventHandler(enableCheckBox_CheckedChanged);
			nfmRadioButton.AutoSize = true;
			nfmRadioButton.Location = new System.Drawing.Point(5, 52);
			nfmRadioButton.Name = "nfmRadioButton";
			nfmRadioButton.Size = new System.Drawing.Size(48, 17);
			nfmRadioButton.TabIndex = 8;
			nfmRadioButton.TabStop = true;
			nfmRadioButton.Text = "NFM";
			nfmRadioButton.UseVisualStyleBackColor = true;
			nfmRadioButton.CheckedChanged += new System.EventHandler(nfmRadioButton_CheckedChanged);
			amRadioButton.AutoSize = true;
			amRadioButton.Location = new System.Drawing.Point(59, 52);
			amRadioButton.Name = "amRadioButton";
			amRadioButton.Size = new System.Drawing.Size(41, 17);
			amRadioButton.TabIndex = 9;
			amRadioButton.TabStop = true;
			amRadioButton.Text = "AM";
			amRadioButton.UseVisualStyleBackColor = true;
			amRadioButton.CheckedChanged += new System.EventHandler(amRadioButton_CheckedChanged);
			lsbRadioButton.AutoSize = true;
			lsbRadioButton.Location = new System.Drawing.Point(112, 52);
			lsbRadioButton.Name = "lsbRadioButton";
			lsbRadioButton.Size = new System.Drawing.Size(45, 17);
			lsbRadioButton.TabIndex = 10;
			lsbRadioButton.TabStop = true;
			lsbRadioButton.Text = "LSB";
			lsbRadioButton.UseVisualStyleBackColor = true;
			lsbRadioButton.CheckedChanged += new System.EventHandler(lsbRadioButton_CheckedChanged);
			usbRadioButton.AutoSize = true;
			usbRadioButton.Location = new System.Drawing.Point(163, 52);
			usbRadioButton.Name = "usbRadioButton";
			usbRadioButton.Size = new System.Drawing.Size(47, 17);
			usbRadioButton.TabIndex = 11;
			usbRadioButton.TabStop = true;
			usbRadioButton.Text = "USB";
			usbRadioButton.UseVisualStyleBackColor = true;
			usbRadioButton.CheckedChanged += new System.EventHandler(usbRadioButton_CheckedChanged);
			wfmRadioButton.AutoSize = true;
			wfmRadioButton.Location = new System.Drawing.Point(5, 75);
			wfmRadioButton.Name = "wfmRadioButton";
			wfmRadioButton.Size = new System.Drawing.Size(51, 17);
			wfmRadioButton.TabIndex = 12;
			wfmRadioButton.TabStop = true;
			wfmRadioButton.Text = "WFM";
			wfmRadioButton.UseVisualStyleBackColor = true;
			wfmRadioButton.CheckedChanged += new System.EventHandler(wfmRadioButton_CheckedChanged);
			dsbRadioButton.AutoSize = true;
			dsbRadioButton.Location = new System.Drawing.Point(59, 75);
			dsbRadioButton.Name = "dsbRadioButton";
			dsbRadioButton.Size = new System.Drawing.Size(47, 17);
			dsbRadioButton.TabIndex = 13;
			dsbRadioButton.TabStop = true;
			dsbRadioButton.Text = "DSB";
			dsbRadioButton.UseVisualStyleBackColor = true;
			dsbRadioButton.CheckedChanged += new System.EventHandler(dsbRadioButton_CheckedChanged);
			cwRadioButton.AutoSize = true;
			cwRadioButton.Location = new System.Drawing.Point(112, 75);
			cwRadioButton.Name = "cwRadioButton";
			cwRadioButton.Size = new System.Drawing.Size(43, 17);
			cwRadioButton.TabIndex = 14;
			cwRadioButton.TabStop = true;
			cwRadioButton.Text = "CW";
			cwRadioButton.UseVisualStyleBackColor = true;
			cwRadioButton.CheckedChanged += new System.EventHandler(cwRadioButton_CheckedChanged);
			rawRadioButton.AutoSize = true;
			rawRadioButton.Location = new System.Drawing.Point(163, 75);
			rawRadioButton.Name = "rawRadioButton";
			rawRadioButton.Size = new System.Drawing.Size(51, 17);
			rawRadioButton.TabIndex = 15;
			rawRadioButton.TabStop = true;
			rawRadioButton.Text = "RAW";
			rawRadioButton.UseVisualStyleBackColor = true;
			rawRadioButton.CheckedChanged += new System.EventHandler(rawRadioButton_CheckedChanged);
			bandwidtNumericUpDown.Increment = new decimal(new int[4]
			{
				100,
				0,
				0,
				0
			});
			bandwidtNumericUpDown.Location = new System.Drawing.Point(112, 121);
			bandwidtNumericUpDown.Maximum = new decimal(new int[4]
			{
				500000,
				0,
				0,
				0
			});
			bandwidtNumericUpDown.Name = "bandwidtNumericUpDown";
			bandwidtNumericUpDown.Size = new System.Drawing.Size(96, 20);
			bandwidtNumericUpDown.TabIndex = 16;
			bandwidtNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			bandwidtNumericUpDown.ValueChanged += new System.EventHandler(bandwidtNumericUpDown_ValueChanged);
			frequencyLabel.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			frequencyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
			frequencyLabel.Location = new System.Drawing.Point(59, 22);
			frequencyLabel.Name = "frequencyLabel";
			frequencyLabel.Size = new System.Drawing.Size(149, 24);
			frequencyLabel.TabIndex = 17;
			frequencyLabel.Text = "0.0 MHz";
			frequencyLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			squelchNumericUpDown.Location = new System.Drawing.Point(5, 121);
			squelchNumericUpDown.Name = "squelchNumericUpDown";
			squelchNumericUpDown.Size = new System.Drawing.Size(90, 20);
			squelchNumericUpDown.TabIndex = 18;
			squelchNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			squelchNumericUpDown.ValueChanged += new System.EventHandler(squelchNumericUpDown_ValueChanged);
			displayUpdateTimer.Enabled = true;
			displayUpdateTimer.Tick += new System.EventHandler(displayUpdateTimer_Tick);
			logTimer.Enabled = true;
			logTimer.Tick += new System.EventHandler(logTimer_Tick);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(panel1);
			base.Name = "VFOPanel";
			base.Size = new System.Drawing.Size(217, 328);
			panel1.ResumeLayout(performLayout: false);
			panel1.PerformLayout();
			groupBox1.ResumeLayout(performLayout: false);
			groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)volumeTrackBar).EndInit();
			((System.ComponentModel.ISupportInitialize)bandwidtNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)squelchNumericUpDown).EndInit();
			ResumeLayout(performLayout: false);
		}
	}
}

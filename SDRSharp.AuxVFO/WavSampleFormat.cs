namespace SDRSharp.AuxVFO
{
	public enum WavSampleFormat
	{
		PCM8Stereo,
		PCM16Stereo,
		Float32,
		PCM8Mono,
		PCM16Mono
	}
}

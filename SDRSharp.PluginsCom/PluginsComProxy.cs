using System.Collections.Generic;

namespace SDRSharp.PluginsCom
{
	public class PluginsComProxy
	{
		public delegate void NewCommandAvailable(string command);

		private static string Command;

		private static List<string> AvailableCommands = new List<string>();

		public static event NewCommandAvailable CommandReady;

		public PluginsComProxy(NewCommandAvailable delegat)
		{
			CommandReady += delegat;
		}

		public void AddAvailableCommands(string command)
		{
			AvailableCommands.Add(command);
		}

		public void SetCommand(string command)
		{
			Command = command;
			CommandChanged();
		}

		public List<string> GetAvailableCommands()
		{
			return AvailableCommands;
		}

		private void CommandChanged()
		{
			if (PluginsComProxy.CommandReady != null)
			{
				PluginsComProxy.CommandReady(Command);
			}
		}
	}
}

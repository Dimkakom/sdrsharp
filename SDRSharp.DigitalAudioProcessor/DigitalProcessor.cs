using SDRSharp.Radio;

namespace SDRSharp.DigitalAudioProcessor
{
	public class DigitalProcessor : OverlapAddProcessor
	{
		public unsafe delegate void FFTReadyDelegate(Complex* buffer, int length);

		private const int FftSize = 8192;

		private readonly UnsafeBuffer _gainBuffer;

		private unsafe readonly float* _gainPtr;

		private readonly UnsafeBuffer _powerBuffer;

		private unsafe readonly float* _powerBufferPtr;

		private readonly UnsafeBuffer _noiseGains;

		private unsafe readonly float* _noiseGainsPtr;

		public bool ShowSpectrumAfterProcess
		{
			get;
			set;
		}

		public bool FilterEnable
		{
			get;
			set;
		}

		public event FFTReadyDelegate FFTReady;

		public unsafe DigitalProcessor(int fftSize = 8192)
			: base(fftSize)
		{
			_gainBuffer = UnsafeBuffer.Create(fftSize, 4);
			_gainPtr = (float*)(void*)_gainBuffer;
			_powerBuffer = UnsafeBuffer.Create(fftSize, 4);
			_powerBufferPtr = (float*)(void*)_powerBuffer;
			_noiseGains = UnsafeBuffer.Create(fftSize, 4);
			_noiseGainsPtr = (float*)(void*)_noiseGains;
		}

		public unsafe void BuildFilter(float* gainBuffer)
		{
			Utils.Memcpy(_gainPtr, gainBuffer, 32768);
		}

		protected unsafe override void ProcessFft(Complex* buffer, int length)
		{
			if (this.FFTReady != null && !ShowSpectrumAfterProcess)
			{
				this.FFTReady(buffer, length);
			}
			if (FilterEnable)
			{
				for (int i = 0; i < length; i++)
				{
					Complex* ptr = buffer + i;
					*ptr *= _gainPtr[i];
				}
			}
			if (this.FFTReady != null && ShowSpectrumAfterProcess)
			{
				this.FFTReady(buffer, length);
			}
		}
	}
}

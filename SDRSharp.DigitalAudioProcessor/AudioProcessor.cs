using SDRSharp.Radio;

namespace SDRSharp.DigitalAudioProcessor
{
	public class AudioProcessor : IRealProcessor, IStreamProcessor, IBaseProcessor
	{
		public const int FFTSize = 8192;

		private DigitalProcessor _digitalProcessorRight;

		private DigitalProcessor _digitalProcessorLeft;

		private double _sampleRate;

		private bool _enabled;

		private bool _needNewFilter;

		private unsafe float* _gainsBufferPtr;

		public double SampleRate
		{
			get
			{
				return _sampleRate;
			}
			set
			{
				_sampleRate = value;
			}
		}

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public bool Mono
		{
			get;
			set;
		}

		public bool FilterEnable
		{
			get;
			set;
		}

		public bool ShowSpectrumAfterProcess
		{
			get;
			set;
		}

		public event DigitalProcessor.FFTReadyDelegate FFTReady;

		public unsafe void Process(float* buffer, int length)
		{
			if (_sampleRate != 0.0)
			{
				if (_digitalProcessorRight == null)
				{
					_digitalProcessorRight = new DigitalProcessor();
					_digitalProcessorRight.FFTReady += this.FFTReady;
				}
				if (_digitalProcessorLeft == null)
				{
					_digitalProcessorLeft = new DigitalProcessor();
				}
				if (_needNewFilter)
				{
					_needNewFilter = false;
					_digitalProcessorRight.BuildFilter(_gainsBufferPtr);
					_digitalProcessorLeft.BuildFilter(_gainsBufferPtr);
				}
				_digitalProcessorRight.ShowSpectrumAfterProcess = ShowSpectrumAfterProcess;
				_digitalProcessorRight.FilterEnable = FilterEnable;
				_digitalProcessorRight.Process(buffer, length, 2);
				if (Mono)
				{
					CopyMonoToStereo(buffer, length);
					return;
				}
				_digitalProcessorLeft.FilterEnable = FilterEnable;
				_digitalProcessorLeft.Process(buffer + 1, length, 2);
			}
		}

		private unsafe void CopyMonoToStereo(float* buffer, int length)
		{
			for (int i = 0; i < length; i += 2)
			{
				buffer[i + 1] = buffer[i];
			}
		}

		public unsafe void UpdateFilter(float* gainsBuffer)
		{
			_gainsBufferPtr = gainsBuffer;
			_needNewFilter = true;
		}
	}
}

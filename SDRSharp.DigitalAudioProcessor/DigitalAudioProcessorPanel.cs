using SDRSharp.Common;
using SDRSharp.PanView;
using SDRSharp.Radio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace SDRSharp.DigitalAudioProcessor
{
	public class DigitalAudioProcessorPanel : UserControl
	{
		private const int fftBins = 8192;

		private AudioProcessor _audioProcessor;

		private ISharpControl _control;

		private SettingsPersister _settings;

		private List<FilterEntry> _filters;

		private const int FFTTimerInterval = 10;

		private int _fftBins;

		private bool _fftSpectrumAvailable;

		private UnsafeBuffer _fftBuffer;

		private unsafe Complex* _fftPtr;

		private UnsafeBuffer _fftSpectrum;

		private unsafe float* _fftSpectrumPtr;

		private UnsafeBuffer _gainsBuffer;

		private unsafe float* _gainsBufferPtr;

		private double _sampleRate;

		private Thread _fftThread;

		private bool _fftThreadRunning;

		private System.Windows.Forms.Timer _fftDisplayTimer;

		private SpectrumAnalyzer _spectrumAnalyzer;

		private readonly SharpEvent _fftEvent = new SharpEvent(initialState: false);

		private readonly ComplexFifoStream _iqStream = new ComplexFifoStream(BlockMode.BlockingRead);

		private float _correction;

		private int _highFilterFrequency;

		private int _lowFilterFrequency;

		private bool _needReinit;

		private int _currentOffset;

		private int _currentRange;

		private bool _moveLowFilterFrequency;

		private bool _moveHighFilterFrequency;

		private bool _moveOffset;

		private bool _mouseKeyDown;

		private bool _showCursor;

		private string _cursorText;

		private Point _cursorPosition;

		private Point _lastCursorPosition;

		private Rectangle _filterEnableRectangle;

		private Rectangle _beforeEnableRectangle;

		private RectangleF _filterLowRectangle;

		private RectangleF _filterHighRectangle;

		private Rectangle _deemphasisEnableRectangle;

		private Rectangle _button1Rectangle;

		private Rectangle _button2Rectangle;

		private Rectangle _button3Rectangle;

		private Rectangle _button4Rectangle;

		private Rectangle _button5Rectangle;

		private Rectangle _button6Rectangle;

		private bool _filterClick;

		private bool _beforeClick;

		private bool _button1Click;

		private bool _button2Click;

		private bool _button3Click;

		private bool _button4Click;

		private bool _button5Click;

		private bool _button6Click;

		private bool _deemphasisClick;

		private int _activeButton;

		private IContainer components;

		private BindingSource notchFilterEntryBindingSource;

		private System.Windows.Forms.Timer displayTimer;

		private CheckBox filterEnableCheckBox;

		private GroupBox groupBox5;

		private RadioButton afterRadioButton;

		private RadioButton beforeRadioButton;

		private CheckBox audioSpectrumCheckBox;

		private ComboBox spectrumPositionComboBox;

		private Label label7;

		private GroupBox groupBox1;

		private Label label2;

		private Label label1;

		private NumericUpDown lowNumericUpDown;

		private NumericUpDown hiNumericUpDown;

		private TrackBar deemphasisFormTrackBar;

		private CheckBox deemphasisCheckBox;

		private Button button6;

		private Button button5;

		private Button button4;

		private Button button3;

		private Button button2;

		private Button button1;

		private TextBox nameTextBox;

		public unsafe DigitalAudioProcessorPanel(AudioProcessor ifProcessor, ISharpControl control)
		{
			InitializeComponent();
			_audioProcessor = ifProcessor;
			_audioProcessor.FFTReady += _ifProcessor_FFTReady;
			_control = control;
			_control.PropertyChanged += PropertyChangedHandler;
			try
			{
				_settings = new SettingsPersister();
			}
			catch (Exception ex)
			{
				MessageBox.Show("filters.xml file error", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			IfProcessorEnabled();
			_fftDisplayTimer = new System.Windows.Forms.Timer();
			_fftDisplayTimer.Tick += fftDisplayTimer_Tick;
			_fftDisplayTimer.Interval = 10;
			_fftBins = 8192;
			InitFFTBuffers();
			_spectrumAnalyzer = new SpectrumAnalyzer();
			_spectrumAnalyzer.Dock = DockStyle.Fill;
			_spectrumAnalyzer.Margin = new Padding(0, 0, 0, 0);
			_spectrumAnalyzer.DisplayRange = 130;
			_spectrumAnalyzer.EnableFilter = false;
			_spectrumAnalyzer.EnableHotTracking = false;
			_spectrumAnalyzer.EnableSideFilterResize = false;
			_spectrumAnalyzer.EnableFilterMove = false;
			_spectrumAnalyzer.EnableFrequencyMarker = false;
			_spectrumAnalyzer.BandType = BandType.Center;
			_spectrumAnalyzer.StepSize = 1;
			_spectrumAnalyzer.UseSmoothing = true;
			_spectrumAnalyzer.Attack = 0.9f;
			_spectrumAnalyzer.Decay = 0.6f;
			_spectrumAnalyzer.StatusText = "";
			_spectrumAnalyzer.FrequencyChanged += spectrumAnalyzer_FrequencyChanged;
			_spectrumAnalyzer.CenterFrequencyChanged += spectrumAnalyzer_CenterFrequencyChanged;
			_spectrumAnalyzer.VisibleChanged += spectrumAnalyzer_VisibleChanged;
			_spectrumAnalyzer.CustomPaint += spectrumCustomPaint;
			_spectrumAnalyzer.MouseDown += _spectrumAnalyzer_MouseDown;
			_spectrumAnalyzer.MouseUp += _spectrumAnalyzer_MouseUp;
			_spectrumAnalyzer.MouseMove += _spectrumAnalyzer_MouseMove;
			_spectrumAnalyzer.MouseLeave += _spectrumAnalyzer_MouseLeave;
			_spectrumAnalyzer.MouseWheel += _spectrumAnalyzer_MouseWheel;
			_spectrumAnalyzer.Visible = false;
			spectrumAnalyzer_VisibleChanged(null, null);
			_filters = _settings.ReadStoredFilters();
			_activeButton = Utils.GetIntSetting("ActiveFiltersButton", 0);
			ProcessFilters();
			audioSpectrumCheckBox.Checked = Utils.GetBooleanSetting("AudioSpectrumShow");
			spectrumPositionComboBox.SelectedIndex = Utils.GetIntSetting("AudioSpectrumPosition", 1);
			beforeRadioButton.Checked = Utils.GetBooleanSetting("ShowAudioSpectrumBefore");
			beforeRadioButton_CheckedChanged(null, null);
			_control.RegisterFrontControl(_spectrumAnalyzer, (PluginPosition)spectrumPositionComboBox.SelectedIndex);
		}

		private void ProcessFilters()
		{
			if (_filters.Count < 7)
			{
				_filters.Clear();
				_filters.Add(new FilterEntry());
				_filters[0].Name = "No name";
				_filters[0].Low = 300;
				_filters[0].Hi = 3500;
				_filters[0].FilterEnable = true;
				_filters[0].Deemphasis = false;
				_filters[0].DeemphForm = 1;
				_filters.Add(new FilterEntry());
				_filters[1].Name = "3k";
				_filters[1].Low = 300;
				_filters[1].Hi = 3000;
				_filters[1].FilterEnable = true;
				_filters[1].Deemphasis = false;
				_filters[1].DeemphForm = 1;
				_filters.Add(new FilterEntry());
				_filters[2].Name = "w3k";
				_filters[2].Low = 100;
				_filters[2].Hi = 3000;
				_filters[2].FilterEnable = true;
				_filters[2].Deemphasis = false;
				_filters[2].DeemphForm = 1;
				_filters.Add(new FilterEntry());
				_filters[3].Name = "5k";
				_filters[3].Low = 300;
				_filters[3].Hi = 5000;
				_filters[3].FilterEnable = true;
				_filters[3].Deemphasis = false;
				_filters[3].DeemphForm = 1;
				_filters.Add(new FilterEntry());
				_filters[4].Name = "w5k";
				_filters[4].Low = 100;
				_filters[4].Hi = 5000;
				_filters[4].FilterEnable = true;
				_filters[4].Deemphasis = false;
				_filters[4].DeemphForm = 1;
				_filters.Add(new FilterEntry());
				_filters[5].Name = "w10k";
				_filters[5].Low = 20;
				_filters[5].Hi = 10000;
				_filters[5].FilterEnable = true;
				_filters[5].Deemphasis = false;
				_filters[5].DeemphForm = 1;
				_filters.Add(new FilterEntry());
				_filters[6].Name = "w16k";
				_filters[6].Low = 20;
				_filters[6].Hi = 16000;
				_filters[6].FilterEnable = true;
				_filters[6].Deemphasis = false;
				_filters[6].DeemphForm = 1;
			}
			UpdateButtonsText();
			SelectFilter();
		}

		public void StoreSettings()
		{
			if (_fftThreadRunning)
			{
				Stop();
			}
			_settings.PersistStoredFilters(_filters);
			Utils.SaveSetting("ActiveFiltersButton", _activeButton);
			Utils.SaveSetting("AudioSpectrumShow", audioSpectrumCheckBox.Checked);
			Utils.SaveSetting("AudioSpectrumPosition", spectrumPositionComboBox.SelectedIndex);
			Utils.SaveSetting("ShowAudioSpectrumBefore", beforeRadioButton.Checked);
		}

		private string GetFrequencyDisplay(long frequency)
		{
			long num = 0L;
			try
			{
				num = Math.Abs(frequency);
			}
			catch
			{
				num = 0L;
			}
			if (num == 0)
			{
				return "DC";
			}
			if (num > 1500000000)
			{
				return $"{(double)frequency / 1000000000.0:#,0.000 000} GHz";
			}
			if (num > 30000000)
			{
				return $"{(double)frequency / 1000000.0:0,0.000###} MHz";
			}
			if (num > 1000)
			{
				return $"{(double)frequency / 1000.0:#,#.###} kHz";
			}
			return frequency.ToString() + " Hz";
		}

		private void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			string propertyName = e.PropertyName;
			switch (propertyName)
			{
			case "StartRadio":
				IfProcessorEnabled();
				ConfigureSpectrumAnalyzer();
				CreateFilter();
				break;
			case "StopRadio":
				IfProcessorEnabled();
				break;
			case "DetectorType":
			case "FilterBandwidth":
				ConfigureSpectrumAnalyzer();
				CreateFilter();
				break;
			case "FFTRange":
			case "FFTOffset":
			case "SAttack":
			case "SDecay":
				ConfigureSpectrumAnalyzer();
				break;
			}
		}

		private void _spectrumAnalyzer_MouseWheel(object sender, MouseEventArgs e)
		{
		}

		private void _spectrumAnalyzer_MouseLeave(object sender, EventArgs e)
		{
			_showCursor = false;
			_cursorText = "";
		}

		private void _spectrumAnalyzer_MouseMove(object sender, MouseEventArgs e)
		{
			_cursorPosition.X = e.X;
			_cursorPosition.Y = e.Y;
			if (_lastCursorPosition == _cursorPosition)
			{
				SetCursors();
			}
			else if (!_mouseKeyDown)
			{
				_moveHighFilterFrequency = (_moveLowFilterFrequency = false);
				_moveOffset = false;
				_filterClick = (_beforeClick = (_deemphasisClick = false));
				_button1Click = (_button2Click = (_button3Click = (_button4Click = (_button5Click = (_button6Click = false)))));
				_showCursor = false;
				_cursorText = "";
				if (_cursorPosition.Y > 30 && _cursorPosition.Y < _spectrumAnalyzer.Height - 30 && _cursorPosition.X > 30 && _cursorPosition.X < _spectrumAnalyzer.Width - 30)
				{
					if (filterEnableCheckBox.Checked)
					{
						if (_filterLowRectangle.Contains(_cursorPosition))
						{
							_moveLowFilterFrequency = true;
						}
						else if (_filterHighRectangle.Contains(_cursorPosition))
						{
							_moveHighFilterFrequency = true;
						}
					}
					_showCursor = (!_moveHighFilterFrequency && !_moveLowFilterFrequency);
					if (_showCursor)
					{
						_cursorText = GetFrequencyDisplay(_spectrumAnalyzer.PointToFrequency(_cursorPosition.X));
					}
				}
				else if (_cursorPosition.Y < 30 && _cursorPosition.Y > 0)
				{
					if (_filterEnableRectangle.Contains(_cursorPosition))
					{
						_filterClick = true;
					}
					else if (_beforeEnableRectangle.Contains(_cursorPosition))
					{
						_beforeClick = true;
					}
					else if (_deemphasisEnableRectangle.Contains(_cursorPosition))
					{
						_deemphasisClick = true;
					}
					else if (_button1Rectangle.Contains(_cursorPosition))
					{
						_button1Click = true;
					}
					else if (_button2Rectangle.Contains(_cursorPosition))
					{
						_button2Click = true;
					}
					else if (_button3Rectangle.Contains(_cursorPosition))
					{
						_button3Click = true;
					}
					else if (_button4Rectangle.Contains(_cursorPosition))
					{
						_button4Click = true;
					}
					else if (_button5Rectangle.Contains(_cursorPosition))
					{
						_button5Click = true;
					}
					else if (_button6Rectangle.Contains(_cursorPosition))
					{
						_button6Click = true;
					}
				}
				else if (_cursorPosition.X < 30 && _cursorPosition.X > 0)
				{
					_moveOffset = true;
					_cursorText = "offset / range";
				}
				SetCursors();
			}
			else
			{
				if (!_mouseKeyDown)
				{
					return;
				}
				_showCursor = false;
				if (_moveLowFilterFrequency)
				{
					int num = (int)_spectrumAnalyzer.PointToFrequency(e.X);
					if ((decimal)num < lowNumericUpDown.Minimum)
					{
						num = (int)lowNumericUpDown.Minimum;
					}
					if ((decimal)num > lowNumericUpDown.Maximum)
					{
						num = (int)lowNumericUpDown.Maximum;
					}
					lowNumericUpDown.Value = num;
					_cursorText = GetFrequencyDisplay(num);
				}
				else if (_moveHighFilterFrequency)
				{
					int num2 = (int)_spectrumAnalyzer.PointToFrequency(e.X);
					if ((decimal)num2 < hiNumericUpDown.Minimum)
					{
						num2 = (int)hiNumericUpDown.Minimum;
					}
					if ((decimal)num2 > hiNumericUpDown.Maximum)
					{
						num2 = (int)hiNumericUpDown.Maximum;
					}
					hiNumericUpDown.Value = num2;
					_cursorText = GetFrequencyDisplay(num2);
				}
				else if (_moveOffset)
				{
					if (Control.ModifierKeys == Keys.Shift)
					{
						_spectrumAnalyzer.DisplayRange = _currentRange + _lastCursorPosition.Y - _cursorPosition.Y;
					}
					else
					{
						_spectrumAnalyzer.DisplayOffset = _currentOffset + _cursorPosition.Y - _lastCursorPosition.Y;
					}
				}
			}
		}

		private void SetCursors()
		{
			if (_filterClick || _beforeClick || _button1Click || _button2Click || _button3Click || _button4Click || _button5Click || _button6Click || _deemphasisClick)
			{
				if (_spectrumAnalyzer.Cursor != Cursors.Hand)
				{
					_spectrumAnalyzer.Cursor = Cursors.Hand;
				}
			}
			else if (_moveHighFilterFrequency || _moveLowFilterFrequency)
			{
				if (_spectrumAnalyzer.Cursor != Cursors.SizeWE)
				{
					_spectrumAnalyzer.Cursor = Cursors.SizeWE;
				}
			}
			else if (_moveOffset)
			{
				if (_spectrumAnalyzer.Cursor != Cursors.HSplit)
				{
					_spectrumAnalyzer.Cursor = Cursors.HSplit;
				}
			}
			else if (_spectrumAnalyzer.Cursor != Cursors.Default)
			{
				_spectrumAnalyzer.Cursor = Cursors.Default;
			}
		}

		private void _spectrumAnalyzer_MouseUp(object sender, MouseEventArgs e)
		{
			_mouseKeyDown = false;
		}

		private void _spectrumAnalyzer_MouseDown(object sender, MouseEventArgs e)
		{
			if (!_fftThreadRunning)
			{
				return;
			}
			_lastCursorPosition = _cursorPosition;
			if (_moveOffset)
			{
				_currentOffset = _spectrumAnalyzer.DisplayOffset;
				_currentRange = _spectrumAnalyzer.DisplayRange;
			}
			if (_filterClick)
			{
				filterEnableCheckBox.Checked = !filterEnableCheckBox.Checked;
			}
			else if (_beforeClick)
			{
				beforeRadioButton.Checked = !beforeRadioButton.Checked;
			}
			else if (_deemphasisClick)
			{
				if (!deemphasisCheckBox.Checked)
				{
					deemphasisCheckBox.Checked = true;
					deemphasisFormTrackBar.Value = deemphasisFormTrackBar.Minimum;
				}
				else if (deemphasisFormTrackBar.Value < deemphasisFormTrackBar.Maximum)
				{
					deemphasisFormTrackBar.Value++;
				}
				else
				{
					deemphasisCheckBox.Checked = false;
				}
			}
			else if (_button1Click)
			{
				button1_Click(null, null);
			}
			else if (_button2Click)
			{
				button2_Click(null, null);
			}
			else if (_button3Click)
			{
				button3_Click(null, null);
			}
			else if (_button4Click)
			{
				button4_Click(null, null);
			}
			else if (_button5Click)
			{
				button5_Click(null, null);
			}
			else if (_button6Click)
			{
				button6_Click(null, null);
			}
			_mouseKeyDown = true;
		}

		private void spectrumAnalyzer_VisibleChanged(object sender, EventArgs e)
		{
			if (_spectrumAnalyzer.Visible)
			{
				Start();
			}
			else
			{
				Stop();
			}
			_spectrumAnalyzer.ResetSpectrum();
		}

		private void spectrumAnalyzer_FrequencyChanged(object sender, FrequencyEventArgs e)
		{
			e.Cancel = true;
		}

		private void spectrumAnalyzer_CenterFrequencyChanged(object sender, FrequencyEventArgs e)
		{
			e.Cancel = true;
		}

		private void ConfigureSpectrumAnalyzer()
		{
			_sampleRate = _audioProcessor.SampleRate;
			float num = (float)(10.0 * Math.Log10((double)_fftBins / 2.0));
			_correction = 24f - num;
			int num2 = (int)_sampleRate / 2;
			double val = (double)_control.FilterBandwidth * 0.65;
			switch (_control.DetectorType)
			{
			case DetectorType.NFM:
				_correction += 55f;
				break;
			case DetectorType.WFM:
				_correction += 55f;
				val = 20000.0;
				break;
			case DetectorType.LSB:
			case DetectorType.USB:
				_correction += 10f;
				val = (double)_control.FilterBandwidth * 1.2;
				break;
			default:
				_correction += 10f;
				break;
			}
			_spectrumAnalyzer.SpectrumWidth = (int)Math.Min(num2, val);
			_spectrumAnalyzer.Frequency = 0L;
			_spectrumAnalyzer.CenterFrequency = (long)_spectrumAnalyzer.SpectrumWidth / 2L;
			_spectrumAnalyzer.DisplayOffset = (int)_control.FFTOffset;
			if (_sampleRate > 0.0)
			{
				_spectrumAnalyzer.DisplayRange = (int)Math.Ceiling(((double)_control.FFTRange + 6.02 * Math.Log((double)_control.RFBandwidth / _sampleRate) / Math.Log(4.0)) * 0.1) * 10;
			}
			_spectrumAnalyzer.StepSize = 1;
			_spectrumAnalyzer.Attack = _control.SAttack;
			_spectrumAnalyzer.Decay = _control.SDecay;
		}

		private unsafe void InitFFTBuffers()
		{
			_fftBuffer = UnsafeBuffer.Create(_fftBins, sizeof(Complex));
			_fftSpectrum = UnsafeBuffer.Create(_fftBins, 4);
			_gainsBuffer = UnsafeBuffer.Create(_fftBins, 4);
			_fftPtr = (Complex*)(void*)_fftBuffer;
			_fftSpectrumPtr = (float*)(void*)_fftSpectrum;
			_gainsBufferPtr = (float*)(void*)_gainsBuffer;
		}

		private void Start()
		{
			_fftThreadRunning = true;
			if (_fftThread == null)
			{
				_fftThread = new Thread(ProcessFFT);
				_fftThread.Name = "Audio Processor Thread";
				_fftThread.Start();
			}
			_fftDisplayTimer.Enabled = true;
		}

		public void Stop()
		{
			_fftDisplayTimer.Enabled = false;
			_fftThreadRunning = false;
			if (_fftThread != null)
			{
				_fftEvent.Set();
				_fftThread.Join();
				_fftThread = null;
			}
		}

		private unsafe void ProcessFFT(object parameter)
		{
			while (_fftThreadRunning)
			{
				_fftEvent.WaitOne();
				Fourier.SpectrumPower(_fftPtr, _fftSpectrumPtr, _fftBins, _correction);
				_fftSpectrumAvailable = true;
			}
		}

		private unsafe void _ifProcessor_FFTReady(Complex* buffer, int length)
		{
			if (_sampleRate != _audioProcessor.SampleRate)
			{
				_needReinit = true;
			}
			if (!_fftSpectrumAvailable)
			{
				Utils.Memcpy(_fftPtr, buffer, length * sizeof(Complex));
				_fftEvent.Set();
			}
		}

		private void IfProcessorEnabled()
		{
			_audioProcessor.Enabled = (_control.IsPlaying && (filterEnableCheckBox.Checked || audioSpectrumCheckBox.Checked || deemphasisCheckBox.Checked));
			_audioProcessor.FilterEnable = (filterEnableCheckBox.Checked || deemphasisCheckBox.Checked);
		}

		private unsafe void CreateFilter()
		{
			bool @checked = filterEnableCheckBox.Checked;
			bool checked2 = deemphasisCheckBox.Checked;
			int num = FrequencyToBins(_lowFilterFrequency);
			int num2 = FrequencyToBins(_highFilterFrequency);
			float num3 = 1f;
			float num4 = 1f - (float)deemphasisFormTrackBar.Value * 0.0003f;
			for (int i = 0; i < _gainsBuffer.Length / 2; i++)
			{
				if ((i < num || i > num2) & @checked)
				{
					_gainsBufferPtr[i] = 0f;
					_gainsBufferPtr[_gainsBuffer.Length - 1 - i] = 0f;
				}
				else
				{
					_gainsBufferPtr[i] = 1f;
					_gainsBufferPtr[_gainsBuffer.Length - 1 - i] = 1f;
				}
				if (checked2)
				{
					_gainsBufferPtr[i] *= num3;
					_gainsBufferPtr[_gainsBuffer.Length - i - 1] *= num3;
					num3 *= num4;
				}
			}
			_audioProcessor.Mono = (_control.DetectorType != DetectorType.WFM);
			_audioProcessor.UpdateFilter(_gainsBufferPtr);
		}

		public int FrequencyToBins(long frequency)
		{
			int num = 0;
			num = (int)((double)_fftBins / _sampleRate * (double)frequency);
			if (num < 0)
			{
				num = 0;
			}
			if (num > _fftBins / 2)
			{
				num = _fftBins / 2;
			}
			return num;
		}

		private void displayTimer_Tick(object sender, EventArgs e)
		{
			if (_needReinit)
			{
				_needReinit = false;
				ConfigureSpectrumAnalyzer();
				CreateFilter();
			}
			button1.FlatStyle = ((_activeButton != 1) ? FlatStyle.Standard : FlatStyle.Flat);
			button2.FlatStyle = ((_activeButton != 2) ? FlatStyle.Standard : FlatStyle.Flat);
			button3.FlatStyle = ((_activeButton != 3) ? FlatStyle.Standard : FlatStyle.Flat);
			button4.FlatStyle = ((_activeButton != 4) ? FlatStyle.Standard : FlatStyle.Flat);
			button5.FlatStyle = ((_activeButton != 5) ? FlatStyle.Standard : FlatStyle.Flat);
			button6.FlatStyle = ((_activeButton != 6) ? FlatStyle.Standard : FlatStyle.Flat);
		}

		private unsafe void fftDisplayTimer_Tick(object sender, EventArgs e)
		{
			if (_control.IsPlaying && _fftSpectrumAvailable)
			{
				_fftSpectrumAvailable = false;
				double num = (double)_spectrumAnalyzer.SpectrumWidth / (_sampleRate / 2.0);
				int length = Math.Min(_fftBins / 2, (int)((double)(_fftBins / 2) * num));
				float* fftSpectrumPtr = _fftSpectrumPtr;
				_spectrumAnalyzer.Render(fftSpectrumPtr, length);
			}
		}

		private void filterEnableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_filters[_activeButton].FilterEnable = filterEnableCheckBox.Checked;
			IfProcessorEnabled();
			CreateFilter();
			lowNumericUpDown.Enabled = filterEnableCheckBox.Checked;
			hiNumericUpDown.Enabled = filterEnableCheckBox.Checked;
		}

		private void beforeRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			_audioProcessor.ShowSpectrumAfterProcess = !beforeRadioButton.Checked;
			afterRadioButton.Checked = !beforeRadioButton.Checked;
		}

		private void audioSpectrumCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_spectrumAnalyzer.Visible = audioSpectrumCheckBox.Checked;
			IfProcessorEnabled();
		}

		private void lowNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_lowFilterFrequency = (int)lowNumericUpDown.Value;
			_filters[_activeButton].Low = _lowFilterFrequency;
			CreateFilter();
		}

		private void hiNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			_highFilterFrequency = (int)hiNumericUpDown.Value;
			_filters[_activeButton].Hi = _highFilterFrequency;
			CreateFilter();
		}

		private void deemphasisCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_filters[_activeButton].Deemphasis = deemphasisCheckBox.Checked;
			deemphasisFormTrackBar.Enabled = deemphasisCheckBox.Checked;
			IfProcessorEnabled();
			CreateFilter();
		}

		private void deemphasisFormTrackBar_ValueChanged(object sender, EventArgs e)
		{
			_filters[_activeButton].DeemphForm = deemphasisFormTrackBar.Value;
			CreateFilter();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (_activeButton != 1)
			{
				_activeButton = 1;
				SelectFilter();
			}
			else
			{
				DisableFilter();
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (_activeButton != 2)
			{
				_activeButton = 2;
				SelectFilter();
			}
			else
			{
				DisableFilter();
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			if (_activeButton != 3)
			{
				_activeButton = 3;
				SelectFilter();
			}
			else
			{
				DisableFilter();
			}
		}

		private void button4_Click(object sender, EventArgs e)
		{
			if (_activeButton != 4)
			{
				_activeButton = 4;
				SelectFilter();
			}
			else
			{
				DisableFilter();
			}
		}

		private void button5_Click(object sender, EventArgs e)
		{
			if (_activeButton != 5)
			{
				_activeButton = 5;
				SelectFilter();
			}
			else
			{
				DisableFilter();
			}
		}

		private void button6_Click(object sender, EventArgs e)
		{
			if (_activeButton != 6)
			{
				_activeButton = 6;
				SelectFilter();
			}
			else
			{
				DisableFilter();
			}
		}

		private void DisableFilter()
		{
			_activeButton = 0;
			SelectFilter();
			filterEnableCheckBox.Checked = false;
			deemphasisCheckBox.Checked = false;
		}

		private void SelectFilter()
		{
			nameTextBox.Text = _filters[_activeButton].Name;
			lowNumericUpDown.Value = _filters[_activeButton].Low;
			hiNumericUpDown.Value = _filters[_activeButton].Hi;
			filterEnableCheckBox.Checked = _filters[_activeButton].FilterEnable;
			deemphasisCheckBox.Checked = _filters[_activeButton].Deemphasis;
			deemphasisFormTrackBar.Value = _filters[_activeButton].DeemphForm;
		}

		private void UpdateButtonsText()
		{
			button1.Text = _filters[1].Name;
			button2.Text = _filters[2].Name;
			button3.Text = _filters[3].Name;
			button4.Text = _filters[4].Name;
			button5.Text = _filters[5].Name;
			button6.Text = _filters[6].Name;
		}

		private void nameTextBox_TextChanged(object sender, EventArgs e)
		{
			_filters[_activeButton].Name = nameTextBox.Text;
			UpdateButtonsText();
		}

		private void spectrumCustomPaint(object sender, CustomPaintEventArgs e)
		{
			SpectrumAnalyzer spectrumAnalyzer = (SpectrumAnalyzer)sender;
			using (new SolidBrush(Color.FromArgb(100, Color.Silver)))
			{
				using (SolidBrush brush6 = new SolidBrush(Color.FromArgb(150, Color.Silver)))
				{
					using (SolidBrush brush4 = new SolidBrush(Color.FromArgb(200, Color.White)))
					{
						using (SolidBrush brush5 = new SolidBrush(Color.Black))
						{
							using (SolidBrush brush3 = new SolidBrush(Color.FromArgb(200, Color.White)))
							{
								using (SolidBrush brush = new SolidBrush(Color.FromArgb(100, Color.Silver)))
								{
									using (SolidBrush brush2 = new SolidBrush(Color.FromArgb(150, Color.Red)))
									{
										using (Font font = new Font("Lucida Console", 9f))
										{
											using (StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic))
											{
												stringFormat.Alignment = StringAlignment.Near;
												RectangleF rect = default(RectangleF);
												if (filterEnableCheckBox.Checked)
												{
													float val = spectrumAnalyzer.FrequencyToPoint(_lowFilterFrequency);
													val = Math.Max(val, 30f);
													rect.X = 30f;
													rect.Y = 30f;
													rect.Width = val - 30f;
													rect.Height = spectrumAnalyzer.Height - 60;
													_filterLowRectangle = new RectangleF(rect.X + rect.Width - 2f, rect.Y, 4f, rect.Height);
													e.Graphics.FillRectangle(brush, rect);
												}
												if (filterEnableCheckBox.Checked)
												{
													float val2 = spectrumAnalyzer.FrequencyToPoint(_highFilterFrequency);
													float num2 = rect.X = Math.Min(val2, spectrumAnalyzer.Width - 30);
													val2 = num2;
													rect.Y = 30f;
													rect.Width = (float)(spectrumAnalyzer.Width - 30) - val2;
													rect.Height = spectrumAnalyzer.Height - 60;
													_filterHighRectangle = new RectangleF(rect.X - 2f, rect.Y, 4f, rect.Height);
													e.Graphics.FillRectangle(brush, rect);
												}
												if (_showCursor)
												{
													using (Pen pen = new Pen(brush2, 1f))
													{
														float num3 = _cursorPosition.X;
														float y = 30f;
														float x = num3;
														float y2 = (float)spectrumAnalyzer.Height - 30f;
														e.Graphics.DrawLine(pen, num3, y, x, y2);
													}
												}
												if (_cursorText != "")
												{
													Point p = default(Point);
													string cursorText = _cursorText;
													SizeF sizeF = e.Graphics.MeasureString(cursorText, font, spectrumAnalyzer.Width, stringFormat);
													p.X = _cursorPosition.X;
													p.Y = (int)((float)_cursorPosition.Y - sizeF.Height - 5f);
													e.Graphics.DrawString(cursorText, font, brush3, p, stringFormat);
												}
												string text = "Audio:";
												Point p2 = default(Point);
												SizeF sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												p2.X = 30;
												p2.Y = 10;
												sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												e.Graphics.DrawString(text, font, brush4, p2, stringFormat);
												text = ((!beforeRadioButton.Checked) ? "after" : "before");
												p2.X = p2.X + (int)sizeF2.Width + 5;
												sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_beforeEnableRectangle = new Rectangle(p2.X, p2.Y, (int)sizeF2.Width, (int)sizeF2.Height);
												e.Graphics.FillRectangle(brush4, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												e.Graphics.DrawString(text, font, brush5, p2, stringFormat);
												text = "Filter";
												p2.X = p2.X + (int)sizeF2.Width + 10;
												sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_filterEnableRectangle = new Rectangle(p2.X, p2.Y, (int)sizeF2.Width, (int)sizeF2.Height);
												if (filterEnableCheckBox.Checked)
												{
													e.Graphics.FillRectangle(brush4, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush5, p2, stringFormat);
												text = "De-emp";
												if (deemphasisCheckBox.Checked)
												{
													text += deemphasisFormTrackBar.Value.ToString("N0");
												}
												p2.X = p2.X + (int)sizeF2.Width + 10;
												sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_deemphasisEnableRectangle = new Rectangle(p2.X, p2.Y, (int)sizeF2.Width, (int)sizeF2.Height);
												if (deemphasisCheckBox.Checked)
												{
													e.Graphics.FillRectangle(brush4, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush5, p2, stringFormat);
												text = button1.Text;
												p2.X = p2.X + (int)sizeF2.Width + 10;
												sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_button1Rectangle = new Rectangle(p2.X, p2.Y, (int)sizeF2.Width, (int)sizeF2.Height);
												if (_activeButton == 1)
												{
													e.Graphics.FillRectangle(brush4, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush5, p2, stringFormat);
												text = button2.Text;
												p2.X = p2.X + (int)sizeF2.Width + 10;
												sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_button2Rectangle = new Rectangle(p2.X, p2.Y, (int)sizeF2.Width, (int)sizeF2.Height);
												if (_activeButton == 2)
												{
													e.Graphics.FillRectangle(brush4, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush5, p2, stringFormat);
												text = button3.Text;
												p2.X = p2.X + (int)sizeF2.Width + 10;
												sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_button3Rectangle = new Rectangle(p2.X, p2.Y, (int)sizeF2.Width, (int)sizeF2.Height);
												if (_activeButton == 3)
												{
													e.Graphics.FillRectangle(brush4, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush5, p2, stringFormat);
												text = button4.Text;
												p2.X = p2.X + (int)sizeF2.Width + 10;
												sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_button4Rectangle = new Rectangle(p2.X, p2.Y, (int)sizeF2.Width, (int)sizeF2.Height);
												if (_activeButton == 4)
												{
													e.Graphics.FillRectangle(brush4, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush5, p2, stringFormat);
												text = button5.Text;
												p2.X = p2.X + (int)sizeF2.Width + 10;
												sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_button5Rectangle = new Rectangle(p2.X, p2.Y, (int)sizeF2.Width, (int)sizeF2.Height);
												if (_activeButton == 5)
												{
													e.Graphics.FillRectangle(brush4, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush5, p2, stringFormat);
												text = button6.Text;
												p2.X = p2.X + (int)sizeF2.Width + 10;
												sizeF2 = e.Graphics.MeasureString(text, font, spectrumAnalyzer.Width, stringFormat);
												_button6Rectangle = new Rectangle(p2.X, p2.Y, (int)sizeF2.Width, (int)sizeF2.Height);
												if (_activeButton == 6)
												{
													e.Graphics.FillRectangle(brush4, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												else
												{
													e.Graphics.FillRectangle(brush6, p2.X - 1, p2.Y - 2, sizeF2.Width + 2f, sizeF2.Height + 2f);
												}
												e.Graphics.DrawString(text, font, brush5, p2, stringFormat);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			notchFilterEntryBindingSource = new System.Windows.Forms.BindingSource(components);
			displayTimer = new System.Windows.Forms.Timer(components);
			filterEnableCheckBox = new System.Windows.Forms.CheckBox();
			groupBox5 = new System.Windows.Forms.GroupBox();
			spectrumPositionComboBox = new System.Windows.Forms.ComboBox();
			label7 = new System.Windows.Forms.Label();
			afterRadioButton = new System.Windows.Forms.RadioButton();
			beforeRadioButton = new System.Windows.Forms.RadioButton();
			audioSpectrumCheckBox = new System.Windows.Forms.CheckBox();
			groupBox1 = new System.Windows.Forms.GroupBox();
			nameTextBox = new System.Windows.Forms.TextBox();
			button6 = new System.Windows.Forms.Button();
			button5 = new System.Windows.Forms.Button();
			button4 = new System.Windows.Forms.Button();
			button3 = new System.Windows.Forms.Button();
			button2 = new System.Windows.Forms.Button();
			button1 = new System.Windows.Forms.Button();
			deemphasisFormTrackBar = new System.Windows.Forms.TrackBar();
			deemphasisCheckBox = new System.Windows.Forms.CheckBox();
			label2 = new System.Windows.Forms.Label();
			label1 = new System.Windows.Forms.Label();
			lowNumericUpDown = new System.Windows.Forms.NumericUpDown();
			hiNumericUpDown = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)notchFilterEntryBindingSource).BeginInit();
			groupBox5.SuspendLayout();
			groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)deemphasisFormTrackBar).BeginInit();
			((System.ComponentModel.ISupportInitialize)lowNumericUpDown).BeginInit();
			((System.ComponentModel.ISupportInitialize)hiNumericUpDown).BeginInit();
			SuspendLayout();
			displayTimer.Enabled = true;
			displayTimer.Tick += new System.EventHandler(displayTimer_Tick);
			filterEnableCheckBox.AutoSize = true;
			filterEnableCheckBox.Location = new System.Drawing.Point(6, 43);
			filterEnableCheckBox.Name = "filterEnableCheckBox";
			filterEnableCheckBox.Size = new System.Drawing.Size(95, 17);
			filterEnableCheckBox.TabIndex = 1;
			filterEnableCheckBox.Text = "Bandpass filter";
			filterEnableCheckBox.UseVisualStyleBackColor = true;
			filterEnableCheckBox.CheckedChanged += new System.EventHandler(filterEnableCheckBox_CheckedChanged);
			groupBox5.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			groupBox5.Controls.Add(spectrumPositionComboBox);
			groupBox5.Controls.Add(label7);
			groupBox5.Controls.Add(afterRadioButton);
			groupBox5.Controls.Add(beforeRadioButton);
			groupBox5.Controls.Add(audioSpectrumCheckBox);
			groupBox5.Location = new System.Drawing.Point(3, 185);
			groupBox5.Name = "groupBox5";
			groupBox5.Size = new System.Drawing.Size(205, 74);
			groupBox5.TabIndex = 8;
			groupBox5.TabStop = false;
			groupBox5.Text = "groupBox5";
			spectrumPositionComboBox.FormattingEnabled = true;
			spectrumPositionComboBox.Items.AddRange(new object[4]
			{
				"Up",
				"Bottom",
				"Left",
				"Right"
			});
			spectrumPositionComboBox.Location = new System.Drawing.Point(67, 42);
			spectrumPositionComboBox.Name = "spectrumPositionComboBox";
			spectrumPositionComboBox.Size = new System.Drawing.Size(97, 21);
			spectrumPositionComboBox.TabIndex = 5;
			label7.AutoSize = true;
			label7.Location = new System.Drawing.Point(6, 45);
			label7.Name = "label7";
			label7.Size = new System.Drawing.Size(44, 13);
			label7.TabIndex = 4;
			label7.Text = "Position";
			afterRadioButton.AutoSize = true;
			afterRadioButton.Location = new System.Drawing.Point(67, 19);
			afterRadioButton.Name = "afterRadioButton";
			afterRadioButton.Size = new System.Drawing.Size(100, 17);
			afterRadioButton.TabIndex = 2;
			afterRadioButton.TabStop = true;
			afterRadioButton.Text = "after processing";
			afterRadioButton.UseVisualStyleBackColor = true;
			beforeRadioButton.AutoSize = true;
			beforeRadioButton.Location = new System.Drawing.Point(6, 19);
			beforeRadioButton.Name = "beforeRadioButton";
			beforeRadioButton.Size = new System.Drawing.Size(55, 17);
			beforeRadioButton.TabIndex = 1;
			beforeRadioButton.TabStop = true;
			beforeRadioButton.Text = "before";
			beforeRadioButton.UseVisualStyleBackColor = true;
			beforeRadioButton.CheckedChanged += new System.EventHandler(beforeRadioButton_CheckedChanged);
			audioSpectrumCheckBox.AutoSize = true;
			audioSpectrumCheckBox.Location = new System.Drawing.Point(6, -1);
			audioSpectrumCheckBox.Name = "audioSpectrumCheckBox";
			audioSpectrumCheckBox.Size = new System.Drawing.Size(99, 17);
			audioSpectrumCheckBox.TabIndex = 0;
			audioSpectrumCheckBox.Text = "Audio spectrum";
			audioSpectrumCheckBox.UseVisualStyleBackColor = true;
			audioSpectrumCheckBox.CheckedChanged += new System.EventHandler(audioSpectrumCheckBox_CheckedChanged);
			groupBox1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right);
			groupBox1.Controls.Add(nameTextBox);
			groupBox1.Controls.Add(button6);
			groupBox1.Controls.Add(button5);
			groupBox1.Controls.Add(button4);
			groupBox1.Controls.Add(button3);
			groupBox1.Controls.Add(button2);
			groupBox1.Controls.Add(button1);
			groupBox1.Controls.Add(deemphasisFormTrackBar);
			groupBox1.Controls.Add(deemphasisCheckBox);
			groupBox1.Controls.Add(label2);
			groupBox1.Controls.Add(filterEnableCheckBox);
			groupBox1.Controls.Add(label1);
			groupBox1.Controls.Add(lowNumericUpDown);
			groupBox1.Controls.Add(hiNumericUpDown);
			groupBox1.Location = new System.Drawing.Point(3, 0);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new System.Drawing.Size(205, 179);
			groupBox1.TabIndex = 9;
			groupBox1.TabStop = false;
			groupBox1.Text = "Filter";
			nameTextBox.BackColor = System.Drawing.SystemColors.Menu;
			nameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			nameTextBox.Location = new System.Drawing.Point(6, 15);
			nameTextBox.Name = "nameTextBox";
			nameTextBox.Size = new System.Drawing.Size(95, 20);
			nameTextBox.TabIndex = 12;
			nameTextBox.TextChanged += new System.EventHandler(nameTextBox_TextChanged);
			button6.Location = new System.Drawing.Point(114, 145);
			button6.Name = "button6";
			button6.Size = new System.Drawing.Size(85, 20);
			button6.TabIndex = 11;
			button6.Text = "button6";
			button6.UseCompatibleTextRendering = true;
			button6.UseVisualStyleBackColor = true;
			button6.Click += new System.EventHandler(button6_Click);
			button5.Location = new System.Drawing.Point(114, 119);
			button5.Name = "button5";
			button5.Size = new System.Drawing.Size(85, 20);
			button5.TabIndex = 10;
			button5.Text = "button5";
			button5.UseCompatibleTextRendering = true;
			button5.UseVisualStyleBackColor = true;
			button5.Click += new System.EventHandler(button5_Click);
			button4.Location = new System.Drawing.Point(114, 93);
			button4.Name = "button4";
			button4.Size = new System.Drawing.Size(85, 20);
			button4.TabIndex = 9;
			button4.Text = "button4";
			button4.UseCompatibleTextRendering = true;
			button4.UseVisualStyleBackColor = true;
			button4.Click += new System.EventHandler(button4_Click);
			button3.Location = new System.Drawing.Point(114, 67);
			button3.Name = "button3";
			button3.Size = new System.Drawing.Size(85, 20);
			button3.TabIndex = 8;
			button3.Text = "button3";
			button3.UseCompatibleTextRendering = true;
			button3.UseVisualStyleBackColor = true;
			button3.Click += new System.EventHandler(button3_Click);
			button2.Location = new System.Drawing.Point(114, 41);
			button2.Name = "button2";
			button2.Size = new System.Drawing.Size(85, 20);
			button2.TabIndex = 7;
			button2.Text = "button2";
			button2.UseCompatibleTextRendering = true;
			button2.UseVisualStyleBackColor = true;
			button2.Click += new System.EventHandler(button2_Click);
			button1.Location = new System.Drawing.Point(114, 15);
			button1.Name = "button1";
			button1.Size = new System.Drawing.Size(85, 20);
			button1.TabIndex = 6;
			button1.Text = "button1";
			button1.UseCompatibleTextRendering = true;
			button1.UseVisualStyleBackColor = true;
			button1.Click += new System.EventHandler(button1_Click);
			deemphasisFormTrackBar.AutoSize = false;
			deemphasisFormTrackBar.Enabled = false;
			deemphasisFormTrackBar.Location = new System.Drawing.Point(6, 141);
			deemphasisFormTrackBar.Maximum = 5;
			deemphasisFormTrackBar.Minimum = 1;
			deemphasisFormTrackBar.Name = "deemphasisFormTrackBar";
			deemphasisFormTrackBar.Size = new System.Drawing.Size(95, 32);
			deemphasisFormTrackBar.TabIndex = 5;
			deemphasisFormTrackBar.Value = 1;
			deemphasisFormTrackBar.ValueChanged += new System.EventHandler(deemphasisFormTrackBar_ValueChanged);
			deemphasisCheckBox.AutoSize = true;
			deemphasisCheckBox.Location = new System.Drawing.Point(6, 118);
			deemphasisCheckBox.Name = "deemphasisCheckBox";
			deemphasisCheckBox.Size = new System.Drawing.Size(87, 17);
			deemphasisCheckBox.TabIndex = 4;
			deemphasisCheckBox.Text = "De-emphasis";
			deemphasisCheckBox.UseVisualStyleBackColor = true;
			deemphasisCheckBox.CheckedChanged += new System.EventHandler(deemphasisCheckBox_CheckedChanged);
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(6, 94);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(17, 13);
			label2.TabIndex = 3;
			label2.Text = "Hi";
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(6, 68);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(27, 13);
			label1.TabIndex = 2;
			label1.Text = "Low";
			lowNumericUpDown.Enabled = false;
			lowNumericUpDown.Increment = new decimal(new int[4]
			{
				10,
				0,
				0,
				0
			});
			lowNumericUpDown.Location = new System.Drawing.Point(39, 66);
			lowNumericUpDown.Maximum = new decimal(new int[4]
			{
				1000000,
				0,
				0,
				0
			});
			lowNumericUpDown.Name = "lowNumericUpDown";
			lowNumericUpDown.Size = new System.Drawing.Size(62, 20);
			lowNumericUpDown.TabIndex = 1;
			lowNumericUpDown.ValueChanged += new System.EventHandler(lowNumericUpDown_ValueChanged);
			hiNumericUpDown.Enabled = false;
			hiNumericUpDown.Increment = new decimal(new int[4]
			{
				100,
				0,
				0,
				0
			});
			hiNumericUpDown.Location = new System.Drawing.Point(39, 92);
			hiNumericUpDown.Maximum = new decimal(new int[4]
			{
				1000000,
				0,
				0,
				0
			});
			hiNumericUpDown.Name = "hiNumericUpDown";
			hiNumericUpDown.Size = new System.Drawing.Size(62, 20);
			hiNumericUpDown.TabIndex = 0;
			hiNumericUpDown.ValueChanged += new System.EventHandler(hiNumericUpDown_ValueChanged);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(groupBox1);
			base.Controls.Add(groupBox5);
			base.Name = "DigitalAudioProcessorPanel";
			base.Size = new System.Drawing.Size(210, 285);
			((System.ComponentModel.ISupportInitialize)notchFilterEntryBindingSource).EndInit();
			groupBox5.ResumeLayout(performLayout: false);
			groupBox5.PerformLayout();
			groupBox1.ResumeLayout(performLayout: false);
			groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)deemphasisFormTrackBar).EndInit();
			((System.ComponentModel.ISupportInitialize)lowNumericUpDown).EndInit();
			((System.ComponentModel.ISupportInitialize)hiNumericUpDown).EndInit();
			ResumeLayout(performLayout: false);
		}
	}
}

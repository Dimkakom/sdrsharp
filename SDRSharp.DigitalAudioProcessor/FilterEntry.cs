namespace SDRSharp.DigitalAudioProcessor
{
	public class FilterEntry
	{
		public string Name
		{
			get;
			set;
		}

		public bool FilterEnable
		{
			get;
			set;
		}

		public int Low
		{
			get;
			set;
		}

		public int Hi
		{
			get;
			set;
		}

		public bool Deemphasis
		{
			get;
			set;
		}

		public int DeemphForm
		{
			get;
			set;
		}
	}
}

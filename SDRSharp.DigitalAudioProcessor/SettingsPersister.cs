using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SDRSharp.DigitalAudioProcessor
{
	public class SettingsPersister
	{
		private const string Filename = "filters.xml";

		private readonly string _settingsFolder;

		public SettingsPersister()
		{
			_settingsFolder = Path.GetDirectoryName(Application.ExecutablePath);
		}

		public List<FilterEntry> ReadStoredFilters()
		{
			List<FilterEntry> list = ReadObject<List<FilterEntry>>("filters.xml");
			if (list != null)
			{
				return list;
			}
			return new List<FilterEntry>();
		}

		public void PersistStoredFilters(List<FilterEntry> entries)
		{
			WriteObject(entries, "filters.xml");
		}

		private T ReadObject<T>(string fileName)
		{
			string path = Path.Combine(_settingsFolder, fileName);
			if (File.Exists(path))
			{
				using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
				{
					return (T)new XmlSerializer(typeof(T)).Deserialize(stream);
				}
			}
			return default(T);
		}

		private void WriteObject<T>(T obj, string fileName)
		{
			using (FileStream stream = new FileStream(Path.Combine(_settingsFolder, fileName), FileMode.Create))
			{
				new XmlSerializer(obj.GetType()).Serialize(stream, obj);
			}
		}
	}
}

using SDRSharp.Common;
using SDRSharp.Radio;
using System.Windows.Forms;

namespace SDRSharp.DigitalAudioProcessor
{
	public class DigitalAudioProcessorPlugin : ISharpPlugin
	{
		private const string _displayName = "Audio Processor";

		private ISharpControl _control;

		private AudioProcessor _audioProcessor;

		private DigitalAudioProcessorPanel _guiControl;

		public string DisplayName => "Audio Processor";

		public UserControl Gui => _guiControl;

		public void Initialize(ISharpControl control)
		{
			_control = control;
			_audioProcessor = new AudioProcessor();
			_control.RegisterStreamHook(_audioProcessor, ProcessorType.DemodulatorOutput);
			_guiControl = new DigitalAudioProcessorPanel(_audioProcessor, _control);
		}

		public void Close()
		{
			_guiControl.StoreSettings();
		}
	}
}

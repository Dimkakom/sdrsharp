using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("SDR#")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: AssemblyCopyright("Copyright © Ian Gilmour 2013")]
[assembly: AssemblyTitle("AudioFFT")]
[assembly: AssemblyDescription("Simple Audio FFT Plugin for SDR#")]
[assembly: ComVisible(false)]
[assembly: CompilationRelaxations(8)]
[assembly: AssemblyTrademark("")]
[assembly: Guid("fb41709c-fdd7-4a90-aa0b-de4324ce4441")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
[assembly: AssemblyVersion("1.0.0.0")]
[module: UnverifiableCode]

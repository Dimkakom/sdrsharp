using SDRSharp.Common;
using SDRSharp.Radio;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SDRSharp.IQCorrection
{
	public class IQCorrectionPanel : UserControl
	{
		private IFProcessor _ifProcessor;

		private ISharpControl _control;

		private IContainer components;

		private Timer displayUpdateTimer;

		private CheckBox enableCheckBox;

		private Label phaseLabel;

		private Label gainLabel;

		private CheckBox dcRemoveCheckBox;

		public IQCorrectionPanel(IFProcessor ifProcessor, ISharpControl control)
		{
			InitializeComponent();
			_ifProcessor = ifProcessor;
			_control = control;
			_control.PropertyChanged += NotifyPropertyChangedHandler;
			enableCheckBox.Checked = Utils.GetBooleanSetting("IQCorrectionEnable");
			dcRemoveCheckBox.Checked = Utils.GetBooleanSetting("OnlyRemoveDC");
			dcRemoveCheckBox_CheckedChanged(null, null);
		}

		public void StoreSettings()
		{
			Utils.SaveSetting("IQCorrectionEnable", enableCheckBox.Checked);
			Utils.SaveSetting("OnlyRemoveDC", dcRemoveCheckBox.Checked);
		}

		private void NotifyPropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			string propertyName;
			if ((propertyName = e.PropertyName) == null)
			{
				return;
			}
			if (!(propertyName == "StartRadio"))
			{
				if (propertyName == "StopRadio")
				{
					_ifProcessor.Stop();
				}
			}
			else
			{
				enableCheckBox_CheckedChanged(null, null);
			}
		}

		private void enableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (enableCheckBox.Checked)
			{
				_ifProcessor.Start();
				dcRemoveCheckBox.Enabled = true;
			}
			else
			{
				_ifProcessor.Stop();
				dcRemoveCheckBox.Enabled = false;
			}
		}

		private void dcRemoveCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			_ifProcessor.OnlyDCRemove = dcRemoveCheckBox.Checked;
		}

		private void DisplayUpdateTimer_Tick(object sender, EventArgs e)
		{
			gainLabel.Text = $"Gain = {_ifProcessor.Gain:F3}";
			phaseLabel.Text = $"Phase = {(double)(_ifProcessor.Phase * 180f) / Math.PI:F3}°";
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			displayUpdateTimer = new System.Windows.Forms.Timer(components);
			enableCheckBox = new System.Windows.Forms.CheckBox();
			phaseLabel = new System.Windows.Forms.Label();
			gainLabel = new System.Windows.Forms.Label();
			dcRemoveCheckBox = new System.Windows.Forms.CheckBox();
			SuspendLayout();
			displayUpdateTimer.Enabled = true;
			displayUpdateTimer.Tick += new System.EventHandler(DisplayUpdateTimer_Tick);
			enableCheckBox.AutoSize = true;
			enableCheckBox.Location = new System.Drawing.Point(3, 3);
			enableCheckBox.Name = "enableCheckBox";
			enableCheckBox.Size = new System.Drawing.Size(98, 17);
			enableCheckBox.TabIndex = 0;
			enableCheckBox.Text = "Auto correct IQ";
			enableCheckBox.UseVisualStyleBackColor = true;
			enableCheckBox.CheckedChanged += new System.EventHandler(enableCheckBox_CheckedChanged);
			phaseLabel.AutoSize = true;
			phaseLabel.Location = new System.Drawing.Point(3, 46);
			phaseLabel.Name = "phaseLabel";
			phaseLabel.Size = new System.Drawing.Size(79, 13);
			phaseLabel.TabIndex = 1;
			phaseLabel.Text = "Phase = 0,000 ";
			gainLabel.AutoSize = true;
			gainLabel.Location = new System.Drawing.Point(107, 46);
			gainLabel.Name = "gainLabel";
			gainLabel.Size = new System.Drawing.Size(68, 13);
			gainLabel.TabIndex = 2;
			gainLabel.Text = "Gain = 1,000";
			dcRemoveCheckBox.AutoSize = true;
			dcRemoveCheckBox.Location = new System.Drawing.Point(3, 26);
			dcRemoveCheckBox.Name = "dcRemoveCheckBox";
			dcRemoveCheckBox.Size = new System.Drawing.Size(103, 17);
			dcRemoveCheckBox.TabIndex = 3;
			dcRemoveCheckBox.Text = "Only DC remove";
			dcRemoveCheckBox.UseVisualStyleBackColor = true;
			dcRemoveCheckBox.CheckedChanged += new System.EventHandler(dcRemoveCheckBox_CheckedChanged);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(dcRemoveCheckBox);
			base.Controls.Add(gainLabel);
			base.Controls.Add(phaseLabel);
			base.Controls.Add(enableCheckBox);
			base.Name = "IQCorrectionPanel";
			base.Size = new System.Drawing.Size(217, 91);
			ResumeLayout(performLayout: false);
			PerformLayout();
		}
	}
}

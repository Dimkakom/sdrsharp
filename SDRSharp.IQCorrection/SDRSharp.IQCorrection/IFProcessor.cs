using SDRSharp.Radio;
using System.Threading;

namespace SDRSharp.IQCorrection
{
	public class IFProcessor : IIQProcessor, IStreamProcessor, IBaseProcessor
	{
		private const float ratio = 1E-05f;

		private UnsafeBuffer _iqBuffer;

		private unsafe Complex* _iqPtr;

		private Thread _calculateThread;

		private bool _calculateThreadRunning;

		private readonly SharpEvent _newBufferEvent = new SharpEvent(initialState: false);

		private bool _needNewBuffer;

		private IQBalancer _iqBalancer;

		private float _gain;

		private float _phase;

		private float _averageReal;

		private float _averageImag;

		private bool _dcRemoveOnly;

		public double SampleRate
		{
			get;
			set;
		}

		public bool Enabled
		{
			get;
			set;
		}

		public float Gain => _gain;

		public float Phase => _phase;

		public bool OnlyDCRemove
		{
			set
			{
				_dcRemoveOnly = value;
			}
		}

		public IFProcessor()
		{
			_iqBalancer = new IQBalancer();
		}

		public unsafe void Process(Complex* buffer, int length)
		{
			if (!_dcRemoveOnly)
			{
				if (_iqBuffer == null || _iqBuffer.Length != length)
				{
					_iqBuffer = UnsafeBuffer.Create(length, sizeof(Complex));
					_iqPtr = (Complex*)(void*)_iqBuffer;
				}
				if (_needNewBuffer)
				{
					_needNewBuffer = false;
					Utils.Memcpy(_iqPtr, buffer, length * sizeof(Complex));
					_newBufferEvent.Set();
				}
			}
			for (int i = 0; i < length; i++)
			{
				_averageReal += 1E-05f * (buffer[i].Real - _averageReal);
				_averageImag += 1E-05f * (buffer[i].Imag - _averageImag);
				buffer[i].Real -= _averageReal;
				buffer[i].Imag -= _averageImag;
				if (!_dcRemoveOnly)
				{
					buffer[i].Real = buffer[i].Real * _gain + _phase * buffer[i].Imag;
				}
			}
		}

		public void Start()
		{
			_iqBalancer.Reset();
			_iqBalancer.Enabled = true;
			_gain = _iqBalancer.Gain;
			_phase = _iqBalancer.Phase;
			_averageImag = 0f;
			_averageReal = 0f;
			_calculateThreadRunning = true;
			if (_calculateThread == null)
			{
				_calculateThread = new Thread(ProcessCalculate);
				_calculateThread.Name = "IQCorrection";
				_calculateThread.Priority = ThreadPriority.Normal;
				_calculateThread.Start();
			}
			_needNewBuffer = true;
			Enabled = true;
		}

		public void Stop()
		{
			Enabled = false;
			_calculateThreadRunning = false;
			if (_calculateThread != null)
			{
				_newBufferEvent.Set();
				_calculateThread.Join();
				_calculateThread = null;
			}
		}

		private unsafe void ProcessCalculate(object parameter)
		{
			while (_calculateThreadRunning)
			{
				_newBufferEvent.WaitOne();
				if (_iqBuffer != null)
				{
					_iqBalancer.Process(_iqPtr, _iqBuffer.Length);
					_gain = _iqBalancer.Gain;
					_phase = _iqBalancer.Phase;
					Thread.Sleep(100);
					_needNewBuffer = true;
				}
			}
		}
	}
}

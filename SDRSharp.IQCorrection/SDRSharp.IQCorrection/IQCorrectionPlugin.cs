using SDRSharp.Common;
using SDRSharp.Radio;
using System.Windows.Forms;

namespace SDRSharp.IQCorrection
{
	public class IQCorrectionPlugin : ISharpPlugin
	{
		private const string _displayName = "Auto correct IQ";

		private ISharpControl _control;

		private IFProcessor _iqProcessor;

		private IQCorrectionPanel _guiControl;

		public string DisplayName => "Auto correct IQ";

		public bool HasGui => true;

		public UserControl GuiControl => _guiControl;

        public UserControl Gui => _guiControl;

		public void Initialize(ISharpControl control)
		{
			_control = control;
			_iqProcessor = new IFProcessor();
			_control.RegisterStreamHook(_iqProcessor, ProcessorType.RawIQ);
			_guiControl = new IQCorrectionPanel(_iqProcessor, control);
		}

		public void Close()
		{
			_iqProcessor.Stop();
			_guiControl.StoreSettings();
		}
	}
}
